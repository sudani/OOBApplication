package com.oob.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.oob.app.R;
import com.oob.tools.NotifyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class TestScreen extends BaseActivity implements View.OnClickListener {

    private static final String SERVICE_NAME = "";
    private int hostPort;
    private InetAddress hostAddress;
    private String TAG = "TEST";
    private EditText et_server_address, et_server_port, et_data;
    private Button btn_test;
    private int SocketServerPort = 23;
    private static final String REQUEST_CONNECT_CLIENT = "request-connect-client";
    private String port;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        btn_test = (Button) findViewById(R.id.btn_test);
        et_server_address = (EditText) findViewById(R.id.et_server_address);
        et_server_port = (EditText) findViewById(R.id.et_server_port);
        et_data = (EditText) findViewById(R.id.et_data);
        btn_test.setOnClickListener(this);

    }


    // NSD Manager, discovery listener code here
    NsdManager.ResolveListener mResolveListener = new NsdManager.ResolveListener() {

        @Override
        public void onResolveFailed(NsdServiceInfo nsdServiceInfo, int i) {

        }

        @Override
        public void onServiceResolved(NsdServiceInfo serviceInfo) {
            Log.d(TAG, "Resolve Succeeded. " + serviceInfo);

            if (serviceInfo.getServiceName().equals(SERVICE_NAME)) {
                Log.d(TAG, "Same IP.");
                return;
            }

            // Obtain port and IP
            hostPort = serviceInfo.getPort();
            hostAddress = serviceInfo.getHost();

            /* Once the client device resolves the service and obtains
             * server's ip address, connect to the server and send data
             */

            connectToHost();
        }
    };


    private void connectToHost() {

        if (hostAddress == null) {
            Log.e(TAG, "Host Address is null");
            return;
        }

        String ipAddress = getLocalIpAddress();
        JSONObject jsonData = new JSONObject();

        try {
            jsonData.put("request", REQUEST_CONNECT_CLIENT);
            jsonData.put("ipAddress", ipAddress);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "can't put request");
            return;
        }

        new SocketServerTask(this).execute("");
    }

    private String getLocalIpAddress() {
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        return ip;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_test:
                testServer();
                break;
        }
    }

    private void testServer() {
        String server_ip, server_port, data;
        server_ip = et_server_address.getText().toString();
        server_port = et_server_port.getText().toString();
        data = et_data.getText().toString();

        if (!server_ip.isEmpty() && !server_port.isEmpty()) {
            if (data.isEmpty())
                NotifyUtils.toast(this, "Please enter some data");
            else
                new SocketServerTask(this).execute(data,server_ip,server_port);
        } else
            NotifyUtils.toast(this, "Please enter server address and port");
    }

    private class SocketServerTask extends AsyncTask<String, Void, Void> {
        private JSONObject jsonData;
        private String data;
        private boolean success=true;
        private ProgressDialog pd;
        private Context mContext;
        private String ip;

        public SocketServerTask(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(mContext);
            pd.setMessage("Sending data....");
            pd.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            Socket socket = null;
            DataInputStream dataInputStream = null;
            DataOutputStream dataOutputStream = null;
            data = params[0];
            ip=params[1];
            port=params[2];
            // jsonData = params[0];

            try {
                // Create a new Socket instance and connect to host
                socket = new Socket(ip, Integer.parseInt(port));
                socket.setSoTimeout(5000);

                dataOutputStream = new DataOutputStream(
                        socket.getOutputStream());
                dataInputStream = new DataInputStream(socket.getInputStream());

                // transfer JSONObject as String to the server
                dataOutputStream.write(data.getBytes());
//                Log.i(TAG, "waiting for response from host");

//                // Thread will wait till server replies
//                String response = dataInputStream.readUTF();
//                if (response != null && response.equals("Connection Accepted")) {
//                    success = true;
//                } else {
//                    success = false;
//                }

            } catch (Exception e) {
                e.printStackTrace();
                success = false;
            } finally {

                // close socket
                if (socket != null) {
                    try {
                        Log.i(TAG, "closing the socket");
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                // close input stream
                if (dataInputStream != null) {
                    try {
                        dataInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                // close output stream
                if (dataOutputStream != null) {
                    try {
                        dataOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pd.dismiss();
            if (success) {
               NotifyUtils.toast(TestScreen.this, "Success");
            } else {
                NotifyUtils.toast(TestScreen.this, "Failed");
            }
        }
    }
}