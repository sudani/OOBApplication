package com.oob.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RemoteViews;

import com.oob.OOBApplication;
import com.oob.app.BuildConfig;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.TextView;

import java.text.DateFormat;
import java.util.Date;

public class SettingsActivity extends BaseActivityChirag {

    //    private MainFragmentOrganizer mainFragmentOrganizer;
    dbHelperOperations db;
    private TextView tvWifiConfig, tvSetup, tvOther, tvSecurity, tvDataTransfer, tvAppVersion;
    private ImageView ivNotification;
    private BroadcastReceiver broadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_settings));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();
        broadcastListener();

    }

    private void initItems() {
        tvAppVersion = (TextView) findViewById(R.id.tvAppVersion);
        tvSetup = (TextView) findViewById(R.id.tvSetup);
        tvSetup.setOnClickListener(this);
        tvWifiConfig = (TextView) findViewById(R.id.tvWifiConfig);
        tvWifiConfig.setOnClickListener(this);
        tvOther = (TextView) findViewById(R.id.tvOther);
        tvOther.setOnClickListener(this);
        tvSecurity = (TextView) findViewById(R.id.tvSecurity);
        tvSecurity.setOnClickListener(this);
        tvDataTransfer = (TextView) findViewById(R.id.tvDataTransfer);
        tvDataTransfer.setOnClickListener(this);
        ivNotification = (ImageView) findViewById(R.id.ivNotification);
        ivNotification.setOnClickListener(this);
        if (Preferences.getAppPrefBool(Preferences.KEY_NOTIFICATION_STATUS, false)) {
            ivNotification.setImageResource(R.drawable.on);
        } else {
            ivNotification.setImageResource(R.drawable.off);
        }
        tvAppVersion.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.tvSetup:
                intent = new Intent(this, SetupActivity.class);
                startActivity(intent);
                break;
            case R.id.tvWifiConfig:
                intent = new Intent(this, ConfigrationActivity.class);
                startActivity(intent);
                break;
            case R.id.tvSecurity:
                intent = new Intent(this, SetupSecurityActivity.class);
                startActivity(intent);
                break;
            case R.id.tvOther:
                intent = new Intent(this, OtherActivity.class);
                startActivity(intent);
                break;
            case R.id.tvDataTransfer:
                setDataMode();
                break;
            case R.id.ivNotification:
                if (Preferences.getAppPrefBool(Preferences.KEY_NOTIFICATION_STATUS, false)) {
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_NOTIFICATION_STATUS, false);
                    ivNotification.setImageResource(R.drawable.off);
                    System.out.println("OFF");
                    generateNotification(false);
                } else {
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_NOTIFICATION_STATUS, true);
                    ivNotification.setImageResource(R.drawable.on);
                    System.out.println("ON");
                    generateNotification(true);
                }
                break;
        }
        super.onClick(v);
    }

    private void setDataMode() {
        //Initialize the Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//Source of the data in the DIalog
        final String[] array = {getString(R.string.auto_mode), getString(R.string.wifi_mode), getString(R.string.internet_mode)};
        final int[] type = new int[1];
        int selection;
        if (Utils.isEmptyString(Preferences.getAppPrefString(Preferences.KEY_DATA_TRANSFER_MODE))) {
            selection = 0;
        } else {
            selection = Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_DATA_TRANSFER_MODE));
            selection--;
        }
        type[0] = (selection + 1);
// Set the dialog title
        builder.setTitle(R.string.title_data_transfer_mode)
                .setSingleChoiceItems(array, selection, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        type[0] = (which + 1);
                    }
                })

// Set the action buttons
                .setPositiveButton(getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Preferences.writeSharedPreferences(Preferences.KEY_DATA_TRANSFER_MODE, String.valueOf(type[0]));
                                if (type[0] == 3 || type[0] == 1) {
                                    if (Utils.isNetworkAvailable(SettingsActivity.this))
                                        OOBApplication.getApp().initMQTT();
                                    if (type[0] == 1 && !Utils.isNetworkAvailable(SettingsActivity.this) && !netCheckin()) {
                                        try {

                                            Intent intent = new Intent();
                                            intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                                            startActivity(intent);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Utils.showToast(SettingsActivity.this, getString(R.string.error_internet));
                                        }

                                    }
                                } else {
                                    OOBApplication.getApp().stopMqtt();
                                    if (netCheckin()) {
                                        try {
                                            Intent intent = new Intent();
                                            intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                                            startActivity(intent);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Utils.showToast(SettingsActivity.this, getString(R.string.error_internet));
                                        }
                                    }
                                }
                            }
                        })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.create().show();
    }

    private boolean netCheckin() {
        boolean isEnabled;
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED) {
            isEnabled = true;
        } else {
            isEnabled = false;
        }
        Log.e("IS ENABLE ", "IS " + isEnabled);
        return isEnabled;
    }

    private void generateNotification(boolean enable) {
        if (enable) {
            // BEGIN_INCLUDE(notificationCompat)
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            // END_INCLUDE(notificationCompat)

            // BEGIN_INCLUDE(intent)
            //Create Intent to launch this Activity again if the notification is clicked.
            Intent i = new Intent(this, RoomDailogTransActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent = PendingIntent.getActivity(this, 0, i,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(intent);
            // END_INCLUDE(intent)

            // BEGIN_INCLUDE(ticker)
            // Sets the ticker text
            builder.setTicker(getResources().getString(R.string.app_name));

            // Sets the small icon for the ticker
            builder.setSmallIcon(R.mipmap.ic_launcher);
            // END_INCLUDE(ticker)

            // BEGIN_INCLUDE(buildNotification)
            // Cancel the notification when clicked
            builder.setAutoCancel(false);

//            builder.setStyle(getR.style.notification_style);

            // Build the notification
            Notification notification = builder.build();
            // END_INCLUDE(buildNotification)
            notification.flags |= Notification.FLAG_NO_CLEAR;
            // BEGIN_INCLUDE(customLayout)
            // Inflate the notification layout as RemoteViews
            RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification);

            // Set text on a TextView in the RemoteViews programmatically.
            final String time = DateFormat.getTimeInstance().format(new Date()).toString();
            final String text = getResources().getString(R.string.app_name);
            contentView.setTextViewText(R.id.textView, text);


        /* Workaround: Need to set the content view here directly on the notification.
         * NotificationCompatBuilder contains a bug that prevents this from working on platform
         * versions HoneyComb.
         * See https://code.google.com/p/android/issues/detail?id=30495
         */
            notification.contentView = contentView;

            // Add a big content view to the notification if supported.
            // Support for expanded notifications was added in API level 16.
            // (The normal contentView is shown when the notification is collapsed, when expanded the
            // big content view set here is displayed.)
//            if (Build.VERSION.SDK_INT >= 16) {
//                // Inflate and set the layout for the expanded notification view
//                RemoteViews expandedView =
//                        new RemoteViews(getPackageName(), R.layout.notification_expand);
//                notification.bigContentView = expandedView;
//                expandedView.setTextViewText(R.id.textView, text);
//            }
            // END_INCLUDE(customLayout)

            // START_INCLUDE(notify)
            // Use the NotificationManager to show the notification
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.notify(0, notification);
            // END_INCLUDE(notify)
        } else {
            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager nMgr = (NotificationManager) getSystemService(ns);
            nMgr.cancelAll();
        }

    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .execute(json, WifiUtils.server_ip, WifiUtils.server_port);
//
//            }
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE)
                            .equalsIgnoreCase("update")) {
                        dataChangeAlert();
                    }

                }
            }
        };
    }

    public void dataChangeAlert() {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                SettingsActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle(getString(R.string.title_data_change));

        // Setting Dialog Message
//        alertDialog.setMessage(getString(R.string.msg_data_change));

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog
                .setButton(AlertDialog.BUTTON_NEUTRAL,getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

}
