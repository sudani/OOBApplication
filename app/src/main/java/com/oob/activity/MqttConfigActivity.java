package com.oob.activity;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.CompoundButton;

import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.Button;
import com.oob.view.CheckBox;
import com.oob.view.EditText;

public class MqttConfigActivity extends BaseActivityChirag {
    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 9;

    //    private MainFragmentOrganizer mainFragmentOrganizer;

    private EditText edtServerUrl;
    private EditText edtServerPort;
    private EditText edtUserName;
    private EditText edtPass;
    private EditText edtSubFromApp;
    private EditText edtPubFromApp;
    private Button btnDone;
    private CheckBox chShowPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mgtt_config);

        setToolbar();
        setTitleName(getString(R.string.title_mqtt_configuration));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();
//        initMQTT();
    }

    private void initItems() {
        edtServerUrl = (EditText) findViewById(R.id.edtServer);
        edtServerPort = (EditText) findViewById(R.id.edtServerPort);
        edtUserName = (EditText) findViewById(R.id.edtUser);
        edtPass = (EditText) findViewById(R.id.edtPass);
        edtSubFromApp = (EditText) findViewById(R.id.edtSubApp);
        edtPubFromApp = (EditText) findViewById(R.id.edtPubApp);
        btnDone = (Button) findViewById(R.id.btnDone);
        chShowPass = (CheckBox) findViewById(R.id.chShowPass);
        btnDone.setOnClickListener(this);
        chShowPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                edtPass.setInputType(b ? InputType.TYPE_CLASS_TEXT : (InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD));
                edtPass.setSelection(edtPass.length());
            }
        });
        if (!Preferences.getAppPrefString(Preferences.KEY_SERVER_URL, "").equalsIgnoreCase("")) {
            edtServerUrl.setText(Preferences.getAppPrefString(Preferences.KEY_SERVER_URL));
            edtServerPort.setText(Preferences.getAppPrefString(Preferences.KEY_SERVER_PORT));
            edtUserName.setText(Preferences.getAppPrefString(Preferences.KEY_SERVER_USER));
            edtPass.setText(Preferences.getAppPrefString(Preferences.KEY_SERVER_PASS));
            edtSubFromApp.setText(Preferences.getAppPrefString(Preferences.KEY_SERVER_SUB_APP));
            edtPubFromApp.setText(Preferences.getAppPrefString(Preferences.KEY_SERVER_PUB_APP));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
//        if (mWifiScanReceiver.isOrderedBroadcast())
//            unregisterReceiver(mWifiScanReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDone:
                if (isValidate()) {
                    saveData();
                }
                break;

        }
        super.onClick(v);
    }

    private void saveData() {
        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected()) {
            String pref = Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE).substring(0, 3);
            System.out.println("pref " + pref);
            OOBApplication.getApp().publishMessage(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "MQTT*" + edtServerUrl.getText().toString() + "*" + edtServerPort.getText().toString() + "*" + edtUserName.getText().toString() + "*" + edtPass.getText().toString() + "*" +pref+"_"+ edtSubFromApp.getText().toString() + "*" +pref+"_"+ edtPubFromApp.getText().toString() + "*Q");
            Preferences.writeSharedPreferences(Preferences.KEY_SERVER_URL, edtServerUrl.getText().toString());
            Preferences.writeSharedPreferences(Preferences.KEY_SERVER_PORT, edtServerPort.getText().toString());
            Preferences.writeSharedPreferences(Preferences.KEY_SERVER_USER, edtUserName.getText().toString());
            Preferences.writeSharedPreferences(Preferences.KEY_SERVER_PASS, edtPass.getText().toString());
            Preferences.writeSharedPreferences(Preferences.KEY_SERVER_SUB_APP, pref+"_"+edtSubFromApp.getText().toString());
            Preferences.writeSharedPreferences(Preferences.KEY_SERVER_PUB_APP, pref+"_"+edtPubFromApp.getText().toString());
            finish();
        }
    }

//    @Override
//    public void onSuccessPublish(String publishMessage) {
//        super.onSuccessPublish(publishMessage);
//        Preferences.writeSharedPreferences(Preferences.KEY_SERVER_URL, edtServerUrl.getText().toString());
//        Preferences.writeSharedPreferences(Preferences.KEY_SERVER_PORT, edtServerPort.getText().toString());
//        Preferences.writeSharedPreferences(Preferences.KEY_SERVER_USER, edtUserName.getText().toString());
//        Preferences.writeSharedPreferences(Preferences.KEY_SERVER_PASS, edtPass.getText().toString());
//        Preferences.writeSharedPreferences(Preferences.KEY_SERVER_SUB_APP, edtSubFromApp.getText().toString());
//        Preferences.writeSharedPreferences(Preferences.KEY_SERVER_PUB_APP, edtPubFromApp.getText().toString());
//        finish();
//    }

    private boolean isValidate() {
        if (Utils.isEmptyEditText(edtServerUrl)) {
            Utils.showToast(this, getString(R.string.err_server_url));
            return false;
        } else if (Utils.isEmptyEditText(edtServerPort)) {
            Utils.showToast(this, getString(R.string.err_server_port));
            return false;
        } else if (Utils.isEmptyEditText(edtUserName)) {
            Utils.showToast(this, getString(R.string.err_server_user));
            return false;
        } else if (Utils.isEmptyEditText(edtPass)) {
            Utils.showToast(this, getString(R.string.err_server_pass));
            return false;
        } else if (Utils.isEmptyEditText(edtSubFromApp)) {
            Utils.showToast(this, getString(R.string.err_server_sub_app));
            return false;
        } else if (Utils.isEmptyEditText(edtPubFromApp)) {
            Utils.showToast(this, getString(R.string.err_server_pub_app));
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

}
