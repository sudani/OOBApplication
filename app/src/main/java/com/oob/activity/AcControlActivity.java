package com.oob.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entAC;
import com.oob.fragment.AcContolFragment;
import com.oob.service.BroadcastServiceAC;

import java.util.ArrayList;
import java.util.List;

public class AcControlActivity extends BaseActivityChirag {

    private dbHelperOperations db;

    private List<entAC> acs = new ArrayList<>();

    private int selectedAc = 0;

    public String tvName[];

    private ViewPager viewPager;

    private PagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ac_control);
        setToolbar();
        db = new dbHelperOperations(this);
        acs.addAll(db.getListAC());
        tvName = new String[acs.size()];
        for (int i = 0; i < acs.size(); i++) {
            tvName[i] = acs.get(i).getAC_NAME();
            System.out.println("IS SELECTED " + acs.get(i).getAC_IS_SELECTED());
//            if (acs.get(i).getAC_IS_SELECTED() == 1) {
//                selectedAc = i;
//            }
        }

//        showSpinnersTv(tvName, selectedAc);
        tvSetting.setVisibility(View.GONE);
        ivOnOff.setText("");
        tvGrouping.setVisibility(View.VISIBLE);
        ivOnOff.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        tvGrouping.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        viewPager = (ViewPager) findViewById(R.id.pager);
        if (tvName.length > 0) {
            setTitleName(tvName[selectedAc]);
            tvGrouping.setText(acs.get(selectedAc).getROOM_NAME());
        } else {
            tvGrouping.setText("");
        }
        adapter = new PagerAdapter
                (getSupportFragmentManager(), acs.size());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(selectedAc);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                    int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                AcControlActivity.this.setTitleName(tvName[position]);
                AcControlActivity.this.tvGrouping.setText(acs.get(position).getROOM_NAME());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    public class PagerAdapter extends FragmentStatePagerAdapter {

        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            AcContolFragment fragment = new AcContolFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("pos", position);
            bundle.putSerializable("entAc", acs.get(position));
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }


    }


    @Override
    public void onOffClick(View v) {
//        super.onOffClick(v);
//        final Calendar c = Calendar.getInstance();
//        final int mHour = c.get(Calendar.HOUR_OF_DAY);
//        final int mMinute = c.get(Calendar.MINUTE);
//        // Launch Time Picker Dialog
//        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
//                new TimePickerDialog.OnTimeSetListener() {
//
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay,
//                            int minute) {
//
//                        System.out.println(hourOfDay + ":" + minute);
//                        System.out.println(hourOfDay + ":" + minute);
//                        Preferences.writeSharedPreferences(Preferences.KEY_FROM_TIME_AC,
//                                Utils.convertToSeconds(mHour, mMinute));
//                        Preferences.writeSharedPreferences(Preferences.KEY_TO_TIME_AC,
//                                Utils.convertToSeconds(hourOfDay, minute));
//                        startService(new Intent(AcControlActivity.this, BroadcastServiceAC.class));
//                    }
//                }, mHour, mMinute, false);
//        timePickerDialog.show();

    }


    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                ivOnOff.setText(intent.getExtras().getString("time"));
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(br, new IntentFilter(BroadcastServiceAC.COUNTDOWN_BR));
        Log.i("TAG", "Registered broacast receiver");
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(br);
        Log.i("TAG", "Unregistered broacast receiver");
    }

    @Override
    public void onStop() {
        try {
            unregisterReceiver(br);
        } catch (Exception e) {
            // Receiver was probably already stopped in onPause()
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
//        stopService(new Intent(this, BroadcastServiceAC.class));
//        Log.i("TAG", "Stopped service");
//        if (acs.size() > 0) {
//            for (int i=0){
//                ac
//            }
//            acs.get(viewPager.getCurrentItem()).setAC_IS_SELECTED(1);
//            db.updateAc(acs.get(viewPager.getCurrentItem()));
//        }

        super.onDestroy();
    }


    @Override
    public void onTimeLongClick() {
        super.onTimeLongClick();
        dataChangeAlert();
    }

    public void dataChangeAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_stop_timer);
        builder.setMessage(getString(R.string.stop_timer));
        builder.setIcon(R.drawable.app_logo);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                stopService(new Intent(AcControlActivity.this, BroadcastServiceAC.class));
                ivOnOff.setText("00:00:00");
//

            }
        });

        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setCancelable(true);
        builder.show();
    }
}
