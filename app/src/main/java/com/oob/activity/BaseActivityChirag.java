package com.oob.activity;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.oob.app.R;
import com.oob.entity.entRooms;
import com.oob.network.HostService;
import com.oob.network.WiFiStateReceiver;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.DialogUtils;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.CircleImageView;
import com.oob.view.TextView;
import com.squareup.picasso.Picasso;

import java.io.File;

public class BaseActivityChirag extends AppCompatActivity
        implements View.OnClickListener, AdapterView.OnItemSelectedListener,
        View.OnLongClickListener {
    private static final String TAG = "MQTT";
    public Toolbar toolbar;
    public TextView txtTitle, tvSetting, tvGrouping, ivOnOff, tvMic;
    public AppCompatSpinner spinRooms;
    public Intent service_intent;
    public ImageView ivMenu;
    private WifiManager wifiManager;
    public WiFiStateReceiver wiFiStateReceiver = new WiFiStateReceiver();
    public IntentFilter intentFilter_action_wifi;
    public BroadcastReceiver data_change_receiver;
    public boolean enableMultiselect = false;
    protected Typeface mTfRegular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }

    private void init() {
        try {
            wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            intentFilter_action_wifi = new IntentFilter("android.net.wifi.STATE_CHANGE");
            mTfRegular = Typeface.createFromAsset(getAssets(), getString(R.string.font));
            initDataChangeReceiver();
        } catch (Exception e) {

        }
//        System.out.println("onCreate " + isMyServiceRunning(HostService.class));
////        if (!isMyServiceRunning(HostService.class)) {
//        if (!DashBoardActivity.isRunnig) {
//            System.out.println("registerReceiver");
//            try {
//                registerReceiver(wiFiStateReceiver, intentFilter_action_wifi);
//            } catch (Exception e) {
//                e.printStackTrace();
//                unregisterReceiver(wiFiStateReceiver);
//                registerReceiver(wiFiStateReceiver, intentFilter_action_wifi);
//            }
//        }
//        service_intent = new Intent(this, HostService.class);
//        new SetupServiceReceiverTask(true).execute();
    }

//    public void initMQTT() {
//        if (!Preferences.getAppPrefString(Preferences.KEY_SERVER_URL, "").equalsIgnoreCase("")) {
//            subscriptionTopic = Preferences.getAppPrefString(Preferences.KEY_SERVER_SUB_APP);
//            publishTopic = Preferences.getAppPrefString(Preferences.KEY_SERVER_PUB_APP);
//            serverUrl = Preferences.getAppPrefString(Preferences.KEY_SERVER_URL);
//            serverPort = Preferences.getAppPrefString(Preferences.KEY_SERVER_PORT);
//            username = Preferences.getAppPrefString(Preferences.KEY_SERVER_USER);
//            password = Preferences.getAppPrefString(Preferences.KEY_SERVER_PASS);
//        } else {
//            return;
//        }
//        if (client != null && client.isConnected()) {
//            Utils.showToast(BaseActivityChirag.this, "Connected");
//            return;
//        }
//        client =
//                new MqttAndroidClient(this.getApplicationContext(), "tcp://" + serverUrl + ":" + serverPort,
//                        clientId + System.currentTimeMillis());
//        try {
//            MqttConnectOptions options = new MqttConnectOptions();
//            options.setAutomaticReconnect(true);
//            options.setCleanSession(false);
//            options.setUserName(username);
//            options.setPassword(password.toCharArray());
//            IMqttToken token = client.connect(options);
//            token.setActionCallback(new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    // We are connected
//                    Log.e(TAG, "onSuccess");
//                    Utils.showToast(BaseActivityChirag.this, "Connected");
//                    subscribeToTopic();
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    // Something went wrong e.g. connection timeout or firewall problems
//                    exception.printStackTrace();
//                    Utils.showToast(BaseActivityChirag.this, "Connection fail");
//                    Log.e(TAG, "onFailure");
//
//                }
//            });
//        } catch (MqttException e) {
//            e.printStackTrace();
//        }
//
//
//    }

//    public void subscribeToTopic() {
//        try {
//            IMqttToken mqttToken = client.subscribe(subscriptionTopic, 1);
//            mqttToken.setActionCallback(new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    Log.e(TAG, "Subscribed");
//                    Utils.showToast(BaseActivityChirag.this, "Subscribed");
//
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    exception.printStackTrace();
//                    Log.e(TAG, "Failed to subscribe");
//                    Utils.showToast(BaseActivityChirag.this, "Failed to subscribe");
//
//                }
//            });
//            client.setCallback(new MqttCallback() {
//                @Override
//                public void connectionLost(Throwable cause) {
//
//                }
//
//                @Override
//                public void messageArrived(String topic, MqttMessage message) throws Exception {
//                    Log.e(TAG, "Incoming message: " + new String(message.getPayload()));
//
//                    processData(topic, new String(message.getPayload()));
//                }
//
//                @Override
//                public void deliveryComplete(IMqttDeliveryToken token) {
//
//                }
//            });
//
//            // THIS DOES NOT WORK!
////            mqttAndroidClient.subscribe(subscriptionTopic, 0, new IMqttMessageListener() {
////                @Override
////                public void messageArrived(String topic, MqttMessage message) throws Exception {
////                    // message Arrived!
////                    System.out.println("Message: " + topic + " : " + new String(message.getPayload()));
////                }
////            });
//
//        } catch (MqttException ex) {
//            System.err.println("Exception whilst subscribing");
//            ex.printStackTrace();
//        }
//    }

//    public void processData(String topic, String data) {
//        Utils.showToast(BaseActivityChirag.this, "Message Arrived " + data);
//
//    }

//    public void publishMessage(String publishMessage) {
//
//        try {
////            publishMessage = "sdsdsadsdkdklasdasdklslsakjasklsldsjkdaksdasdasdsasdsdsddkjdsfjkdfsjdsfjkdfkjfdjkkjdfkjdfkjdfskjsdfkjsdsdsadsdkdklasdasdklslsakjasklsldsjkdaksdasdasdsasdsdsddkjdsfjkdfsjdsfjkdfkjfdjkkjdfkjdfkjdfskjsdfkj100";
//            MqttMessage message = new MqttMessage();
//            message.setPayload(publishMessage.getBytes());
//            if (client != null) {
//                client.publish(publishTopic, message);
//            }
//            Log.e(TAG, "Message Published " + publishMessage);
//            Utils.showToast(BaseActivityChirag.this, "Message Published " + publishMessage);
//            onSuccessPublish(publishMessage);
//        } catch (MqttException e) {
//            System.err.println("Error Publishing: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    public void onSuccessPublish(String publishMessage) {
//
//    }

    public void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        txtTitle = (TextView) findViewById(R.id.text_title);
        ivOnOff = (TextView) findViewById(R.id.ivOnOff);
        ivMenu = (ImageView) findViewById(R.id.ivMenu);
        tvSetting = (TextView) findViewById(R.id.tvSetting);
        spinRooms = (AppCompatSpinner) findViewById(R.id.spinRooms);
        tvGrouping = (TextView) findViewById(R.id.tvGrouping);
        tvMic = (TextView) findViewById(R.id.tvMic);
        tvGrouping.setOnClickListener(this);
        tvMic.setOnClickListener(this);
        tvGrouping.setOnLongClickListener(this);
        ivOnOff.setOnClickListener(this);
        ivOnOff.setOnLongClickListener(this);
        ivMenu.setOnClickListener(this);
        tvSetting.setOnClickListener(this);
//        setSupportActionBar(toolbar);
    }

    public void setTitleName(String title) {
        txtTitle.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivOnOff:
                onOffClick(v);
                break;
            case R.id.tvSetting:
                onSetting();
                break;
            case R.id.ivMenu:
                onMenuClick(v);
                break;
            case R.id.tvGrouping:
                if (!enableMultiselect) {
                    onGrouping(v);
                } else {
                    enableMultiselect = false;
                    desableMultiselect();
                }
                break;
            case R.id.tvMic:
                onMicClick();
                break;

        }
    }

    public void onMenuClick(View v) {
    }

    public void desableMultiselect() {

    }

    public void onGrouping(View v) {

    }

    public void onOffClick(View v) {

    }

    public void showSpinners(entRooms[] rooms, int selected) {
        System.out.println("SELECTED " + selected);
        spinRooms.setVisibility(View.VISIBLE);
        spinRooms.setOnItemSelectedListener(this);
        txtTitle.setVisibility(View.GONE);
        tvSetting.setVisibility(View.GONE);
        tvGrouping.setVisibility(View.VISIBLE);
        ivMenu.setVisibility(View.VISIBLE);
//        String[] rooms = getResources().getStringArray(R.array.room_selection);
        ArrayAdapter adapter = new MyCustomAdapter(this, R.layout.spinner_item, rooms);
        spinRooms.setAdapter(adapter);
        spinRooms.setSelection(selected);

    }

    public void showSpinnersTv(String[] tvs, int selected) {
        System.out.println("SELECTED " + selected);
        spinRooms.setVisibility(View.VISIBLE);
        spinRooms.setOnItemSelectedListener(this);
        txtTitle.setVisibility(View.GONE);
        tvSetting.setVisibility(View.GONE);
        tvGrouping.setVisibility(View.VISIBLE);
//        ivMenu.setVisibility(View.VISIBLE);
//        String[] rooms = getResources().getStringArray(R.array.room_selection);
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spiner_item, tvs);
        spinRooms.setAdapter(adapter);
        spinRooms.setSelection(selected);

    }

    public void showSpinnersCurtain(String[] curtain) {
        spinRooms.setVisibility(View.VISIBLE);
        spinRooms.setOnItemSelectedListener(this);
        txtTitle.setVisibility(View.GONE);
        tvSetting.setVisibility(View.GONE);
        tvGrouping.setVisibility(View.VISIBLE);
//        ivMenu.setVisibility(View.VISIBLE);
//        String[] rooms = getResources().getStringArray(R.array.room_selection);
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spiner_item, curtain);
        spinRooms.setAdapter(adapter);

    }

    public class MyCustomAdapter extends ArrayAdapter<entRooms> {

        entRooms[] DayOfWeek;

        Context context;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               entRooms[] objects) {

            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
            DayOfWeek = objects;
            this.context = context;
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.spinner_item, parent, false);
            TextView label = (TextView) row.findViewById(R.id.tvName);
            label.setText(DayOfWeek[position].getROOM_NAME());
            CircleImageView icon = (CircleImageView) row.findViewById(R.id.icon);
            if (!DayOfWeek[position].getPROFILE_DATA().equalsIgnoreCase("")) {
                Picasso.with(context).load(new File(DayOfWeek[position].getPROFILE_DATA())).placeholder(R.drawable.app_logo).error(R.drawable.app_logo).into(icon);

//                Glide.with(context)
//                        .load(DayOfWeek[position].getPROFILE_DATA()).skipMemoryCache(false)
//                        .placeholder(R.drawable.app_logo).error(R.drawable.app_logo)
//                        .into(icon);
            }
//            icon.setImageBitmap(Utils.ByteToBitmap(DayOfWeek[position].getPROFILE_DATA()));

            else {
                icon.setImageResource(R.drawable.app_logo);
            }
//            if (DayOfWeek[position]=="Sunday"){
//                icon.setImageResource(R.drawable.icon);
//            }
//            else{
//                icon.setImageResource(R.drawable.icongray);
//            }

            return row;
        }
    }

    public void onSetting() {
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.tvGrouping:
                enableMultiselect = true;
                enableMultiselect();
                break;
            case R.id.ivOnOff:
                onTimeLongClick();
                break;

        }
        return true;
    }

    public void onMicClick() {
    }

    public void onTimeLongClick() {

    }

    public void enableMultiselect() {

    }

    private void initDataChangeReceiver() {
        data_change_receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra(BroadcastConstant.DATA_CHANGE)
                        ) {
                    if (Preferences.getAppPrefBool(Preferences.KEY_DIALOG_SHOW, true)) {
                        DialogUtils.showAlert(BaseActivityChirag.this
                                , getString(R.string.title_data_change)
                                , getString(R.string.msg_data_change));
                        Preferences.writeSharedPreferencesBool(Preferences.KEY_DIALOG_SHOW, false);
                    }
                }
            }
        };

    }

//    public MainFragmentOrganizer getFrag_organizer() {
//        return mainFragmentOrganizer;
//    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        System.out.println("ONRESUME");
////        registerReceiver(wiFiStateReceiver, intentFilter_action_wifi);
//        new SetupServiceReceiverTask(true).execute();
//    }

    @Override
    protected void onPause() {
        super.onPause();
        //wifiManager.setWifiEnabled(false);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onStart() {
        System.out.println("onStart " + isMyServiceRunning(HostService.class));
//        if (!isMyServiceRunning(HostService.class)) {
        if (!DashBoardActivity.isRunnig) {
            System.out.println("registerReceiver");
            try {
                registerReceiver(wiFiStateReceiver, intentFilter_action_wifi);
            } catch (Exception e) {
                e.printStackTrace();
                unregisterReceiver(wiFiStateReceiver);
                registerReceiver(wiFiStateReceiver, intentFilter_action_wifi);
            }
        }
        service_intent = new Intent(this, HostService.class);
        new SetupServiceReceiverTask(true).execute();
//        }
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("onDestroy " + DashBoardActivity.isRunnig);
        Utils.backupDatabase();
        Utils.backupSharedPreferencesToFile(BaseActivityChirag.this);
        if (!DashBoardActivity.isRunnig) {
            try {
                if (wiFiStateReceiver != null) {
                    System.out.println("unregisterReceiver");
                    unregisterReceiver(wiFiStateReceiver);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new SetupServiceReceiverTask(false).execute();
            Preferences.writeSharedPreferencesBool(Preferences.KEY_DIALOG_SHOW, true);
        }
    }

    private void registerLocalReceiver() {
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(data_change_receiver, intentFilter);
    }

    private void unregisterLocalReceiver() {
        if (data_change_receiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(data_change_receiver);
        }
    }

    public class SetupServiceReceiverTask extends AsyncTask<Void, Void, Void> {

        private boolean isResume;

        public SetupServiceReceiverTask(boolean isResume) {
            this.isResume = isResume;

        }

        @Override
        protected Void doInBackground(Void... params) {
//            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
            if (isResume) {

                if (!wifiManager.isWifiEnabled()) {
//                    wifiManager.setWifiEnabled(true);
                }
//                for (WifiConfiguration configuration1:list){
//                    System.out.println("PRIORITY "+configuration1.priority);
//                    System.out.println("SSID "+configuration1.SSID);
//                    if (configuration1.SSID.equalsIgnoreCase("Oob_Hub_1")){
//                        configuration1.priority=100000;
//                        wifiManager.updateNetwork(configuration1);
//                        wifiManager.saveConfiguration();
//                        wifiManager.reconnect();
//                        break;
//                    }
//                }
                startService(service_intent);
//                registerReceiver(wiFiStateReceiver, intentFilter_action_wifi);
                registerLocalReceiver();
            } else {
                //  wifiManager.setWifiEnabled(false);
                if (service_intent != null) {
                    stopService(service_intent);
                }
//                for (WifiConfiguration configuration1:list){
//                    System.out.println("PRIORITY "+configuration1.priority);
//                    System.out.println("SSID "+configuration1.SSID);
//                    if (configuration1.SSID.equalsIgnoreCase("Oob_Hub_1")){
//                        configuration1.priority=0;
//                        wifiManager.updateNetwork(configuration1);
//                        wifiManager.saveConfiguration();
//                        wifiManager.disconnect();
//                        break;
//                    }
//                }
//                unregisterReceiver(wiFiStateReceiver);
                unregisterLocalReceiver();
            }
            return null;
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_HOME)) {
            Toast.makeText(this, "You pressed the home button!", Toast.LENGTH_LONG).show();
            Log.e("BASE ACT","onKeyDown");
            this.finishAffinity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
//    @Override
//    public void onAttachedToWindow() {
//            this.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION);
//            super.onAttachedToWindow();
//    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        if ( (event.getKeyCode() == KeyEvent.KEYCODE_HOME)) {
            Toast.makeText(this, "You pressed the home button!", Toast.LENGTH_LONG).show();
            Log.e("BASE ACT","dispatchKeyEventE");
            this.finishAffinity();
            return true;

        } else {
            return super.dispatchKeyEvent(event);
        }

    }
    @Override
    public void onBackPressed() {
        Log.e("BASE ACT","ON BACK");
        super.onBackPressed();
    }

//    @Override
//    protected void onUserLeaveHint() {
//        Log.e("BASE ACT","USER LEAVE");
//        this.finishAffinity();
//        super.onUserLeaveHint();
//    }
}

