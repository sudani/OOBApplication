package com.oob.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;

import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Preferences;
import com.oob.view.TextView;

import java.util.List;

public class OtherActivity extends BaseActivityChirag {

    //    private MainFragmentOrganizer mainFragmentOrganizer;
    dbHelperOperations db;
    private TextView tvTimer;
    private ImageView ivPowerOff, ivMute;
    private BroadcastReceiver broadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_settings));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();
        broadcastListener();

    }

    private void initItems() {


        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvTimer.setOnClickListener(this);
        ivPowerOff = (ImageView) findViewById(R.id.ivPowerOff);
        ivPowerOff.setOnClickListener(this);
        ivMute = (ImageView) findViewById(R.id.ivMuteBuzzer);
        ivMute.setOnClickListener(this);

        if (Preferences.getAppPrefBool(Preferences.KEY_MAIN_POWER_OFF, true)) {
            ivPowerOff.setImageResource(R.drawable.on);
        } else {
            ivPowerOff.setImageResource(R.drawable.off);
        }
        if (Preferences.getAppPrefBool(Preferences.KEY_MAIN_MUTE_BUZZER, true)) {
            ivMute.setImageResource(R.drawable.buzzer);
        } else {
            ivMute.setImageResource(R.drawable.mute);
        }

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {

            case R.id.tvTimer:
                intent = new Intent(this, TimerActivity.class);
                startActivity(intent);
                break;
            case R.id.ivPowerOff:
                if (Preferences.getAppPrefBool(Preferences.KEY_MAIN_POWER_OFF, true)) {
                    ivPowerOff.setImageResource(R.drawable.off);
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_MAIN_POWER_OFF, false);
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                            + "OOBALLOFFLOCK");
                } else {
                    ivPowerOff.setImageResource(R.drawable.on);
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_MAIN_POWER_OFF, true);
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                            + "OOBALLONLOCK");
                }
                break;
            case R.id.ivMuteBuzzer:
                List<entDevice> al_data = db.getListOfDevices(-2, true);
                if (Preferences.getAppPrefBool(Preferences.KEY_MAIN_MUTE_BUZZER, true)) {
                    ivMute.setImageResource(R.drawable.mute);
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_MAIN_MUTE_BUZZER, false);
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                            + "OOBALLBUZZOFF");
                } else {
                    ivMute.setImageResource(R.drawable.buzzer);
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_MAIN_MUTE_BUZZER, true);
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                            + "OOBALLBUZZON");
                }
                for (entDevice device : al_data) {
                    device.setMIC(
                            Preferences.getAppPrefBool(Preferences.KEY_MAIN_MUTE_BUZZER, true) ? 1
                                    : 0);
                    db.updateDevice(device);
                }
                break;

        }
        super.onClick(v);
    }


    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .execute(json, WifiUtils.server_ip, WifiUtils.server_port);
//
//            }
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE)
                            .equalsIgnoreCase("update")) {
                        dataChangeAlert();
                    }

                }
            }
        };
    }

    public void dataChangeAlert() {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                OtherActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle(getString(R.string.title_data_change));

        // Setting Dialog Message
//        alertDialog.setMessage(getString(R.string.msg_data_change));

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog
                .setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

}
