package com.oob.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RemoteViews;

import com.oob.OOBApplication;
import com.oob.SocketServerTask;
import com.oob.adapter.CheeseDynamicAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDashboard;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.tools.WifiUtils;

import org.askerov.dynamicgrid.DynamicGridView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DashBoardActivity extends BaseActivityChirag {

    //    private MainFragmentOrganizer mainFragmentOrganizer;
    public static boolean isRunnig = false;
    private DynamicGridView gridView;
    dbHelperOperations db;
    private List<entDashboard> dashboardItems = new ArrayList<>();
    private BroadcastReceiver broadcast;
    boolean isOn = true;
    public static boolean isResumed = false;
    private boolean isUpdated = false;
    boolean isFromNoti = false;
//    private Intent service_intent;
//    private WifiManager wifiManager;
//    private WiFiStateReceiver wiFiStateReceiver = new WiFiStateReceiver();
//    private IntentFilter intentFilter_action_wifi;
//    private BroadcastReceiver data_change_receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
//        initBackgroundTasks();
        isRunnig = true;


        db = new dbHelperOperations(this);
        if (getIntent().hasExtra(Constants.EXTRA_FROM_NOTIFICATION)) {
            isFromNoti = getIntent().getBooleanExtra(Constants.EXTRA_FROM_NOTIFICATION, false);
        }
        gridView = (DynamicGridView) findViewById(R.id.dynamic_grid);
        dashboardItems.addAll(db.getListOfItems());
        gridView.setAdapter(new CheeseDynamicAdapter(this,
                dashboardItems, 3));
        gridView.setOnDragListener(new DynamicGridView.OnDragListener() {
            @Override
            public void onDragStarted(int position) {
                Log.d("", "drag started at position " + position);
                System.out.println("position " + position);
            }

            @Override
            public void onDragPositionsChanged(int oldPosition, int newPosition) {
                Log.d("", String.format("drag item position changed from %d to %d", oldPosition,
                        newPosition));
                System.out.println("oldPosition  " + oldPosition + "newPosition " + newPosition);
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position,
                                           long id) {
                gridView.startEditMode(position);
                return true;
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(DashBoardActivity.this,
//                        parent.getAdapter().getItem(position).toString(),
//                        Toast.LENGTH_SHORT).show();
                System.out.println("ITEM " + dashboardItems.get(position).getITEM_NAME());
                Intent intent;
                if (dashboardItems.get(position).getITEM_NAME().equalsIgnoreCase(getString(R.string.lighting))) {
//                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
//                            + "OOBUPDATE");
                    intent = new Intent(DashBoardActivity.this, RoomActivity.class);
                    intent.putExtra("update", isUpdated);
                    startActivity(intent);
                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.security))) {
                    intent = new Intent(DashBoardActivity.this, SecurityActivity.class);
                    startActivity(intent);
                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.appliances))) {
                    intent = new Intent(DashBoardActivity.this, AppliancesActivity.class);
                    startActivity(intent);
                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.tv))) {
                    intent = new Intent(DashBoardActivity.this, TVControlActivity.class);
                    startActivity(intent);
                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.curtain))) {
                    intent = new Intent(DashBoardActivity.this, CurtainActivity.class);
                    startActivity(intent);
                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.ac))) {
                    intent = new Intent(DashBoardActivity.this, AcControlActivity.class);
                    startActivity(intent);
                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.water_level))) {
                    if (Preferences.getAppPrefBool(Preferences.KEY_IS_BUNGALOW, true)) {
                        intent = new Intent(DashBoardActivity.this, WaterControlActivity.class);
                        startActivity(intent);
                    } else {
                        intent = new Intent(DashBoardActivity.this, WaterControlFlatActivity.class);
                        startActivity(intent);
                    }
                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.cctv))) {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.mcu.iVMS");
                    if (launchIntent != null) {
                        startActivity(launchIntent);//null pointer check in case package name was not found
                    }else {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.mcu.iVMS")));
                    }

                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.energy))) {
                    intent = new Intent(DashBoardActivity.this, EnergyActivity.class);
                    startActivity(intent);
                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.gate))) {
                    intent = new Intent(DashBoardActivity.this, GateActivity.class);
                    startActivity(intent);
                } else if (dashboardItems.get(position).getITEM_NAME()
                        .equalsIgnoreCase(getString(R.string.scheduler))) {
                    intent = new Intent(DashBoardActivity.this, ShedulerActivity.class);
                    startActivity(intent);
                } else {
//                    intent = new Intent(DashBoardActivity.this, ShedulerActivity.class);
//                    startActivity(intent);
                }
            }
        });
        setToolbar();

        setTitleName(getString(R.string.title_home));
        broadcastListener();
        // showAddUserData();
//        netCheckin();
//        isMobileDataEnabled(this);
        checkForNotification();
        if (isFromNoti) {
            Intent intent = new Intent(DashBoardActivity.this, RoomActivity.class);
            intent.putExtra("update", isUpdated);
            startActivity(intent);
        }

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        int mode;
        if (Utils.isEmptyString(Preferences.getAppPrefString(Preferences.KEY_DATA_TRANSFER_MODE))) {
            mode = 1;
        } else {
            mode = Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_DATA_TRANSFER_MODE));
        }
        System.out.println("IS INTERNET " + Utils.isNetworkAvailable(this));
        if ((mode == 3 || mode == 1) && Utils.isNetworkAvailable(this)) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    OOBApplication.getApp().initMQTT();
                }
            });

        }
    }

    private void checkForNotification() {
        if (Preferences.getAppPrefBool(Preferences.KEY_NOTIFICATION_STATUS, false)) {
            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager nMgr = (NotificationManager) getSystemService(ns);
            nMgr.cancelAll();
            // BEGIN_INCLUDE(notificationCompat)
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            // END_INCLUDE(notificationCompat)

            // BEGIN_INCLUDE(intent)
            //Create Intent to launch this Activity again if the notification is clicked.
            Intent i = new Intent(this, RoomDailogTransActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent = PendingIntent.getActivity(this, 0, i,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(intent);
            // END_INCLUDE(intent)

            // BEGIN_INCLUDE(ticker)
            // Sets the ticker text
            builder.setTicker(getResources().getString(R.string.app_name));

            // Sets the small icon for the ticker
            builder.setSmallIcon(R.mipmap.ic_launcher);
            // END_INCLUDE(ticker)

            // BEGIN_INCLUDE(buildNotification)
            // Cancel the notification when clicked
            builder.setAutoCancel(false);

//            builder.setStyle(getR.style.notification_style);

            // Build the notification
            Notification notification = builder.build();
            // END_INCLUDE(buildNotification)
            notification.flags |= Notification.FLAG_NO_CLEAR;
            // BEGIN_INCLUDE(customLayout)
            // Inflate the notification layout as RemoteViews
            RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification);

            // Set text on a TextView in the RemoteViews programmatically.
            final String time = DateFormat.getTimeInstance().format(new Date()).toString();
            final String text = getResources().getString(R.string.app_name);
            contentView.setTextViewText(R.id.textView, text);


        /* Workaround: Need to set the content view here directly on the notification.
         * NotificationCompatBuilder contains a bug that prevents this from working on platform
         * versions HoneyComb.
         * See https://code.google.com/p/android/issues/detail?id=30495
         */
            notification.contentView = contentView;

            // Add a big content view to the notification if supported.
            // Support for expanded notifications was added in API level 16.
            // (The normal contentView is shown when the notification is collapsed, when expanded the
            // big content view set here is displayed.)
//            if (Build.VERSION.SDK_INT >= 16) {
//                // Inflate and set the layout for the expanded notification view
//                RemoteViews expandedView =
//                        new RemoteViews(getPackageName(), R.layout.notification_expand);
//                notification.bigContentView = expandedView;
//                expandedView.setTextViewText(R.id.textView, text);
//            }
            // END_INCLUDE(customLayout)

            // START_INCLUDE(notify)
            // Use the NotificationManager to show the notification
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.notify(0, notification);
        }
    }


    private void netCheckin() {
        boolean isEnabled;

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (telephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED) {
            isEnabled = true;
        } else {
            isEnabled = false;
        }
        Log.e("IS ENABLE ", "IS " + isEnabled);

        if (isEnabled) {
            mobileDataDesableDialog();

//

        }
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE)
                            .equalsIgnoreCase("update")) {
                        isUpdated = true;
                        dataChangeAlert();
                    }

                }
            }
        };
    }

    public void dataChangeAlert() {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                DashBoardActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle(getString(R.string.title_data_change));

        // Setting Dialog Message
//        alertDialog.setMessage(getString(R.string.msg_data_change));

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog
                .setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }

    public void mobileDataDesableDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                DashBoardActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Disable data!");

        // Setting Dialog Message
        alertDialog.setMessage("Application functions will not work while mobile data is ENABLE.\n\nWould you like to DISABLE mobile data to work application properly?");

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog
                .setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                        startActivity(intent);
                    }
                });
        alertDialog
                .setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });

        // Showing Alert Message
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
//    private void initBackgroundTasks() {
//        service_intent = new Intent(this, HostService.class);
//        wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
//        intentFilter_action_wifi = new IntentFilter("android.net.wifi.STATE_CHANGE");
//        initDataChangeReceiver();
//    }

//    private void showAddUserData() {
//        String udata = Preferences.getAppPrefString(Preferences.KEY_DIALOG_USERDATA);
//        if (udata.isEmpty()) {
//            DialogUtils.showUserDataAlert(this, "Add information..");
//        }
//    }


    @Override
    public void onBackPressed() {
//        Fragment frag=getFrag_organizer().getOpenFragment();
        if (gridView.isEditMode()) {
            gridView.stopEditMode();

            for (int i = 0; i < dashboardItems.size(); i++) {
                int pos = gridView.getPositionForID(i);
                pos++;
                System.out.println("CURRENT " + pos);
                System.out.println("ORIGINAL " + dashboardItems.get(i).getITEM_POSITION());
                db.updateItemPosition(dashboardItems.get(i).getITEM_PID(), pos);

            }
            dashboardItems = new ArrayList<>();
            dashboardItems.addAll(db.getListOfItems());
            gridView.setAdapter(new CheeseDynamicAdapter(this,
                    dashboardItems, 3));
        } else {
            isRunnig = false;
            super.onBackPressed();
        }
//        if (!getFrag_organizer().handleBackNavigation()) {
//            finish();
//        }
    }


    @Override
    public void onOffClick(View v) {

        if (isOn) {
            isOn = false;
//            ivOnOff.setTextColor(Color.parseColor("#000000"));
        } else {
            isOn = false;
//            ivOnOff.setTextColor(Color.parseColor("#000000"));
        }
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "OOBALLOFF");
        super.onOffClick(v);
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//
//            }
//        }
    }

    @Override
    public void onSetting() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
        super.onSetting();
    }


    private void initDataChangeReceiver() {
//        data_change_receiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                System.out.println("data updated");
//                if (intent.hasExtra(BroadcastConstant.DATA_CHANGE)
//                        && !(getFrag_organizer().getOpenFragment() instanceof RoomFragment)) {
//                    if(Preferences.getAppPrefBool(Preferences.KEY_DIALOG_SHOW,true))
//                    {
//                        DialogUtils.showAlert(DashBoardActivity.this
//                                , getString(R.string.title_data_change)
//                                , getString(R.string.msg_data_change));
//                        Preferences.writeSharedPreferencesBool(Preferences.KEY_DIALOG_SHOW, false);
//                    }
//                }
//            }
//        };
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!isResumed) {
            isResumed = true;
            String myip = WifiUtils.getIPAddress(true);

            System.out.println("MY IP ACT " + myip);
            new SocketServerTask(this, null)
                    .execute(WifiUtils.encodeToServerString("CLIENTIP=" + myip),
                            WifiUtils.server_ip, WifiUtils.server_port);

//            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "OOBUPDATE");
        }
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);
        IntentFilter intentFilter1 = new IntentFilter(BroadcastConstant.BROADCAST_ACTION_MASSAGE_ARRAIVED);
        Utils.backupWaterData(this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        //wifiManager.setWifiEnabled(false);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // WifiUtils.hasEnabling=false;
//        unregisterReceiver(wiFiStateReceiver);
//        new SetupServiceReceiverTask(false).execute();
//        Preferences.writeSharedPreferencesBool(Preferences.KEY_DIALOG_SHOW, true);
    }

//
//    private void registerLocalReceiver() {
//        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
//        LocalBroadcastManager.getInstance(this).registerReceiver(data_change_receiver, intentFilter);
//    }
//
//    private void unregisterLocalReceiver() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(data_change_receiver);
//    }
//
//
//    public class SetupServiceReceiverTask extends AsyncTask<Void, Void, Void> {
//        private boolean isResume;
//
//        public SetupServiceReceiverTask(boolean isResume) {
//            this.isResume = isResume;
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            if (isResume) {
//                if(!wifiManager.isWifiEnabled())
//                    wifiManager.setWifiEnabled(true);
//                startService(service_intent);
////                registerReceiver(wiFiStateReceiver, intentFilter_action_wifi);
//                registerLocalReceiver();
//            } else {
//                //  wifiManager.setWifiEnabled(false);
//                stopService(service_intent);
////                unregisterReceiver(wiFiStateReceiver);
//                unregisterLocalReceiver();
//            }
//            return null;
//        }
//    }

}
