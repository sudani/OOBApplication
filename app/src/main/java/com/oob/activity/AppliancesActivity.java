package com.oob.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.oob.OOBApplication;
import com.oob.adapter.AppliancesRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.PixelUtil;
import com.oob.tools.Preferences;

import java.util.ArrayList;
import java.util.List;

public class AppliancesActivity extends BaseActivityChirag {

    private RecyclerView rv_room;

    private AppliancesRecyclerAdapter rv_adapter;

    private dbHelperOperations db;

    List<entDevice> al_data = new ArrayList<>();

    private BroadcastReceiver broadcast;

    private boolean isOn = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);

        setToolbar();
//        setTitleName("Room");
        bindViewControls();
        initItems();
        setTitleName(getString(R.string.title_appliances));
        ivOnOff.setVisibility(View.VISIBLE);
        tvGrouping.setVisibility(View.VISIBLE);
        tvSetting.setText(getString(R.string.icon_onoff));
        ivOnOff.setText(getString(R.string.icon_back));

    }

    private void bindViewControls() {
        rv_room = (RecyclerView) findViewById(R.id.rv_room);
    }

    private void initItems() {

        al_data = new ArrayList<>();
        al_data = db.getListOfDevices(-2, true);
        rv_adapter = new AppliancesRecyclerAdapter(AppliancesActivity.this, al_data);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_room.setLayoutManager(linearLayoutManager);
        rv_room.setAdapter(rv_adapter);
        rv_room.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));
        broadcastListener();

    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    @Override
    public void onGrouping(View v) {
        Intent intent = new Intent(AppliancesActivity.this, AppliancesGroupingActivity.class);
        startActivity(intent);
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE)
                            .equalsIgnoreCase("device")) {
                        if (rv_adapter != null) {
                            al_data = db.getListOfDevices(-2, true);
                            rv_adapter.updateData(al_data);
                        }
                    }
//
                }
            }
        };
    }

    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

    @Override
    public void onSetting() {
        if (Preferences.getAppPrefBool(Preferences.KEY_MAIN_POWER_OFF, true)) {
            Preferences.writeSharedPreferencesBool(Preferences.KEY_MAIN_POWER_OFF, false);
            db.updateAllONOFF(-1, 0);
            rv_adapter.sendAllOnOFFData(
                    Preferences.getAppPrefBool(Preferences.KEY_MAIN_POWER_OFF, true));
            tvSetting.setTextColor(Color.parseColor("#000000"));
            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "OOBALLOFF");
        } else {
            Preferences.writeSharedPreferencesBool(Preferences.KEY_MAIN_POWER_OFF, true);
            db.updateAllONOFF(-1, 1);
            rv_adapter.sendAllOnOFFData(
                    Preferences.getAppPrefBool(Preferences.KEY_MAIN_POWER_OFF, true));
            tvSetting.setTextColor(Color.parseColor("#ffffff"));
            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "OOBALLON");
        }

    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//
//            }
//        }
    }
}
