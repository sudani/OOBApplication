package com.oob.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.oob.app.R;
import com.oob.fragment.IntroFragment;
import com.oob.tools.Utils;
import com.oob.view.Button;
import com.oob.view.CircleIndicator;

public class IntroActivity extends AppCompatActivity {


    private ViewPager viewPager;
private Button btnDemo;
    private PagerAdapter adapter;
    CircleIndicator mIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        viewPager = (ViewPager) findViewById(R.id.pager);
        btnDemo = (Button) findViewById(R.id.btnDemo);
        btnDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showToast(IntroActivity.this,getString(R.string.under_dev));
            }
        });
        mIndicator = (CircleIndicator) findViewById(R.id.indicator);
        mIndicator.configureIndicator(mIndicator.dip2px(10),mIndicator.dip2px(10),mIndicator.dip2px(10));
        adapter = new PagerAdapter
                (getSupportFragmentManager(), 5, getResources().getStringArray(R.array.intro_title),
                        getResources().getStringArray(R.array.intro_detail));
        viewPager.setAdapter(adapter);
        mIndicator.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    public class PagerAdapter extends FragmentStatePagerAdapter {

        int mNumOfTabs;

        String[] titles;

        String[] details;

        public PagerAdapter(FragmentManager fm, int NumOfTabs, String[] titles, String[] details) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
            this.titles = titles;
            this.details = details;
        }

        @Override
        public Fragment getItem(int position) {
            IntroFragment introFragment = new IntroFragment();
            Bundle bundle = new Bundle();
            bundle.putString("title", titles[position]);
            bundle.putString("detail", details[position]);
            introFragment.setArguments(bundle);
            return introFragment;
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }


    }


}
