package com.oob.activity;

import android.os.Bundle;
import android.view.View;

import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entTV;
import com.oob.view.Button;
import com.oob.view.EditText;

public class TVRemoteSetupActivity extends BaseActivityChirag {

    private dbHelperOperations db;

    private EditText edtSetOnOff, edtMute, edtUp, edtDown, edtLeft, edtRight, edtOk, edtBack,
            edtExit, edtVolPlus, edtVolMinus, edtChPlus, edtChMinus, edtOne, edtTwo, edtThree,
            edtFour, edtFive, edtSix, edtSeven, edtEight, edtNine, edtZero, edtStar, edtHas;

    private Button btnSave;


    public int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_setup);
        id = getIntent().getExtras().getInt("id");
        setToolbar();
        setTitleName(getString(R.string.title_tv_remote_setup));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();

        // showAddUserData();
    }

    private void initItems() {
        db = new dbHelperOperations(this);
        entTV tv = db.getTVData(id);

        btnSave = (Button) findViewById(R.id.btnSaveData);
        btnSave.setOnClickListener(this);
        edtSetOnOff = (EditText) findViewById(R.id.edtSetOnOff);
        edtMute = (EditText) findViewById(R.id.edtMute);
        edtUp = (EditText) findViewById(R.id.edtUp);
        edtDown = (EditText) findViewById(R.id.edtDown);
        edtLeft = (EditText) findViewById(R.id.edtLeft);
        edtRight = (EditText) findViewById(R.id.edtRight);
        edtOk = (EditText) findViewById(R.id.edtOK);
        edtBack = (EditText) findViewById(R.id.edtBack);
        edtExit = (EditText) findViewById(R.id.edtExit);
        edtVolPlus = (EditText) findViewById(R.id.edtVolPlus);
        edtVolMinus = (EditText) findViewById(R.id.edtVolMinus);
        edtChPlus = (EditText) findViewById(R.id.edtChPlus);
        edtChMinus = (EditText) findViewById(R.id.edtChMinus);
        edtOne = (EditText) findViewById(R.id.edtOne);
        edtTwo = (EditText) findViewById(R.id.edtTwo);
        edtThree = (EditText) findViewById(R.id.edtThree);
        edtFour = (EditText) findViewById(R.id.edtFour);
        edtFive = (EditText) findViewById(R.id.edtFive);
        edtSix = (EditText) findViewById(R.id.edtSix);
        edtSeven = (EditText) findViewById(R.id.edtSeven);
        edtEight = (EditText) findViewById(R.id.edtEight);
        edtNine = (EditText) findViewById(R.id.edtNine);
        edtZero = (EditText) findViewById(R.id.edtZero);
        edtStar = (EditText) findViewById(R.id.edtStar);
        edtHas = (EditText) findViewById(R.id.edtHas);
        setCurrentData(tv);


    }

    private void setCurrentData(entTV tv) {
        edtSetOnOff.setText(tv.getSET_STT());
        edtMute.setText(tv.getMUTE());
        edtUp.setText(tv.getTV_UP());
        edtDown.setText(tv.getTV_DOWN());
        edtLeft.setText(tv.getTV_LEFT());
        edtRight.setText(tv.getTV_RIGHT());
        edtOk.setText(tv.getTV_OK());
        edtBack.setText(tv.getTV_BACK());
        edtExit.setText(tv.getTV_EXIT());
        edtVolPlus.setText(tv.getTV_VOL_PLUS());
        edtVolMinus.setText(tv.getTV_VOL_MINUS());
        edtChPlus.setText(tv.getTV_CH_PLUS());
        edtChMinus.setText(tv.getTV_CH_MINUS());
        edtOne.setText(tv.getTV_ONE());
        edtTwo.setText(tv.getTV_TWO());
        edtThree.setText(tv.getTV_THREE());
        edtFour.setText(tv.getTV_FOUR());
        edtFive.setText(tv.getTV_FIVE());
        edtSix.setText(tv.getTV_SIX());
        edtSeven.setText(tv.getTV_SEVEN());
        edtEight.setText(tv.getTV_EIGHT());
        edtNine.setText(tv.getTV_NINE());
        edtZero.setText(tv.getTV_ZERO());
        edtStar.setText(tv.getTV_STAR());
        edtHas.setText(tv.getTV_HAS());
    }


    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnSaveData:
                entTV tv = new entTV();
                tv.setSET_STT(edtSetOnOff.getText().toString());
                tv.setMUTE(edtMute.getText().toString());
                tv.setTV_UP(edtUp.getText().toString());
                tv.setTV_DOWN(edtDown.getText().toString());
                tv.setTV_LEFT(edtLeft.getText().toString());
                tv.setTV_RIGHT(edtRight.getText().toString());
                tv.setTV_OK(edtOk.getText().toString());
                tv.setTV_BACK(edtBack.getText().toString());
                tv.setTV_EXIT(edtExit.getText().toString());
                tv.setTV_VOL_PLUS(edtVolPlus.getText().toString());
                tv.setTV_VOL_MINUS(edtVolMinus.getText().toString());
                tv.setTV_CH_PLUS(edtChPlus.getText().toString());
                tv.setTV_CH_MINUS(edtChMinus.getText().toString());
                tv.setTV_ONE(edtOne.getText().toString());
                tv.setTV_TWO(edtTwo.getText().toString());
                tv.setTV_THREE(edtThree.getText().toString());
                tv.setTV_FOUR(edtFour.getText().toString());
                tv.setTV_FIVE(edtFive.getText().toString());
                tv.setTV_SIX(edtSix.getText().toString());
                tv.setTV_SEVEN(edtSeven.getText().toString());
                tv.setTV_EIGHT(edtEight.getText().toString());
                tv.setTV_NINE(edtNine.getText().toString());
                tv.setTV_ZERO(edtZero.getText().toString());
                tv.setTV_STAR(edtStar.getText().toString());
                tv.setTV_HAS(edtHas.getText().toString());
                tv.setTV_Id(id);
                db.updateRemoteValue(tv);
                finish();

        }
    }
}
