package com.oob.activity;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.oob.adapter.TVSetupRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entTV;
import com.oob.tools.PixelUtil;

import java.util.ArrayList;
import java.util.List;

public class TVSetupActivity extends BaseActivityChirag {

    private dbHelperOperations db;

    private RecyclerView rv_rooms;

    private TVSetupRecyclerAdapter rv_adapter;

    List<entTV> TVs = new ArrayList<>();

    public int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_tv_setup));
        ivMenu.setVisibility(View.VISIBLE);
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();

        // showAddUserData();
    }

    private void initItems() {
        db = new dbHelperOperations(this);
        TVs = db.getListTV();
        rv_rooms = (RecyclerView) findViewById(R.id.rv_room);
        rv_adapter = new TVSetupRecyclerAdapter(TVSetupActivity.this, TVs);
        rv_rooms.setLayoutManager(new LinearLayoutManager(TVSetupActivity.this));
        rv_rooms.setAdapter(rv_adapter);
        rv_rooms.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));
    }


    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    @Override
    public void onMenuClick(View v) {
        PopupMenu popup = new PopupMenu(TVSetupActivity.this, v);
        popup.getMenuInflater().inflate(R.menu.tv_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.addTV:
                        addNewTv();
                        break;
                }
                return true;
            }
        });

        popup.show();//showing popup menu

        super.onMenuClick(v);
    }

    private void addNewTv() {
        entTV entTV = new entTV();
        entTV.setTV_NAME(getString(R.string.tv_name));
        entTV.setROOM_NAME(getString(R.string.room_name));
        entTV.setSET_NAME(getString(R.string.set_name));
        entTV.setTV_Id((int) db.insertTv(entTV));
        rv_adapter.addTV(entTV);
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }
}
