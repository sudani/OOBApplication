package com.oob.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.oob.OOBApplication;
import com.oob.adapter.LogDataPager;
import com.oob.adapter.RoomsHoriZontalAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entLogData;
import com.oob.entity.entRooms;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.view.EditText;

import java.util.ArrayList;
import java.util.List;

import static com.github.mikephil.charting.utils.ColorTemplate.rgb;

public class EnergyActivity extends BaseActivityChirag implements TabLayout.OnTabSelectedListener {

    //    private MainFragmentOrganizer mainFragmentOrganizer;
    dbHelperOperations db;

    private BroadcastReceiver broadcast;
    ProgressDialog dialog;
    RecyclerView rvRooms;
    private TabLayout tabLayout;
    //This is our viewPager
    private ViewPager viewPager;
    private List<entRooms> rooms = new ArrayList<>();
    public static final int[] BAR_COLOOR = {
            rgb("#ff00afef"), rgb("#FFFFFF")
    };
    public int roomId;
    LogDataPager logDataPager;
    ArrayList<entLogData> logDatas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_energy);
        db = new dbHelperOperations(this);
        broadcastListener();
        setToolbar();
        setTitleName(getString(R.string.title_energy));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        tvSetting.setText(getString(R.string.icon_setting));
        ivMenu.setVisibility(View.VISIBLE);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText(R.string.daily));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.weekly));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.monthly));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.yearly));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);
        initItems();
        logDatas = generateDummyData();

    }

    private void initItems() {
        rvRooms = (RecyclerView) findViewById(R.id.rvRooms);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvRooms.setLayoutManager(horizontalLayoutManagaer);
        rooms = new ArrayList<>();
        rooms = db.getListOfRooms(false);

        if (rooms.size() > 0) {
            roomId = rooms.get(0).getROOM_Id();
            final RoomsHoriZontalAdapter horiZontalAdapter = new RoomsHoriZontalAdapter(rooms, this, roomId);
//            adapter.setOnRecyclerViewItemClickListener(new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
//                @Override
//                public void onItemClick(View view, int i) {
////                    initChart();
////                    setDataFromString(rooms.get(i).getROOM_Id());
//                    if (roomId == rooms.get(i).getROOM_Id()) {
//                        return;
//                    }
//                    roomId = rooms.get(i).getROOM_Id();
//                    adapter.roomId = roomId;
//                    if (logDataPager != null) {
//                        int pos = viewPager.getCurrentItem();
//                        viewPager.setAdapter(logDataPager);
//                        viewPager.setCurrentItem(pos);
//                    }
//                    adapter.notifyDataSetChanged();
//                }
//            });
            rvRooms.setAdapter(horiZontalAdapter);
            rvRooms.addOnItemTouchListener(new OnItemClickListener() {
                @Override
                public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int i) {
                    if (roomId == rooms.get(i).getROOM_Id()) {
                        return;
                    }
                    logDatas = generateDummyData();
                    roomId = rooms.get(i).getROOM_Id();
                    horiZontalAdapter.roomId = roomId;
                    if (logDataPager != null) {
                        int pos = viewPager.getCurrentItem();
                        viewPager.setAdapter(logDataPager);
                        viewPager.setCurrentItem(pos);
                    }
                    adapter.notifyDataSetChanged();

                }
            });
//            initChart();
//            setDataFromString(rooms.get(0).getROOM_Id());


        }


        //Creating our pager adapter
        viewPager.setOffscreenPageLimit(tabLayout.getTabCount());
        logDataPager = new LogDataPager(getSupportFragmentManager(), tabLayout.getTabCount());
        //Adding adapter to pager
        viewPager.setAdapter(logDataPager);
        viewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));        //Adding onTabSelectedListener to swipe views
        tabLayout.addOnTabSelectedListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION_LOGDATA);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);
    }

    private ArrayList<entLogData> generateDummyData() {
        ArrayList<entLogData> value = new ArrayList<>();
        entLogData entLogData;
        String devices[] = new String[]{"Plug", "Light", "AC", "TV", "Fan", "Light1", "Fan1", "TV",};
        for (int i = 0; i < 5; i++) {
            long mult = (500000000000l + 1);
            long val = (long) (Math.random() * mult);
            entLogData = new entLogData();
            entLogData.setDateTime(val);
            entLogData.setDeviceName(devices[i]);
            value.add(entLogData);
        }
        return value;
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                boolean data = intent.getBooleanExtra(BroadcastConstant.MESSAGE, false);
                int total = intent.getIntExtra(BroadcastConstant.TOTAL, 1);
                int count = intent.getIntExtra(BroadcastConstant.COUNT, 0);
                System.out.println("total " + total + "  count " + count);
                int per = 100 * count / total;
                if (data) {
                    if (dialog == null) {
                        dialog = new ProgressDialog(EnergyActivity.this);
                        dialog.setMessage(getString(R.string.msg_update_log_data) + per + "%");
                        dialog.setCancelable(false);
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                    } else {
                        dialog.setMessage(getString(R.string.msg_update_log_data) + per + "%");
                    }
                } else {
                    if (dialog != null) {
                        dialog.dismiss();
                        initItems();
                    }
                }
            }

        };
    }


    @Override
    public void onMenuClick(View v) {
        PopupMenu popup = new PopupMenu(EnergyActivity.this, v);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.energy_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent;
                switch (item.getItemId()) {
                    case R.id.update:
                        selectMonthYearDialog();
                        break;
                    case R.id.clear:
                        db.deleteLogData();
                        initItems();
                        break;

                }

//                rv_adapter.AddData(getData(5,8));
//                Toast.makeText(RoomActivity.this, "You Clicked : " + item.getTitle(),
//                        Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        popup.show();//showing popup menu

        super.onMenuClick(v);
    }


    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .execute(json, WifiUtils.server_ip, WifiUtils.server_port);
//
//            }
//        }
    }

    @Override
    public void onSetting() {
        super.onSetting();

    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }


    private void selectMonthYearDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_tv_picker);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtModel);
        edtName.setVisibility(View.GONE);
        final AppCompatSpinner spinMonth = (AppCompatSpinner) dialog.findViewById(R.id.spinTvName);
        final AppCompatSpinner spinYears = (AppCompatSpinner) dialog
                .findViewById(R.id.spintvProtocol);
        final AppCompatSpinner spinOnOff = (AppCompatSpinner) dialog
                .findViewById(R.id.spinOnOff);
        spinOnOff.setVisibility(View.GONE);

        final String[] months = getResources().getStringArray(R.array.months);
        int currentYear = 2017;
        int lenth = 100;
        int year;
        final String[] years = new String[100];
        for (int i = 0; i < lenth; i++) {
            year = currentYear + i;
            years[i] = String.valueOf(year);
        }
        final ArrayAdapter adapterMonths = new ArrayAdapter<String>(this,
                R.layout.simple_spiner_item_white, months);
        adapterMonths.setDropDownViewResource(R.layout.simple_spiner_item_white);
        spinMonth.setAdapter(adapterMonths);
        final ArrayAdapter adapterYears = new ArrayAdapter<String>(this,
                R.layout.simple_spiner_item_white, years);
        adapterYears.setDropDownViewResource(R.layout.simple_spiner_item_white);
        spinYears.setAdapter(adapterYears);
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "SD" + Constants.getTwoDigit((spinMonth.getSelectedItemPosition() + 1)) + spinYears.getSelectedItem());
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public ArrayList<entLogData> getMonthData() {
        ArrayList<entLogData> value = new ArrayList<>();
        entLogData entLogData;
        for (int i = 0; i < logDatas.size(); i++) {
            entLogData = new entLogData();
            entLogData.setDateTime(logDatas.get(i).getDateTime() / 12);
            entLogData.setDeviceName(logDatas.get(i).getDeviceName());
            value.add(entLogData);
        }
        return value;
    }

    public ArrayList<entLogData> getWeekData() {
        ArrayList<entLogData> value = new ArrayList<>();
        entLogData entLogData;
        for (int i = 0; i < logDatas.size(); i++) {
            entLogData = new entLogData();
            entLogData.setDateTime(logDatas.get(i).getDateTime() / 52);
            entLogData.setDeviceName(logDatas.get(i).getDeviceName());
            value.add(entLogData);
        }
        return value;
    }

    public ArrayList<entLogData> getDayData() {
        ArrayList<entLogData> value = new ArrayList<>();
        entLogData entLogData;
        for (int i = 0; i < logDatas.size(); i++) {
            entLogData = new entLogData();
            entLogData.setDateTime(logDatas.get(i).getDateTime() / 365);
            entLogData.setDeviceName(logDatas.get(i).getDeviceName());
            value.add(entLogData);
        }
        return value;
    }

    public ArrayList<entLogData> getYearData() {
        return logDatas;
    }
}
