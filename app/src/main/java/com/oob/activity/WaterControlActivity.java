package com.oob.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.view.TextView;

import java.util.Calendar;

import me.itangqi.waveloadingview.WaveLoadingView;

public class WaterControlActivity extends BaseActivityChirag
        implements TimePickerDialog.OnTimeSetListener {
    dbHelperOperations db;
    entDevice device;
    private TextView tv_fromTimer1, tv_toTimer1, tv_fromTimer2, tv_toTimer2, tvProgress, tvTotalUsed, tvProgressML;
    private TextView tv1, tv2, tv3, tv4, tv5, tv6;
    private ImageView ivDevice;
    private LinearLayout ll_days, layTimer1, layTimer2;
    WaveLoadingView mLoadingView;
    private BroadcastReceiver broadcast;
    private View viewHighLimit, viewLowLimit;
    int total;
    private int picker = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water);
        if (Preferences.getAppPrefString(Preferences.KEY_TOTAL_CAPACITY).equalsIgnoreCase("")) {
            Intent intent = new Intent(this, SettingsWaterActivity.class);
            startActivity(intent);
            finish();
        }
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_water_level));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.VISIBLE);
        initItems();
        broadcastListener();
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "WATERLEVEL1");
    }

    private void initItems() {
        device = db.getDeviceOfPump();
        viewHighLimit = (View) findViewById(R.id.viewHighLimit);
        viewLowLimit = (View) findViewById(R.id.viewLowLimit);
        ll_days = (LinearLayout) findViewById(R.id.ll_days);
        mLoadingView = (WaveLoadingView) findViewById(R.id.waveLoadingView);
        layTimer1 = (LinearLayout) findViewById(R.id.layTimer1);
        layTimer2 = (LinearLayout) findViewById(R.id.layTimer2);
        tv_fromTimer1 = (TextView) findViewById(R.id.tvTimer1From);
        tv_toTimer1 = (TextView) findViewById(R.id.tvTimer1to);
        tv_fromTimer2 = (TextView) findViewById(R.id.tvTimer2From);
        tv_toTimer2 = (TextView) findViewById(R.id.tvTimer2to);
        ivDevice = (ImageView) findViewById(R.id.ivDevice);
        tv1 = (TextView) findViewById(R.id.tv11);
        tv2 = (TextView) findViewById(R.id.tv22);
        tv3 = (TextView) findViewById(R.id.tv33);
        tv4 = (TextView) findViewById(R.id.tv44);
        tv5 = (TextView) findViewById(R.id.tv55);
        tv6 = (TextView) findViewById(R.id.tv66);
        tvProgress = (TextView) findViewById(R.id.tvProgress);
        tvTotalUsed = (TextView) findViewById(R.id.tvTotalUsed);
        tvProgressML = (TextView) findViewById(R.id.tvProgressMl);

        System.out.println("id " + device.getDEVICE_NAME());
        if (device.getDEVICE_NAME() != null) {

            tv_fromTimer1.setText(device.getTIME_ONE_A());
            tv_toTimer1.setText(device.getTIME_ONE_B());
            tv_fromTimer2.setText(device.getTIME_TWO_A());
            tv_toTimer2.setText(device.getTIME_TWO_B());
            ivDevice.setImageResource(
                    device.getSTT() == 1 ? R.drawable.on : R.drawable.off);
            ivDevice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS)) >= Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_HIGH_CAPACITY))) {
                        return;
                    }
                    device.setSTT(device.getSTT() == 0 ? 1 : 0);
                    device.setALL_STT(device.getSTT());
                    Log.d("TEST", "from check");
                    if (ivDevice.isShown()) {

                        ivDevice.setImageResource(
                                device.getSTT() == 1 ? R.drawable.on : R.drawable.off);

                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                + Constants
                                .getRoomcode(device.getROOM_Id())
                                + Constants
                                .getModuleAndDevicecode(device.getMODULE())
                                + device
                                .getSTT()
                                + "D" + Constants
                                .getThreeDigit(device.getDIM()) + "RX"
                                + device.getRGBSTT()
                                + device.getMIC()
                        );

                    } else {

                    }
                    // TODO: 4/30/2016 UPDATE DATABASE

                }
            });
            layTimer1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    picker = 1;
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog tpd = TimePickerDialog.newInstance(
                            WaterControlActivity.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            false
                    );

                    tpd.show(getFragmentManager(), "Timepickerdialog");
                }
            });
            layTimer2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    picker = 2;

                    Calendar now = Calendar.getInstance();
                    TimePickerDialog tpd = TimePickerDialog.newInstance(
                            WaterControlActivity.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            false
                    );

                    tpd.show(getFragmentManager(), "Timepickerdialog");
                }
            });
            setupDaysClick(ll_days);
            setDaysFromBean(ll_days);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    @Override
    protected void onResume() {
        String suffix = "\nLiters";
        if (Preferences.getAppPrefString(Preferences.KEY_TOTAL_CAPACITY).equalsIgnoreCase("")) {
            Intent intent = new Intent(this, SettingsWaterActivity.class);
            startActivity(intent);
            finish();
        }
        if (!Preferences.getAppPrefBool(Preferences.KEY_IS_BUNGALOW, true)) {
            Intent  intent = new Intent(this, WaterControlFlatActivity.class);
            startActivity(intent);
            finish();
        }

        total = Integer.parseInt((Preferences.getAppPrefString(Preferences.KEY_TOTAL_CAPACITY)).equalsIgnoreCase("") ? "5000" : Preferences.getAppPrefString(Preferences.KEY_TOTAL_CAPACITY));
        int partTotal = total / 5;
        tv1.setText((int) (partTotal * 5) + suffix);
        tv2.setText((int) (partTotal * 4) + suffix);
        tv3.setText((int) (partTotal * 3) + suffix);
        tv4.setText((int) (partTotal * 2) + suffix);
        tv5.setText((int) (partTotal * 1) + suffix);
        if (Preferences.getAppPrefBool(Preferences.KEY_IS_RECT_SHAPE, true)) {
            mLoadingView.setShapeType(WaveLoadingView.ShapeType.SQUARE);
        } else {
            mLoadingView.setShapeType(WaveLoadingView.ShapeType.CIRCLE);
        }
        if (!Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS).equalsIgnoreCase("")) {
            int progress = Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS));
            int proPersant = progress * 100 / total;
            System.out.println("Progress " + proPersant);
            mLoadingView.setProgressValue(proPersant);
        } else {
            Preferences.writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS, "0");
        }
        tvProgress.setText(Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS));
        tvTotalUsed.setText(Preferences.getAppPrefString(Preferences.KEY_TOTAL_USED));
        tvProgressML.setText(Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS_ML));
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int hight = mLoadingView.getMeasuredHeight();
                if (Preferences.getAppPrefString(Preferences.KEY_HIGH_CAPACITY).equalsIgnoreCase("")||Preferences.getAppPrefString(Preferences.KEY_LOW_CAPACITY).equalsIgnoreCase("")){
                    return;
                }
                int high = Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_HIGH_CAPACITY,"0"));
                int low = Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_LOW_CAPACITY,"0"));
                int total = Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_TOTAL_CAPACITY,"0"));
                System.out.println("hight " + mLoadingView.getMeasuredHeight());
                System.out.println("width " + mLoadingView.getMeasuredWidth());
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewHighLimit.getLayoutParams();
                RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) viewLowLimit.getLayoutParams();
                int highMargin, lowMargin, leftmargin;
                leftmargin = hight / 2;
                highMargin = hight * high / total;
                highMargin = hight - highMargin;
                params.setMargins(leftmargin, highMargin, 0, 0);
                viewHighLimit.setLayoutParams(params);

                lowMargin = hight * low / total;
                lowMargin = hight - lowMargin;
                params1.setMargins(leftmargin, lowMargin, 0, 0);
                viewLowLimit.setLayoutParams(params1);
            }
        }, 100);
        super.onResume();
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

    private void setupDaysClick(final LinearLayout ll_days) {
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View view = ll_days.getChildAt(j);
//            if (view instanceof RelativeLayout) {
//                RelativeLayout layout = ((RelativeLayout) view);
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tag = (String) v.getTag();
                        switch (tag) {
                            case "0":
//                                ((TextView) v).setTypeface(null, Typeface.BOLD);
//                                ((TextView) v).setTextColor(Color.CYAN);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_active_bg);
                                v.setTag("1");
                                break;
                            case "1":
//                                ((TextView) v).setTextColor(Color.BLACK);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_bg);
                                v.setTag("0");
                                break;
                        }
                        boolean checked = ((Character) device.getDAYS().charAt(7))
                                .toString()
                                .equals("1");
                        device.setDAYS(getFinalDays(checked, ll_days));
//                        db.updateDevice(bean);
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                + Constants
                                .getRoomcode(device.getROOM_Id())
                                + Constants.getModuleAndDevicecode(device.getMODULE()) + device
                                .getDAYS());

                    }
                });
            }
//            }
        }
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE).equalsIgnoreCase("device")) {
                        initItems();
                    } else if (intent.getExtras().getString(BroadcastConstant.MESSAGE).equalsIgnoreCase("water")) {
                        if (!Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS).equalsIgnoreCase("")) {
                            int progress = Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS));
                            int proPersant = progress * 100 / total;
                            System.out.println("Progress " + proPersant);
                            mLoadingView.setProgressValue(proPersant);
                            if (progress >= Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_HIGH_CAPACITY))) {
//                                OFF
                                device.setSTT(device.getSTT() == 0 ? 1 : 1);
                                device.setALL_STT(device.getSTT());
                                callOnOff();
                            } else if (progress <= Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_LOW_CAPACITY))) {
//                                ON
                                device.setSTT(device.getSTT() == 0 ? 0 : 0);
                                device.setALL_STT(device.getSTT());
                                callOnOff();
                            }
                            tvProgress.setText(Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS));
                            tvTotalUsed.setText(Preferences.getAppPrefString(Preferences.KEY_TOTAL_USED));
                            tvProgressML.setText(Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS_ML));
                        } else {
                            Preferences.writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS, "0");
                        }
                    }
                }
            }
        };
    }

    private void callOnOff() {
        if (device.getROOM_NAME() != null) {
            device.setSTT(device.getSTT() == 0 ? 1 : 0);
            device.setALL_STT(device.getSTT());
            Log.d("TEST", "from check");
            if (ivDevice.isShown()) {

                ivDevice.setImageResource(
                        device.getSTT() == 1 ? R.drawable.on : R.drawable.off);

                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                        + Constants
                        .getRoomcode(device.getROOM_Id())
                        + Constants
                        .getModuleAndDevicecode(device.getMODULE())
                        + device
                        .getSTT()
                        + "D" + Constants
                        .getThreeDigit(device.getDIM()) + "RX"
                        + device.getRGBSTT()
                        + device.getMIC()
                );
            }
        }
    }

    private void setDaysFromBean(LinearLayout ll_days) {
        String days = device.getDAYS();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            String tag = ((Character) days.charAt(j)).toString();
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setTag(tag);
                switch (tag) {
                    case "0":
                        ((com.oob.view.TextView) v).setBackgroundResource(R.drawable.text_bg);
                        break;
                    case "1":
                        ((com.oob.view.TextView) v)
                                .setBackgroundResource(R.drawable.text_active_bg);
                        break;
                }
            }
        }
    }

    private String getFinalDays(boolean isChecked, LinearLayout ll_days) {
        String daysStatus = getSelectedDays(ll_days);
        if (isChecked) {
            daysStatus = daysStatus + "1";
        } else {
            daysStatus = daysStatus + "0";
        }

        return daysStatus;
    }

    private String getSelectedDays(LinearLayout ll_days) {
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                builder.append((String) v.getTag());
            }

        }
        return builder.toString();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.tvRoomSelect:
                intent = new Intent(this, RoomSelectionActivity.class);
                startActivity(intent);
                break;
        }
        super.onClick(v);
    }

    @Override
    public void onOffClick(View v) {
        db.updateDevice(device);
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "WATERLEVEL0");

        finish();
        super.onOffClick(v);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        db.updateDevice(device);
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "WATERLEVEL0");

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd,
                          int minuteEnd) {
        int hour, endHour;
        String ampm, endAMPM, FromDate, ToDate, timer;
        if (picker == 1) {
            timer = "T1";
            System.out.println("hourOfDay " + hourOfDay);
            System.out.println("minute " + minute);
            System.out.println("hourOfDayEnd " + hourOfDayEnd);
            System.out.println("minuteEnd " + minuteEnd);
            if (hourOfDay > 12) {
                hour = hourOfDay - 12;
                ampm = "pm";
            } else {
                hour = hourOfDay;
                ampm = "am";
            }
            if (hourOfDayEnd > 12) {
                endHour = hourOfDayEnd - 12;
                endAMPM = "pm";
            } else {
                endHour = hourOfDayEnd;
                endAMPM = "am";
            }
            FromDate = hour + ":" + minute + " " + ampm;
            ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
            device.setTIME_ONE_A(FromDate);
            device.setTIME_ONE_B(ToDate);
            tv_fromTimer1.setText(FromDate);
            tv_toTimer1.setText(ToDate);
        } else {
            timer = "T2";
            System.out.println("hourOfDay " + hourOfDay);
            System.out.println("minute " + minute);
            System.out.println("hourOfDayEnd " + hourOfDayEnd);
            System.out.println("minuteEnd " + minuteEnd);
            if (hourOfDay > 12) {
                hour = hourOfDay - 12;
                ampm = "pm";
            } else {
                hour = hourOfDay;
                ampm = "am";
            }
            if (hourOfDayEnd > 12) {
                endHour = hourOfDayEnd - 12;
                endAMPM = "pm";
            } else {
                endHour = hourOfDayEnd;
                endAMPM = "am";
            }
            FromDate = hour + ":" + minute + " " + ampm;
            ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
            device.setTIME_TWO_A(FromDate);
            device.setTIME_TWO_B(ToDate);
            tv_fromTimer2.setText(FromDate);
            tv_toTimer2.setText(ToDate);

        }
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(device.getROOM_Id())
                + Constants.getModuleAndDevicecode(device.getMODULE()) + timer
                + Constants.getTwoDigit(hourOfDay) + Constants.getTwoDigit(minute) + Constants
                .getTwoDigit(hourOfDayEnd) + Constants.getTwoDigit(minuteEnd) + "XXXXREP" + device
                .getDAYS());

    }

    @Override
    public void onSetting() {
        Intent intent = new Intent(this, SettingsWaterActivity.class);
        startActivity(intent);
        super.onSetting();
    }
}
