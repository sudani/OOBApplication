package com.oob.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entChannelModel;
import com.oob.entity.entTvModel;
import com.oob.tools.Constants;
import com.oob.tools.DialogUtils;
import com.oob.tools.FieldValidator;
import com.oob.tools.NotifyUtils;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.EditText;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_login;
    private Button btn_lernMore;
    private EditText et_uname, et_pass;
    private CheckBox chk_remember;
    private File mFileTemp;
    private TextView tv_reset_uname, tv_reset_pass;
    public static final String TEMP_PASS_FILE_NAME = "pass.txt";
    boolean isFromNoti = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (getIntent().hasExtra(Constants.EXTRA_FROM_NOTIFICATION)) {
            isFromNoti = getIntent().getBooleanExtra(Constants.EXTRA_FROM_NOTIFICATION, false);
        }
        initialSetup();
//        hideTitle();

//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
//
//                boolean hasPermission = (ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                        == PackageManager.PERMISSION_GRANTED);
//                if (!hasPermission) {
//                    ActivityCompat.requestPermissions(this,
//                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            112);
//                }
//            }else {
//                File dir = new File(Constants.SD_CARD);
//                if (!dir.exists()) {
//                    boolean b = dir.mkdir();
//                    System.out.println("B is " + b);
//                    if (b){
//                        Preferences.writeSharedPreferencesBool(Preferences.KEY_IS_FIRST, false);
//                        dbInsert();
//                    }
//                }
//            }
//

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            if (!Preferences.getAppPrefString(Preferences.KEY_DATA_TRANSFER_MODE).equalsIgnoreCase("3"))
                wifiManager.setWifiEnabled(true);
        }

    }

    private void initialSetup() {
        System.out.println("Init " + Preferences.getAppPrefString(Preferences.KEY_IP_ADDRESS));
        if (Preferences.getAppPrefString(Preferences.KEY_IP_ADDRESS).equalsIgnoreCase("")) {
            Preferences.writeSharedPreferences(Preferences.KEY_IP_ADDRESS, "192.168.0.201");
        }
        boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    13);
            return;
        }
        if (Preferences.getAppPrefBool(Preferences.KEY_IS_FIRST, true)) {
            Utils.restoreSharedPreferencesFromFile(this);
            String outFileName = Constants.DATABASE_BACKUP_LOCATION;
            File dbFileOut = new File(outFileName, Constants.DATABASE_BACKUP_FILE);
            if (dbFileOut.exists()) {
                Utils.restorDataBaseFromSdCard();
            } else {
                dbInsert();
            }
            Preferences.writeSharedPreferencesBool(Preferences.KEY_IS_FIRST, false);

        }
        updateUnameAndPass();
        onCreateCall();
//        login();
    }

    private void onCreateCall() {
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_lernMore = (Button) findViewById(R.id.btn_lernMore);
        et_uname = (EditText) findViewById(R.id.edtUsername);
        et_pass = (EditText) findViewById(R.id.edtPass);
        chk_remember = (CheckBox) findViewById(R.id.chk_remember);
        tv_reset_uname = (TextView) findViewById(R.id.tv_reset_uname);
        tv_reset_pass = (TextView) findViewById(R.id.tv_reset_pass);
        btn_login.setOnClickListener(this);
        btn_lernMore.setOnClickListener(this);
        updateUsernamePass();
        tv_reset_uname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUnameResetlDialog();
            }
        });
        tv_reset_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPassResetlDialog();
            }
        });
        if (isFromNoti) {
            btn_login.callOnClick();
        }
    }

    private void dbInsert() {
        // Fill Dashboard database

        dbHelperOperations db = new dbHelperOperations(this);
        List<String> itmes = new ArrayList<String>(
                Arrays.asList(getResources().getStringArray(R.array.dashboard)));
        List<String> itmeImages = new ArrayList<String>(
                Arrays.asList(getResources().getStringArray(R.array.dashboard_imsges)));
        for (int i = 0; i < itmes.size(); i++) {
            db.insertDashboard(itmes.get(i), itmeImages.get(i), i + 1);
        }
        // Fill Rooms database

        List<String> rooms = new ArrayList<String>(
                Arrays.asList(getResources().getStringArray(R.array.room_selection)));
        for (int i = 0; i < rooms.size(); i++) {
            db.insertRooms(rooms.get(i));
        }
        insertChannelData();

//        try {
//            buffer = new BufferedReader(new InputStreamReader(getAssets().open("oob_automation_name_number.xlsx")));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
////        BufferedReader buffer = new BufferedReader(file);
//        String line = "";
//        if (buffer!=null) {
//            try {
//                entChannelModel chennel;
//                while ((line = buffer.readLine()) != null) {
//                    String[] str = line.split(",");  // defining 3 columns with null or blank field //values acceptance
//                    //Id, Company,Name,Price
//                    chennel = new entChannelModel();
//                    chennel.setCHANNEL_NAME(str[0]);
//                    chennel.setCHANNEL_IMAGE(str[1]);
//                    chennel.setGTPL_CODE(Integer.parseInt(str[2]));
//                    chennel.setDISTV_CODE(Integer.parseInt(str[3]));
//                    chennel.setAIRTEL_CODE(Integer.parseInt(str[4]));
//                    chennel.setVIDEOCON_CODE(Integer.parseInt(str[5]));
//                    chennel.setTATASKY_CODE(Integer.parseInt(str[6]));
//                    chennel.setRELIANCE_CODE(Integer.parseInt(str[7]));
//                    chennel.setJIO_CODE(Integer.parseInt(str[8]));
//                    db.insertChannals(chennel);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        // Fill Security database
//        List<String> security = new ArrayList<String>(
//                Arrays.asList(getResources().getStringArray(R.array.security_sensor)));
//        List<String> securityImages = new ArrayList<String>(
//                Arrays.asList(getResources().getStringArray(R.array.security_image)));
//        for (int i = 0; i < security.size(); i++) {
//            db.insertSecurity(security.get(i), securityImages.get(i));
//        }
//        readAndInsert(db);
//        readExcelFileFromAssets();
    }

    private void insertChannelData() {
        dbHelperOperations db = new dbHelperOperations(this);
//
//        AssetManager assetManager = getAssets();
//        InputStream is = null;
//
//        try {
//            is = assetManager.open("oob_automation_name_number.xlsx");
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//        BufferedReader reader = null;
//        reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
//
//        String line = "";
//        StringTokenizer st = null;
//        BufferedReader buffer = null;
//
//        try {
//            entChannelModel chennel;
//            while ((line = reader.readLine()) != null) {
//                System.out.println("LINE " + line);
//                st = new StringTokenizer(line, ",");
//                System.out.println("DATA "+st.nextToken());
////                chennel = new entChannelModel();
////                chennel.setCHANNEL_NAME(st.nextToken());
////                chennel.setCHANNEL_IMAGE(st.nextToken());
////                chennel.setGTPL_CODE(Integer.parseInt(st.nextToken()));
////                chennel.setDISTV_CODE(Integer.parseInt(st.nextToken()));
////                chennel.setAIRTEL_CODE(Integer.parseInt(st.nextToken()));
////                chennel.setVIDEOCON_CODE(Integer.parseInt(st.nextToken()));
////                chennel.setTATASKY_CODE(Integer.parseInt(st.nextToken()));
////                chennel.setRELIANCE_CODE(Integer.parseInt(st.nextToken()));
////                chennel.setJIO_CODE(0);
////                db.insertChannals(chennel);
////                entTvModel tvModel = new entTvModel();
////                //your attributes
////                tvModel.setBRAND_NAME(st.nextToken());
////                tvModel.setBRAND_MODEL(st.nextToken());
////                tvModel.setPOWER_DATA(st.nextToken());
////                entTvModels.add(tvModel);
//
//
//            }
//        } catch (IOException e) {
//
//            e.printStackTrace();
//        }

        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new InputStreamReader(getAssets().open("oob_automation_name_number.csv")));
        } catch (IOException e) {
            e.printStackTrace();
        }

//        BufferedReader buffer = new BufferedReader(file);
        String line = "";
        if (buffer != null) {
            try {
                entChannelModel chennel;
                while ((line = buffer.readLine()) != null) {
                    System.out.println("LINE " + line);
                    String[] str = line.split(",");  // defining 3 columns with null or blank field //values acceptance
                    //Id, Company,Name,Price
                    chennel = new entChannelModel();
                    chennel.setCHANNEL_NAME(str[0]);
                    chennel.setCHANNEL_IMAGE(str[1]);
                    chennel.setGTPL_CODE(Utils.getNumber(str[2]));
                    chennel.setDISTV_CODE(Utils.getNumber(str[3]));
                    chennel.setAIRTEL_CODE(Utils.getNumber(str[4]));
                    chennel.setVIDEOCON_CODE(Utils.getNumber(str[5]));
                    chennel.setTATASKY_CODE(Utils.getNumber(str[6]));
                    chennel.setRELIANCE_CODE(Utils.getNumber(str[7]));
                    chennel.setJIO_CODE(0);
                    db.insertChannals(chennel);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case 112: {
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    //reload my activity with permission granted or use the features what required the permission
//                    File dir = new File(Constants.SD_CARD);
//                    if (!dir.exists()) {
//                        boolean b = dir.mkdir();
//                        System.out.println("B is " + b);
//                        if (b){
//                            Preferences.writeSharedPreferencesBool(Preferences.KEY_IS_FIRST, false);
//                            dbInsert();
//                        }
//                    }
//                } else {
//                    Toast.makeText(this,
//                            "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission",
//                            Toast.LENGTH_LONG).show();
//                }
//            }
//        }
//
//    }


    private void updateUnameAndPass() {
        String uname = Preferences.getAppPrefString(Preferences.KEY_USER);
        String pass = Preferences.getAppPrefString(Preferences.KEY_PASS);
        if (uname.isEmpty() && pass.isEmpty()) {
            boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED);
            if (!hasPermission) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        12);
                return;
            }
            File f = new File(Environment.getExternalStorageDirectory() + "/oob",
                    TEMP_PASS_FILE_NAME);
            if (f.exists()) {
                try {
                    JSONObject jsonObject = new JSONObject(Utils.readPassword(f));
                    Preferences.writeSharedPreferences(Preferences.KEY_USER,
                            jsonObject.getString("USER"));
                    Preferences.writeSharedPreferences(Preferences.KEY_PASS,
                            jsonObject.getString("PASSWORD"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Preferences.writeSharedPreferences(Preferences.KEY_USER, "oob");
                Preferences.writeSharedPreferences(Preferences.KEY_PASS, "oob");
            }
        }
    }

    private void createTempFile() {

        boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    10);
            return;
        }

        File f = new File(Environment.getExternalStorageDirectory() + "/oob");
        if (!f.exists()) {
            f.mkdir();
        }
        mFileTemp = new File(Environment.getExternalStorageDirectory() + "/oob",
                TEMP_PASS_FILE_NAME);
        Utils.writePassword(
                createJson(et_uname.getText().toString(), et_pass.getText().toString()), mFileTemp);

        if (Utils.isEmptyString(Preferences.getAppPrefString(Preferences.KEY_APP_USAGE_TYPE))) {
            resumeApp();
        } else {
            DashBoardActivity.isResumed = false;
            startActivity(new Intent(LoginActivity.this, DashBoardActivity.class).putExtra(Constants.EXTRA_FROM_NOTIFICATION, isFromNoti));
            finish();
        }

//        } else {
//            mFileTemp = new File(getFilesDir(), System.currentTimeMillis() + TEMP_PASS_FILE_NAME);
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 10: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createTempFile();
                    //reload my activity with permission granted or use the features what required the permission
                } else {
                    Toast.makeText(this,
                            getString(R.string.erroe_permision_deny_storage),
                            Toast.LENGTH_LONG).show();

                }
                break;
            }
            case 12: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    updateUnameAndPass();
                    //reload my activity with permission granted or use the features what required the permission
                } else {

                    Toast.makeText(this,
                            getString(R.string.erroe_permision_deny_storage),
                            Toast.LENGTH_LONG).show();
                }
            }
            break;
            case 13:
                initialSetup();
                break;

        }

    }

    private void resumeApp() {
        //Initialize the Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//Source of the data in the DIalog
        final String[] array = {getString(R.string.sample_house), getString(R.string.flat), getString(R.string.bungalow), getString(R.string.commercial), getString(R.string.hotel)};
        final String[] type = new String[1];
// Set the dialog title
        builder.setTitle(R.string.title_select_option)
                .setSingleChoiceItems(array, -1, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        type[0] = array[which];
                    }
                })

// Set the action buttons
                .setPositiveButton(getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                if (Utils.isEmptyString(type[0])) {
                                    Utils.showToast(LoginActivity.this, getString(R.string.error_select_option));
                                } else {
                                    dialog.dismiss();
                                    if (type[0].equalsIgnoreCase(getString(R.string.flat)) || type[0].equalsIgnoreCase(getString(R.string.bungalow))) {
                                        Preferences.writeSharedPreferences(Preferences.KEY_APP_USAGE_TYPE, type[0]);
                                        DashBoardActivity.isResumed = false;
                                        startActivity(new Intent(LoginActivity.this, DashBoardActivity.class).putExtra(Constants.EXTRA_FROM_NOTIFICATION, isFromNoti));
                                        finish();
                                    } else {

                                    }
                                }
                            }
                        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.create().show();
    }


    private void updateUsernamePass() {
        if (Preferences.getAppPrefBool(Preferences.KEY_REMEMBER, false)) {
            chk_remember.setChecked(true);
            et_uname.setText(Preferences.getAppPrefString(Preferences.KEY_USER));
            et_pass.setText(Preferences.getAppPrefString(Preferences.KEY_PASS));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                login();
                break;
            case R.id.btn_lernMore:
                Intent intent = new Intent(this, IntroActivity.class);
                startActivity(intent);

                break;
        }
    }

    private void login() {

        if (et_uname.length() > 0 && et_pass.length() > 0) {
            String uname = Preferences.getAppPrefString(Preferences.KEY_USER);
            String pass = Preferences.getAppPrefString(Preferences.KEY_PASS);
            String edt_uname = et_uname.getText().toString();
            String edt_pass = et_pass.getText().toString();

            if (edt_uname.equals(uname) & edt_pass.equals(pass)) {

                if (chk_remember.isChecked()) {
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_REMEMBER, true);
                } else {
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_REMEMBER, false);
                }
                createTempFile();
            } else {
                NotifyUtils.toast(this, getString(R.string.error_wrong_credential));
            }

        } else {
            NotifyUtils.toast(this, getString(R.string.error_enter_credential));
        }
    }

    private String createJson(String user, String pass) {
        JSONObject job = new JSONObject();
        try {
            job.put("USER", user);
            job.put("PASSWORD", pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return job.toString();

    }

    void showUnameResetlDialog() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_reset_uname);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        lp.width = size.x;
        lp.height = size.y;

        window.setAttributes(lp);
        ((TextView) dialog.findViewById(R.id.tv_title)).setText(R.string.reset_username);
        // set values for custom dialog components - text, image and button
        final EditText et_new_uname = (EditText) dialog.findViewById(R.id.et_new_uname);
        final EditText et_pass = (EditText) dialog.findViewById(R.id.et_pass);

        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.btn_close);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String enter_pass = et_pass.getText().toString();
                String enter_uname = et_new_uname.getText().toString();
                if (enter_pass.isEmpty() || enter_uname.isEmpty()) {
                    DialogUtils.showAlertDialog(LoginActivity.this, getString(R.string.app_name),
                            getString(R.string.error_enter_value));
                } else if (!FieldValidator.validatePassword(enter_pass)) {
                    DialogUtils.showAlertDialog(LoginActivity.this, getString(R.string.app_name),
                            getString(R.string.error_password_not_match));
                } else {
                    Preferences.writeSharedPreferences(Preferences.KEY_USER, enter_uname);
                    DialogUtils.showAlertDialog(LoginActivity.this, getString(R.string.app_name),
                            getString(R.string.username_updated));
                    dialog.dismiss();
                }
            }
        });

    }

    void showPassResetlDialog() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_reset_pass);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        lp.width = size.x;
        lp.height = size.y;

        window.setAttributes(lp);
        // set values for custom dialog components - text, image and button
        ((TextView) dialog.findViewById(R.id.tv_title)).setText(R.string.reset_password);
        final EditText et_old_pass = (EditText) dialog.findViewById(R.id.et_old_pass);
        final EditText et_new_pass = (EditText) dialog.findViewById(R.id.et_new_pass);

        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.btn_close);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String enter_pass = et_old_pass.getText().toString();
                String enter_new_pass = et_new_pass.getText().toString();
                if (enter_pass.isEmpty() || enter_new_pass.isEmpty()) {
                    DialogUtils.showAlertDialog(LoginActivity.this, getString(R.string.app_name),
                            getString(R.string.error_enter_value));
                } else if (!FieldValidator.validatePassword(enter_pass)) {
                    DialogUtils.showAlertDialog(LoginActivity.this, getString(R.string.app_name),
                            getString(R.string.error_old_password_not_match));
                } else {
                    Preferences.writeSharedPreferences(Preferences.KEY_PASS, enter_new_pass);
                    DialogUtils.showAlertDialog(LoginActivity.this, getString(R.string.app_name),
                            getString(R.string.password_updated));
                    dialog.dismiss();
                }
            }
        });

    }

    private void readAndInsert(dbHelperOperations db) {

        ArrayList<entTvModel> entTvModels = new ArrayList<entTvModel>();
        AssetManager assetManager = getAssets();
        InputStream is = null;

        try {
            is = assetManager.open("remote_model_code.xlsx");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        BufferedReader reader = null;
        reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));

        String line = "";
        StringTokenizer st = null;
        try {

            while ((line = reader.readLine()) != null) {
                System.out.println("LINE " + line);
                st = new StringTokenizer(line, ",");
                entTvModel tvModel = new entTvModel();
                //your attributes
                tvModel.setBRAND_NAME(st.nextToken());
                tvModel.setBRAND_MODEL(st.nextToken());
                tvModel.setPOWER_DATA(st.nextToken());
                entTvModels.add(tvModel);


            }
        } catch (IOException e) {

            e.printStackTrace();
        }

        db.insetTvModelDetail(entTvModels);

    }

    public void readExcelFileFromAssets() {

        try {
            ArrayList<entTvModel> entTvModels = new ArrayList<entTvModel>();

            AssetManager assetManager = this.getAssets();
            InputStream inStream;
            Workbook wb = null;
            try {
                inStream = assetManager.open("remote_model_code.xls");
                wb = new HSSFWorkbook(inStream);
                inStream.close();
            } catch (IOException e) {

                e.printStackTrace();
            }
            Sheet sheet1 = wb.getSheetAt(0);
            if (sheet1 == null) {
                return;
            }
            for (Iterator<Row> rit = sheet1.rowIterator(); rit.hasNext(); ) {
                Row row = rit.next();
                row.getCell(0, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);
                row.getCell(1, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);
                row.getCell(2, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);

                entTvModel tvModel = new entTvModel();
                //your attributes
                tvModel.setBRAND_NAME(
                        row.getCell(0, Row.CREATE_NULL_AS_BLANK).getStringCellValue());
                tvModel.setBRAND_MODEL(
                        row.getCell(1, Row.CREATE_NULL_AS_BLANK).getStringCellValue());
                tvModel.setPOWER_DATA(
                        row.getCell(2, Row.CREATE_NULL_AS_BLANK).getStringCellValue());
                entTvModels.add(tvModel);

            }
            new dbHelperOperations(this).insetTvModelDetail(entTvModels);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return;
    }
}
