package com.oob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.oob.adapter.SectionAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entChannel;
import com.oob.entity.entMySection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.oob.app.R.id.rv_room;

public class ChannelSelectionActivity extends BaseActivityChirag
        {

    private RecyclerView rv_channel;

    private dbHelperOperations db;

    private List<entMySection> channelData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);

        setToolbar();
        bindViewControls();
        initItems();
        setTitleName(getString(R.string.title_channel_selection));
        ivOnOff.setVisibility(View.VISIBLE);
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);

    }

    private void bindViewControls() {
        rv_channel = (RecyclerView) findViewById(rv_room);
        rv_channel.setHasFixedSize(true);
    }

    private void initItems() {
        generateData();
        rv_channel.setLayoutManager(
                new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        SectionAdapter sectionAdapter = new SectionAdapter(this, R.layout.raw_channel,
                R.layout.raw_channel_head, channelData);
        rv_channel.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int i) {
                entMySection mySection = channelData.get(i);
                if (mySection.isHeader) {
//            Toast.makeText(this, mySection.header, Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("name", mySection.t.getName());
                    intent.putExtra("image", mySection.t.getImg());
                    setResult(RESULT_OK, intent);
                    finish();
//            Toast.makeText(this, mySection.t.getName(), Toast.LENGTH_LONG).show();
                }

            }
        });

        rv_channel.setAdapter(sectionAdapter);


    }

    private void generateData() {
        List<entMySection> list = new ArrayList<>();
        List<String> images = new ArrayList<>();
        List<String> names = new ArrayList<>();

        list.add(new entMySection(true, getString(R.string.entertainment), true));
        images = Arrays.asList(getResources().getStringArray(R.array.entertainment_image));
        names = Arrays.asList(getResources().getStringArray(R.array.entertainment_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }
        list.add(new entMySection(true, getString(R.string.movies), true));
        images = Arrays.asList(getResources().getStringArray(R.array.movie_image));
        names = Arrays.asList(getResources().getStringArray(R.array.movie_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }
        list.add(new entMySection(true, getString(R.string.sports), true));
        images = Arrays.asList(getResources().getStringArray(R.array.sports_image));
        names = Arrays.asList(getResources().getStringArray(R.array.sports_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }
        list.add(new entMySection(true, getString(R.string.music), true));
        images = Arrays.asList(getResources().getStringArray(R.array.music_image));
        names = Arrays.asList(getResources().getStringArray(R.array.music_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.news), true));
        images = Arrays.asList(getResources().getStringArray(R.array.news_image));
        names = Arrays.asList(getResources().getStringArray(R.array.news_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.b_news), true));
        images = Arrays.asList(getResources().getStringArray(R.array.business_news_image));
        names = Arrays.asList(getResources().getStringArray(R.array.business_news_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.infotainment), true));
        images = Arrays.asList(getResources().getStringArray(R.array.infotainment_image));
        names = Arrays.asList(getResources().getStringArray(R.array.infotainment_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }
        list.add(new entMySection(true, getString(R.string.kids), true));
        images = Arrays.asList(getResources().getStringArray(R.array.kids_image));
        names = Arrays.asList(getResources().getStringArray(R.array.kids_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.gujarati), true));
        images = Arrays.asList(getResources().getStringArray(R.array.gujarati_image));
        names = Arrays.asList(getResources().getStringArray(R.array.gujarati_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.hindi), true));
        images = Arrays.asList(getResources().getStringArray(R.array.hindi_image));
        names = Arrays.asList(getResources().getStringArray(R.array.hindi_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.bangali), true));
        images = Arrays.asList(getResources().getStringArray(R.array.bengali_image));
        names = Arrays.asList(getResources().getStringArray(R.array.bengali_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }
        list.add(new entMySection(true, getString(R.string.malayalam), true));
        images = Arrays.asList(getResources().getStringArray(R.array.malayalam_image));
        names = Arrays.asList(getResources().getStringArray(R.array.malayalam_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.marathi), true));
        images = Arrays.asList(getResources().getStringArray(R.array.marathi_image));
        names = Arrays.asList(getResources().getStringArray(R.array.marathi_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }






        list.add(new entMySection(true, getString(R.string.north_east), true));
        images = Arrays.asList(getResources().getStringArray(R.array.north_east_image));
        names = Arrays.asList(getResources().getStringArray(R.array.north_east_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.oriya), true));
        images = Arrays.asList(getResources().getStringArray(R.array.oriya_image));
        names = Arrays.asList(getResources().getStringArray(R.array.oriya_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.punjabi), true));
        images = Arrays.asList(getResources().getStringArray(R.array.punjabi_image));
        names = Arrays.asList(getResources().getStringArray(R.array.punjabi_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.spiritual), true));
        images = Arrays.asList(getResources().getStringArray(R.array.spiritual_image));
        names = Arrays.asList(getResources().getStringArray(R.array.spiritual_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }



        list.add(new entMySection(true, getString(R.string.tamil), true));
        images = Arrays.asList(getResources().getStringArray(R.array.tamil_image));
        names = Arrays.asList(getResources().getStringArray(R.array.tamil_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.telugu), true));
        images = Arrays.asList(getResources().getStringArray(R.array.telugu_image));
        names = Arrays.asList(getResources().getStringArray(R.array.telugu_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.kannada), true));
        images = Arrays.asList(getResources().getStringArray(R.array.kannada_image));
        names = Arrays.asList(getResources().getStringArray(R.array.kannada_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.all_south), true));
        images = Arrays.asList(getResources().getStringArray(R.array.all_south_image));
        names = Arrays.asList(getResources().getStringArray(R.array.all_south_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.dd), true));
        images = Arrays.asList(getResources().getStringArray(R.array.dd_image));
        names = Arrays.asList(getResources().getStringArray(R.array.dd_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.international), true));
        images = Arrays.asList(getResources().getStringArray(R.array.international_image));
        names = Arrays.asList(getResources().getStringArray(R.array.international_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }

        list.add(new entMySection(true, getString(R.string.life_style), true));
        images = Arrays.asList(getResources().getStringArray(R.array.life_style_image));
        names = Arrays.asList(getResources().getStringArray(R.array.life_style_name));
        for (int i = 0; i < images.size(); i++) {
            list.add(new entMySection(new entChannel(images.get(i), names.get(i))));
        }



        channelData = list;
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);

        finish();
    }


//    @Override
//    public void onItemClick(View view, int position) {
//        entMySection mySection = channelData.get(position);
//        if (mySection.isHeader) {
////            Toast.makeText(this, mySection.header, Toast.LENGTH_LONG).show();
//        } else {
//            Intent intent = new Intent();
//            intent.putExtra("name", mySection.t.getName());
//            intent.putExtra("image", mySection.t.getImg());
//            setResult(RESULT_OK, intent);
//            finish();
////            Toast.makeText(this, mySection.t.getName(), Toast.LENGTH_LONG).show();
//        }
//
//    }
}
