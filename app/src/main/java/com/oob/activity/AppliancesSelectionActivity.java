package com.oob.activity;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.oob.OOBApplication;
import com.oob.adapter.AppliancesSelectionAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.Constants;
import com.oob.tools.PixelUtil;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.Button;

import java.util.ArrayList;
import java.util.List;

public class AppliancesSelectionActivity extends BaseActivityChirag {

    private RecyclerView rv_room;

    private AppliancesSelectionAdapter rv_adapter;

    private dbHelperOperations db;

    List<entDevice> al_data = new ArrayList<>();
    List<entDevice> al_dataselected = new ArrayList<>();
    String ids;
    int pos;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);

        setToolbar();
//        setTitleName("Room");
        bindViewControls();

        setTitleName(getString(R.string.title_select_appliances));
        ids = getIntent().getExtras().getString("ids");
        pos = getIntent().getExtras().getInt("pos");
        System.out.println("Selected IDS " + ids);
        initItems();
        ivOnOff.setVisibility(View.VISIBLE);
        tvGrouping.setVisibility(View.GONE);
        tvSetting.setVisibility(View.GONE);
//        tvSetting.setText(getString(R.string.icon_onoff));
        ivOnOff.setText(getString(R.string.icon_back));
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "MULGRP" + Constants.getTwoDigit(pos) + "1");

    }

    private void bindViewControls() {
        rv_room = (RecyclerView) findViewById(R.id.rv_room);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setVisibility(View.VISIBLE);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<entDevice> data = rv_adapter.getData();
                String ids = "";
                for (int i = 0; i < data.size(); i++) {
                    if (data.get(i).getIsSelected() == 1) {
                        if (i == 0) {
                            ids = "" + data.get(i).getDEVICE_Id();
                        } else {
                            ids = ids + "," + data.get(i).getDEVICE_Id();
                        }
                    }
                }
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "GRPSUMBIT");
                Intent intent = new Intent();
                intent.putExtra("ids", ids);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    private void initItems() {

        al_data = new ArrayList<>();
        al_dataselected = new ArrayList<>();
        al_data = db.getListOfDevices(-2, true);
        String id[] = null;
        if (!Utils.isEmptyString(ids)) {
            id = ids.split(",");
        }
        System.out.println("id-- " + id);
        for (entDevice device : al_data) {
            device.setROOM_NAME(db.getRoomName(device.getROOM_Id()));

            if (id != null && id.length > 0) {
                for (int i = 0; i < id.length; i++) {
                    if (!Utils.isEmptyString(id[i]) && device.getDEVICE_Id() == Integer.parseInt(id[i])) {
                        device.setIsSelected(1);
                        al_dataselected.add(device);
                    }
                }

            }
        }
        rv_adapter = new AppliancesSelectionAdapter(AppliancesSelectionActivity.this, R.layout.raw_appliances, al_data);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_room.setLayoutManager(linearLayoutManager);
        rv_room.setAdapter(rv_adapter);
//        rv_adapter.setSelected(al_dataselected);
        rv_room.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));
        rv_room.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                rv_adapter.getData().get(position).setIsSelected(rv_adapter.getData().get(position).getIsSelected() == 1 ? 0 : 1);
                rv_adapter.notifyItemChanged(position);
//                        roomItemList.get(i).setALL_STT(roomItemList.get(i).getSTT());
                rv_adapter.getItem(position).setSTT(rv_adapter.getItem(position).getIsSelected());
                rv_adapter.getItem(position).setALL_STT(rv_adapter.getItem(position).getSTT());
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                        + Constants
                        .getRoomcode(rv_adapter.getItem(position).getROOM_Id())
                        + Constants
                        .getModuleAndDevicecode(rv_adapter.getItem(position).getMODULE())
                        + rv_adapter.getItem(position)
                        .getSTT()
                        + "D" + Constants
                        .getThreeDigit(rv_adapter.getItem(position).getDIM()) + "RX"
                        + rv_adapter.getItem(position).getRGBSTT()
                        + rv_adapter.getItem(position).getMIC()
                );
                db.updateDevice(rv_adapter.getItem(position));
            }
        });

    }


    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        onBackPresh();
    }

    private void onBackPresh() {
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "MULGRP" + Constants.getTwoDigit(pos) + "0");
        finish();
    }

    @Override
    public void onBackPressed() {
        onBackPresh();
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//
//            }
//        }
    }
}
