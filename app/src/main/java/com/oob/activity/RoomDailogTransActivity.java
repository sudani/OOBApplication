package com.oob.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.oob.OOBApplication;
import com.oob.adapter.RoomDialogRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.entity.entRooms;
import com.oob.tools.Constants;
import com.oob.tools.PixelUtil;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.List;

public class RoomDailogTransActivity extends Activity {
    private RecyclerView rv_room;
    private RoomDialogRecyclerAdapter rv_adapter;
    private TextView tv_servermsg;
    private BroadcastReceiver broadcast;
    private List<entRooms> rooms = new ArrayList<>();
    private ProgressDialog progressDialog;
    private dbHelperOperations db;
    private int selectedRoom = 0;
    private int selectedRoomId = -1;
    List<entDevice> al_data = new ArrayList<>();
    String roomName[];
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_room_dialog_trans);
        db = new dbHelperOperations(this);
        rooms.addAll(db.getListOfRooms(false));
        roomName = new String[rooms.size()];
        for (int i = 0; i < rooms.size(); i++) {
            roomName[i] = rooms.get(i).getROOM_NAME();
            System.out.println("IS SELECTED " + rooms.get(i).getROOM_IS_SELECTED());
            if (rooms.get(i).getROOM_IS_SELECTED() == 1) {
                selectedRoom = i;
            }
        }
        if (roomName.length > 0) {
            selectedRoomId = rooms.get(selectedRoom).getROOM_Id();
        }
        showDialogData();
        // set values for custom dialog components - text, image and button

//        db = new dbHelperOperations(this);
//        rooms.addAll(db.getListOfRooms(false));
//        entRooms roomName[] = new entRooms[rooms.size()];
//        for (int i = 0; i < rooms.size(); i++) {
//            roomName[i] = rooms.get(i);
//            System.out.println("IS SELECTED " + rooms.get(i).getROOM_IS_SELECTED());
//            if (rooms.get(i).getROOM_IS_SELECTED() == 1) {
//                selectedRoom = i;
//            }
//        }
//        if (roomName.length > 0) {
//            selectedRoomId = rooms.get(selectedRoom).getROOM_Id();
//        }

//        bindViewControls();
//        initItems();
//        tvMic.setVisibility(View.VISIBLE);
//        tv_servermsg.setText(String.format("MY IP:%s\n", WifiUtils.getIPAddress(true)));
//        progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Waiting for Acknowledgment..");

    }

    private void showDialogData() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.activity_room_dialog_trans);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Window window = dialog.getWindow();
//        window.setBackgroundDrawable(
//                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvRoom);
        ImageView ivLogo = (ImageView) dialog.findViewById(R.id.ivLogo);
        tvTitle.setText(db.getRoomName(selectedRoomId));
        tvTitle.setVisibility(View.VISIBLE);
        rv_room = (RecyclerView) dialog.findViewById(R.id.rv_room);
        ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RoomDailogTransActivity.this, LoginActivity.class);
                intent.putExtra(Constants.EXTRA_FROM_NOTIFICATION, true);
                startActivity(intent);
                finish();
            }
        });
//        final AppCompatSpinner compatSpinner = (AppCompatSpinner) dialog.findViewById(R.id.spinRooms);
//        compatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                System.out.println(" poS " + position);
//                selectedRoom = position;
//                selectedRoomId = rooms.get(selectedRoom).getROOM_Id();
////                ArrayAdapter adapter = new MyCustomAdapter(RoomDailogTransActivity.this, R.layout.spinner_item, roomName);
//                ArrayAdapter adapter = new ArrayAdapter(RoomDailogTransActivity.this,R.layout.simple_spiner_item, roomName);
//                compatSpinner.setAdapter(adapter);
//                compatSpinner.setSelection(selectedRoom);
//                setData();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
////        ArrayAdapter adapter = new MyCustomAdapter(RoomDailogTransActivity.this, R.layout.spinner_item, roomName);
//        ArrayAdapter adapter = new ArrayAdapter(RoomDailogTransActivity.this, R.layout.simple_spiner_item, roomName);
//        compatSpinner.setAdapter(adapter);
//        compatSpinner.setSelection(selectedRoom);
        setData();
        dialog.show();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
    }

    public void showSpinners(AppCompatSpinner spinRooms, entRooms[] rooms, int selected) {
        System.out.println("SELECTED " + selected);

    }

    private void setData() {
        al_data = new ArrayList<>();
        al_data = db.getListOfDevicesForNotification(selectedRoomId);
        rv_adapter = new RoomDialogRecyclerAdapter(RoomDailogTransActivity.this, al_data);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 5);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_room.setLayoutManager(gridLayoutManager);
        rv_room.setAdapter(rv_adapter);
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

    private void bindViewControls() {
        rv_room = (RecyclerView) findViewById(R.id.rv_room);
        rv_room.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));
        tv_servermsg = (TextView) findViewById(R.id.tv_servermsg);
    }

    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    public class MyCustomAdapter extends ArrayAdapter<entRooms> {
        entRooms[] DayOfWeek;
        Context context;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               entRooms[] objects) {

            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
            DayOfWeek = objects;
            this.context = context;
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.spinner_item_notification, parent, false);
            TextView label = (TextView) row.findViewById(R.id.tvName);
            label.setText(DayOfWeek[position].getROOM_NAME());

            return row;
        }
    }
}
