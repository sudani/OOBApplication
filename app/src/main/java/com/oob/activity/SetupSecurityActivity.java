package com.oob.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.EditText;
import com.oob.view.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class SetupSecurityActivity extends BaseActivityChirag {

    //    private MainFragmentOrganizer mainFragmentOrganizer;
    dbHelperOperations db;

    private TextView tvPasswordChange, tvSecurityCode;


    private File mFileTemp;

    public static final String TEMP_PASS_FILE_NAME = "pass.txt";

    private BroadcastReceiver broadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_security);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_setup_security));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();
        broadcastListener();

    }

    private void initItems() {
        tvPasswordChange = (TextView) findViewById(R.id.tvChangePassword);
        tvPasswordChange.setOnClickListener(this);
        tvSecurityCode = (TextView) findViewById(R.id.tvSecurityCode);
        tvSecurityCode.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.tvChangePassword:
                openPasswordDialog();
                break;

            case R.id.tvSecurityCode:
                openSecurityCodeDialog();
                break;

        }
        super.onClick(v);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE)
                            .equalsIgnoreCase("update")) {
                        dataChangeAlert();
                    }

                }
            }
        };
    }

    public void dataChangeAlert() {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                SetupSecurityActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle(getString(R.string.title_data_change));

        // Setting Dialog Message
//        alertDialog.setMessage(getString(R.string.msg_data_change));

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog
                .setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

    private void openPasswordDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_change_password);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtOldPass = (EditText) dialog.findViewById(R.id.edtOldPass);
        final EditText edtNewPass = (EditText) dialog.findViewById(R.id.edtNewPass);
        final EditText edtConfirmPass = (EditText) dialog.findViewById(R.id.edtConfirmPass);

        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate(edtOldPass.getText().toString(), edtNewPass.getText().toString(),
                        edtConfirmPass.getText().toString())) {
                    Utils.showToast(SetupSecurityActivity.this, getString(R.string.password_changed));
                    String state = Environment.getExternalStorageState();
                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        File f = new File(Environment.getExternalStorageDirectory() + "/oob");
                        if (!f.exists()) {
                            f.mkdir();
                        }
                        mFileTemp = new File(Environment.getExternalStorageDirectory() + "/oob",
                                TEMP_PASS_FILE_NAME);
                        Utils.writePassword(
                                createJson(Preferences.getAppPrefString(Preferences.KEY_USER),
                                        edtNewPass.getText().toString()), mFileTemp);

                    } else {
                        mFileTemp = new File(getFilesDir(),
                                System.currentTimeMillis() + TEMP_PASS_FILE_NAME);
                    }
                    dialog.dismiss();
                }

            }

        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    private String createJson(String user, String pass) {
        JSONObject job = new JSONObject();
        try {
            job.put("USER", user);
            job.put("PASSWORD", pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return job.toString();

    }

    private boolean validate(String s, String s1, String s2) {
        boolean isValidate = false;
        if (s.equalsIgnoreCase("")) {
            Utils.showToast(SetupSecurityActivity.this, getString(R.string.error_enter_currect_password));
            return isValidate;
        }
        if (s1.equalsIgnoreCase("")) {
            Utils.showToast(SetupSecurityActivity.this, getString(R.string.error_new_password));
            return isValidate;
        }
        if (s2.equalsIgnoreCase("")) {
            Utils.showToast(SetupSecurityActivity.this, getString(R.string.error_confirm_password));
            return isValidate;
        }
        if (!s.equalsIgnoreCase(Preferences.getAppPrefString(Preferences.KEY_PASS))) {
            Utils.showToast(SetupSecurityActivity.this, getString(R.string.error_valid_current_password));
            return isValidate;
        }
        if (!s1.equalsIgnoreCase(s2)) {
            Utils.showToast(SetupSecurityActivity.this, getString(R.string.error_new_pass_confirm_pass_not_match));
            return isValidate;
        }
        Preferences.writeSharedPreferences(Preferences.KEY_PASS, s1);
        return true;
    }

    private void openSecurityCodeDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_apliences_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        edtName.setHint(R.string.security_code);
        if (!Utils.isEmptyString(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE))) {
            edtName.setText(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE));
            edtName.setInputType((InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD));
            edtName.setSelection(edtName.length());
        }
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Preferences.writeSharedPreferences(Preferences.KEY_SECURITY_CODE,
                        edtName.getText().toString());
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

}
