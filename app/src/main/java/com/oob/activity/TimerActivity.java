package com.oob.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entRooms;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.EditText;
import com.oob.view.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.oob.app.R.id.spinOnOff;

public class TimerActivity extends BaseActivityChirag {

    //    private MainFragmentOrganizer mainFragmentOrganizer;
    dbHelperOperations db;

    private TextView tvSetTimer, tvReadTimer, tvSetDate, tvReadDate, tvIp;
//    private ImageView ivDevMode;
//    private CardView cardViewIp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_timer));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();

    }

    private void initItems() {
        tvSetTimer = (TextView) findViewById(R.id.tvSetTimer);
        tvSetTimer.setOnClickListener(this);
        tvReadTimer = (TextView) findViewById(R.id.tvReadTimer);
        tvReadTimer.setOnClickListener(this);
        tvSetDate = (TextView) findViewById(R.id.tvSetDate);
        tvSetDate.setOnClickListener(this);
        tvReadDate = (TextView) findViewById(R.id.tvReadDate);
        tvReadDate.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.tvSetTimer:

                selectRoomModuleDialog("SETRTC");
//                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "SETRTC" + formattedDate);
                break;
            case R.id.tvReadTimer:
                selectRoomModuleDialog("TIMERSYNC");

                break;
            case R.id.tvSetDate:

                dateSetDialog("SETDATE");
//                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "SETRTC" + formattedDate);
                break;
            case R.id.tvReadDate:
                dateSetDialog("SYNCDATE");

                break;
//            case ivDevMode:
//                openSecurityCodeDialog(false);
//                break;
//            case R.id.tvIp:
//                openSecurityCodeDialog(true);
//                break;


        }
        super.onClick(v);
    }






    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .execute(json, WifiUtils.server_ip, WifiUtils.server_port);
//
//            }
//        }
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

    String roomCode, moduleCode;

    private void selectRoomModuleDialog(
            final String timersync) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_tv_picker);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtModel);
        edtName.setVisibility(View.GONE);
        final AppCompatSpinner spinType = (AppCompatSpinner) dialog.findViewById(R.id.spinTvName);
        final AppCompatSpinner spinRooms = (AppCompatSpinner) dialog
                .findViewById(R.id.spintvProtocol);
        final AppCompatSpinner spinModules = (AppCompatSpinner) dialog
                .findViewById(spinOnOff);

        final List<entRooms> list = db.getListOfRooms(true);
        final String[] arrayRooms = new String[(list.size())];
        for (int i = 0; i < list.size(); i++) {
            arrayRooms[i] = list.get(i).getROOM_NAME();
        }
        final ArrayAdapter adapterType = new ArrayAdapter<String>(this,
                R.layout.simple_spiner_item_white, new String[]{"MODULE", "BABY HUB"});
        adapterType.setDropDownViewResource(R.layout.simple_spiner_item_white);
        spinType.setAdapter(adapterType);

        final ArrayAdapter adapterRooms = new ArrayAdapter<String>(this,
                R.layout.simple_spiner_item_white, arrayRooms);
        adapterRooms.setDropDownViewResource(R.layout.simple_spiner_item_white);
        spinRooms.setAdapter(adapterRooms);
        spinType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spinRooms.setVisibility(View.VISIBLE);
                    spinModules.setVisibility(View.VISIBLE);
                } else {
                    spinRooms.setVisibility(View.INVISIBLE);
                    spinModules.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinRooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] arrayModules = new String[list.get(position).getMODULE_COUNT()];
                for (int i = 0; i < arrayModules.length; i++) {
                    arrayModules[i] = "MODULE " + (i + 1);
                }
                ArrayAdapter adapterProtocol = new ArrayAdapter<String>(TimerActivity.this
                        ,
                        R.layout.simple_spiner_item_white, arrayModules);
                adapterProtocol.setDropDownViewResource(R.layout.simple_spiner_item_white);

                spinModules.setAdapter(adapterProtocol);
                roomCode = "R" + Constants.getTwoDigit(list.get(position).getROOM_Id());
                System.out.println("moduleCode " + moduleCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinModules.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                moduleCode = "M" + Constants.getTwoDigit((position + 1));
                System.out.println("moduleCode " + moduleCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinRooms.getVisibility() == View.VISIBLE) {
                    if (roomCode != null && moduleCode != null) {
                        dialog.dismiss();
                        if (timersync.equalsIgnoreCase("SETRTC")) {
                            Calendar c = Calendar.getInstance();
                            System.out.println("Current time => " + c.getTime());
                            SimpleDateFormat df = new SimpleDateFormat("HHmmss");
                            String formattedDate = df.format(c.getTime());
                            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + roomCode + moduleCode + timersync + formattedDate);
                        } else {
                            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + roomCode + moduleCode + timersync);
                        }
                    } else {
                        Utils.showToast(TimerActivity.this, getString(R.string.error_select_room_module));
                    }
                } else {
                    dialog.dismiss();
                    if (timersync.equalsIgnoreCase("SETRTC")) {
                        Calendar c = Calendar.getInstance();
                        System.out.println("Current time => " + c.getTime());
                        SimpleDateFormat df = new SimpleDateFormat("HHmmss");
                        String formattedDate = df.format(c.getTime());
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "BABYHUB" + timersync + formattedDate);
                    } else {
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "BABYHUB" + timersync);
                    }
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    private void dateSetDialog(
            final String timersync) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_tv_picker);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtModel);
        edtName.setVisibility(View.GONE);
        final AppCompatSpinner spinType = (AppCompatSpinner) dialog.findViewById(R.id.spinTvName);
        final AppCompatSpinner spinRooms = (AppCompatSpinner) dialog
                .findViewById(R.id.spintvProtocol);
        final AppCompatSpinner spinModules = (AppCompatSpinner) dialog
                .findViewById(spinOnOff);

        final List<entRooms> list = db.getListOfRooms(true);
        final String[] arrayRooms = new String[(list.size())];
        for (int i = 0; i < list.size(); i++) {
            arrayRooms[i] = list.get(i).getROOM_NAME();
        }
        final ArrayAdapter adapterType = new ArrayAdapter<String>(this,
                R.layout.simple_spiner_item_white, new String[]{"MODULE", "BABY HUB"});
        adapterType.setDropDownViewResource(R.layout.simple_spiner_item_white);
        spinType.setAdapter(adapterType);

        final ArrayAdapter adapterRooms = new ArrayAdapter<String>(this,
                R.layout.simple_spiner_item_white, arrayRooms);
        adapterRooms.setDropDownViewResource(R.layout.simple_spiner_item_white);
        spinRooms.setAdapter(adapterRooms);
        spinType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spinRooms.setVisibility(View.VISIBLE);
                    spinModules.setVisibility(View.VISIBLE);
                } else {
                    spinRooms.setVisibility(View.INVISIBLE);
                    spinModules.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinRooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] arrayModules = new String[list.get(position).getMODULE_COUNT()];
                for (int i = 0; i < arrayModules.length; i++) {
                    arrayModules[i] = "MODULE " + (i + 1);
                }
                ArrayAdapter adapterProtocol = new ArrayAdapter<String>(TimerActivity.this
                        ,
                        R.layout.simple_spiner_item_white, arrayModules);
                adapterProtocol.setDropDownViewResource(R.layout.simple_spiner_item_white);

                spinModules.setAdapter(adapterProtocol);
                roomCode = "R" + Constants.getTwoDigit(list.get(position).getROOM_Id());
                System.out.println("moduleCode " + moduleCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinModules.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                moduleCode = "M" + Constants.getTwoDigit((position + 1));
                System.out.println("moduleCode " + moduleCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinRooms.getVisibility() == View.VISIBLE) {
                    if (roomCode != null && moduleCode != null) {
                        dialog.dismiss();
                        if (timersync.equalsIgnoreCase("SETDATE")) {
                            Calendar c = Calendar.getInstance(Locale.getDefault());
                            System.out.println("Current time => " + c.getTime());
                            SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
                            String formattedDate = df.format(c.getTime());
                            c.setFirstDayOfWeek(Calendar.MONDAY);
                            formattedDate = formattedDate + c.get(Calendar.DAY_OF_WEEK);
                            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + roomCode + moduleCode + timersync + formattedDate);
                        } else {
                            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + roomCode + moduleCode + timersync);
                        }
                    } else {
                        Utils.showToast(TimerActivity.this, getString(R.string.error_select_room_module));
                    }
                } else {
                    dialog.dismiss();
                    if (timersync.equalsIgnoreCase("SETDATE")) {
                        Calendar c = Calendar.getInstance(Locale.getDefault());
                        System.out.println("Current time => " + c.getTime());
                        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
                        String formattedDate = df.format(c.getTime());
                        c.setFirstDayOfWeek(Calendar.MONDAY);
                        formattedDate = formattedDate + c.get(Calendar.DAY_OF_WEEK);
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "BABYHUB" + timersync + formattedDate);
                    } else {
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "BABYHUB" + timersync);
                    }
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

}
