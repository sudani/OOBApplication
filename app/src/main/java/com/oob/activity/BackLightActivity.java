package com.oob.activity;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.oob.adapter.BackLightRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.PixelUtil;

import java.util.ArrayList;
import java.util.List;

public class BackLightActivity extends BaseActivityChirag {

    private RecyclerView rv_room;

    private BackLightRecyclerAdapter rv_adapter;

    private dbHelperOperations db;

    List<entDevice> al_data = new ArrayList<>();

    List<entDevice> al_data_module = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);

        setToolbar();
//        setTitleName("Room");
        bindViewControls();
        initItems();
        setTitleName(getString(R.string.title_back_light));
        tvSetting.setVisibility(View.GONE);

        ivOnOff.setVisibility(View.VISIBLE);
        ivOnOff.setText(getString(R.string.icon_back));


    }

    private void bindViewControls() {
        rv_room = (RecyclerView) findViewById(R.id.rv_room);
    }

    private void initItems() {

        al_data = new ArrayList<>();
        al_data_module = new ArrayList<>();
        al_data = db.getListOfDevices(-2, true);
        for (int i = 0; i < al_data.size(); i++) {

            if (i == 0) {
                al_data_module.add(al_data.get(i));
            } else {
                int val = al_data_module.size() - 1;
                if (al_data_module.get(val).getROOM_Id() == al_data.get(i).getROOM_Id()) {
                    if (al_data_module.get(val).getMODULE_NUMBER() != al_data.get(i)
                            .getMODULE_NUMBER()) {
                        al_data_module.add(al_data.get(i));
                    }
                }else {
                    al_data_module.add(al_data.get(i));
                }
            }
        }
        rv_adapter = new BackLightRecyclerAdapter(BackLightActivity.this, al_data_module);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_room.setLayoutManager(linearLayoutManager);
        rv_room.setAdapter(rv_adapter);
        rv_room.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));


    }

    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }
}
