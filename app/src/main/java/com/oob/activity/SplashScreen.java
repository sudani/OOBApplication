package com.oob.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.oob.app.R;
import com.oob.tools.Preferences;

import java.util.ArrayList;
import java.util.Arrays;

public class SplashScreen extends BaseActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 500;

    private ImageView ivLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        hideTitle();
        //  ActiveAndroid.execSQL("ALTER TABLE RoomDevice ADD COLUMN Days TEXT");

        ArrayList<String> colors = new ArrayList<>(
                Arrays.asList(getResources().getStringArray(R.array.light_colors)));
        ivLogo = (ImageView) findViewById(R.id.ivLogo);
        for (int i = 0; i < 7; i++) {
            ivLogo.setColorFilter(Color.parseColor(colors.get(i)),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
            new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity


                }
            }, SPLASH_TIME_OUT);

        }

        String uname = Preferences.getAppPrefString(Preferences.KEY_USER);
        String pass = Preferences.getAppPrefString(Preferences.KEY_PASS);
        if (!uname.equalsIgnoreCase("") && !pass.equalsIgnoreCase("")) {
            Intent intent = new Intent(SplashScreen.this, DashBoardActivity.class);
            startActivity(intent);
        } else {
            updateUnameAndPass();
            Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(intent);
        }

        // close this activity
        finish();
    }

    private void updateUnameAndPass() {
        String uname = Preferences.getAppPrefString(Preferences.KEY_USER);
        String pass = Preferences.getAppPrefString(Preferences.KEY_PASS);
        if (uname.isEmpty() && pass.isEmpty()) {
            Preferences.writeSharedPreferences(Preferences.KEY_USER, "oob");
            Preferences.writeSharedPreferences(Preferences.KEY_PASS, "oob");
        }
    }
}
