package com.oob.activity;

import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.entity.entTV;
import com.oob.service.BroadcastServiceTV;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TVControlActivity extends BaseActivityChirag {


    private dbHelperOperations db;

    private TextView tvMainOnOff, tvMute, tvStbOnOff, tvUp, tvDown, tvLeft, tvRight, tvOk, tvBack,
            tvExit, tvVolPlus, tvVolMinus, tvChPlus, tvChMinus;

    private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv0, tvStar, tvHas;

    private List<entTV> tvs = new ArrayList<>();

    private int selectedTv = 0;

    private int selectedTvId = -1;
    private entDevice tvDevice;
    private RelativeLayout layCenter;

    private entTV mEntTV;

    private String tvName[];

    private String prefix1 = "";

    private String prefixStb = "";

    private String prefixTv = "";
    PowerManager.WakeLock wl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_control);
        setToolbar();
        db = new dbHelperOperations(this);
        tvs.addAll(db.getListTV());
        tvName = new String[tvs.size()];
        for (int i = 0; i < tvs.size(); i++) {
            tvName[i] = tvs.get(i).getTV_NAME();
            System.out.println("IS SELECTED " + tvs.get(i).getTV_IS_SELECTED());
            if (tvs.get(i).getTV_IS_SELECTED() == 1) {
                selectedTv = i;
            }
        }
        setTvValue();
        showSpinnersTv(tvName, selectedTv);
        ivOnOff.setText("00:00:00");
        ivOnOff.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        tvGrouping.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        initItems();
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My Tag");
        wl.acquire();

    }

    private void setTvValue() {

        if (tvName.length > 0) {
            mEntTV = tvs.get(selectedTv);
            selectedTvId = tvs.get(selectedTv).getTV_Id();
            tvGrouping.setText(tvs.get(selectedTv).getSET_NAME());
            prefix1 = Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "IR";
            prefixStb = "STBR" + Constants.getTwoDigit(mEntTV.getROOM_Id());// + mEntTV.getSET_NAME();
//                    + mEntTV
//                    .getSET_PROTOCOL();
            prefixTv = "TVR" + Constants.getTwoDigit(mEntTV.getROOM_Id());// + mEntTV.getTV_NAME();
//                    + mEntTV
//                    .getTV_PROTOCOL();
            tvDevice = db.getTvDevice(mEntTV.getROOM_Id());
        } else {
            tvGrouping.setText("");

        }
    }

    private void initItems() {
        tvMainOnOff = (TextView) findViewById(R.id.tvMainOnOff);
        tvMute = (TextView) findViewById(R.id.tvMute);
        tvStbOnOff = (TextView) findViewById(R.id.tvStbOnOff);
        tvUp = (TextView) findViewById(R.id.tvUp);
        tvDown = (TextView) findViewById(R.id.tvDown);
        tvLeft = (TextView) findViewById(R.id.tvLeft);
        tvRight = (TextView) findViewById(R.id.tvRight);
        tvOk = (TextView) findViewById(R.id.tvOk);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvExit = (TextView) findViewById(R.id.tvExit);
        tvVolPlus = (TextView) findViewById(R.id.tvVolPlus);
        tvVolMinus = (TextView) findViewById(R.id.tvVolMinus);
        tvChPlus = (TextView) findViewById(R.id.tvChPlus);
        tvChMinus = (TextView) findViewById(R.id.tvChMinus);
        tv0 = (TextView) findViewById(R.id.tv0);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        tv8 = (TextView) findViewById(R.id.tv8);
        tv9 = (TextView) findViewById(R.id.tv9);
        tvStar = (TextView) findViewById(R.id.tvStar);
        tvHas = (TextView) findViewById(R.id.tvHas);
        layCenter = (RelativeLayout) findViewById(R.id.layCenter);
        tvMainOnOff.setOnClickListener(this);
        tvMute.setOnClickListener(this);
        tvStbOnOff.setOnClickListener(this);
        tvUp.setOnClickListener(this);
        tvDown.setOnClickListener(this);
        tvLeft.setOnClickListener(this);
        tvRight.setOnClickListener(this);
        tvOk.setOnClickListener(this);
        tvBack.setOnClickListener(this);
        tvExit.setOnClickListener(this);
        tvVolPlus.setOnClickListener(this);
        tvVolMinus.setOnClickListener(this);
        tvChPlus.setOnClickListener(this);
        tvChMinus.setOnClickListener(this);
        tv0.setOnClickListener(this);
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
        tv3.setOnClickListener(this);
        tv4.setOnClickListener(this);
        tv5.setOnClickListener(this);
        tv6.setOnClickListener(this);
        tv7.setOnClickListener(this);
        tv8.setOnClickListener(this);
        tv9.setOnClickListener(this);
        tvStar.setOnClickListener(this);
        tvHas.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.tvMainOnOff:
                sendData(prefix1 + prefixTv + "TV");
                break;
            case R.id.tvMute:
                if (tvMute.getText().toString().equalsIgnoreCase(getString(R.string.icon_vol))) {
                    tvMute.setText(getString(R.string.icon_vol_mute));

                } else {
                    tvMute.setText(getString(R.string.icon_vol));
                }
                sendData(prefix1 + prefixStb + "MUTE");
                break;
            case R.id.tvStbOnOff:
                sendData(prefix1 + prefixStb + "STB");
                break;
            case R.id.tvUp:
                sendData(prefix1 + prefixStb + "UP");
                break;
            case R.id.tvDown:
                sendData(prefix1 + prefixStb + "DOWN");
                break;
            case R.id.tvLeft:
                sendData(prefix1 + prefixStb + "LEFT");
                break;
            case R.id.tvRight:
                sendData(prefix1 + prefixStb + "RIGHT");
                break;
            case R.id.tvOk:
                sendData(prefix1 + prefixStb + "OK");
                break;
            case R.id.tvBack:
                sendData(prefix1 + prefixStb + "BACK");
                break;
            case R.id.tvExit:
                sendData(prefix1 + prefixStb + "EXIT");
                break;
            case R.id.tvVolPlus:
                sendData(prefix1 + prefixStb + "VOLPLUS");
                break;
            case R.id.tvVolMinus:
                sendData(prefix1 + prefixStb + "VOLMINUS");
                break;
            case R.id.tvChPlus:
                sendData(prefix1 + prefixStb + "CHPLUS");
                break;
            case R.id.tvChMinus:
                sendData(prefix1 + prefixStb + "CHMINUS");
                break;
            case R.id.tv0:
                sendData(prefix1 + prefixStb + "0");
                break;
            case R.id.tv1:
                sendData(prefix1 + prefixStb + "1");
                break;
            case R.id.tv2:
                sendData(prefix1 + prefixStb + "2");
                break;
            case R.id.tv3:
                sendData(prefix1 + prefixStb + "3");
                break;
            case R.id.tv4:
                sendData(prefix1 + prefixStb + "4");
                break;
            case R.id.tv5:
                sendData(prefix1 + prefixStb + "5");
                break;
            case R.id.tv6:
                sendData(prefix1 + prefixStb + "6");
                break;
            case R.id.tv7:
                sendData(prefix1 + prefixStb + "7");
                break;
            case R.id.tv8:
                sendData(prefix1 + prefixStb + "8");
                break;
            case R.id.tv9:
                sendData(prefix1 + prefixStb + "9");
                break;
            case R.id.tvStar:
                sendData(prefix1 + prefixStb + "*");
                break;
            case R.id.tvHas:
                sendData(prefix1 + prefixStb + "#");
                break;
            case R.id.tvFav1:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 0);
                intent.putExtra("data", mEntTV);
                startActivity(intent);
                break;
            case R.id.tvFav2:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 1);
                intent.putExtra("data", mEntTV);
                startActivity(intent);
                break;
            case R.id.tvSocial:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 2);
                intent.putExtra("data", mEntTV);
                startActivity(intent);
                break;
            case R.id.tvSports:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 3);
                intent.putExtra("data", mEntTV);
                startActivity(intent);
                break;
            case R.id.tvNews:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 4);
                intent.putExtra("data", mEntTV);
                startActivity(intent);
                break;
            case R.id.tvMusic:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 5);
                intent.putExtra("data", mEntTV);
                startActivity(intent);
                break;
            case R.id.tvRegion:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 6);
                intent.putExtra("data", mEntTV);
                startActivity(intent);
                break;

            case R.id.tvMovie:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 7);
                intent.putExtra("data", mEntTV);
                startActivity(intent);
                break;
            case R.id.tvCartoon:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 8);
                intent.putExtra("data", mEntTV);
                startActivity(intent);
                break;
            case R.id.tvDiscovery:
                intent = new Intent(this, TVControlGroupChannelActivity.class);
                intent.putExtra("pos", 9);
                intent.putExtra("data", mEntTV);

                startActivity(intent);
                break;

        }
        super.onClick(v);
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        final Calendar c = Calendar.getInstance();
        final int mHour = c.get(Calendar.HOUR_OF_DAY);
        final int mMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        System.out.println(hourOfDay + ":" + minute);
                        Preferences.writeSharedPreferences(Preferences.KEY_FROM_TIME,
                                Utils.convertToSeconds(mHour, mMinute));
                        Preferences.writeSharedPreferences(Preferences.KEY_TO_TIME,
                                Utils.convertToSeconds(hourOfDay, minute));
                        Intent intent = new Intent(TVControlActivity.this, BroadcastServiceTV.class);
                        intent.putExtra("tvId", selectedTvId);
                        intent.putExtra("device", tvDevice);
                        intent.putExtra("pre1", prefix1);
                        intent.putExtra("preTv", prefixTv);
//                        startService(intent);/
                        start(intent);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();


    }

    @Override
    public void onTimeLongClick() {
        super.onTimeLongClick();
        dataChangeAlert();
    }

    public void dataChangeAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_stop_timer));
        builder.setMessage(getString(R.string.stop_timer));
        builder.setIcon(R.drawable.app_logo);
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                stopService(new Intent(TVControlActivity.this, BroadcastServiceTV.class));
                ivOnOff.setText("00:00:00");
            }
        });

        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setCancelable(true);
        builder.show();
    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                if (selectedTvId == intent.getExtras().getInt("tvId")) {
                    ivOnOff.setText(intent.getExtras().getString("time"));

                } else {
                    ivOnOff.setText("00:00:00");
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(br, new IntentFilter(BroadcastServiceTV.COUNTDOWN_BR));
        Log.i("TAG", "Registered broacast receiver");
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(br);
        Log.i("TAG", "Unregistered broacast receiver");
    }

    @Override
    public void onStop() {
        try {
            unregisterReceiver(br);
        } catch (Exception e) {
            // Receiver was probably already stopped in onPause()
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
//        stopService(new Intent(this, BroadcastServiceAC.class));
//        Log.i("TAG", "Stopped service");
        wl.release();

        super.onDestroy();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedTv = position;
        selectedTvId = tvs.get(selectedTv).getTV_Id();
        setTvValue();
        super.onItemSelected(parent, view, position, id);
    }


    private void updateGUI(Intent intent) {
        if (intent.getExtras() != null) {
//            long millisUntilFinished = intent.getLongExtra("countdown", 0);
//            Log.i("TAG", "Countdown seconds remaining: " + millisUntilFinished / 1000);
            ivOnOff.setText(intent.getExtras().getString("time"));
        }

    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        json = json.replace(" ", "");
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//            }
//        }
    }

    protected ServiceConnection mServerConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.d("TV", "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("TV", "onServiceDisconnected");
        }
    };

    public void start(Intent intent) {
        // mContext is defined upper in code, I think it is not necessary to explain what is it
        bindService(intent, mServerConn, Context.BIND_AUTO_CREATE);
        startService(intent);
    }

    public void stop() {
        stopService(new Intent(this, BroadcastServiceTV.class));
        unbindService(mServerConn);
    }
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_UP) {
                    if (event.getEventTime() - event.getDownTime() > ViewConfiguration.getLongPressTimeout()) {
                        //TODO long click action
                        System.out.println("UP long click");
                        sendData(prefix1 + prefixStb + "CHPLUS");
                    } else {
                        //TODO click action
                        System.out.println("UP  click");
                        sendData(prefix1 + prefixStb + "VOLPLUS");
                    }
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_UP) {
                    if (event.getEventTime() - event.getDownTime() > ViewConfiguration.getLongPressTimeout()) {
                        //TODO long click action
                        System.out.println("DOWN long click");
                        sendData(prefix1 + prefixStb + "CHMINUS");
                    } else {
                        //TODO click action
                        System.out.println("DOWN click");
                        sendData(prefix1 + prefixStb + "VOLMINUS");
                    }
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);


        }
    }
}
