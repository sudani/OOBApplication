package com.oob.activity;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.oob.adapter.PrioritySetupRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.interfaces.OnCustomerListChangedListener;
import com.oob.interfaces.OnStartDragListener;
import com.oob.tools.PixelUtil;
import com.oob.tools.SimpleItemTouchHelperCallback;

import java.util.ArrayList;
import java.util.List;

public class PrioritySetupActivity extends BaseActivityChirag implements OnCustomerListChangedListener,
        OnStartDragListener {

    private RecyclerView rv_room;
    private PrioritySetupRecyclerAdapter rv_adapter;
    private dbHelperOperations db;
    private ItemTouchHelper mItemTouchHelper;
    List<entDevice> al_data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);

        setToolbar();
        bindViewControls();
        initItems();
        setTitleName(getString(R.string.title_priority));
        ivOnOff.setVisibility(View.VISIBLE);
        ivOnOff.setText(getString(R.string.icon_back));


    }
    private void bindViewControls() {
        rv_room = (RecyclerView) findViewById(R.id.rv_room);
        rv_room.setHasFixedSize(true);
    }
    private void initItems() {

        al_data = new ArrayList<>();
        al_data = db.getListOfDevices(getIntent().getExtras().getInt("roomId"),true);
        rv_adapter = new PrioritySetupRecyclerAdapter(PrioritySetupActivity.this, al_data,this,this);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(rv_adapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(rv_room);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_room.setLayoutManager(linearLayoutManager);
        rv_room.setAdapter(rv_adapter);
        rv_room.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));


    }

    @Override
    public void onNoteListChanged(List<entDevice> customers) {
//        rv_adapter.notifyDataSetChanged();
//        List<entDevice> customerss = rv_adapter.getDataList();
        for (int i=0;i<customers.size();i++){
            System.out.println("NAME "+customers.get(i).getDEVICE_NAME()+"  POS "+i);
        }

    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);

    }

    @Override
    public void onStopDrag(RecyclerView.ViewHolder viewHolder) {
        System.out.println("STOP");
    }

    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        rv_adapter.notifyDataSetChanged();
        rv_adapter.notifyDataSetChanged();
        List<entDevice> customers = rv_adapter.getDataList();
        for (int i=0;i<customers.size();i++){
            int pos = i+1;
            customers.get(i).setPOSITION(pos);
        }
        db.updateDevicePosition(customers);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onBackPressed() {
        rv_adapter.notifyDataSetChanged();
        rv_adapter.notifyDataSetChanged();
        List<entDevice> customers = rv_adapter.getDataList();
        for (int i=0;i<customers.size();i++){
            int pos = i+1;
            customers.get(i).setPOSITION(pos);
        }
        db.updateDevicePosition(customers);
        setResult(RESULT_OK);
        finish();
        super.onBackPressed();
    }
}
