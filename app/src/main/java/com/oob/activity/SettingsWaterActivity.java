
package com.oob.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.Button;
import com.oob.view.EditText;

public class SettingsWaterActivity extends BaseActivityChirag {
    private EditText edtTotal, edtLow, edtHigh, edtCurrent;
    private RadioButton rBtnRect, rBtnCircle;
    private RadioButton rBtnBunglow, rBtnFlat;
    private Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_water);
        setToolbar();
        setTitleName(getString(R.string.title_settings));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();

    }

    private void initItems() {
        edtTotal = (EditText) findViewById(R.id.edtTotal);
        edtLow = (EditText) findViewById(R.id.edtLow);
        edtHigh = (EditText) findViewById(R.id.edtHigh);
        edtCurrent = (EditText) findViewById(R.id.edtCurrent);
        btnSave = (Button) findViewById(R.id.btnSave);
        rBtnRect = (RadioButton) findViewById(R.id.rBtnRect);
        rBtnCircle = (RadioButton) findViewById(R.id.rBtnCircle);
        rBtnBunglow = (RadioButton) findViewById(R.id.rBtnBungalow);
        rBtnFlat = (RadioButton) findViewById(R.id.rBtnFlat);
        btnSave.setOnClickListener(this);
        edtTotal.setText(Preferences.getAppPrefString(Preferences.KEY_TOTAL_CAPACITY));
        edtLow.setText(Preferences.getAppPrefString(Preferences.KEY_LOW_CAPACITY));
        edtHigh.setText(Preferences.getAppPrefString(Preferences.KEY_HIGH_CAPACITY));
        edtCurrent.setText(Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS));
        if (Preferences.getAppPrefBool(Preferences.KEY_IS_RECT_SHAPE, true)) {
            rBtnRect.setChecked(true);
        } else {
            rBtnCircle.setChecked(true);
        }
        if (Preferences.getAppPrefBool(Preferences.KEY_IS_BUNGALOW, true)) {
            rBtnBunglow.setChecked(true);
        } else {
            rBtnFlat.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        Utils.hideSoftKeyboard(this);
        switch (v.getId()) {
            case R.id.btnSave:
                Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_CAPACITY,
                        edtTotal.getText().toString());
                Preferences.writeSharedPreferences(Preferences.KEY_LOW_CAPACITY,
                        edtLow.getText().toString());
                Preferences.writeSharedPreferences(Preferences.KEY_HIGH_CAPACITY,
                        edtHigh.getText().toString());
                Preferences.writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS,
                        edtCurrent.getText().toString());
                if (rBtnRect.isChecked()) {
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_IS_RECT_SHAPE, true);
                } else if (rBtnCircle.isChecked()) {
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_IS_RECT_SHAPE, false);
                }
                if (rBtnBunglow.isChecked()) {
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_IS_BUNGALOW, true);
                } else if (rBtnFlat.isChecked()) {
                    Preferences.writeSharedPreferencesBool(Preferences.KEY_IS_BUNGALOW, false);
                }
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "WLC" + Constants.getFiveDigite(edtTotal.getText().toString()) + "M" + Constants.getFiveDigite(edtLow.getText().toString()) + "L" + Constants.getFiveDigite(edtHigh.getText().toString()) + "A" + Constants.getFiveDigite(edtCurrent.getText().toString()));
                finish();
                break;

        }
        super.onClick(v);
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json  " + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

}
