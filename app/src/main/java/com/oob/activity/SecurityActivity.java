package com.oob.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.oob.adapter.SecurityRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entSecurity;

import java.util.ArrayList;
import java.util.List;

public class SecurityActivity extends BaseActivityChirag {

    private RecyclerView rv_security;

    private SecurityRecyclerAdapter rv_adapter;

    List<entSecurity> sensors = new ArrayList<>();

    private dbHelperOperations db;

    private int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 22;


    private GoogleApiClient mClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);
        setToolbar();
        setTitleName(getString(R.string.title_security));
        ivOnOff.setText(getString(R.string.icon_back));
        ivMenu.setVisibility(View.VISIBLE);
        tvSetting.setVisibility(View.GONE);
        initItems();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mClient = new GoogleApiClient.Builder(this)
                .addApi(AppIndex.API).build();
        checkPermission();
    }

    private void initItems() {
        db = new dbHelperOperations(this);
        sensors = db.getListSensor();
        rv_security = (RecyclerView) findViewById(R.id.rv_room);
        rv_adapter = new SecurityRecyclerAdapter(SecurityActivity.this, sensors);
        GridLayoutManager lLayout = new GridLayoutManager(this, 2);
        rv_security.setHasFixedSize(true);
        rv_security.setLayoutManager(lLayout);
//        rv_security.setLayoutManager(new LinearLayoutManager(SecurityActivity.this));
        rv_security.setAdapter(rv_adapter);
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

    private void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_PHONE_STATE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);

                // MY_PERMISSIONS_REQUEST_READ_PHONE_STATE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
            String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 22: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("permission granted");
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onMenuClick(View v) {
        super.onMenuClick(v);

        PopupMenu popup = new PopupMenu(SecurityActivity.this, v);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.security_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.addSensor:
                        entSecurity entSecurity = new entSecurity();
                        entSecurity.setSENSOR_NAME(getString(R.string.sensor_name));
                        entSecurity.setSENSOR_LOCATION(getString(R.string.location));
                        entSecurity.setIS_ENAMBLE(1);
                        entSecurity.setSECURITY_ID(db.insertSecurity(entSecurity));
                        rv_adapter.addSensor(entSecurity);
                        break;


                }

                return true;
            }
        });

        popup.show();//showing popup menu

    }


}
