package com.oob.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Constants;
import com.oob.tools.MyAxisValueFormatter;
import com.oob.tools.MyYAxisValueFormatter;
import com.oob.tools.Preferences;
import com.oob.tools.XYMarkerView;
import com.oob.view.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.oob.activity.EnergyActivity.BAR_COLOOR;

public class WaterControlFlatActivity extends BaseActivityChirag
        implements TimePickerDialog.OnTimeSetListener, OnChartValueSelectedListener {
    dbHelperOperations db;
    entDevice device;
    private TextView tv_fromTimer1, tv_toTimer1, tv_fromTimer2, tv_toTimer2, tvTotalUsed;
    private ImageView ivDevice;
    private LinearLayout ll_days, layTimer1, layTimer2;
    private BroadcastReceiver broadcast;
    int total;
    private int picker = 0;
    protected BarChart mChart;
    private boolean isDummyData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flat_water);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_water_level));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.VISIBLE);
        initItems();
        broadcastListener();
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "FLATWATERLEVEL1");
    }

    private void initItems() {
        device = db.getDeviceOfPump();
        ll_days = (LinearLayout) findViewById(R.id.ll_days);
        layTimer1 = (LinearLayout) findViewById(R.id.layTimer1);
        layTimer2 = (LinearLayout) findViewById(R.id.layTimer2);
        tv_fromTimer1 = (TextView) findViewById(R.id.tvTimer1From);
        tv_toTimer1 = (TextView) findViewById(R.id.tvTimer1to);
        tv_fromTimer2 = (TextView) findViewById(R.id.tvTimer2From);
        tv_toTimer2 = (TextView) findViewById(R.id.tvTimer2to);
        ivDevice = (ImageView) findViewById(R.id.ivDevice);
        tvTotalUsed = (TextView) findViewById(R.id.tvTotalUsed);

        System.out.println("id " + device.getDEVICE_NAME());
        if (device.getDEVICE_NAME() != null) {

            tv_fromTimer1.setText(device.getTIME_ONE_A());
            tv_toTimer1.setText(device.getTIME_ONE_B());
            tv_fromTimer2.setText(device.getTIME_TWO_A());
            tv_toTimer2.setText(device.getTIME_TWO_B());
            ivDevice.setImageResource(
                    device.getSTT() == 1 ? R.drawable.on : R.drawable.off);
            ivDevice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS)) >= Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_HIGH_CAPACITY))) {
                        return;
                    }
                    device.setSTT(device.getSTT() == 0 ? 1 : 0);
                    device.setALL_STT(device.getSTT());
                    Log.d("TEST", "from check");
                    if (ivDevice.isShown()) {

                        ivDevice.setImageResource(
                                device.getSTT() == 1 ? R.drawable.on : R.drawable.off);

                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                + Constants
                                .getRoomcode(device.getROOM_Id())
                                + Constants
                                .getModuleAndDevicecode(device.getMODULE())
                                + device
                                .getSTT()
                                + "D" + Constants
                                .getThreeDigit(device.getDIM()) + "RX"
                                + device.getRGBSTT()
                                + device.getMIC()
                        );

                    } else {

                    }
                    // TODO: 4/30/2016 UPDATE DATABASE

                }
            });
            layTimer1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    picker = 1;
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog tpd = TimePickerDialog.newInstance(
                            WaterControlFlatActivity.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            false
                    );

                    tpd.show(getFragmentManager(), "Timepickerdialog");
                }
            });
            layTimer2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    picker = 2;

                    Calendar now = Calendar.getInstance();
                    TimePickerDialog tpd = TimePickerDialog.newInstance(
                            WaterControlFlatActivity.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            false
                    );

                    tpd.show(getFragmentManager(), "Timepickerdialog");
                }
            });
            setupDaysClick(ll_days);
            setDaysFromBean(ll_days);

        }
        initChart();
        setDataFromString();
    }

    MyAxisValueFormatter xAxisFormatter;

    private void initChart() {
        mChart = (BarChart) findViewById(R.id.chart1);
        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(10);

//        mChart.setVisibleXRange(10, 10);
        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);
        xAxisFormatter = new MyAxisValueFormatter();

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTfRegular);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(10);
        xAxis.setTextColor(Color.parseColor("#FFFFFF"));
        xAxis.setValueFormatter(xAxisFormatter);

        MyYAxisValueFormatter custom = new MyYAxisValueFormatter("Ltr");

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTypeface(mTfRegular);
        leftAxis.setLabelCount(8, false);
        leftAxis.setTextColor(Color.parseColor("#FFFFFF"));
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setEnabled(false);
        rightAxis.setDrawLabels(false);
//        rightAxis.setTypeface(mTfRegular);
//        rightAxis.setLabelCount(8, false);
//        rightAxis.setValueFormatter(custom);
//        rightAxis.setSpaceTop(15f);
//        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = mChart.getLegend();
        l.setEnabled(false);
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//        l.setForm(Legend.LegendForm.SQUARE);
//        l.setFormSize(9f);
//        l.setTextSize(11f);
//        l.setTextColor(Color.parseColor("#FFFFFF"));
//        l.setXEntrySpace(4f);
        // l.setExtra(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });
        // l.setCustom(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });


        XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart

    }

    private void setChartData(String[] lable, String[] value) {
        mChart.setScaleMinima(value.length/6, 0.5f);
        System.out.println("CLEARED GENERATED");
//        if (mChart != null) {
//            mChart.invalidate();
//            mChart.getData().clearValues();
//            mChart.clearValues();
//            mChart.clear();
//            mChart.removeAllViews();
//            System.out.print("CLEARED CHART");
//        } else {
//            mChart = (BarChart) findViewById(R.id.chart1);
//        }
//        mChart.setOnChartValueSelectedListener(this);
//
//        mChart.setDrawBarShadow(false);
//        mChart.setDrawValueAboveBar(true);
//
//        mChart.getDescription().setEnabled(false);
//
//        // if more than 60 entries are displayed in the chart, no values will be
//        // drawn
//        mChart.setMaxVisibleValueCount(60);
//
//        // scaling can now only be done on x- and y-axis separately
//        mChart.setPinchZoom(false);
//
//        mChart.setDrawGridBackground(false);
//        // mChart.setDrawYLabels(false);
//
//        MyAxisValueFormatter xAxisFormatter = new MyAxisValueFormatter(lable);
//
//        XAxis xAxis = mChart.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTypeface(mTfRegular);
//        xAxis.setDrawGridLines(false);
//        xAxis.setGranularity(1f); // only intervals of 1 day
//        xAxis.setLabelCount(10);
//        xAxis.setTextColor(Color.parseColor("#FFFFFF"));
//        xAxis.setValueFormatter(xAxisFormatter);
//
//        MyYAxisValueFormatter custom = new MyYAxisValueFormatter("Ltr");
//
//        YAxis leftAxis = mChart.getAxisLeft();
//        leftAxis.setTypeface(mTfRegular);
//        leftAxis.setLabelCount(8, false);
//        leftAxis.setTextColor(Color.parseColor("#FFFFFF"));
//        leftAxis.setValueFormatter(custom);
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        leftAxis.setSpaceTop(15f);
//        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//
//        YAxis rightAxis = mChart.getAxisRight();
//        rightAxis.setDrawGridLines(false);
//        rightAxis.setEnabled(false);
//        rightAxis.setDrawLabels(false);
////        rightAxis.setTypeface(mTfRegular);
////        rightAxis.setLabelCount(8, false);
////        rightAxis.setValueFormatter(custom);
////        rightAxis.setSpaceTop(15f);
////        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//
//        Legend l = mChart.getLegend();
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//        l.setForm(Legend.LegendForm.SQUARE);
//        l.setFormSize(9f);
//        l.setTextSize(11f);
//        l.setTextColor(Color.parseColor("#FFFFFF"));
//        l.setXEntrySpace(4f);
//        // l.setExtra(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
//        // "def", "ghj", "ikl", "mno" });
//        // l.setCustom(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
//        // "def", "ghj", "ikl", "mno" });
//
//
//        XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
//        mv.setChartView(mChart); // For bounds control
//        mChart.setMarker(mv); // Set the marker to the chart
        xAxisFormatter.setData(lable);
        xAxisFormatter.setDataValue(value);
        setData(lable, value);
    }

    private void setData(String[] count, String range[]) {

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        int total = 0;
        for (int i = 0; i < count.length; i++) {
            float val = Float.parseFloat(range[i]);
            total = total + (int) val;
            yVals1.add(new BarEntry(i, val));
        }
        System.out.print(total);
        tvTotalUsed.setText(String.valueOf(total));


        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            xAxisFormatter.setData(count);
            xAxisFormatter.setDataValue(range);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, getCurrentMonth());
//            set1.setColors(ColorTemplate.MATERIAL_COLORS);
            set1.setColors(BAR_COLOOR);
            set1.setValueTextColor(Color.parseColor("#FFFFFF"));
            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTextColor(Color.parseColor("#FFFFFF"));
            data.setValueTypeface(mTfRegular);
            data.setBarWidth(0.8f);

            mChart.setData(data);
            mChart.animateY(2000);
        }
    }

    private String getCurrentMonth() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("MMM yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    @Override
    protected void onResume() {
        String suffix = "\nLiters";
        if (Preferences.getAppPrefString(Preferences.KEY_TOTAL_CAPACITY).equalsIgnoreCase("")) {
            Intent intent = new Intent(this, SettingsWaterActivity.class);
            startActivity(intent);
            finish();
        }
        if (Preferences.getAppPrefBool(Preferences.KEY_IS_BUNGALOW, true)) {
            Intent intent = new Intent(this, WaterControlActivity.class);
            startActivity(intent);
            finish();
        }
        total = Integer.parseInt((Preferences.getAppPrefString(Preferences.KEY_TOTAL_CAPACITY)).equalsIgnoreCase("") ? "5000" : Preferences.getAppPrefString(Preferences.KEY_TOTAL_CAPACITY));

        if (Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS).equalsIgnoreCase("")) {
            Preferences.writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS, "0");
        }
//        tvTotalUsed.setText(Preferences.getAppPrefString(Preferences.KEY_TOTAL_USED));
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);


        super.onResume();
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

    private void setupDaysClick(final LinearLayout ll_days) {
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View view = ll_days.getChildAt(j);
//            if (view instanceof RelativeLayout) {
//                RelativeLayout layout = ((RelativeLayout) view);
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tag = (String) v.getTag();
                        switch (tag) {
                            case "0":
//                                ((TextView) v).setTypeface(null, Typeface.BOLD);
//                                ((TextView) v).setTextColor(Color.CYAN);
                                ((TextView) v)
                                        .setBackgroundResource(R.drawable.text_active_bg);
                                v.setTag("1");
                                break;
                            case "1":
//                                ((TextView) v).setTextColor(Color.BLACK);
                                ((TextView) v)
                                        .setBackgroundResource(R.drawable.text_bg);
                                v.setTag("0");
                                break;
                        }
                        boolean checked = ((Character) device.getDAYS().charAt(7))
                                .toString()
                                .equals("1");
                        device.setDAYS(getFinalDays(checked, ll_days));
//                        db.updateDevice(bean);
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                + Constants
                                .getRoomcode(device.getROOM_Id())
                                + Constants.getModuleAndDevicecode(device.getMODULE()) + device
                                .getDAYS());

                    }
                });
            }
//            }
        }
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE).equalsIgnoreCase("device")) {
                        initItems();
                    } else if (intent.getExtras().getString(BroadcastConstant.MESSAGE).equalsIgnoreCase("flat_water")) {
                        setDataFromString();
                    }
                }
            }
        };
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE).equalsIgnoreCase("device")) {
                        initItems();
                    } else if (intent.getExtras().getString(BroadcastConstant.MESSAGE).equalsIgnoreCase("flat_water")) {
                        setDataFromString();
                    }
                }
            }
        };
    }

    private void setDataFromString() {
        String data = Preferences.getAppPrefString(Preferences.KEY_FLAT_WATER);
        if (data != null && !data.equalsIgnoreCase("")) {
            String lable[];
            String value[];
            String main[] = data.split(",");
            lable = new String[main.length];
            value = new String[main.length];
            for (int i = 0; i < main.length; i++) {
                lable[i] = main[i].substring(0, 4);
                value[i] = main[i].substring(4, main[i].length());
                total = total + Integer.parseInt(value[i]);
            }
            setChartData(lable, value);
            mChart.invalidate();
        } else {
            isDummyData = true;
            data = "A10110000,A10212000,A20120000,A20209000,A30114000,A30225000,A40110000,A40212000,A50120000,A50209000,A60114000,A60225000";
            data = data + ",B10110000,B10212000,B20120000,B20209000,B30114000,B30225000,B40110000,B40212000,B50120000,B50209000,B60114000,B60225000";
            data = data + ",C10110000,C10212000,C20120000,C20209000,C30114000,C30225000,C40110000,C40212000,C50120000,C50209000,C60114000,C60225000";
            data = data + ",D10110000,D10212000,D20120000,D20209000,D30114000,D30225000,D40110000,D40212000,D50120000,D50209000,D60114000,D60225000";
            data = data + ",E10110000,E10212000,E20120000,E20209000,E30114000,E30225000,E40110000,E40212000,E50120000,E50209000,E60114000,E60225000";
            String lable[];
            String value[];
            String main[] = data.split(",");
            lable = new String[main.length];
            value = new String[main.length];
            for (int i = 0; i < main.length; i++) {
                lable[i] = main[i].substring(0, 4);
                value[i] = main[i].substring(4, main[i].length());
                total = total + Integer.parseInt(value[i]);
            }
            setChartData(lable, value);
            mChart.invalidate();
//            Preferences.writeSharedPreferences(Preferences.KEY_FLAT_WATER, "A10110000,A10212000,A20120000,A20209000,A30114000,A30225000");
//            setDataFromString();
        }

    }

    private void callOnOff() {
        if (device.getROOM_NAME() != null) {
            device.setSTT(device.getSTT() == 0 ? 1 : 0);
            device.setALL_STT(device.getSTT());
            Log.d("TEST", "from check");
            if (ivDevice.isShown()) {

                ivDevice.setImageResource(
                        device.getSTT() == 1 ? R.drawable.on : R.drawable.off);

                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                        + Constants
                        .getRoomcode(device.getROOM_Id())
                        + Constants
                        .getModuleAndDevicecode(device.getMODULE())
                        + device
                        .getSTT()
                        + "D" + Constants
                        .getThreeDigit(device.getDIM()) + "RX"
                        + device.getRGBSTT()
                        + device.getMIC()
                );
            }
        }
    }

    private void setDaysFromBean(LinearLayout ll_days) {
        String days = device.getDAYS();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            String tag = ((Character) days.charAt(j)).toString();
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setTag(tag);
                switch (tag) {
                    case "0":
                        ((TextView) v).setBackgroundResource(R.drawable.text_bg);
                        break;
                    case "1":
                        ((TextView) v)
                                .setBackgroundResource(R.drawable.text_active_bg);
                        break;
                }
            }
        }
    }

    private String getFinalDays(boolean isChecked, LinearLayout ll_days) {
        String daysStatus = getSelectedDays(ll_days);
        if (isChecked) {
            daysStatus = daysStatus + "1";
        } else {
            daysStatus = daysStatus + "0";
        }

        return daysStatus;
    }

    private String getSelectedDays(LinearLayout ll_days) {
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                builder.append((String) v.getTag());
            }

        }
        return builder.toString();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.tvRoomSelect:
                intent = new Intent(this, RoomSelectionActivity.class);
                startActivity(intent);
                break;
        }
        super.onClick(v);
    }

    @Override
    public void onOffClick(View v) {
        db.updateDevice(device);
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "FLATWATERLEVEL0");

        finish();
        super.onOffClick(v);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        db.updateDevice(device);
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "FLATWATERLEVEL0");

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd,
                          int minuteEnd) {
        int hour, endHour;
        String ampm, endAMPM, FromDate, ToDate, timer;
        if (picker == 1) {
            timer = "T1";
            System.out.println("hourOfDay " + hourOfDay);
            System.out.println("minute " + minute);
            System.out.println("hourOfDayEnd " + hourOfDayEnd);
            System.out.println("minuteEnd " + minuteEnd);
            if (hourOfDay > 12) {
                hour = hourOfDay - 12;
                ampm = "pm";
            } else {
                hour = hourOfDay;
                ampm = "am";
            }
            if (hourOfDayEnd > 12) {
                endHour = hourOfDayEnd - 12;
                endAMPM = "pm";
            } else {
                endHour = hourOfDayEnd;
                endAMPM = "am";
            }
            FromDate = hour + ":" + minute + " " + ampm;
            ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
            device.setTIME_ONE_A(FromDate);
            device.setTIME_ONE_B(ToDate);
            tv_fromTimer1.setText(FromDate);
            tv_toTimer1.setText(ToDate);
        } else {
            timer = "T2";
            System.out.println("hourOfDay " + hourOfDay);
            System.out.println("minute " + minute);
            System.out.println("hourOfDayEnd " + hourOfDayEnd);
            System.out.println("minuteEnd " + minuteEnd);
            if (hourOfDay > 12) {
                hour = hourOfDay - 12;
                ampm = "pm";
            } else {
                hour = hourOfDay;
                ampm = "am";
            }
            if (hourOfDayEnd > 12) {
                endHour = hourOfDayEnd - 12;
                endAMPM = "pm";
            } else {
                endHour = hourOfDayEnd;
                endAMPM = "am";
            }
            FromDate = hour + ":" + minute + " " + ampm;
            ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
            device.setTIME_TWO_A(FromDate);
            device.setTIME_TWO_B(ToDate);
            tv_fromTimer2.setText(FromDate);
            tv_toTimer2.setText(ToDate);

        }
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(device.getROOM_Id())
                + Constants.getModuleAndDevicecode(device.getMODULE()) + timer
                + Constants.getTwoDigit(hourOfDay) + Constants.getTwoDigit(minute) + Constants
                .getTwoDigit(hourOfDayEnd) + Constants.getTwoDigit(minuteEnd) + "XXXXREP" + device
                .getDAYS());

    }

    @Override
    public void onSetting() {
        Intent intent = new Intent(this, SettingsWaterActivity.class);
        startActivity(intent);
        super.onSetting();
    }

    String name;
    String value;

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        name = xAxisFormatter.getFormattedValue(e.getX(), null);
        value = xAxisFormatter.getValueUsage((int) e.getX());
//        System.out.println("value x "+ xAxisFormatter.getValueUsage((int) e.getX()));
//        System.out.println("value x "+ e.getX());
//        System.out.println("value y "+ e.getX());
//        System.out.println("value x "+ h.getX());
//        System.out.println("value y "+ h.getX());


    }

    @Override
    public void onNothingSelected() {
        System.out.println("name " + name);
//        if (!isDummyData) {
        Intent intent = new Intent(this, WaterControlFlatDetailActivity.class);
        intent.putExtra("flatName", name);
        intent.putExtra("usage", value);
        startActivity(intent);
//        }
    }
}
