package com.oob.activity;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.oob.adapter.RoomSelectionRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entRooms;
import com.oob.tools.Constants;
import com.oob.tools.PixelUtil;
import com.oob.tools.Utils;

import java.util.ArrayList;
import java.util.List;

public class RoomSelectionActivity extends BaseActivityChirag {

//    private MainFragmentOrganizer mainFragmentOrganizer;



    dbHelperOperations db;

    private RecyclerView rv_rooms;
    private RoomSelectionRecyclerAdapter rv_adapter;
    List<entRooms> rooms = new ArrayList<>();
    public  int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);


        setToolbar();
        getSupportFragmentManager();
        setTitleName(getString(R.string.title_room_selection));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();

        // showAddUserData();
    }
    private void initItems() {
        db = new dbHelperOperations(this);
        rooms= db.getListOfRooms(true);
        rv_rooms = (RecyclerView) findViewById(R.id.rv_room);
        rv_adapter=new RoomSelectionRecyclerAdapter(RoomSelectionActivity.this,rooms);
        rv_rooms.setLayoutManager(new LinearLayoutManager(RoomSelectionActivity.this));
        rv_rooms.setAdapter(rv_adapter);
        rv_rooms.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this,10)));
    }


    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }
    public void actionProfilePic(String action,int position) {
        this.position = position;
        Intent intent = new Intent(this, ImageCropActivity.class);
        intent.putExtra("ACTION", action);
        intent.putExtra("tag", "profile");
        startActivityForResult(intent, Constants.REQUEST_CODE_UPDATE_PIC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_UPDATE_PIC) {
            if (resultCode == RESULT_OK) {
                String imagePath = data.getStringExtra(Constants.IntentExtras.IMAGE_PATH);
                System.out.println("imagePath " + imagePath);

                byte [] byteData =Utils.getBytesFromPath(imagePath);
                rv_adapter.updateData(imagePath,position);
                db.updateRoomPhoto(imagePath,rooms.get(position).getROOM_Id());
//                showCroppedImage(imagePath);
//                Glide.with(getActivity()).load(imagePath).placeholder(R.drawable.bescom_logo).diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true).dontAnimate().into(ivProfile);
//                this.imagePath = imagePath;
            } else if (resultCode == RESULT_CANCELED) {

            } else {
                String errorMsg = data.getStringExtra(ImageCropActivity.ERROR_MSG);
                Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }
}
