package com.oob.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.HorizontalScrollView;

import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entChannel;
import com.oob.entity.entTV;
import com.oob.fragment.TabFragment;
import com.oob.view.EditText;
import com.oob.view.TextView;

public class TVControlGroupChannelActivity extends BaseActivityChirag {


    private dbHelperOperations db;

    String[] tabList;

    private int pos;

    private HorizontalScrollView mScrollView;

    private TextView tvFav1, tvFav2, tvSocial, tvSport, tvNews, tvMusic, tvRegion, tvMovie,
            tvCartoon, tvDiscovery;

    private ViewPager viewPager;

    private PagerAdapter adapter;
    private entTV mEntTV;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_control_ch_group);
        setToolbar();
        db = new dbHelperOperations(this);
        mEntTV = (entTV) getIntent().getExtras().getSerializable("data");
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        ivMenu.setVisibility(View.VISIBLE);
        pos = getIntent().getExtras().getInt("pos");
        initItems();
        setTitleName(getString(R.string.title_group_channel));

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    public entTV getTVdata() {
        return mEntTV;
    }

    private void initItems() {
        tabList = getResources().getStringArray(R.array.channel_groups);
        tvFav1 = (TextView) findViewById(R.id.tvFav1);
        tvFav2 = (TextView) findViewById(R.id.tvFav2);
        tvSocial = (TextView) findViewById(R.id.tvSocial);
        tvSport = (TextView) findViewById(R.id.tvSports);
        tvNews = (TextView) findViewById(R.id.tvNews);
        tvMusic = (TextView) findViewById(R.id.tvMusic);
        tvRegion = (TextView) findViewById(R.id.tvRegion);
        tvMovie = (TextView) findViewById(R.id.tvMovie);
        tvCartoon = (TextView) findViewById(R.id.tvCartoon);
        tvDiscovery = (TextView) findViewById(R.id.tvDiscovery);
        mScrollView = (HorizontalScrollView) findViewById(R.id.scrollView);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("Fav1"));
        tabLayout.addTab(tabLayout.newTab().setText("Fav2"));
        tabLayout.addTab(tabLayout.newTab().setText("SOCIAL"));
        tabLayout.addTab(tabLayout.newTab().setText("SPORTS"));
        tabLayout.addTab(tabLayout.newTab().setText("NEWS"));
        tabLayout.addTab(tabLayout.newTab().setText("MUSIC"));
        tabLayout.addTab(tabLayout.newTab().setText("REGION"));
        tabLayout.addTab(tabLayout.newTab().setText("MOVIE"));
        tabLayout.addTab(tabLayout.newTab().setText("CARTOON"));
        tabLayout.addTab(tabLayout.newTab().setText("DISCOVERY"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new PagerAdapter
                (getSupportFragmentManager(), 10);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pos);
        setCurrentPosition(pos);
//        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                pos = tab.getPosition();
                setCurrentPosition(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset,
//                                       int positionOffsetPixels) {
//                pos = position;
//                setCurrentPosition(position);
//            }
//
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });

    }

    @Override
    public void onMenuClick(View v) {
        PopupMenu popup = new PopupMenu(TVControlGroupChannelActivity.this, v);
        popup.getMenuInflater().inflate(R.menu.channel_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.addChannel:
                        Intent intent = new Intent(TVControlGroupChannelActivity.this,
                                ChannelSelectionActivity.class);
                        startActivityForResult(intent, pos);
                        break;
                }
                return true;
            }
        });

        popup.show();//showing popup menu

        super.onMenuClick(v);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            String name = data.getExtras().getString("name");
            String image = data.getExtras().getString("image");
            entChannel entChannel = new entChannel(image, name);
            switch (requestCode) {
                case 0:
                    entChannel.setChannelGroup("fav1");
                    break;
                case 1:
                    entChannel.setChannelGroup("fav2");
                    break;
                case 2:
                    entChannel.setChannelGroup("social");
                    break;
                case 3:
                    entChannel.setChannelGroup("sport");
                    break;
                case 4:
                    entChannel.setChannelGroup("news");
                    break;
                case 5:
                    entChannel.setChannelGroup("music");
                    break;
                case 6:
                    entChannel.setChannelGroup("region");
                    break;
                case 7:
                    entChannel.setChannelGroup("movie");
                    break;
                case 8:
                    entChannel.setChannelGroup("cartoon");
                    break;
                case 9:
                    entChannel.setChannelGroup("discovery");
                    break;
            }
            openChannelNumberDialog(entChannel);

        }
    }

    private void openChannelNumberDialog(final entChannel entChannel) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_channel_number);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        edtName.setText(""+db.getChannelNumber(entChannel.getName(),mEntTV));
        edtName.setSelection(edtName.getText().length());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                entChannel.setNumber(edtName.getText().toString());
                db.insertChannel(entChannel);
                adapter = new PagerAdapter
                        (getSupportFragmentManager(), 10);
                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(pos);
                setCurrentPosition(pos);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                db.insertChannel(entChannel);
                adapter = new PagerAdapter
                        (getSupportFragmentManager(), 10);
                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(pos);
                setCurrentPosition(pos);

            }
        });


    }


    private void setCurrentPosition(int position) {
        switch (position) {
            case 0:
                allBackNormal();
                tvFav1.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case 1:
                allBackNormal();
                tvFav2.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case 2:
                allBackNormal();
                tvSocial.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case 3:
                allBackNormal();
                tvSport.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case 4:
                allBackNormal();
                tvNews.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case 5:
                allBackNormal();
                tvMusic.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case 6:
                allBackNormal();
                tvRegion.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case 7:
                allBackNormal();
                tvMovie.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case 8:
                allBackNormal();
                tvCartoon.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case 9:
                allBackNormal();
                tvDiscovery.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvFav1:
                allBackNormal();
                viewPager.setCurrentItem(0);
                tvFav1.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case R.id.tvFav2:
                viewPager.setCurrentItem(1);
                allBackNormal();
                tvFav2.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case R.id.tvSocial:
                viewPager.setCurrentItem(2);
                allBackNormal();
                tvSocial.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case R.id.tvSports:
                viewPager.setCurrentItem(3);
                allBackNormal();
                tvSport.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case R.id.tvNews:
                viewPager.setCurrentItem(4);
                allBackNormal();
                tvNews.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case R.id.tvMusic:
                viewPager.setCurrentItem(5);
                allBackNormal();
                tvMusic.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case R.id.tvRegion:
                viewPager.setCurrentItem(6);
                allBackNormal();
                tvRegion.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case R.id.tvMovie:
                viewPager.setCurrentItem(7);
                allBackNormal();
                tvMovie.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case R.id.tvCartoon:
                viewPager.setCurrentItem(8);
                allBackNormal();
                tvCartoon.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
            case R.id.tvDiscovery:
                viewPager.setCurrentItem(9);
                allBackNormal();
                tvDiscovery.setBackgroundResource(R.drawable.background_btn_border_fill);
                break;
        }
        super.onClick(v);
    }

    private void allBackNormal() {
        tvFav1.setBackgroundResource(R.drawable.background_btn_border);
        tvFav2.setBackgroundResource(R.drawable.background_btn_border);
        tvSocial.setBackgroundResource(R.drawable.background_btn_border);
        tvSport.setBackgroundResource(R.drawable.background_btn_border);
        tvNews.setBackgroundResource(R.drawable.background_btn_border);
        tvMusic.setBackgroundResource(R.drawable.background_btn_border);
        tvRegion.setBackgroundResource(R.drawable.background_btn_border);
        tvMovie.setBackgroundResource(R.drawable.background_btn_border);
        tvCartoon.setBackgroundResource(R.drawable.background_btn_border);
        tvDiscovery.setBackgroundResource(R.drawable.background_btn_border);
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();


    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            TabFragment tabFragment;
            Bundle bundle = new Bundle();
            bundle.putInt("pos", position);
            switch (position) {
                case 0:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "fav1");
                    tabFragment.setArguments(bundle);
                    return tabFragment;
                case 1:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "fav2");
                    tabFragment.setArguments(bundle);

                    return tabFragment;
                case 2:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "social");
                    tabFragment.setArguments(bundle);
                    return tabFragment;
                case 3:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "sport");
                    tabFragment.setArguments(bundle);
                    return tabFragment;
                case 4:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "news");
                    tabFragment.setArguments(bundle);
                    return tabFragment;
                case 5:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "music");
                    tabFragment.setArguments(bundle);
                    return tabFragment;
                case 6:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "region");
                    tabFragment.setArguments(bundle);
                    return tabFragment;
                case 7:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "movie");
                    tabFragment.setArguments(bundle);
                    return tabFragment;
                case 8:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "cartoon");
                    tabFragment.setArguments(bundle);
                    return tabFragment;
                case 9:
                    tabFragment = new TabFragment();
                    bundle.putString("group", "discovery");
                    tabFragment.setArguments(bundle);
                    return tabFragment;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabList[position];
        }
    }


}
