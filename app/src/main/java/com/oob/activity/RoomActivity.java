package com.oob.activity;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.oob.OOBApplication;
import com.oob.SocketServerTask;
import com.oob.adapter.RoomRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.entity.entRooms;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Constants;
import com.oob.tools.PixelUtil;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.tools.WifiUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RoomActivity extends BaseActivityChirag {

    private RecyclerView rv_room;
    private RoomRecyclerAdapter rv_adapter;
    private TextView tv_servermsg;
    public static final int SERVERPORT = 6000;
    private BroadcastReceiver broadcast;
    private List<entRooms> rooms = new ArrayList<>();
    private ProgressDialog progressDialog;
    private boolean isOn = true;
    private boolean isGrouping = true;
    private dbHelperOperations db;
    private int selectedRoom = 0;
    private int selectedRoomId = -1;
    List<entDevice> al_data = new ArrayList<>();
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private boolean isUpdated;
    private SocketServerTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        isUpdated = getIntent().getExtras().getBoolean("update");
        db = new dbHelperOperations(this);
        rooms.addAll(db.getListOfRooms(false));
        entRooms roomName[] = new entRooms[rooms.size()];
        for (int i = 0; i < rooms.size(); i++) {
            roomName[i] = rooms.get(i);
            System.out.println("IS SELECTED " + rooms.get(i).getROOM_IS_SELECTED());
            if (rooms.get(i).getROOM_IS_SELECTED() == 1) {
                selectedRoom = i;
            }
        }
        if (roomName.length > 0) {
            selectedRoomId = rooms.get(selectedRoom).getROOM_Id();
        }
        setToolbar();
        bindViewControls();
        initItems();
        showSpinners(roomName, selectedRoom);
//        tvMic.setVisibility(View.VISIBLE);
        tv_servermsg.setText(String.format("MY IP:%s\n", WifiUtils.getIPAddress(true)));
        broadcastListener();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.waiting_for_acknowl));

    }


//    @Override
//    public void processData(String topic, String message) {
//        if (message.contains(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE))) {
//            String msg = message
//                    .replace(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE), "");
//            int roomId = Utils.getRoomId(msg);
//            if (roomId != -1) {
//                String module = Utils.getModule(msg);
//                System.out.println("ROOM ID " + roomId);
//                System.out.println("Module ID " + module);
//                entDevice device = db
//                        .getDeviceFromModule(roomId, module);
////                        System.out.println("ID "+device.getID());
//                System.out.println("MMMM " + message);
//                if (msg.substring(7, 9).equalsIgnoreCase("CL")) {
//                    device.setCL(getNumberFromString(message, 19));
//                } else {
//                    device.setSTT(getNumberFromString(message, 17));
//                    device.setDIM(getNumberFromSubString(message, 19, 22));
//                }
//                db.updateDevice(device);
//                if (rv_adapter != null) {
//                    System.out.println("adaper not null");
//                    al_data = db.getListOfDevices(selectedRoomId, true);
//                    rv_adapter.updateData(al_data);
//
//                }
//            }
//        }
//    }

    private int getNumberFromString(String str, int poss) {
        try {
            System.out.println("CHAR " + str.charAt(poss));
            return Integer.parseInt(str.charAt(poss) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int getNumberFromSubString(String str, int poss1, int poss2) {
        try {
            System.out.println("SUBSTRING " + str.substring(poss1, poss2));
            return Integer.parseInt(str.substring(poss1, poss2) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    public void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected()) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
////            HostUtils.sendToHost(this, json, "http://gsiot-njf4-3sej.try.yaler.io");
////        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                if (task == null) {
//                    task = new SocketServerTask(this, null);
//                }
////            if (task.getStatus() != AsyncTask.Status.RUNNING) {
//                task = new SocketServerTask(this, null);
//                task.execute(json, WifiUtils.server_ip,
//                        WifiUtils.server_port);
//            } else {
//                System.out.println("RUNNING");
//            }
////            json = WifiUtils.encodeToServerString("JSON=" + json);
////            Log.d("TEST", json);
////            if (!json.isEmpty()) {
////                new SocketServerTask(this, null)
////                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
////                                WifiUtils.server_port);
////
////
////            }
//        }
    }

    @Override
    public void onMicClick() {
        System.out.println("mice long clock");
        promptSpeechInput();
        super.onMicClick();
    }

    @Override
    public void onMenuClick(View v) {
        PopupMenu popup = new PopupMenu(RoomActivity.this, v);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.over_flow_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (selectedRoomId != -1) {
                    Intent intent;
                    switch (item.getItemId()) {
                        case R.id.one:
                            rv_adapter.AddData(getData(1), selectedRoomId);
                            break;
                        case R.id.two:
                            rv_adapter.AddData(getData(2), selectedRoomId);
                            break;
                        case R.id.three:
                            rv_adapter.AddData(getData(3), selectedRoomId);
                            break;
                        case R.id.four:
                            rv_adapter.AddData(getData(4), selectedRoomId);
                            break;
                        case R.id.five:
                            rv_adapter.AddData(getData(5), selectedRoomId);
                            break;
                        case R.id.six:
                            rv_adapter.AddData(getData(6), selectedRoomId);
                            break;
                        case R.id.seven:
                            rv_adapter.AddData(getData(7), selectedRoomId);
                            break;
                        case R.id.eight:
                            rv_adapter.AddData(getData(8), selectedRoomId);
                            break;
                        case R.id.nine:
                            rv_adapter.AddData(getData(9), selectedRoomId);
                            break;
                        case R.id.ten:
                            rv_adapter.AddData(getData(10), selectedRoomId);
                            break;
                        case R.id.delete:
                            deleteDialog();
                            break;
                        case R.id.priority:
                            intent = new Intent(RoomActivity.this, PrioritySetupActivity.class);
                            intent.putExtra("roomId", selectedRoomId);
                            RoomActivity.this.startActivityForResult(intent, 11);
                            break;
                        case R.id.backLight:
                            intent = new Intent(RoomActivity.this, BackLightActivity.class);
                            intent.putExtra("roomId", selectedRoomId);
                            RoomActivity.this.startActivity(intent);
                            break;


                    }
                } else {
                    Toast.makeText(RoomActivity.this, R.string.error_select_room, Toast.LENGTH_LONG)
                            .show();
                }
//                rv_adapter.AddData(getData(5,8));
//                Toast.makeText(RoomActivity.this, "You Clicked : " + item.getTitle(),
//                        Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        popup.show();//showing popup menu

        super.onMenuClick(v);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 11) {
            initItems();
        } else if (resultCode == RESULT_OK && null != data) {

            ArrayList<String> result = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            System.out.println("text is " + result.get(0));
            speechTextAlert(result.get(0));
        }
    }

    private void deleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_delete_device);
        builder.setMessage(R.string.msg_delete_device);
        builder.setIcon(R.drawable.app_logo);
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                al_data = new ArrayList<entDevice>();
                db.deletedevice(selectedRoomId);
                db.updateModuleCount(selectedRoomId, 0);
                rv_adapter.clearAdapter();
//

            }
        });

        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setCancelable(true);
        builder.show();

    }


    private void bindViewControls() {
        rv_room = (RecyclerView) findViewById(R.id.rv_room);
        rv_room.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));
        tv_servermsg = (TextView) findViewById(R.id.tv_servermsg);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE)
                            .equalsIgnoreCase("device")) {
                        if (rv_adapter != null) {
                            System.out.println("adaper not null");
                            al_data = db.getListOfDevices(selectedRoomId, true);
//                            rv_adapter.clearAdapter();
                            rv_adapter.updateData(al_data);

                        }
                    } else if (intent.getExtras().getString(BroadcastConstant.MESSAGE)
                            .equalsIgnoreCase("update")) {
                        if (!isUpdated) {
                            if (rv_adapter != null) {
                                al_data = db.getListOfDevices(selectedRoomId, true);
                                rv_adapter.updateData(al_data);
                            }
                            dataChangeAlert();
                        }
                    }
//                    String msg = intent.getStringExtra(BroadcastConstant.MESSAGE);
//                    if (msg != null) {
//                        if (msg.equals(getString(R.string.error_wrong))) {
//                            NotifyUtils.toast(RoomActivity.this, msg);
//                        } else if (msg.equals(getString(R.string.str_ack_msg))) {
//                            progressDialog.show();
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                    if (progressDialog.isShowing()) {
//                                        progressDialog.dismiss();
//                                        rv_adapter.updateContentFromDB(selectedRoomId);
//                                        NotifyUtils.toast(RoomActivity.this,
//                                                getString(R.string.str_no_ack));
//                                    }
//
//                                }
//                            }, 5000);
//                        }
//                    }
//                }
//
//                if (intent.hasExtra(BroadcastConstant.DATA_CHANGE)) {
////                    devicesEntity = intent
////                            .getParcelableArrayListExtra(BroadcastConstant.DATA_CHANGE);
//
////                    if (devicesEntity != null) {
//                    if (progressDialog.isShowing()) {
//                        progressDialog.dismiss();
//                        NotifyUtils.toast(RoomActivity.this, getString(R.string.str_ack_recv));
//                    }
//
//                    if (Preferences.getAppPrefBool(Preferences.KEY_DIALOG_SHOW, true)) {
//                        dataChangeAlert();
//                        Preferences
//                                .writeSharedPreferencesBool(Preferences.KEY_DIALOG_SHOW, false);
//                    } else {
//                        rv_adapter.updateContentFromDB(selectedRoomId);
////                            roomAdapter.updateContentFromDB(selectedRoomId);
//                    }
////                    } else {
////                        NotifyUtils.toast(RoomActivity.this, "Not Received successfully");
////                    }
                }
            }
        };
    }

    public void dataChangeAlert() {
        Utils.showToast(this, getString(R.string.title_data_change));
//        AlertDialog alertDialog = new AlertDialog.Builder(
//                RoomActivity.this).create();
//
//        // Setting Dialog Title
//        alertDialog.setTitle(getString(R.string.title_data_change));
//
//        // Setting Dialog Message
//        //alertDialog.setMessage(getString(R.string.msg_data_change));
//
//        // Setting Icon to Dialog
//        alertDialog.setIcon(R.mipmap.ic_launcher);
//
//        alertDialog
//                .setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        rv_adapter.updateContentFromDB(selectedRoomId);
////                        roomAdapter.updateContentFromDB(selectedRoomId);
//                    }
//                });
//
//        // Showing Alert Message
//        alertDialog.show();
    }

    public void speechTextAlert(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(
                RoomActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("You spoke!");

        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog
                .setButton(AlertDialog.BUTTON_NEUTRAL, "OK, GOT IT",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

        // Showing Alert Message
        alertDialog.show();
    }


    private void initItems() {
        if (db.getAllStt(selectedRoomId) == 1) {
            ivOnOff.setTextColor(Color.parseColor("#ffffff"));

        } else {
            ivOnOff.setTextColor(Color.parseColor("#000000"));

        }
        if (db.getAllGrp(selectedRoomId) == 1) {
            tvGrouping.setTextColor(getResources().getColor(R.color.White));

        } else {
            tvGrouping.setTextColor(getResources().getColor(R.color.Black));
        }
        al_data = new ArrayList<>();
        al_data = db.getListOfDevices(selectedRoomId, true);
        rv_adapter = new RoomRecyclerAdapter(RoomActivity.this, al_data, this, selectedRoomId);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_room.setLayoutManager(linearLayoutManager);
        rv_room.setAdapter(rv_adapter);


    }


    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }

    }

    private ArrayList<entDevice> getData(int Msize) {
        for (int i = 1; i <= Msize; i++) {
            entDevice bean = new entDevice();
            bean.setROOM_Id(selectedRoomId);
            bean.setGROUP(0);
            bean.setSTT(0);
            bean.setDAYS("00000000");
            bean.setMIC(1);
            bean.setALL_STT(0);
            bean.setBLUECLR(255);
            bean.setCL(0);
            bean.setDIM(0);
            bean.setENGRD(0);
            bean.setGREENCLR(255);
            bean.setREDCLR(255);
            bean.setRGB(0);
            bean.setRGBSTT(2);
            bean.setTIME_ONE_A("0000");
            bean.setTIME_ONE_B("0000");
            bean.setTIME_TWO_A("0000");
            bean.setTIME_TWO_B("0000");
            bean.setMODULE_STT(1);
            bean.setBACKLIGHT_RGB_STT(2);
            bean.setBACKLIGHT_DIM_STT(99);
            bean.setIS_DELETE(0);
            bean.setMODULE_NUMBER(db.getModuleCount(selectedRoomId));
            switch (i) {
                case 1:
                    bean.setMODULE(1 + "," + bean.getMODULE_NUMBER());
                    bean.setDEVICE_NAME(getString(R.string.light));
                    break;
                case 2:
                    bean.setMODULE(2 + "," + bean.getMODULE_NUMBER());
                    bean.setDEVICE_NAME(getString(R.string.fan));
                    break;
                case 3:
                    bean.setMODULE(3 + "," + bean.getMODULE_NUMBER());
                    bean.setDEVICE_NAME(getString(R.string.ac));
                    break;
                case 4:
                    bean.setMODULE(4 + "," + bean.getMODULE_NUMBER());
                    bean.setDEVICE_NAME(getString(R.string.plug));
                    break;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    bean.setMODULE(i + "," + bean.getMODULE_NUMBER());
                    bean.setDEVICE_NAME(getString(R.string.light));
                    break;
//                case 5:
//                    bean.setMODULE(5 + "," + bean.getMODULE_NUMBER());
//                    bean.setDEVICE_NAME("LIGHT");
//                    break;
//                case 6:
//                    bean.setMODULE(6 + "," + bean.getMODULE_NUMBER());
//                    bean.setDEVICE_NAME("LIGHT");
//                    break;


            }
//            System.out.println("MODULE INSERT " + bean.getMODULE());
            db.insertDevice(bean);
//            bean.save();
            al_data.add(bean);
        }
        db.updateModuleCount(selectedRoomId, db.getModuleCount(selectedRoomId));

        return (ArrayList<entDevice>) al_data;
    }


    @Override
    public void onOffClick(View v) {
        if (al_data.size() > 0) {
            if (isOn) {
                isOn = false;
                db.updateAllONOFF(selectedRoomId, 0);
                rv_adapter.sendAllOnOFFData(isOn);
                ivOnOff.setTextColor(Color.parseColor("#000000"));

            } else {
                isOn = true;
                db.updateAllONOFF(selectedRoomId, 1);
                rv_adapter.sendAllOnOFFData(isOn);
                ivOnOff.setTextColor(Color.parseColor("#ffffff"));
            }
        }
        super.onOffClick(v);
    }

    @Override
    public void onGrouping(View v) {
        if (al_data.size() > 0) {

            if (isGrouping) {
                isGrouping = false;
                db.updateAllGoruping(selectedRoomId, 0);
                rv_adapter.sendGroupingData(isGrouping);
                ((com.oob.view.TextView) v).setTextColor(getResources().getColor(R.color.Black));
            } else {
                isGrouping = true;
                db.updateAllGoruping(selectedRoomId, 1);
                rv_adapter.sendGroupingData(isGrouping);
                ((com.oob.view.TextView) v).setTextColor(getResources().getColor(R.color.White));

            }
        }

        super.onGrouping(v);
    }

//    @Override
//    public void onBackPressed() {
//        db.updateRoomSelection(rooms.get(spinRooms.getSelectedItemPosition()).getROOM_Id(), rooms.size());
//        super.onBackPressed();
//    }


    @Override
    public void onBackPressed() {
        db.updateDevicePosition(rv_adapter.getItemList());
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (rooms.size() > 0) {
            db.updateRoomSelection(rooms.get(spinRooms.getSelectedItemPosition()).getROOM_Id(),
                    rooms.size());
        }
        super.onDestroy();
    }

    @Override
    public void enableMultiselect() {

        rv_adapter.setMultiselectEnable(true);
    }

    @Override
    public void desableMultiselect() {
        rv_adapter.setMultiselectEnable(false);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedRoom = position;
        selectedRoomId = rooms.get(selectedRoom).getROOM_Id();
        initItems();
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                + "OOBUPDATER" + Constants.getTwoDigit(selectedRoomId));
        super.onItemSelected(parent, view, position, id);
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Say something!");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn\\'t support speech input",
                    Toast.LENGTH_SHORT).show();
        }
    }


}
