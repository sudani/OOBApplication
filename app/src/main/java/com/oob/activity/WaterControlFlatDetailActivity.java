package com.oob.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entWaterUsage;
import com.oob.tools.MyAxisValueFormatter;
import com.oob.tools.MyYAxisValueFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.oob.activity.EnergyActivity.BAR_COLOOR;

public class WaterControlFlatDetailActivity extends BaseActivityChirag
        implements OnChartValueSelectedListener {
    dbHelperOperations db;
    String flatName;
    String usage = "15000";
    ArrayList<entWaterUsage> waterUsages = new ArrayList<>();
    protected BarChart mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flat_water_detail);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_water_level));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();
    }

    private void initItems() {
        flatName = getIntent().getExtras().getString("flatName");
        usage = getIntent().getExtras().getString("usage");
        setTitleName(flatName);
        initChart();
        setDataFromString();
    }

    private void setDataFromString() {
        String lable[];
        String value[];
        waterUsages = db.getWaterUsageData(flatName);
//        waterUsages =
        if (waterUsages.size() > 0) {
            setData(waterUsages);
        } else {
            waterUsages = generateDummyData();
            setData(waterUsages);
        }
        mChart.invalidate();
    }

    private ArrayList<entWaterUsage> generateDummyData() {
        ArrayList<entWaterUsage> list = new ArrayList<>();
        String devices[] = new String[]{"01-2017", "02-2017", "03-2017", "04-2017", "05-2017", "06-2017", "07-2017", "08-2017",};

        entWaterUsage waterUsage;
        for (int i = 0; i < devices.length; i++) {
            long mult = (Integer.parseInt(usage) / 8 + 1000);
            long val = (long) (Math.random() * mult);
            waterUsage = new entWaterUsage();
            waterUsage.setBackupDate(devices[i]);
            waterUsage.setFaltName(flatName);
            waterUsage.setWaterUsage(String.valueOf(val));
            list.add(waterUsage);
        }
        return list;
    }

    MyAxisValueFormatter xAxisFormatter;

    private void initChart() {
        mChart = (BarChart) findViewById(R.id.chart1);
        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(10);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);
        xAxisFormatter = new MyAxisValueFormatter();

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTfRegular);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(10);
        xAxis.setTextColor(Color.parseColor("#FFFFFF"));
        xAxis.setValueFormatter(xAxisFormatter);

        MyYAxisValueFormatter custom = new MyYAxisValueFormatter("Ltr");

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTypeface(mTfRegular);
        leftAxis.setLabelCount(8, false);
        leftAxis.setTextColor(Color.parseColor("#FFFFFF"));
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setEnabled(false);
        rightAxis.setDrawLabels(false);
//        rightAxis.setTypeface(mTfRegular);
//        rightAxis.setLabelCount(8, false);
//        rightAxis.setValueFormatter(custom);
//        rightAxis.setSpaceTop(15f);
//        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = mChart.getLegend();
        l.setEnabled(false);
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//        l.setForm(Legend.LegendForm.SQUARE);
//        l.setFormSize(9f);
//        l.setTextSize(11f);
//        l.setTextColor(Color.parseColor("#FFFFFF"));
//        l.setXEntrySpace(4f);
        // l.setExtra(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });
        // l.setCustom(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });


//        XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
//        mv.setChartView(mChart); // For bounds control
//        mChart.setMarker(mv); // Set the marker to the chart

    }

    private void setChartData(ArrayList<entWaterUsage> value) {
        System.out.println("CLEARED GENERATED");
//
//        xAxisFormatter.setData(lable);
//        setData(lable, value);
    }

    private void setData(ArrayList<entWaterUsage> value) {
        mChart.setScaleMinima(((float) value.size()) / 6f, 0.5f);
        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        String lable[] = new String[value.size()];
        String values[] = new String[value.size()];
        int total = 0;
        for (int i = 0; i < value.size(); i++) {
            float val = Float.parseFloat(value.get(i).getWaterUsage());
            total = total + (int) val;
            yVals1.add(new BarEntry(i, val));
            lable[i] = value.get(i).getBackupDate();
            values[i] = String.valueOf(val);
        }
        xAxisFormatter.setData(lable);
        xAxisFormatter.setDataValue(values);
        System.out.print(total);


        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            xAxisFormatter.setData(lable);
            xAxisFormatter.setDataValue(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, name);
//            set1.setColors(ColorTemplate.MATERIAL_COLORS);
            set1.setColors(BAR_COLOOR);
            set1.setValueTextColor(Color.parseColor("#FFFFFF"));
            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTextColor(Color.parseColor("#FFFFFF"));
            data.setValueTypeface(mTfRegular);
            data.setBarWidth(0.8f);

            mChart.setData(data);
            mChart.animateY(2000);
        }
    }

    private String getCurrentMonth() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("MMM yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {


        super.onResume();
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

    @Override
    public void onOffClick(View v) {
        finish();
        super.onOffClick(v);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.tvRoomSelect:
                intent = new Intent(this, RoomSelectionActivity.class);
                startActivity(intent);
                break;
        }
        super.onClick(v);
    }


    String name;
    String value;

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        name = xAxisFormatter.getFormattedValue(e.getX(), null);
        value = xAxisFormatter.getValueUsage((int) e.getX());
//        System.out.println("value x "+ xAxisFormatter.getValueUsage((int) e.getX()));
//        System.out.println("value x "+ e.getX());
//        System.out.println("value y "+ e.getX());
//        System.out.println("value x "+ h.getX());
//        System.out.println("value y "+ h.getX());


    }

    @Override
    public void onNothingSelected() {
        System.out.println("name " + name);
//        if (!isDummyData) {
        Intent intent = new Intent(this, WaterControlFlatMonthActivity.class);
        intent.putExtra("flatName", flatName);
        intent.putExtra("monthName", name);
        intent.putExtra("usage", value);
        startActivity(intent);
//        }
    }
}
