package com.oob.activity;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.oob.adapter.ACSetupRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entAC;
import com.oob.tools.PixelUtil;

import java.util.ArrayList;
import java.util.List;

public class ACSetupActivity extends BaseActivityChirag {

    private dbHelperOperations db;

    private RecyclerView rv_rooms;

    private ACSetupRecyclerAdapter rv_adapter;

    List<entAC> ACs = new ArrayList<>();

    public int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_ac_setup));
        ivMenu.setVisibility(View.VISIBLE);
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();

        // showAddUserData();
    }

    private void initItems() {
        db = new dbHelperOperations(this);
        ACs = db.getListAC();

        rv_rooms = (RecyclerView) findViewById(R.id.rv_room);
        rv_adapter = new ACSetupRecyclerAdapter(ACSetupActivity.this, ACs);
        rv_rooms.setLayoutManager(new LinearLayoutManager(ACSetupActivity.this));
        rv_rooms.setAdapter(rv_adapter);
        rv_rooms.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));
        if (ACs.size()==0){
            for (int i = 0;i<6;i++){
                addNewAC();
            }
        }

    }


    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

//    @Override
//    public void onMenuClick(View v) {
//        PopupMenu popup = new PopupMenu(ACSetupActivity.this, v);
//        popup.getMenuInflater().inflate(R.menu.ac_menu, popup.getMenu());
//        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            public boolean onMenuItemClick(MenuItem item) {
//
//                switch (item.getItemId()) {
//                    case R.id.addAC:
//                        addNewAC();
//                        break;
//                }
//                return true;
//            }
//        });
//
//        popup.show();//showing popup menu
//
//        super.onMenuClick(v);
//    }

    private void addNewAC() {
        entAC entAc = new entAC();
        entAc.setAC_NAME(getString(R.string.ac_name));
        entAc.setROOM_NAME(getString(R.string.room_name));
        entAc.setROOM_Id(0);
        entAc.setAC_Id((int) db.insertAC(entAc));
        rv_adapter.addAC(entAc);
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }
}
