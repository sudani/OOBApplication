package com.oob.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.DhcpInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.text.format.Formatter;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;

import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.Button;
import com.oob.view.CheckBox;
import com.oob.view.EditText;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.List;

public class ConfigrationActivity extends BaseActivityChirag {
    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 9;

    //    private MainFragmentOrganizer mainFragmentOrganizer;

    private EditText edtPass;
    private Spinner spinSSID;
    private Button btnDone;
    private CheckBox chShowPass;
    private List<String> listSSIDs = new ArrayList<>();
    ArrayAdapter adapterSSID;
    WifiManager wifiManager;
    private TextView tvIp, tvRouterIp, tvMqttConfig;
    private String addressGateway;
    private ImageView ivDevMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configration);

        setToolbar();
        setTitleName(getString(R.string.title_configuration));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();

    }

    private void initItems() {
        edtPass = (EditText) findViewById(R.id.edtPassword);
        btnDone = (Button) findViewById(R.id.btnDone);
        tvIp = (TextView) findViewById(R.id.tvIp);
        tvRouterIp = (TextView) findViewById(R.id.tvRouterIp);
        chShowPass = (CheckBox) findViewById(R.id.chShowPass);
        spinSSID = (Spinner) findViewById(R.id.spinSSID);
        tvMqttConfig = (TextView) findViewById(R.id.tvMqttConfig);
        tvMqttConfig.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        tvIp.setOnClickListener(this);
        tvRouterIp.setOnClickListener(this);
        ivDevMode = (ImageView) findViewById(R.id.ivDevMode);
        ivDevMode.setOnClickListener(this);
        adapterSSID = new ArrayAdapter<String>(this,
                R.layout.simple_spiner_item_black, listSSIDs);
        adapterSSID.setDropDownViewResource(R.layout.simple_spiner_item_white);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        spinSSID.setAdapter(adapterSSID);
        chShowPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                edtPass.setInputType(b ? InputType.TYPE_CLASS_TEXT : (InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD));
                edtPass.setSelection(edtPass.length());
            }
        });
        tvIp.setText(getString(R.string.app_ip) + Preferences.getAppPrefString(Preferences.KEY_IP_ADDRESS));
        WifiManager manager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        DhcpInfo dhcp = manager.getDhcpInfo();
        addressGateway = Formatter.formatIpAddress(dhcp.gateway);
        tvRouterIp.setText(getString(R.string.router_ip) + addressGateway);
        if (Preferences.getAppPrefBool(Preferences.KEY_DEVELOPER_MODE, false)) {
            ivDevMode.setImageResource(R.drawable.on);
        } else {
            ivDevMode.setImageResource(R.drawable.off);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final LocationManager manager = (LocationManager) getSystemService(
                    Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
            } else {
                startTakingLocation();
            }
        } else {
//            registerReceiver(mWifiScanReceiver,
//                    new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            wifiManager.startScan();
            getScanResult();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5) {
            startTakingLocation();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.gps_enable_msg)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog,
                                        @SuppressWarnings("unused") final int id) {
                        startActivityForResult(new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                5);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        @SuppressWarnings("unused") final int id) {
                        dialog.cancel();

                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (ContextCompat.checkSelfPermission(ConfigrationActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(ConfigrationActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    startTakingLocation();
                } else {
                    Utils.showToast(this, "Permission not granted");
                }
                return;
            }

        }
    }

    private void startTakingLocation() {
        if (ContextCompat.checkSelfPermission(ConfigrationActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(ConfigrationActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        } else {
            wifiManager.startScan();
            getScanResult();
        }
    }

    private void getScanResult() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                List<ScanResult> mScanResults = wifiManager.getScanResults();
                System.out.println("size " + mScanResults.size());
                listSSIDs.clear();
                for (int i = 0; i < mScanResults.size(); i++) {
                    listSSIDs.add(mScanResults.get(i).SSID);
                }
                adapterSSID.notifyDataSetChanged();
            }
        }, 2000);
    }

    private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            System.out.println(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
            if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                List<ScanResult> mScanResults = wifiManager.getScanResults();
                System.out.println("size " + mScanResults.size());

                listSSIDs.clear();
                for (int i = 0; i < mScanResults.size(); i++) {
                    listSSIDs.add(mScanResults.get(i).SSID);
                }
                adapterSSID.notifyDataSetChanged();
                if (mScanResults.size() == 0) {
                    wifiManager.startScan();
                    getScanResult();
                }
                // add your logic here
            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
//        if (mWifiScanReceiver.isOrderedBroadcast())
//            unregisterReceiver(mWifiScanReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDone:
                if (listSSIDs.size() > 0) {
                    if (!edtPass.getText().toString().isEmpty()) {
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "WIFI*" + spinSSID.getSelectedItem().toString() + "*" + edtPass.getText().toString() + "*" + Preferences.getAppPrefString(Preferences.KEY_IP_ADDRESS) + "*" + addressGateway + "*");
                    } else {
                        Utils.showToast(ConfigrationActivity.this, getString(R.string.enter_wifi_password));
                    }
                } else {
                    Utils.showToast(ConfigrationActivity.this, getString(R.string.no_wifi_found));

                }
                break;
            case R.id.tvIp:
                openIpDialog(true);
                break;
            case R.id.tvRouterIp:
                openIpDialog(false);
                break;
            case R.id.tvMqttConfig:
                Intent intent = new Intent(this, MqttConfigActivity.class);
                startActivity(intent);
                break;
            case R.id.ivDevMode:
                openSecurityCodeDialog();
                break;
        }
        super.onClick(v);
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .execute(json, WifiUtils.server_ip, WifiUtils.server_port);
//            }
//        }
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

    private void openIpDialog(final boolean isMyIP) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_apliences_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(16);
        edtName.setFilters(FilterArray);
        if (isMyIP) {
            edtName.setText(Preferences.getAppPrefString(Preferences.KEY_IP_ADDRESS));
        } else {
            edtName.setText(addressGateway);

        }
        final TextInputLayout textInputLayout = (TextInputLayout) dialog.findViewById(R.id.ilayUsername);
        textInputLayout.setHint(getString(R.string.set_new_ip));
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtName.getText().toString().isEmpty()) {
//                    if (edtName.getText().toString().equalsIgnoreCase(Preferences.getAppPrefString(Preferences.KEY_IP_ADDRESS))){
//                        dialog.dismiss();
//                    }else {
                    tvIp.setText(getString(R.string.app_ip) + Preferences.getAppPrefString(Preferences.KEY_IP_ADDRESS));
                    //9714090305WIFI#192.168.2.201#Q
                    if (isMyIP) {
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "WIFI#" + edtName.getText().toString() + "#");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                                Preferences.writeSharedPreferences(Preferences.KEY_IP_ADDRESS, edtName.getText().toString());
                                tvIp.setText(edtName.getText().toString());
                            }
                        }, 1500);
                    } else {
                        addressGateway = edtName.getText().toString();
                        dialog.dismiss();
                    }
                } else {
                    Utils.showToast(ConfigrationActivity.this, getString(R.string.enter_ip));
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
    }

    private void openSecurityCodeDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_apliences_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        final TextInputLayout textInputLayout = (TextInputLayout) dialog.findViewById(R.id.ilayUsername);
        textInputLayout.setHint(getString(R.string.dev_mode));
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtName.getText().toString().equalsIgnoreCase("oob.9714#")) {
                    dialog.dismiss();
//                    if (isIp) {
//                        openIpDialog(true);
//                    } else {
                    if (Preferences.getAppPrefBool(Preferences.KEY_DEVELOPER_MODE, false)) {
                        Preferences.writeSharedPreferencesBool(Preferences.KEY_DEVELOPER_MODE, false);
                        ivDevMode.setImageResource(R.drawable.off);
                    } else {
                        Preferences.writeSharedPreferencesBool(Preferences.KEY_DEVELOPER_MODE, true);
                        ivDevMode.setImageResource(R.drawable.on);
                    }
//                    }
                } else {
                    Utils.showToast(ConfigrationActivity.this, getString(R.string.enter_currect_dev_code));
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
    }


}
