package com.oob.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.oob.OOBApplication;
import com.oob.adapter.GroupAppliancesRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entGroupAppliances;
import com.oob.tools.Constants;
import com.oob.tools.PixelUtil;
import com.oob.tools.Preferences;
import com.oob.view.EditText;

import java.util.ArrayList;
import java.util.List;

public class AppliancesGroupingActivity extends BaseActivityChirag {

    private RecyclerView rv_room;

    private GroupAppliancesRecyclerAdapter rv_adapter;

    private dbHelperOperations db;
    List<entGroupAppliances> listData = new ArrayList<>();
    private int groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);
        setToolbar();
//        setTitleName("Room");
        bindViewControls();
        initItems();
        setTitleName(getString(R.string.title_appliances_grouping));
        tvSetting.setVisibility(View.GONE);
        ivMenu.setVisibility(View.VISIBLE);
        ivOnOff.setVisibility(View.VISIBLE);
        ivOnOff.setText(getString(R.string.icon_back));
//        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "APPGRP1");
    }

    private void bindViewControls() {
        rv_room = (RecyclerView) findViewById(R.id.rv_room);
    }

    private void initItems() {
        listData = db.getAllGroups();
        rv_adapter = new GroupAppliancesRecyclerAdapter(AppliancesGroupingActivity.this, R.layout.raw_group_appliances, listData);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_room.setLayoutManager(linearLayoutManager);
        rv_room.setAdapter(rv_adapter);
        rv_room.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));
        rv_room.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int i) {
                groupId = listData.get(i).getLgroupId();
                Intent intent = new Intent(AppliancesGroupingActivity.this, AppliancesSelectionActivity.class);
                intent.putExtra("ids", listData.get(i).getDeviceIds());
                intent.putExtra("pos", (i+1));
                startActivityForResult(intent, 2);
            }

            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.tvGroupName:
                        openNameDialog(rv_adapter.getItem(position), position);
                        break;
                    case R.id.ivOnOff:
                        System.out.println("ON OFF");
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)+"GRPAPP"+ Constants.getTwoDigit((position+1))+(rv_adapter.getItem(position).getStatus() == 0 ? 1 : 0));

                        rv_adapter.getItem(position).setStatus(rv_adapter.getItem(position).getStatus() == 0 ? 1 : 0);
                        rv_adapter.notifyItemChanged(position);
                        break;
                }
                super.onItemChildClick(adapter, view, position);
            }
        });
    }

    private void openNameDialog(final entGroupAppliances item,
                                final int position) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_apliences_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        edtName.setText(item.getName());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((entGroupAppliances) rv_adapter.getData().get(position)).setName(edtName.getText().toString());
                db.updateGroup(item);
                rv_adapter.notifyDataSetChanged();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 2) {
            entGroupAppliances groupAppliances = null;
            for (int i = 0; i < listData.size(); i++) {
                if (listData.get(i).getLgroupId() == groupId) {
                    System.out.println("Result id " + data.getStringExtra("ids"));
                    listData.get(i).setDeviceIds(data.getStringExtra("ids"));
                    groupAppliances = listData.get(i);
                    break;
                }
            }
            if (groupAppliances != null) {
                db.updateGroup(groupAppliances);
                rv_adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onMenuClick(View v) {
        super.onMenuClick(v);
        PopupMenu popup = new PopupMenu(AppliancesGroupingActivity.this, v);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.grouping_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.add:
                        getData();
                        break;

                    case R.id.delete:
                        deleteDialog();
                        break;
                }

//                rv_adapter.AddData(getData(5,8));
//                Toast.makeText(RoomActivity.this, "You Clicked : " + item.getTitle(),
//                        Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        popup.show();//showing popup menu

    }

    private void deleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_delete_group);
        builder.setMessage(R.string.msg_delete_group);
        builder.setIcon(R.drawable.app_logo);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listData = new ArrayList<entGroupAppliances>();
                rv_adapter.setNewData(listData);
                rv_adapter.notifyDataSetChanged();
                db.deleteAllGroup();
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)+"MULGRPALLDEL");

            }
        });

        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setCancelable(true);
        builder.show();
    }

    private entGroupAppliances getData() {
        entGroupAppliances groupAppliances = new entGroupAppliances();
        groupAppliances.setName("Group " + (rv_adapter.getData().size() + 1));
        groupAppliances.setImage("");
        groupAppliances.setStatus(0);
        groupAppliances.setIsDelete(0);
        groupAppliances.setDeviceIds("");
        groupAppliances = db.insertGroup(groupAppliances);
        listData.add(groupAppliances);
        rv_adapter.setNewData(listData);
        return groupAppliances;
    }

    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        onbackPresh();
    }

    private void onbackPresh() {
//        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "APPGRP0");
        finish();
    }

    @Override
    public void onBackPressed() {
        onbackPresh();
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//
//            }
//        }
    }
}
