package com.oob.activity;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.oob.adapter.CurtainSetupRecyclerAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entCurtain;
import com.oob.tools.Constants;
import com.oob.tools.PixelUtil;

import java.util.ArrayList;
import java.util.List;

public class CurtainSetupActivity extends BaseActivityChirag {

    private dbHelperOperations db;

    private RecyclerView rv_rooms;

    private CurtainSetupRecyclerAdapter rv_adapter;
    List<entCurtain> curtains = new ArrayList<>();

    public int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_selection);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_curtain_setup));
        ivMenu.setVisibility(View.VISIBLE);
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();

        // showAddUserData();
    }

    private void initItems() {
        db = new dbHelperOperations(this);
        curtains = db.getListCurtain();
        rv_rooms = (RecyclerView) findViewById(R.id.rv_room);
        rv_adapter = new CurtainSetupRecyclerAdapter(CurtainSetupActivity.this, curtains);
        rv_rooms.setLayoutManager(new LinearLayoutManager(CurtainSetupActivity.this));
        rv_rooms.setAdapter(rv_adapter);
        rv_rooms.addItemDecoration(new VerticalSpaceItemDecoration(PixelUtil.dpToPx(this, 10)));
    }

    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }
    public void actionProfilePic(String action,int position) {
        this.position = position;
        Intent intent = new Intent(this, ImageCropActivity.class);
        intent.putExtra("ACTION", action);
        intent.putExtra("tag", "curtain");
        startActivityForResult(intent, Constants.REQUEST_CODE_UPDATE_PIC);
    }
    public void onMenuClick(View v) {
        PopupMenu popup = new PopupMenu(CurtainSetupActivity.this, v);
        popup.getMenuInflater().inflate(R.menu.curtain_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.addCurtain:

                        addNewCurtain();
                        break;
                }
                return true;
            }
        });

        popup.show();//showing popup menu

        super.onMenuClick(v);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_UPDATE_PIC) {
            if (resultCode == RESULT_OK) {
                String imagePath = data.getStringExtra(Constants.IntentExtras.IMAGE_PATH);
                System.out.println("imagePath " + imagePath);
                rv_adapter.updateData(imagePath,position);

//                showCroppedImage(imagePath);
//                Glide.with(getActivity()).load(imagePath).placeholder(R.drawable.bescom_logo).diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true).dontAnimate().into(ivProfile);
//                this.imagePath = imagePath;
            } else if (resultCode == RESULT_CANCELED) {

            } else {
                String errorMsg = data.getStringExtra(ImageCropActivity.ERROR_MSG);
                Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private void addNewCurtain() {
        entCurtain curtain = new entCurtain();
        curtain.setCURTAIN_NAME(getString(R.string.curtain_name));
        curtain.setROOM_NAME(getString(R.string.room_name));
        curtain.setCURTAIN_STT("1");
        curtain.setCURTAIN_Id((int) db.insertCurtain(curtain));
        rv_adapter.addCurtain(curtain);
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }
}
