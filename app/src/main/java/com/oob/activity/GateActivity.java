package com.oob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.tools.Preferences;

import java.util.Calendar;

public class GateActivity extends BaseActivityChirag implements TimePickerDialog.OnTimeSetListener {

    private dbHelperOperations db;
    private RelativeLayout layOnOff;
    private TextView tv_fromTimer1, tv_toTimer1, tv_fromTimer2, tv_toTimer2, tvPercent;
    private LinearLayout layTimer1, layTimer2, ll_days;
    private boolean isTimer1 = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gate);
        db = new dbHelperOperations(this);
        setToolbar();
        initItems();

    }

    private void initItems() {
        ivMenu.setVisibility(View.GONE);
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        tvGrouping.setVisibility(View.VISIBLE);
        tvGrouping.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        tvGrouping.setText("");
        setTitleName(getString(R.string.title_gate));
        layTimer1 = (LinearLayout) findViewById(R.id.layTimer1);
        layTimer2 = (LinearLayout) findViewById(R.id.layTimer2);
        ll_days = (LinearLayout) findViewById(R.id.ll_days);
        tv_fromTimer1 = (TextView) findViewById(R.id.tvTimer1From);
        tv_toTimer1 = (TextView) findViewById(R.id.tvTimer1to);
        tv_fromTimer2 = (TextView) findViewById(R.id.tvTimer2From);
        tv_toTimer2 = (TextView) findViewById(R.id.tvTimer2to);
        layOnOff = (RelativeLayout) findViewById(R.id.layOnOff);
        layTimer1.setOnClickListener(this);
        layTimer2.setOnClickListener(this);
        tv_fromTimer1.setText(Preferences.getAppPrefString(Preferences.KEY_GATE_TIME_ONE_A, "0000"));
        tv_toTimer1.setText(Preferences.getAppPrefString(Preferences.KEY_GATE_TIME_ONE_B, "0000"));
        tv_fromTimer2.setText(Preferences.getAppPrefString(Preferences.KEY_GATE_TIME_TWO_A, "0000"));
        tv_toTimer2.setText(Preferences.getAppPrefString(Preferences.KEY_GATE_TIME_TWO_B, "0000"));
        db = new dbHelperOperations(this);
        layOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)+"#GATE" );
            }
        });
        setupDaysClick(ll_days);
        setDaysFromBean(ll_days);
    }


    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json.replace(" ", "");
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }


    @Override
    public void onSetting() {
        super.onSetting();
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layTimer1:
                isTimer1 = true;
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false, getString(R.string.open), getString(R.string.close)
                );
//                TimePickerDialog tpd = TimePickerDialog.newInstance(
//                        this,
//                        now.get(Calendar.HOUR_OF_DAY),
//                        now.get(Calendar.MINUTE),
//                        false
//                );
                tpd.show(GateActivity.this.getFragmentManager(), "Timepickerdialog");
                break;
            case R.id.layTimer2:
                isTimer1 = false;
                Calendar now1 = Calendar.getInstance();
                TimePickerDialog tpd1 = TimePickerDialog.newInstance(
                        this,
                        now1.get(Calendar.HOUR_OF_DAY),
                        now1.get(Calendar.MINUTE),
                        false,  getString(R.string.open), getString(R.string.close)
                );
//                Calendar now1 = Calendar.getInstance();
//                TimePickerDialog tpd1 = TimePickerDialog.newInstance(
//                        this,
//                        now1.get(Calendar.HOUR_OF_DAY),
//                        now1.get(Calendar.MINUTE),
//                        false
//                );

                tpd1.show(GateActivity.this.getFragmentManager(), "Timepickerdialog");
                break;
        }
        super.onClick(v);
    }




    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        int hour, endHour;
        String ampm, endAMPM, FromDate, ToDate, timer;

        System.out.println("hourOfDay " + hourOfDay);
        System.out.println("minute " + minute);
        System.out.println("hourOfDayEnd " + hourOfDayEnd);
        System.out.println("minuteEnd " + minuteEnd);
        if (hourOfDay > 12) {
            hour = hourOfDay - 12;
            ampm = "pm";
        } else {
            hour = hourOfDay;
            ampm = "am";
        }
        if (hourOfDayEnd > 12) {
            endHour = hourOfDayEnd - 12;
            endAMPM = "pm";
        } else {
            endHour = hourOfDayEnd;
            endAMPM = "am";
        }
        FromDate = hour + ":" + minute + " " + ampm;
        ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
        if (isTimer1) {
            timer = "T1";
            Preferences.writeSharedPreferences(Preferences.KEY_CUR_TIME_ONE_A, FromDate);
            Preferences.writeSharedPreferences(Preferences.KEY_CUR_TIME_ONE_B, ToDate);

            tv_fromTimer1.setText(FromDate);
            tv_toTimer1.setText(ToDate);
        } else {
            timer = "T2";
            Preferences.writeSharedPreferences(Preferences.KEY_CUR_TIME_TWO_A, FromDate);
            Preferences.writeSharedPreferences(Preferences.KEY_CUR_TIME_TWO_B, ToDate);

            tv_fromTimer2.setText(FromDate);
            tv_toTimer2.setText(ToDate);
        }
    }


    private void setupDaysClick(final LinearLayout ll_days) {
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View view = ll_days.getChildAt(j);
//            if (view instanceof RelativeLayout) {
//                RelativeLayout layout = ((RelativeLayout) view);
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tag = (String) v.getTag();
                        switch (tag) {
                            case "0":
//                                ((TextView) v).setTypeface(null, Typeface.BOLD);
//                                ((TextView) v).setTextColor(Color.CYAN);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_active_bg);
                                v.setTag("1");
                                break;
                            case "1":
//                                ((TextView) v).setTextColor(Color.BLACK);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_bg);
                                v.setTag("0");
                                break;
                        }
                        boolean checked = ((Character) Preferences.getAppPrefString(Preferences.KEY_GATE_DAYS, "00000000").charAt(7))
                                .toString()
                                .equals("1");
                        Preferences.writeSharedPreferences(Preferences.KEY_GATE_DAYS, getFinalDays(checked, ll_days));


                    }
                });
            }
//            }
        }
    }

    private void setDaysFromBean(LinearLayout ll_days) {
        String days = Preferences.getAppPrefString(Preferences.KEY_CUR_DAYS, "00000000");
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            String tag = ((Character) days.charAt(j)).toString();
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setTag(tag);
                switch (tag) {
                    case "0":
                        ((com.oob.view.TextView) v).setBackgroundResource(R.drawable.text_bg);
                        break;
                    case "1":
                        ((com.oob.view.TextView) v)
                                .setBackgroundResource(R.drawable.text_active_bg);
                        break;
                }
            }
        }
    }

    private String getFinalDays(boolean isChecked, LinearLayout ll_days) {
        String daysStatus = getSelectedDays(ll_days);
        if (isChecked) {
            daysStatus = daysStatus + "1";
        } else {
            daysStatus = daysStatus + "0";
        }

        return daysStatus;
    }

    private String getSelectedDays(LinearLayout ll_days) {
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                builder.append((String) v.getTag());
            }

        }
        return builder.toString();
    }


}
