package com.oob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.oob.app.R;
import com.oob.tools.Constants;
import com.oob.view.VerticalSeekBar;
import com.squareup.picasso.Picasso;

import java.io.File;

public class CurtainStatSelectionActivity extends BaseActivityChirag {
    private VerticalSeekBar seekBar;
    private View viewBottom;
    private ImageView viewTop;
    private String imagePath = "";
    private String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curtain_state_selection);
        setToolbar();
        initItems();

    }

    private void initItems() {
        ivMenu.setVisibility(View.GONE);
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        tvGrouping.setVisibility(View.VISIBLE);
        tvGrouping.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        tvGrouping.setText(R.string.done);
        viewTop = (ImageView) findViewById(R.id.viewTop);
        viewBottom = (View) findViewById(R.id.viewBottom);
        seekBar = (VerticalSeekBar) findViewById(R.id.seekBar);
        txtTitle.setText(R.string.title_state_selection);
        imagePath = getIntent().getExtras().getString("path");
        if (!imagePath.equalsIgnoreCase("")) {
            Picasso.with(this)
                    .load(new File(imagePath))
                    .placeholder(R.drawable.app_logo).error(R.drawable.app_logo)
//                    .skipMemoryCache(false)
                    .into(viewTop);
        } else {
            Picasso.with(this)
                    .load(R.drawable.app_logo)
                    .placeholder(R.drawable.app_logo).error(R.drawable.app_logo)
//                    .skipMemoryCache(false)
                    .into(viewTop);
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setViewWeight(viewTop, (100 - progress));
                setViewWeight(viewBottom, progress);
                System.out.println(" prograss " + seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                System.out.println("onStartTrackingTouch");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                System.out.println("onStopTrackingTouch");
                data = Constants.getTwoDigit(100 - seekBar.getProgress()) + "%";
                System.out.println("DATA VALUE "+data);
            }
        });
    }

    private void setViewWeight(View viewTop, float weight) {
        // Get params:
        LinearLayout.LayoutParams loparams = (LinearLayout.LayoutParams) viewTop.getLayoutParams();

        // Set only target params:
        loparams.height = 0;
        loparams.weight = weight;
        viewTop.setLayoutParams(loparams);
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

    @Override
    public void onGrouping(View v) {

        Intent intent = new Intent();
        intent.putExtra("data", data);
        setResult(RESULT_OK, intent);
        finish();
        super.onGrouping(v);
    }

    @Override
    public void onSetting() {

        Intent intent = new Intent();
        intent.putExtra("data", data);
        setResult(RESULT_OK, intent);
        finish();
        super.onSetting();
    }
}
