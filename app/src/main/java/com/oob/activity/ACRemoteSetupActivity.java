package com.oob.activity;

import android.os.Bundle;
import android.view.View;

import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entAC;
import com.oob.view.Button;
import com.oob.view.EditText;

public class ACRemoteSetupActivity extends BaseActivityChirag {

    private dbHelperOperations db;

    private EditText edtOnOff, edtLeft,  edtRight, edtTemp, edtPlus, edtMinus, edtFan, edtMode,
            edtSwing, edtTurbo, edtSensor;

    private Button btnSave;


    public int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ac_remote_setup);
        id = getIntent().getExtras().getInt("id");
        setToolbar();
        setTitleName(getString(R.string.title_ac_remote_setup));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();

        // showAddUserData();
    }

    private void initItems() {
        db = new dbHelperOperations(this);
        entAC ac = db.getACData(id);

        btnSave = (Button) findViewById(R.id.btnSaveData);
        btnSave.setOnClickListener(this);
        edtOnOff = (EditText) findViewById(R.id.edtACOnOff);
        edtLeft = (EditText) findViewById(R.id.edtLeft);
        edtRight = (EditText) findViewById(R.id.edtRight);
        edtTemp = (EditText) findViewById(R.id.edtTemp);
        edtPlus = (EditText) findViewById(R.id.edtPlus);
        edtMinus = (EditText) findViewById(R.id.edtMinus);
        edtFan = (EditText) findViewById(R.id.edtFan);
        edtMode = (EditText) findViewById(R.id.edtMode);
        edtSwing = (EditText) findViewById(R.id.edtSwing);
        edtTurbo = (EditText) findViewById(R.id.edtTurbo);
        edtSensor = (EditText) findViewById(R.id.edtSensor);

        setCurrentData(ac);


    }

    private void setCurrentData(entAC ac) {
        edtOnOff.setText(ac.getAC_STT());
        edtLeft.setText(ac.getAC_LEFT());
        edtRight.setText(ac.getAC_RIGHT());
        edtTemp.setText(ac.getAC_TEMP());
        edtPlus.setText(ac.getAC_PLUS());
        edtMinus.setText(ac.getAC_MINUS());
        edtFan.setText(ac.getAC_FAN());
        edtMode.setText(ac.getAC_MODE());
        edtSwing.setText(ac.getAC_SWING());
        edtTurbo.setText(ac.getAC_TURBO());
        edtSensor.setText(ac.getAC_SENSOR());

    }


    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnSaveData:
                entAC ac = new entAC();
                ac.setAC_STT(edtOnOff.getText().toString());
                ac.setAC_LEFT(edtLeft.getText().toString());
                ac.setAC_RIGHT(edtRight.getText().toString());
                ac.setAC_TEMP(edtTemp.getText().toString());

                ac.setAC_PLUS(edtPlus.getText().toString());
                ac.setAC_MINUS(edtMinus.getText().toString());
                ac.setAC_FAN(edtFan.getText().toString());
                ac.setAC_MODE(edtMode.getText().toString());
                ac.setAC_SWING(edtSwing.getText().toString());
                ac.setAC_TURBO(edtTurbo.getText().toString());
                ac.setAC_SENSOR(edtSensor.getText().toString());

                ac.setAC_Id(id);
                db.updateACRemoteValue(ac);
                finish();

        }
    }
}
