package com.oob.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.tools.BroadcastConstant;
import com.oob.view.TextView;

import java.io.File;

public class SetupActivity extends BaseActivityChirag {

    //    private MainFragmentOrganizer mainFragmentOrganizer;
    dbHelperOperations db;

    private TextView tvRoomSelection,  tvTvSetup, tvAcSetup,
            tvCurtain;


    private File mFileTemp;

    public static final String TEMP_PASS_FILE_NAME = "pass.txt";

    private BroadcastReceiver broadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        db = new dbHelperOperations(this);
        setToolbar();
        setTitleName(getString(R.string.title_setup));
        ivOnOff.setText(getString(R.string.icon_back));
        tvSetting.setVisibility(View.GONE);
        initItems();
        broadcastListener();

    }

    private void initItems() {
        tvRoomSelection = (TextView) findViewById(R.id.tvRoomSelect);
        tvRoomSelection.setOnClickListener(this);
        tvTvSetup = (TextView) findViewById(R.id.tvSetup);
        tvTvSetup.setOnClickListener(this);
        tvAcSetup = (TextView) findViewById(R.id.acSetup);
        tvAcSetup.setOnClickListener(this);
        tvCurtain = (TextView) findViewById(R.id.curtainSetup);
        tvCurtain.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.tvRoomSelect:
                intent = new Intent(this, RoomSelectionActivity.class);
                startActivity(intent);
                break;

            case R.id.tvSetup:
                intent = new Intent(this, TVSetupActivity.class);
                startActivity(intent);
                break;
            case R.id.acSetup:
                intent = new Intent(this, ACSetupActivity.class);
                startActivity(intent);
                break;
            case R.id.curtainSetup:
                intent = new Intent(this, CurtainSetupActivity.class);
                startActivity(intent);
                break;

        }
        super.onClick(v);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcast, intentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcast);
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE)
                            .equalsIgnoreCase("update")) {
                        dataChangeAlert();
                    }

                }
            }
        };
    }

    public void dataChangeAlert() {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                SetupActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle(getString(R.string.title_data_change));

        // Setting Dialog Message
//        alertDialog.setMessage(getString(R.string.msg_data_change));

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog
                .setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onOffClick(View v) {
        super.onOffClick(v);
        finish();
    }

}
