package com.oob.network;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.oob.activity.LoginActivity;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class Server {

    Context activity;

    ServerSocket serverSocket;

    String message = "";

    String messageKey = "";

    static final int socketServerPORT = 8080;

    private boolean isStoped;

    Handler mHandler;
    Handler mHandlerSync;

    Runnable mUpdateResults;
    Runnable mUpdateSync;
    private boolean isSync = false;
    int count, total = 1;

    public Server(final Context activity) {
        this.activity = activity;
        System.out.println("thread started");
        Thread socketServerThread = new Thread(new SocketServerThread());
        socketServerThread.start();
        mHandler = new Handler();
        mHandlerSync = new Handler();

        mUpdateResults = new Runnable() {
            public void run() {
                isSync = false;
                if (Preferences.getAppPrefBool(Preferences.KEY_DEVELOPER_MODE, false)) {
                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                }
                String msg = message
                        .replace(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE), "");
                if (msg.startsWith("WLP")) {
//                    9714090305WLP02000Q
                    messageKey = "water";
                    String progressML = msg.substring(3, msg.length() - 1);
                    String progress = String.valueOf(Long.parseLong(progressML) / 1000);
                    if (!Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS)
                            .equalsIgnoreCase("") && !Preferences
                            .getAppPrefString(Preferences.KEY_HIGH_PROGRESS_ML)
                            .equalsIgnoreCase("")) {
                        int diff = Integer.parseInt(
                                Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS))
                                - Integer.parseInt(progress);
                        long diff_ml = Long.parseLong(
                                Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS_ML))
                                - Long.parseLong(progressML);
                        if (diff > 0) {
                            if (!Preferences.getAppPrefString(Preferences.KEY_TOTAL_USED)
                                    .equalsIgnoreCase("")) {
                                Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED,
                                        (Integer.parseInt(Preferences
                                                .getAppPrefString(Preferences.KEY_TOTAL_USED))
                                                + diff) + "");
                            } else {
                                Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED,
                                        diff + "");
                            }
                        }
                        if (diff_ml > 0) {
                            if (!Preferences.getAppPrefString(Preferences.KEY_TOTAL_USED_ML)
                                    .equalsIgnoreCase("")) {
                                Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED_ML,
                                        (Long.parseLong(Preferences
                                                .getAppPrefString(Preferences.KEY_TOTAL_USED_ML))
                                                + diff_ml) + "");
                            } else {
                                Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED_ML,
                                        diff_ml + "");
                            }
                        }
                    }
                    Preferences.writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS, progress);
                    Preferences
                            .writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS_ML, progressML);

                } else if (msg.startsWith("FLATWLP")) {
//                    FLATWLPA10199999,A10299999,A10399999Q
                    messageKey = "flat_water";
                    String msgFlat = msg.substring(7, msg.length() - 1);
                    System.out.println("msg flat " + msgFlat);
                    Preferences.writeSharedPreferences(Preferences.KEY_FLAT_WATER, msgFlat);

                } else if (msg.contains("ALARM")) {
                    System.out.println("CAll ALARM");
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    Ringtone r = RingtoneManager.getRingtone(activity, notification);
                    r.play();
                } else if (msg.contains("PHONE")) {
//               SSU010R01M01PHONEQ
                    System.out.println("CAll PHONE");
                    String sensor = msg.substring(2, 3);
                    String sensorNumer = msg.substring(3, 6);
                    generateNotification(Utils.getSensorName(sensor, sensorNumer));
                } else if (msg.startsWith("SYNC")) {
//               17/03/201705:08:00AM-R01M01C1D000RX61Q
                    //9714090305SDCOUNT00020Q
                    isSync = true;
                    msg = msg.replace("SYNC", "");
                    if (msg.contains("SDCOUNT")) {
                        total = 0;
                        count = 0;
                        msg = msg.replace(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "SDCOUNT", "");
                        msg = msg.replace("SDCOUNT", "");
                        total = Integer.parseInt(msg.replace("Q", ""));
                        System.out.println("total > " + total);
                    } else {
                        count++;
                        dbHelperOperations dbHelperOperations = new dbHelperOperations(activity);
                        String msgArray[] = msg.split("-");
                        int roomId = Utils.getRoomId(msgArray[1]);
                        String module = Utils.getModule(msgArray[1]);
                        int deviceId = dbHelperOperations
                                .getDeviceIdFromModule(roomId, module);
                        int state = 0;
                        try {
                            state = Integer.parseInt(msgArray[1].charAt(7) + "");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dbHelperOperations.insertLogValue(roomId, deviceId, msgArray[0], state);
                    }
                    return;
                } else if (msg.startsWith("RTCTIME")) {
                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                } else {
                    dbHelperOperations dbHelperOperations = new dbHelperOperations(activity);
                    if (msg.startsWith("UPDATE")) {
//                    R01M01A0B00C0D0E0F0,
//                    R01M02A0B00C0D0E0F0
//                    R01M03A0B00C0D0E0F0  All module string
//                    R01M01A0000001B0000001C0000001D0000001E0000001F0000001G0000011H0000011I0000011J0000011

                        //R01M03A1000001B0000001C0D00001D0000021E1000021F0000021G0000011H0000011I0000011J0000011Q
                        //R01M02A0000011B0000011C0000011D0000011E0000011F0000011G0000011H0000011I0000011J0000011
                        messageKey = "update";
                        msg = msg.replace("UPDATE", "");
                        String msgArray[] = msg.split(",");
                        for (int i = 0; i < msgArray.length; i++) {
                            String str = msgArray[i];
                            int roomId = Utils.getRoomId(str);
                            if (roomId != -1) {
                                System.out.println("STR>>> " + str);
                                int subModule = getNumberFromSubString(str, 4, 6);
                                // device 1.
                                int m1 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(6) + "");
                                int onOff1 = getNumberFromString(str, 7);
                                int cl = getNumberFromString(str, 8);
                                int dim = getNumberFromSubString(str, 9, 12);
                                int rgb = getNumberFromString(str, 12);
                                int buz = getNumberFromString(str, 13);

                                updateDevice(dbHelperOperations, roomId, m1 + "," + subModule,
                                        onOff1,
                                        cl, dim, rgb, buz);
                                // device 2.
                                int m2 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(14) + "");
                                int onOff2 = getNumberFromString(str, 15);
                                int cl2 = getNumberFromString(str, 16);
                                int dim2 = getNumberFromSubString(str, 17, 20);
                                int rgb2 = getNumberFromString(str, 20);
                                int buz2 = getNumberFromString(str, 21);
                                updateDevice(dbHelperOperations, roomId, m2 + "," + subModule,
                                        onOff2, cl2, dim2, rgb2, buz2
                                );
                                // device 3.
                                int m3 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(22) + "");
                                int onOff3 = getNumberFromString(str, 23);
                                int cl3 = getNumberFromString(str, 24);
                                int dim3 = getNumberFromSubString(str, 25, 28);
                                int rgb3 = getNumberFromString(str, 28);
                                int buz3 = getNumberFromString(str, 29);
                                updateDevice(dbHelperOperations, roomId, m3 + "," + subModule,
                                        onOff3, cl3, dim3, rgb3,
                                        buz3);
                                // device 4.
                                int m4 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(30) + "");
                                int onOff4 = getNumberFromString(str, 31);
                                int cl4 = getNumberFromString(str, 32);
                                int dim4 = getNumberFromSubString(str, 33, 36);
                                int rgb4 = getNumberFromString(str, 36);
                                int buz4 = getNumberFromString(str, 37);
                                updateDevice(dbHelperOperations, roomId, m4 + "," + subModule,
                                        onOff4, cl4, dim4, rgb4,
                                        buz4);
                                // device 5.
                                int m5 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(38) + "");
                                int onOff5 = getNumberFromString(str, 39);
                                int cl5 = getNumberFromString(str, 40);
                                int dim5 = getNumberFromSubString(str, 41, 44);
                                int rgb5 = getNumberFromString(str, 44);
                                int buz5 = getNumberFromString(str, 45);
                                updateDevice(dbHelperOperations, roomId, m5 + "," + subModule,
                                        onOff5, cl5, dim5, rgb5,
                                        buz5);
                                // device 6.
                                int m6 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(46) + "");
                                int onOff6 = getNumberFromString(str, 47);
                                int cl6 = getNumberFromString(str, 48);
                                int dim6 = getNumberFromSubString(str, 49, 52);
                                int rgb6 = getNumberFromString(str, 52);
                                int buz6 = getNumberFromString(str, 53);
                                updateDevice(dbHelperOperations, roomId, m6 + "," + subModule,
                                        onOff6, cl6, dim6, rgb6,
                                        buz6);
                                if (str.length() > 61) {
                                    // device 7.
                                    int m7 = (Arrays.asList(Constants.DEVICES))
                                            .indexOf(str.charAt(54) + "");
                                    int onOff7 = getNumberFromString(str, 55);
                                    int cl7 = getNumberFromString(str, 56);
                                    int dim7 = getNumberFromSubString(str, 57, 60);
                                    int rgb7 = getNumberFromString(str, 60);
                                    int buz7 = getNumberFromString(str, 61);
                                    updateDevice(dbHelperOperations, roomId, m7 + "," + subModule,
                                            onOff7, cl7, dim7, rgb7,
                                            buz7);
                                }
                                if (str.length() >69) {
                                    // device 8.
                                    int m8 = (Arrays.asList(Constants.DEVICES))
                                            .indexOf(str.charAt(62) + "");
                                    int onOff8 = getNumberFromString(str, 63);
                                    int cl8 = getNumberFromString(str, 64);
                                    int dim8 = getNumberFromSubString(str, 65, 68);
                                    int rgb8 = getNumberFromString(str, 68);
                                    int buz8 = getNumberFromString(str, 69);
                                    updateDevice(dbHelperOperations, roomId, m8 + "," + subModule,
                                            onOff8, cl8, dim8, rgb8,
                                            buz8);
                                }
                                if (str.length() > 77) {
                                    // device 9.
                                    int m9 = (Arrays.asList(Constants.DEVICES))
                                            .indexOf(str.charAt(70) + "");
                                    int onOff9 = getNumberFromString(str, 71);
                                    int cl9 = getNumberFromString(str, 72);
                                    int dim9 = getNumberFromSubString(str, 73, 76);
                                    int rgb9 = getNumberFromString(str, 76);
                                    int buz9 = getNumberFromString(str, 77);
                                    updateDevice(dbHelperOperations, roomId, m9 + "," + subModule,
                                            onOff9, cl9, dim9, rgb9,
                                            buz9);
                                }
                                if (str.length() > 85) {
                                    // device 10.
                                    int m10 = (Arrays.asList(Constants.DEVICES))
                                            .indexOf(str.charAt(78) + "");
                                    int onOff10 = getNumberFromString(str, 79);
                                    int cl10 = getNumberFromString(str, 80);
                                    int dim10 = getNumberFromSubString(str, 81, 84);
                                    int rgb10 = getNumberFromString(str, 84);
                                    int buz10 = getNumberFromString(str, 85);
                                    updateDevice(dbHelperOperations, roomId, m10 + "," + subModule,
                                            onOff10, cl10, dim10, rgb10,
                                            buz10);
                                }
                            }
                        }
                    } else {

                        // for ON/OFF >> R01M01A0D009RX01Q
//                        // for child lock >>> R01M01CCL1Q
                        System.out.println("msg " + msg);
                        messageKey = "device";
                        int roomId = Utils.getRoomId(msg);
                        if (roomId != -1) {
                            String module = Utils.getModule(msg);
                            System.out.println("ROOM ID " + roomId);
                            System.out.println("Module ID " + module);
                            entDevice device = dbHelperOperations
                                    .getDeviceFromModule(roomId, module);
//                        System.out.println("ID "+device.getID());
                            System.out.println("MMMM " + message);
                            if (msg.substring(7, 9).equalsIgnoreCase("CL")) {
                                device.setCL(getNumberFromString(message, 19));
                            } else {
                                device.setSTT(getNumberFromString(message, 17));
                                device.setDIM(getNumberFromSubString(message, 19, 22));
                            }
                            dbHelperOperations.updateDevice(device);
                        }
                    }
                }
                sendBroadCast();
            }

        };
        mUpdateSync = new Runnable() {
            @Override
            public void run() {
                sendBroadCastLogData(total, count);
            }
        };
    }


    private int getNumberFromString(String str, int poss) {
        try {
            System.out.println("CHAR " + str.charAt(poss));
            return Integer.parseInt(str.charAt(poss) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int getNumberFromSubString(String str, int poss1, int poss2) {
        try {
            System.out.println("SUBSTRING " + message.substring(poss1, poss2));
            return Integer.parseInt(str.substring(poss1, poss2) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void sendBroadCast() {
        if (message != null && message.length() > 0 && (String
                .valueOf(message.charAt(message.length() - 1)).equalsIgnoreCase("Q"))) {

            Intent intent = new Intent(BroadcastConstant.BROADCAST_ACTION);
            System.out.println("KEY " + messageKey);
            intent.putExtra(BroadcastConstant.MESSAGE, messageKey);
//                  intent.putExtra(BroadcastConstant.DATA_CHANGE, bean != null ? bean.getDevices() : null);
            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
        }
    }

    private void sendBroadCastLogData(int total, int count) {
        Intent intent = new Intent(BroadcastConstant.BROADCAST_ACTION_LOGDATA);
        intent.putExtra(BroadcastConstant.MESSAGE, total != count);
        intent.putExtra(BroadcastConstant.TOTAL, total);
        intent.putExtra(BroadcastConstant.COUNT, count);
        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);

    }

    private void updateDevice(dbHelperOperations dbHelperOperations, int roomId, String s,
                              int onOff, int cl, int dim, int rgb, int buz) {
        System.out.println("ROOM ID " + roomId);
        System.out.println("Module ID " + s);
        System.out.println("onOff " + onOff);
        System.out.println("cl " + cl);
        System.out.println("dim " + dim);
        System.out.println("rgb " + rgb);
        System.out.println("buz " + buz);
        entDevice device = dbHelperOperations
                .getDeviceFromModule(roomId, s);
//        System.out.println("ID "+device.getID());
//        if (device.getID() != 0) {
        device.setSTT(onOff);
        device.setCL(cl);
        device.setDIM(dim);
        device.setRGBSTT(rgb);
        device.setMIC(buz);
        dbHelperOperations.updateDevice(device);
//
    }

    private void generateNotification(String msg) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(activity)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Oob Security Alert")
                        .setContentText(msg);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(activity, LoginActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(activity);
// Adds the back stack
        stackBuilder.addParentStack(LoginActivity.class);
// Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
// Gets a PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification note = mBuilder.build();
        note.defaults |= Notification.DEFAULT_VIBRATE;
        note.defaults |= Notification.DEFAULT_SOUND;
        note.defaults |= Notification.DEFAULT_LIGHTS;
        mNotificationManager.notify(0, note);
    }

    public int getPort() {
        return socketServerPORT;
    }

    public void onDestroy() {
        if (serverSocket != null) {
            try {
                isStoped = true;
                serverSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private class SocketServerThread extends Thread {

        int count = 0;

        @Override
        public void run() {
            try {
                // create ServerSocket using specified port
                serverSocket = new ServerSocket(socketServerPORT);

                System.out.println("thread running");

                while (!isStoped) {
                    // block the call until connection is created and return
                    // Socket object
                    Log.e("TEST", "Waiting new....");
                    Socket socket = serverSocket.accept();
                    count++;
                    message = "#" + count + " from "
                            + socket.getInetAddress() + ":"
                            + socket.getPort() + "\n";
                    System.out.println("MSG " + message);
//                    mHandlerSync.removeCallbacks(mUpdateSync);
                    if (isSync) {
                        mHandlerSync.postDelayed(mUpdateSync, 2000);
                    }
//               NotifyUtils.toast(activity,message);
//               Intent intent=new Intent(BroadcastConstant.BROADCAST_ACTION);
//               intent.putExtra(BroadcastConstant.MESSAGE,message);
//               LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    SocketServerReplyThread socketServerReplyThread =
                            new SocketServerReplyThread(socket, count);
                    socketServerReplyThread.run();

                }
            } catch (
                    IOException e)

            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    private class SocketServerReplyThread extends Thread {

        private Socket hostThreadSocket;

        private int cnt;

        private BufferedReader input;

        SocketServerReplyThread(Socket socket, int c) {
            hostThreadSocket = socket;
            cnt = c;
        }

        @Override
        public void run() {
            OutputStream outputStream;
//         RoomDevice bean=null;
            String msgReply = "Hello you are my#" + cnt + " client";

            try {
                outputStream = hostThreadSocket.getOutputStream();
                PrintStream printStream = new PrintStream(outputStream);
                printStream.print(msgReply);
                //  printStream.close();

                this.input = new BufferedReader(
                        new InputStreamReader(this.hostThreadSocket.getInputStream()));

//            while (hostThreadSocket!=null&&hostThreadSocket.isConnected())
//            {
                message = input.readLine();
                System.out.println("message > " + message);

                if (message != null && message.length() > 0 && (String
                        .valueOf(message.charAt(message.length() - 1)).equalsIgnoreCase("Q"))) {
//                    System.out.println("INSIDE IF");
                    mHandler.post(mUpdateResults);
//               bean = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(jObj.toString(), RoomDevice.class);
//
//               for (DevicesEntity entity:bean.getDevices()) {
//
//                  Long save = entity.save();
//                  Log.d("TEST","SAVE:"+save);
//               }
                } else {
                    message = activity.getString(R.string.error_wrong);
                }

//            }

//            byte[] messageByte = new byte[1000];
//            boolean end = false;
//            String dataString = "";
//
//            try
//            {
//               DataInputStream in = new DataInputStream(hostThreadSocket.getInputStream());
//
//               while(!end)
//               {
//                  int bytesRead = in.read(messageByte);
//                  message = new String(messageByte, 0, bytesRead);
//                  System.out.println("MSG 2 "+message);
////                  intent.putExtra(BroadcastConstent.MESSAGE,message);
////                  LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
//                  if (message.length() == 10)
//                  {
//                     end = true;
//                  }
//               }
//
//            }
//            catch (Exception e)
//            {
//               e.printStackTrace();
//            }
//

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                message = activity.getString(R.string.error_wrong);
            } finally {
//                System.out.println("Finaly " + message);

            }
        }

    }
}