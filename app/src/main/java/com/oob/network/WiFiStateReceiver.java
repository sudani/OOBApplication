package com.oob.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;

import com.oob.SocketServerTask;
import com.oob.tools.WifiUtils;

public class WiFiStateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, Intent intent) {

        System.out.println("STATE RECEIVE");
//            System.out.println("data "+intent.getExtras().getString("data"));
        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled()) {

            if (!WifiUtils.hasEnabling) {
                //  Toast.makeText(context, "WiFi Enabled", Toast.LENGTH_SHORT).show();

                WifiUtils.hasEnabling = true;
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (info != null) {
                    if (info.isAvailable()) {
                        //Do your work.

                        //To check the Network Name or other info:
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                                String ssid = wifiInfo.getSSID();
                                String myip = WifiUtils.getIPAddress(true);
                                //  NotifyUtils.toast(context, ssid);
                                //  NotifyUtils.toast(context, myip);
//                                if (ssid.contains(WifiUtils.ssid))
                                System.out.println("MY IP "+myip);
                                    new SocketServerTask(context, null).execute(WifiUtils.encodeToServerString("CLIENTIP=" + myip), WifiUtils.server_ip, WifiUtils.server_port);

//                                        else
//                                            WifiUtils.connectToSSID(wifiManager, WifiUtils.ssid, WifiUtils.key);
                            }
                        }, 10);

                    }
//                        else
//                            WifiUtils.connectToSSID(wifiManager, WifiUtils.ssid, WifiUtils.key);
                }
            }

        } else {
            //               wifiManager.setWifiEnabled(true);
            if (WifiUtils.hasEnabling) {
                // Toast.makeText(context, "WiFi Disabled", Toast.LENGTH_SHORT).show();
                WifiUtils.hasEnabling = false;
            }
        }

    }
}