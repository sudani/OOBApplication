package com.oob.network;

import com.oob.tools.Utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by pranav on 16/1/16.
 */
public class HostService extends Service {

    private Server server;

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("service created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("service started");
        server=new Server(this);
        //server.getIpAddress();
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("service stopped");
        Utils.backupDatabase();
        Utils.backupSharedPreferencesToFile(this);
        //NotifyUtils.toast(this,"service stopped");
        server.onDestroy();
    }
}
