package com.oob.interfaces;

import com.oob.entity.entDevice;

import java.util.List;

public interface OnCustomerListChangedListener {
    void onNoteListChanged(List<entDevice> customers);
}