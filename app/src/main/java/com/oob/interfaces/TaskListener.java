package com.oob.interfaces;

public interface TaskListener<T> {
   void onTaskComplete(T result);
}