package com.oob.tools;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.oob.app.R;

/**
 * Created by pranav on 22/1/16.
 */
public class DialogUtils
{
    public static void showAlert(final Context mContext, String title, String message){
        AlertDialog alertDialog = new AlertDialog.Builder(
                mContext).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
      //  alertDialog.setMessage(message);

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                NotifyUtils.toast(mContext, "Successfully updated");
            }
        });


        // Showing Alert Message
        alertDialog.show();
    }
    public static void showAlertDialog(final Context mContext, String title, String message){
        AlertDialog alertDialog = new AlertDialog.Builder(
                mContext).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        // Showing Alert Message
        alertDialog.show();
    }
    public static void showUserDataAlerct(final Context mContext,String title){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_apliences_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        lp.width = size.x;
        lp.height = size.y;

        window.setAttributes(lp);
        // set values for custom dialog components - text, image and button
        ((TextView)dialog.findViewById(R.id.tv_title)).setText(title);
        final EditText et_udata = (EditText) dialog.findViewById(R.id.edtName);


        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.btn_close);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String udata=et_udata.getText().toString();
                if(!udata.isEmpty())
                {
                    Preferences.writeSharedPreferences(Preferences.KEY_DIALOG_USERDATA,udata);
                    dialog.dismiss();
                }
                else
                    NotifyUtils.toast(mContext, "Please enter something..");
            }
        });
    }
}
