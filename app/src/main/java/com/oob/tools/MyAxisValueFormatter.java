package com.oob.tools;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class MyAxisValueFormatter implements IAxisValueFormatter {

    private DecimalFormat mFormat;
    String block[];
    String value[];

    public MyAxisValueFormatter() {
        mFormat = new DecimalFormat("##.##");

    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
//        System.out.println("Value "+value);
        if (block.length == value)
            return block[(block.length) - 1];
        else if (block.length < value)
            return block[(block.length) - 1];
        else
            return block[(int) value];

    }

    public void setData(String[] block) {
        this.block = block;
    }

    public void setDataValue(String[] value) {
        this.value = value;
    }

    public String getValueUsage(int pos) {
        return value[pos];
    }
}
