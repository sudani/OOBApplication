package com.oob.tools;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class TimeValueFormatter implements IAxisValueFormatter {

    private DecimalFormat mFormat;
    String unit[] = {"Sec", "Min", "Hrs", "Days", "Months"};

    public TimeValueFormatter() {
        mFormat = new DecimalFormat("##.##");
//       this.unit=unit;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        if (value < 60) {
            // seconds
            return mFormat.format(value) + " " + unit[0];
        } else if (value < (60 * 1000)) {
            // minutes
            return mFormat.format(value/60) + " " + unit[1];
        } else if (value < (43200 * 1000)) {//(value < (1440 * 1000)) {
            return mFormat.format(value / (60 * 1000)) + " " + unit[2];
//        } else if (value < (43200 * 1000)) {
//            return mFormat.format(value / (1440 * 1000)) + " " + unit[3];
        } else {
            return mFormat.format(value / (43200 * 1000)) + " " + unit[4];
        }
    }
}
