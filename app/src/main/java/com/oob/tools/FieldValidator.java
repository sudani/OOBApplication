package com.oob.tools;

/**
 * Created by ViRus on 2/24/2016.
 */
public class FieldValidator
{

    public static boolean validatePassword(String enter_pass)
    {
        String saved_pass=Preferences.getAppPrefString(Preferences.KEY_PASS);
        return enter_pass.equals(saved_pass);
    }
    public static boolean validateUname(String enter_uname)
    {
        String saved_uname=Preferences.getAppPrefString(Preferences.KEY_USER);
        return enter_uname.equals(saved_uname);
    }
}
