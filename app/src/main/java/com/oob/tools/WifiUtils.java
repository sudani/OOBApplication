package com.oob.tools;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

/**
 * Created by pranav on 18/1/16.
 */
public class WifiUtils
{
    public static String server_ip =Preferences.getAppPrefString(Preferences.KEY_IP_ADDRESS);//"192.168.0.201";
    public static String server_port ="80";
    public static String ssid="Oob4M001";
    public static String key="Ao@.01k2";
//    public static String ssid="AndroidWorkers";
//    public static String key="35718502";
    public static boolean hasEnabling =false;

    public static void connectToSSID(WifiManager wifiManager,String ssid, String key) {
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.status=WifiConfiguration.Status.ENABLED;
        wifiConfig.SSID = String.format("\"%s\"", ssid);
        wifiConfig.preSharedKey = String.format("\"%s\"", key);

//remember id
        int netId = wifiManager.updateNetwork(wifiConfig);
        if(netId==-1)
           netId = wifiManager.addNetwork(wifiConfig);

        if(netId!=-1)
        {
            wifiManager.disconnect();
            wifiManager.enableNetwork(netId, true);
            wifiManager.reconnect();
        }

    }


    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) { } // for now eat exceptions
        return "";
    }
    public static String encodeToServerString(String data)
    {
        return "GET /?"+data+" HTTP/1.1\r\n\r\n";
       // return "/?"+data;
    }public static String encodeToServerStringClient(String data)
    {
        return "/?"+data;
       // return "/?"+data;
    }
}
