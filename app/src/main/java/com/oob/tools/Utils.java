package com.oob.tools;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.oob.database.dbHelperOperations;
import com.oob.view.EditText;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by chirag.sudani on 5/13/2016.
 */
public class Utils {

    public static Uri getImageUri(String path) {
        return Uri.fromFile(new File(path));
    }

    public static byte[] getBytesFromPath(String imagePath) {
        Bitmap src = BitmapFactory.decodeFile(imagePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        src.compress(Bitmap.CompressFormat.PNG, 50, baos);
        return baos.toByteArray();
    }

    public static Bitmap ByteToBitmap(byte[] profile_data) {

        return BitmapFactory.decodeByteArray(profile_data, 0, profile_data.length);
    }

    public static void hideSoftKeyboard(Activity context) {
        if (context.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context
                    .getSystemService(context.INPUT_METHOD_SERVICE);
            inputMethodManager
                    .hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void writePassword(String pass, File file) {
        try {
            File myFile = file;
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(pass);
            myOutWriter.close();
            fOut.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ProgressDialog showProgress(Context activity, String message, boolean cancelable) {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
        return progressDialog;
    }
    /**
     * Check is internet is available or not.
     *
     * @param context
     * @return
     */
    public static final boolean isNetworkAvailable(Context context) {
        boolean connected = false;
        if (context == null) {
            return connected;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nfMobiel = connectivityManager.getNetworkInfo(connectivityManager.TYPE_MOBILE);
        NetworkInfo nfWifi = connectivityManager.getNetworkInfo(connectivityManager.TYPE_WIFI);
        NetworkInfo nf = connectivityManager.getActiveNetworkInfo();
        if (nfMobiel!=null&& nfMobiel.isConnected()){
            return true;
        }else if (nfWifi!=null && nfWifi.isConnected()){
//            return isOnline();
            return true;
        }
//        if (nf != null && (nf.isConnected() )) { //|| nf.isConnectedOrConnecting()
//           return isOnline();// connected = true;
//        } else
//            connected = false;
        return connected;
    }
    public static boolean isOnline() {
        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal==0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        try {
//            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
//            urlc.setRequestProperty("User-Agent", "Test");
//            urlc.setRequestProperty("Connection", "close");
//            urlc.setConnectTimeout(1500);
//            urlc.connect();
//            return (urlc.getResponseCode() == 200);
//        } catch (IOException e) {
//            Log.e("INTERNET", "Error checking internet connection", e);
//        }
        return false;
    }
    public static boolean netCheckin(Context context) {
        boolean isEnabled;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED) {
            isEnabled = true;
        } else {
            isEnabled = false;
        }
        Log.e("IS ENABLE ", "IS " + isEnabled);
        return isEnabled;

    }

    public static String readPassword(File file) {
        String pass = "";
        try {

            File myFile = file;
            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(fIn));
            String aDataRow = "";
            String aBuffer = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow;
            }
            pass = aBuffer;
            myReader.close();
            System.out.println("pass  " + pass);
            return pass;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return pass;
    }

    public static void showToast(Context settingsActivity, String s) {
        Toast.makeText(settingsActivity, s, Toast.LENGTH_LONG).show();
    }

    public static String convertToSeconds(int mHour, int mMinute) {
        long sec = mHour * 60 * 60;
        sec = sec + mMinute * 60;

        return String.valueOf(sec);
    }

    public static String convertToHourMinutSecond(long seconds) {
//        seconds = seconds * 1000;
        int hours = (int) ((seconds / (1000 * 60 * 60)));
        int minutes = (int) ((seconds / (1000 * 60)) % 60);
        int secs = (int) ((seconds / 1000) % 60);

        return Constants.getTwoDigit(hours) + ":" + Constants.getTwoDigit(minutes) + ":" + Constants
                .getTwoDigit(secs);
    }

    public static String getSensorCode(String sensor_name, String sensor_number) {
        String preFix = "", suFix = "";

        if (sensor_name.contains("MOTION")) {
            preFix = "U";
        } else if (sensor_name.contains("LIGHT")) {
            preFix = "V";
        } else if (sensor_name.contains("FIRE")) {
            preFix = "W";
        } else if (sensor_name.contains("CLAP")) {
            preFix = "X";
        } else if (sensor_name.contains("TEMPERATURE")) {
            preFix = "Y";
        } else if (sensor_name.contains("LPG")) {
            preFix = "Z";
        } else if (sensor_name.contains("GLASS")) {
            preFix = "T";
        } else if (sensor_name.contains("DOOR")) {
            preFix = "P";
        }
        if (sensor_number == null || sensor_number.equalsIgnoreCase("") || sensor_number
                .equalsIgnoreCase("1")) {
            suFix = "100";
        } else if (sensor_number.equalsIgnoreCase("2")) {
            suFix = "010";
        } else if (sensor_number.equalsIgnoreCase("3")) {
            suFix = "001";
        }
        return preFix + suFix;
    }

    public static String getSensorName(String later, String sensor_number) {
        String preFix = "", suFix = "";

        if (later.equalsIgnoreCase("U")) {
            preFix = "MOTION SENSOR";
        } else if (later.equalsIgnoreCase("V")) {
            preFix = "LIGHT SENSOR";
        } else if (later.equalsIgnoreCase("W")) {
            preFix = "FIRE SENSOR";
        } else if (later.equalsIgnoreCase("X")) {
            preFix = "CLAP SENSOR";
        } else if (later.equalsIgnoreCase("Y")) {
            preFix = "TEMPERATURE SENSOR";
        } else if (later.equalsIgnoreCase("Z")) {
            preFix = "LPG GAS SENSOR";
        } else if (later.equalsIgnoreCase("T")) {
            preFix = "GLASS BREAK SENSOR";
        } else if (later.equalsIgnoreCase("P")) {
            preFix = "DOOR BALL";
        }
        if (sensor_number == null || sensor_number.equalsIgnoreCase("") || sensor_number
                .equalsIgnoreCase("100")) {
            suFix = " 1";
        } else if (sensor_number.equalsIgnoreCase("010")) {
            suFix = " 2";
        } else if (sensor_number.equalsIgnoreCase("001")) {
            suFix = " 3";
        }
        return preFix + suFix;
    }

    public static String getDevideCode(String code, String deviceCode) {
        if (code.equalsIgnoreCase("A")) {
            return "100000";
        } else if (code.equalsIgnoreCase("B")) {
            return "010000";
        } else if (code.equalsIgnoreCase("C")) {
            return "001000";
        } else if (code.equalsIgnoreCase("D")) {
            return "000100";
        } else if (code.equalsIgnoreCase("E")) {
            return "000010";
        } else if (code.equalsIgnoreCase("F")) {
            return "000001";
        } else {
            if (deviceCode != null) {
                if (deviceCode.equalsIgnoreCase("LED COLOR")) {
                    return "LC";
                } else if (deviceCode.equalsIgnoreCase("BUZZER")) {
                    return "BUZZ";
                } else if (deviceCode.equalsIgnoreCase("LED FLASH")) {
                    return "LF";
                } else if (deviceCode.equalsIgnoreCase("DIMMING")) {
                    return "DIM";
                } else if (deviceCode.equalsIgnoreCase("LAMP COLOR")) {
                    return "LLC";
                } else if (deviceCode.equalsIgnoreCase("ALARM")) {
                    return "ALARM";
                } else if (deviceCode.equalsIgnoreCase("PHONE ALERT")) {
                    return "PHONE";
                }
            }
        }
        return "000000";
    }

    public static int getRoomId(String msg) {
//        R01M01A0D009RX01Q
        String id = msg.substring(1, 3);
        int roomId = -1;
        try {
            roomId = Integer.parseInt(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return roomId;
    }

    public static String getModule(String msg) {
//        R01M01A0D009RX01Q
        String moduleSub = msg.substring(4, 7);//01A
        String module = moduleSub.substring(moduleSub.length() - 1, moduleSub.length());
        int m = Arrays.asList(Constants.DEVICES).indexOf(module);
        try {

            module = m + "," + Integer.parseInt(moduleSub.substring(0, 2));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("module " + module);
        return module;
    }

    public static boolean backupSharedPreferencesToFile(Context context) {
        boolean res = false;
        ObjectOutputStream output = null;
        try {
            File dst = new File(Constants.PREF_BACKUP_LOCATION);
            if (!dst.exists()) {
                dst.mkdirs();

            }

            dst = new File(Constants.PREF_BACKUP_LOCATION, Constants.PREF_BACKUP_FILE);
            output = new ObjectOutputStream(new FileOutputStream(dst));
            SharedPreferences pref =
                    context.getSharedPreferences(Preferences.PREFS_NAME, context.MODE_PRIVATE);
            output.writeObject(pref.getAll());

            res = true;
            System.out.println("backupSharedPreferencesToFile");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return res;
    }

    @SuppressWarnings({"unchecked"})
    public static boolean restoreSharedPreferencesFromFile(Context context) {
        boolean res = false;
        ObjectInputStream input = null;
        try {
            File src = new File(Constants.PREF_BACKUP_LOCATION + Constants.PREF_BACKUP_FILE);
            input = new ObjectInputStream(new FileInputStream(src));
            SharedPreferences.Editor prefEdit = context
                    .getSharedPreferences(Preferences.PREFS_NAME, context.MODE_PRIVATE).edit();
            prefEdit.clear();
            Map<String, ?> entries = (Map<String, ?>) input.readObject();
            for (Map.Entry<String, ?> entry : entries.entrySet()) {
                Object v = entry.getValue();
                String key = entry.getKey();

                if (v instanceof Boolean) {
                    prefEdit.putBoolean(key, ((Boolean) v).booleanValue());
                } else if (v instanceof Float) {
                    prefEdit.putFloat(key, ((Float) v).floatValue());
                } else if (v instanceof Integer) {
                    prefEdit.putInt(key, ((Integer) v).intValue());
                } else if (v instanceof Long) {
                    prefEdit.putLong(key, ((Long) v).longValue());
                } else if (v instanceof String) {
                    prefEdit.putString(key, ((String) v));
                }
            }
            prefEdit.commit();
            res = true;
            System.out.println("restoreSharedPreferencesFromFile");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return res;
    }

    public static void backupDatabase() {
        final String inFileName = "/data/data/com.oob.app/databases/oob.db";
        File dbFile = new File(inFileName);

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(dbFile);
            String outFileName = Constants.DATABASE_BACKUP_LOCATION;
            File dbFileOut = new File(outFileName);
            if (!dbFileOut.exists()) {
                dbFileOut.mkdirs();
            }

            // Open the empty db as the output stream
            OutputStream output = new FileOutputStream(
                    outFileName + Constants.DATABASE_BACKUP_FILE);

            // Transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }

            // Close the streams
            output.flush();
            output.close();
            fis.close();
            System.out.println("backupDatabases");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void restorDataBaseFromSdCard() {
        try {
            String DB_PATH = "/data/data/com.oob.app/databases/";
            String DB_NAME = "oob.db";

            File file = new File(Constants.DATABASE_BACKUP_LOCATION,
                    Constants.DATABASE_BACKUP_FILE);
            // Open your local db as the input stream
            InputStream myInput = new FileInputStream(file);

            // Path to created empty db
            String outFileName = DB_PATH + DB_NAME;

            // Opened assets database structure
            File f = new File(DB_PATH);
            if (!f.exists()) {
                f.mkdirs();
            }
            OutputStream myOutput = new FileOutputStream(outFileName);

            // transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            // Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
            System.out.println("DATABASE COPIED....");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void backupWaterData(Context context) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        String date = (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.YEAR);
        System.out.println("date " + date);
        dbHelperOperations db = new dbHelperOperations(context);
        if (!db.checkBackupForLastMonth(date)) {
            db.backupWaterData(date);
        }
    }

    public static long getDateTimeFromDateString(String string) {
//        String dateString = "25/03/201705:14:49PM";
//        System.out.println("DATE STRING "+string);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyyhh:mm:ssaa");
        Date date = null;
        long value;
        try {
            date = sdf.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        value = date != null ? date.getTime() : 0;
//System.out.println("Date>> "+string+  "   VALUE>>>  "+value);
        return value;
    }

    public static String getCurrentDate(Calendar calendar) {
        String date = Constants.getTwoDigit(calendar.get(Calendar.DAY_OF_MONTH)) + "/" + Constants.getTwoDigit((calendar.get(Calendar.MONTH) + 1)) + "/" + calendar.get(Calendar.YEAR);
        return date;
    }

    public static String getNextDayDate(Calendar calendar) {
        calendar.add(Calendar.DAY_OF_YEAR, +1);
        String date = Constants.getTwoDigit(calendar.get(Calendar.DAY_OF_MONTH)) + "/" + Constants.getTwoDigit((calendar.get(Calendar.MONTH) + 1)) + "/" + calendar.get(Calendar.YEAR);
        return date;
    }

    public static String getFirstDateOfWeek(Calendar calendar) {
        while (calendar.get(Calendar.DAY_OF_WEEK) > calendar.getFirstDayOfWeek()) {
            calendar.add(Calendar.DATE, -1); // Substract 1 day until first day of week.
        }
        String date = Constants.getTwoDigit(calendar.get(Calendar.DAY_OF_MONTH)) + "/" + Constants.getTwoDigit((calendar.get(Calendar.MONTH) + 1)) + "/" + calendar.get(Calendar.YEAR);
        return date;
    }

    public static String getLastDateOfWeek(Calendar calendar) {
        while (calendar.get(Calendar.DAY_OF_WEEK) > calendar.getFirstDayOfWeek()) {
            calendar.add(Calendar.DATE, -1); // Substract 1 day until first day of week.
        }
        calendar.add(Calendar.DATE, +7);
        String date = Constants.getTwoDigit(calendar.get(Calendar.DAY_OF_MONTH)) + "/" + Constants.getTwoDigit((calendar.get(Calendar.MONTH) + 1)) + "/" + calendar.get(Calendar.YEAR);
        return date;
    }

    public static String getFirstDateOfMonth(Calendar calendar) {
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        String date = Constants.getTwoDigit(calendar.get(Calendar.DAY_OF_MONTH)) + "/" + Constants.getTwoDigit((calendar.get(Calendar.MONTH) + 1)) + "/" + calendar.get(Calendar.YEAR);
        return date;
    }

    public static String getLastDateOfMonth(Calendar calendar) {
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        String date = Constants.getTwoDigit(calendar.get(Calendar.DAY_OF_MONTH)) + "/" + Constants.getTwoDigit((calendar.get(Calendar.MONTH) + 1)) + "/" + calendar.get(Calendar.YEAR);
        return date;
    }

    public static Calendar getFirstCal(Calendar calendar) {
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        return calendar;
    }

    public static Calendar getLastCal(Calendar calendar) {
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar;
    }

    public static long getLongDate(String dateFrom) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateFrom);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate.getTime();
    }

    public static String getFirstDateOfYear(Calendar calendar) {
        String date = "01/01/" + calendar.get(Calendar.YEAR);
        return date;
    }

    public static String getLastDateOfYear(Calendar calendar) {
        String date = "31/12/" + calendar.get(Calendar.YEAR);
        return date;
    }

    public static int getNumber(String s) {
        if (s != null && !s.equalsIgnoreCase("")) {
            return Integer.parseInt(s);
        }
        return 0;
    }

    public static boolean isEmptyString(String number) {
        if (number != null && !number.equalsIgnoreCase("") && number.trim().length() > 0)
            return false;
        else
            return true;
    }

    public static boolean isEmptyEditText(EditText editText) {
        if (isEmptyString(editText.getText().toString()))
            return true;
        else
            return false;
    }

    public static void showSnakBar(Activity context,String msg) {
        Snackbar.make(context.findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

    }
}
