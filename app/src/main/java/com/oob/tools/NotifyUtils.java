package com.oob.tools;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

public class NotifyUtils {

    public static void toast(Context context,String toastMessage) {

        final Toast toast = Toast.makeText(context, toastMessage, Toast.LENGTH_LONG);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 500);
    }


}