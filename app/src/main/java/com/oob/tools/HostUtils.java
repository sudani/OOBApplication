package com.oob.tools;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.oob.OOBApplication;

import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;

import static android.R.attr.data;

/**
 * Created by ViRus on 3/25/2016.
 */
public class HostUtils {
    public static boolean sendToHost(String data, String ip, String port) {
        Socket socket = null;
        DataOutputStream dataOutputStream = null;
        DataInputStream dataInputStream = null;
        try {
//         Create a new Socket instance and connect to host

            socket = new Socket(ip, Integer.parseInt(port));
            socket.setSoTimeout(100);
//            System.out.println("BUF "+socket.getSendBufferSize());
            if (socket.isConnected()) {
//                 socket.setKeepAlive(true);

                dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataInputStream = new DataInputStream(socket.getInputStream());
                // transfer JSONObject as String to the server
                dataOutputStream.write(data.getBytes());

//               BufferedReader input = new BufferedReader(dataInputStream.readByte());

                String message = dataInputStream.readLine();
                System.out.println("message " + message);

            } else {
                return false;
            }

        } catch (SocketTimeoutException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
//            if (e instanceof ConnectException)
//                msg_failed = "Failed to connect to host...";
            return false;
        } finally {

            // close output stream
            if (dataOutputStream != null) {
                try {
                    dataOutputStream.flush();
                    dataOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // close socket
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return true;
    }

    public static void sendToHost(final Context context, String data, String url) {
        data = data.replace("GET /?JSON=", "");
        data = data.replace("HTTP/1.1", "");
        String urlData = url + "/" + data;
        System.out.println("URL DATA" + urlData);
//        Ion.with(OOBApplication.getAppContext()).load(url + "/" + data)
//        final ProgressDialog dialog = Utils.showProgress(context, "Please wait", true);
        Ion.with(context).load(urlData)
//        Ion.with(OOBApplication.getAppContext()).load("http://gsiot-njf4-3sej.try.yaler.io/9714090305R01M01A1Q")
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String response) {
//                        dialog.dismiss();
                        Log.e("sendToHost response", "" + response);
                        NotifyUtils.toast(context, response);
//                        NotifyUtils.toast(context, e == null ? "Successfully sent.." : "Failed to connect to host..");

                    }

                });
    }

}
