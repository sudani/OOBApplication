package com.oob.tools;

/**
 * Created by pranav on 22/1/16.
 */
public class BroadcastConstant
{
    public static final String DATA_ID = "id";
    public static final String TOTAL = "total_strings";
    public static final String COUNT = "count_number";
    public static String BROADCAST_ACTION="roomUIupdate";
    public static String BROADCAST_ACTION_MASSAGE_ARRAIVED="mqtt_massage_arrived";
    public static String BROADCAST_ACTION_LOGDATA="LOG_DATA_UPDATE";
    public static String MESSAGE="msg";
    public static String TOPIC="topic";
    public static String DATA_CHANGE="data";
}
