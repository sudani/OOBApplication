package com.oob.tools;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class MyYAxisValueFormatter implements IAxisValueFormatter
{

    private DecimalFormat mFormat;
    String unit;

    public MyYAxisValueFormatter(String unit) {
        mFormat = new DecimalFormat("##.##");
       this.unit=unit;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value)+" "+unit;
    }
}
