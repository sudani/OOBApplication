
package com.oob.tools;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.oob.app.R;

import java.text.DecimalFormat;

/**
 * Custom implementation of the MarkerView.
 *
 * @author Philipp Jahoda
 */
public class TimeMarkerView extends MarkerView {

    private TextView tvContent;
    private IAxisValueFormatter xAxisValueFormatter;
//    String unit[] = {"Min", "Hrs", "Days", "Months"};
    String unit[] = {"Sec", "Min", "Hrs", "Days", "Months"};

    private DecimalFormat format;

    public TimeMarkerView(Context context, IAxisValueFormatter xAxisValueFormatter) {
        super(context, R.layout.custom_marker_view);

        this.xAxisValueFormatter = xAxisValueFormatter;
        tvContent = (TextView) findViewById(R.id.tvContent);
        format = new DecimalFormat("##.##");
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {
//        xAxisValueFormatter.getFormattedValue(e.getX(), null) +
        tvContent.setText(xAxisValueFormatter.getFormattedValue(e.getX(), null)+"\nStart Time: " + getValue(e.getY()));

        super.refreshContent(e, highlight);
    }

//    private String getValue(float value) {
//        System.out.println("VALUE  "+value);
//        if (value < 60) {
//            return format.format(value) + " " + unit[0];
//        } else if (value < 43200){ //(value < 1440) {
//            return format.format(value / 60) + " " + unit[1];
////        } else if (value < 43200) {
////            return format.format(value / 1440) + " " + unit[2];
//        } else {
//            return format.format(value / 43200) + " " + unit[3];
//        }
//    }
    private String getValue(float value) {
        if (value < 60) {
            // seconds
            return format.format(value) + " " + unit[0];
        } else if (value < (60 * 1000)) {
            // minutes
            return format.format(value/60) + " " + unit[1];
        } else if (value < (43200 * 1000)) {//(value < (1440 * 1000)) {
            return format.format(value / (60 * 1000)) + " " + unit[2];
//        } else if (value < (43200 * 1000)) {
//            return mFormat.format(value / (1440 * 1000)) + " " + unit[3];
        } else {
            return format.format(value / (43200 * 1000)) + " " + unit[4];
        }
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
