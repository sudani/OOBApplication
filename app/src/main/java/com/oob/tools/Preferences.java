package com.oob.tools;

import android.content.Context;
import android.content.SharedPreferences;

import com.oob.OOBApplication;

public class Preferences {
    public static final String PREFS_NAME = "Fit4Ergonomics";
    /**
     * Preference keys
     */
    public static final String KEY_DIALOG_SHOW = "dialog_show";
    public static final String KEY_USER = "username";
    public static final String KEY_PASS = "password";
    public static final String KEY_REMEMBER = "remember";
    public static final String KEY_DIALOG_USERDATA = "dialog_udata";
    public static final String KEY_IS_FIRST = "is_first";
    public static final String KEY_FROM_TIME = "from_time";
    public static final String KEY_TO_TIME = "to_time";
    public static final String KEY_FROM_TIME_AC = "from_time_ac";
    public static final String KEY_TO_TIME_AC = "to_time_ac";
    public static final String KEY_SECURITY_CODE = "security_code";

    public static final String KEY_NOTIFICATION_STATUS = "notification_status";
    public static final String KEY_FLAT_WATER ="flat_water_string" ;
    public static final String KEY_DEVELOPER_MODE = "developer_mode";
    public static final java.lang.String KEY_IP_ADDRESS = "ip_address";
    public static final String KEY_APP_USAGE_TYPE = "app_usage_type" ;
    public static final java.lang.String KEY_SERVER_URL = "server_url";
    public static final java.lang.String KEY_SERVER_PORT = "server_port";
    public static final java.lang.String KEY_SERVER_USER = "server_user";
    public static final java.lang.String KEY_SERVER_PASS = "server_pass";
    public static final java.lang.String KEY_SERVER_SUB_APP = "server_sub_app";
    public static final java.lang.String KEY_SERVER_PUB_APP = "server_pub_app";
    public static final String KEY_DATA_TRANSFER_MODE = "key_data_transfer_mode";

    public static String KEY_TOTAL_USED = "total_used";
    public static String KEY_TOTAL_CAPACITY = "total_capacity";
    public static String KEY_LOW_CAPACITY = "low_capacity";
    public static String KEY_HIGH_CAPACITY = "high_capacity";
    public static String KEY_HIGH_PROGRESS = "progress";
    public static String KEY_IS_RECT_SHAPE = "is_rect_shape";
    public static String KEY_IS_BUNGALOW = "is_bungalow";
    public static String KEY_MAIN_POWER_OFF="main_power_off";
    public static String KEY_MAIN_MUTE_BUZZER="main_mute_buzzer";

    public static String KEY_TOTAL_USED_ML="total_used_ml";
    public static String KEY_HIGH_PROGRESS_ML = "progress_ml";
    public static String KEY_CUR_TIME_ONE_A="curtain_time_one_a";
    public static String KEY_CUR_TIME_ONE_B="curtain_time_one_b";
    public static String KEY_CUR_TIME_TWO_A="curtain_time_two_a";
    public static String KEY_CUR_TIME_TWO_B="curtain_time_two_b";
    public static String KEY_GATE_TIME_ONE_A="gate_time_one_a";
    public static String KEY_GATE_TIME_ONE_B="gate_time_one_b";
    public static String KEY_GATE_TIME_TWO_A="gate_time_two_a";
    public static String KEY_GATE_TIME_TWO_B="gate_time_two_b";
    public static String KEY_CUR_DAYS="curtain_days";
    public static String KEY_CUR_STATE="curtain_state";
    public static String KEY_GATE_DAYS="gate_days";
    public static String KEY_GATE_STATE="gate_state";

    private static SharedPreferences get() {
        return OOBApplication.getAppContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static void registerPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener) {
        get().registerOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    public static void writeSharedPreferences(String key, String value) {
        SharedPreferences settings = get();
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void writeSharedPreferencesBool(String key, boolean value) {
        get().edit().putBoolean(key, value).commit();
    }

    public static String getAppPrefString(String key) {
        return get().getString(key, "");
    } public static String getAppPrefString(String key,String defaultValue) {
        return get().getString(key, defaultValue);
    }

    public static boolean getAppPrefBool(String key, boolean default_value) {
        return get().getBoolean(key, default_value);
    }

    public static void deleteSharedPreferences(String key) {
        get().edit().remove(key).commit();
    }

    public static boolean containsKey(String key) {
        return get().contains(key);
    }

}