package com.oob.tools;

import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

import com.oob.app.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pranav on 5/11/15.
 */
public class Constants {

    public static final String ACTION_TIMER = "com.ACTION.TIME.UPDATE";
    public static final String EXTRA_FROM_NOTIFICATION = "isFromNotification";

    public static int TEXT_COLOR = Color.BLACK;

    public static int LINE_COLOR = Color.RED;

    public static final int REQUEST_CODE_UPDATE_PIC = 0x1;

    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";

    //    public static String MOBILE = "9714090305";
    public static String DEVICES[] = {"", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

//    public static String DB_PTH = Environment.getExternalStorageDirectory().getAbsolutePath()
//            + "/oob/oob.db";

    public static String SD_CARD = Environment.getExternalStorageDirectory().getAbsolutePath()
            + "/oob/";
    public static String PREF_BACKUP_LOCATION = SD_CARD + "BACKUP/";
    public static String PREF_BACKUP_FILE = "PrefBackup.xml";
    public static String DATABASE_BACKUP_FILE = "DatabaseBackup.db";
    public static String DATABASE_BACKUP_LOCATION = SD_CARD + "BACKUP/";

    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(
                (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight
                            * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(
                (int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static int getImage(Context context, String item_image) {

        int id = context.getResources()
                .getIdentifier(item_image, "drawable", context.getPackageName());
        return id;
    }

    public static String getRoomcode(int roomId) {
//        System.out.println("ROOM ID " + roomId);
        String code = "R";
        if (roomId < 10) {
            code = code + "0" + roomId;
        } else {
            code = code + roomId;
        }
//        System.out.println("DEVICE " + code);

        return code;
    }

    public static String getModuleAndDevicecode(String moduleId) {
        System.out.println("MODULE ID " + moduleId);
        String code = "M";
        if (!Utils.isEmptyString(moduleId)) {
            String[] codes = moduleId.split(",");
            if (Integer.parseInt(codes[1]) < 10) {
                code = code + "0" + codes[1];
            } else {
                code = code + codes[1];
            }
            code = code + DEVICES[Integer.parseInt(codes[0])];
//        System.out.println("MODULE " + code);
            return code;
        }else{
            return "";
        }
    }

    public static String getDevicecode(String moduleId) {
//        System.out.println("DEVICE ID " + moduleId);
        String code = "";
        String[] codes = moduleId.split(",");

        code = DEVICES[Integer.parseInt(codes[0])];

//        System.out.println("DEVICE " + code);
        return code;
    }

    public static String getModulecode(String moduleId) {
//        System.out.println("MODULE ID " + moduleId);
        String code = "M";
        String[] codes = moduleId.split(",");
        if (Integer.parseInt(codes[1]) < 10) {
            code = code + "0" + codes[1];
        } else {
            code = code + codes[1];
        }

//        System.out.println("MODULE " + code);
        return code;
    }

    public static int getImageForDevice(Context context, String item_image) {
        item_image = item_image.trim();
        item_image = item_image.replace(" ", "");
        if (item_image.contains("light")) {
            return R.drawable.light;
        } else if (item_image.contains("dishtv")) {
            return R.drawable.dishtv;
        } else if (item_image.contains("fridge")) {
            return R.drawable.fridge;
        } else if (item_image.contains("iron")) {
            return R.drawable.iron;
        } else if (item_image.contains("microoven")) {
            return R.drawable.microoven;
        } else if (item_image.contains("night")) {
            return R.drawable.nightlamp;
        } else if (item_image.contains("plug")) {
            return R.drawable.plug;
        } else if (item_image.contains("printer")) {
            return R.drawable.printer;
        } else if (item_image.contains("washing")) {
            return R.drawable.washingmachine;
        } else if (item_image.contains("machine")) {
            return R.drawable.washingmachine;
        } else if (item_image.contains("water")) {
            return R.drawable.waterpump;
        } else if (item_image.contains("pump")) {
            return R.drawable.waterpump;
        } else if (item_image.contains("fan")) {
            return R.drawable.fan;
        } else if (item_image.contains("ac")) {
            return R.drawable.ac;
        } else if (item_image.contains("tv")) {
            return R.drawable.tv;
        }
        return 0;
    }

    public static boolean isFromOurData(String item_image) {
        item_image = item_image.trim();
        item_image = item_image.replace(" ", "");
        if (item_image.startsWith("light")) {
            return true;
        } else if (item_image.startsWith("fan")) {
            return true;
        } else if (item_image.startsWith("ac")) {
            return true;
        } else if (item_image.contains("dishtv")) {
            return true;
        } else if (item_image.contains("fridge")) {
            return true;
        } else if (item_image.contains("iron")) {
            return true;
        } else if (item_image.contains("oven")) {
            return true;
        } else if (item_image.contains("night")) {
            return true;
        } else if (item_image.contains("plug")) {
            return true;
        } else if (item_image.contains("printer")) {
            return true;
        } else if (item_image.startsWith("tv")) {
            return true;
        } else if (item_image.contains("washing")) {
            return true;
        } else if (item_image.contains("machine")) {
            return true;
        } else if (item_image.contains("waterpump")) {
            return true;
        } else {
            return false;
        }
    }

    public static int getColor(Context context, String item_image) {

        int id = context.getResources()
                .getIdentifier(item_image, "color", context.getPackageName());
        return id;
    }

    public static String getTimeerValue(String time_one_a) {
        String code = "";
//        System.out.println("Original "+time_one_a);
        if (!time_one_a.equalsIgnoreCase("0000")) {
            SimpleDateFormat displayFormat = new SimpleDateFormat("HHmm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            Date date = null;
            try {
                date = parseFormat.parse(time_one_a);
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
            code = displayFormat.format(date);
        } else {
            code = time_one_a;
        }
        return code;
    }

    public static String getTwoDigit(int hourOfDay) {
        String twoDigit = "";
        if (hourOfDay < 10) {
            twoDigit = "0" + hourOfDay;
        } else {
            twoDigit = "" + hourOfDay;
        }
        return twoDigit;
    }

    public static String getThreeDigit(int redclr) {
        String threeDigit = "";
        if (redclr < 10) {
            threeDigit = "00" + redclr;
        } else if (redclr < 100) {
            threeDigit = "0" + redclr;
        } else {
            threeDigit = "" + redclr;
        }
        return threeDigit;
    }
   public static String getThreeDigit(String redclr) {
        String threeDigit = "";
       if (Utils.isEmptyString(redclr)){
           return threeDigit;
       }
       int num = Integer.parseInt(redclr);
        if (num < 10) {
            threeDigit = "00" + redclr;
        } else if (num < 100) {
            threeDigit = "0" + redclr;
        } else {
            threeDigit = "" + redclr;
        }
        return threeDigit;
    }

    public static int getImageForSensor(Context context, String item_image) {
        item_image = item_image.trim();
        item_image = item_image.replace(" ", "");
        if (item_image.contains("motion")) {
            return R.drawable.app_motion;
        } else if (item_image.contains("light")) {
            return R.drawable.app_light;
        } else if (item_image.contains("fire")) {
            return R.drawable.app_fire;
        } else if (item_image.contains("clap")) {
            return R.drawable.app_clap;
        } else if (item_image.contains("temperature")) {
            return R.drawable.app_temparature;
        } else if (item_image.contains("lpg") || item_image.contains("gas")) {
            return R.drawable.app_lpg_gas;
        } else if (item_image.contains("sms")) {
            return R.drawable.app_sms;
        } else if (item_image.contains("call")) {
            return R.drawable.call;
        } else if (item_image.contains("alert")) {
            return R.drawable.app_alert;
        } else if (item_image.contains("email")) {
            return R.drawable.app_email;
        } else if (item_image.contains("glass")) {
            return R.drawable.app_glass_break;
        } else if (item_image.contains("alarm")) {
            return R.drawable.app_alaram;
        } else if (item_image.contains("door")) {
            return R.drawable.app_door_ball;
        } else if (item_image.contains("calender")) {
            return R.drawable.calendar;
        }
        return 0;
    }

    public static String getFiveDigite(String value) {
        String fiveDigite = "";
        if (value == null || value.equalsIgnoreCase("")) {
            fiveDigite = "00000";
        } else if (Integer.parseInt(value) < 10) {
            fiveDigite = "0000" + value;
        } else if (Integer.parseInt(value) < 100) {
            fiveDigite = "000" + value;
        } else if (Integer.parseInt(value) < 1000) {
            fiveDigite = "00" + value;
        } else if (Integer.parseInt(value) < 10000) {
            fiveDigite = "0" + value;
        } else if (Integer.parseInt(value) < 100000) {
            fiveDigite = value;
        } else {
            fiveDigite = value;
        }
        return fiveDigite;
    }


    public interface PicModes {

        //        String CAMERA = "Camera";
//        String GALLERY = "Gallery";
        String CAMERA = "Take Photo";
        String GALLERY = "Choose from Gallery";
    }

    public interface IntentExtras {

        String ACTION_CAMERA = "action-camera";
        String ACTION_GALLERY = "action-gallery";
        String IMAGE_PATH = "image-path";
    }
}
