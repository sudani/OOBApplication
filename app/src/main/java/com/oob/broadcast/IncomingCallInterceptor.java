package com.oob.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.entity.entSecurity;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;

import java.util.ArrayList;

public class IncomingCallInterceptor extends BroadcastReceiver {
    String TAG = "IncomingCallInterceptor";
    Context mContext;
    private ArrayList<entSecurity> mSecuritis;
    private static String laststat;

    @Override
    public void onReceive(final Context context, Intent intent) {
        mContext = context;
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if (!state.equals(laststat)) {
            laststat = state;
            if (TelephonyManager.EXTRA_STATE_RINGING.equals(state)) {
                Log.e(TAG, "RInging ");
                dbHelperOperations db = new dbHelperOperations(context);
                mSecuritis = db.getSensorAction("CALL");

                for (entSecurity mSecurity : mSecuritis) {
                    if (mSecurity.getIS_ENAMBLE() == 1) {
                        if (mSecurity.getACTION_NAME() != null) {
                            Log.e(TAG, mSecurity.getACTION_NAME());
                            if (mSecurity.getACTION_NAME().contains(",")) {
                                String deviceInfo[] = mSecurity.getACTION_NAME().toString()
                                        .split(",");
                                String device = deviceInfo[0];
                                String roomName = deviceInfo[1].replace("(", "").replace(")", "");
                                Log.e(TAG, device);
                                Log.e(TAG, roomName);
                                if (mSecurity.getACTION_VALUE() != null) {
                                    Log.e(TAG, mSecurity.getACTION_VALUE());
                                    String number = intent
                                            .getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                                    System.out.println("number " + number);
                                    if (number.contains(mSecurity.getACTION_VALUE())) {
                                        generateData(db.getDeviceFromName(device,
                                                db.getRoomId(roomName)),
                                                db);
                                    }

                                } else {
                                    generateData(
                                            db.getDeviceFromName(device, db.getRoomId(roomName)),
                                            db);
                                }
                            } else {
                                if (mSecurity.getACTION_NAME().equalsIgnoreCase("LED COLOR")) {
                                    sendData(Preferences
                                            .getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                            + mSecurity.getROOM_CODE() + mSecurity.getMODULE_CODE()
                                            + "SSLC");
                                } else if (mSecurity.getACTION_NAME().equalsIgnoreCase("BUZZER")) {
                                    sendData(Preferences
                                            .getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                            + mSecurity.getROOM_CODE() + mSecurity.getMODULE_CODE()
                                            + "SSBUZZ");
                                } else if (mSecurity.getACTION_NAME()
                                        .equalsIgnoreCase("LED FLASH")) {
                                    sendData(Preferences
                                            .getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                            + mSecurity.getROOM_CODE() + mSecurity.getMODULE_CODE()
                                            + "SSLF");
                                } else if (mSecurity.getACTION_NAME().equalsIgnoreCase("DIMMING")) {
                                    sendData(Preferences
                                            .getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                            + mSecurity.getROOM_CODE() + mSecurity.getMODULE_CODE()
                                            + "SSDIM");
                                } else if (mSecurity.getACTION_NAME()
                                        .equalsIgnoreCase("LAMP COLOR")) {
                                    sendData(Preferences
                                            .getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                            + mSecurity.getROOM_CODE() + mSecurity.getMODULE_CODE()
                                            + "SSLLC");
                                }

                            }
                            // Phone is ringing

                        }
                    }
                }
            }
        }
    }

    private void generateData(entDevice device, dbHelperOperations db) {
        if (device.getMODULE() != null) {
            device.setSTT(device.getSTT() == 0 ? 1 : 0);

            device.setALL_STT(device.getSTT());
            Log.d("TEST", "from check");

            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                    + Constants
                    .getRoomcode(device.getROOM_Id())
                    + Constants
                    .getModuleAndDevicecode(device.getMODULE())
                    + device
                    .getSTT()
                    + "D" + Constants
                    .getThreeDigit(device.getDIM()) + "RX"
                    + device.getRGBSTT()
                    + device.getMIC()
            );
            db.updateDevice(device);
        }
    }

    private void sendData(String json) {
//        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(mContext)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(mContext, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

}