package com.oob.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class dbHelper extends SQLiteOpenHelper {

    private Context CONTEXT;

    SQLiteDatabase DB;
// adb shell "run-as com.oob.app chmod 600 /data/data/com.oob.app/databases/oob.db"
// adb exec-out run-as com.oob.app cat databases/oob.db > oob.db

//    adb shell "run-as com.oob.app chmod 666 /data/data/com.oob.app/databases/oob.db"
//    adb pull /data/data/com.oob.app/databases/oob.db
//    adb shell "run-as com.oob.app chmod 600 /data/data/com.oob.app/databases/oob.db"

    public dbHelper(Context context) {
        super(context, "oob.db", null, 4);
//        Constants.SD_CARD + "oob/
        CONTEXT = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        DB = db;


        String Room
                = "CREATE TABLE IF NOT EXISTS RoomTable (rID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,RoomName Varchar,IsSelected integer,All_STT integer,GRP_STT integer,moduleCount integer,active integer,profilePhoto Varchar,groupColor integer,groupBuzz integer);";
        String Device
                = "CREATE TABLE IF NOT EXISTS DeviceTable (dID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,rID integer,NAME Varchar,STT integer,DIM integer,TIME_ONE_A Varchar,TIME_ONE_B Varchar,TIME_TWO_A Varchar,TIME_TWO_B Varchar,CL integer,RGBSTT integer,ENGR integer,REDCLR integer,GREENCLR integer,BLUECLR integer,RGB integer,DAYS Varchar,ALLSTT integer,MIC integer,GRP integer,deviceCount integer,module Varchar,GroupDIM integer,position integer,moduleSTT integer,backLightColor integer,backLightDim integer,moduleNumber integer,isDelete integer);";
        String LogTable
                = "CREATE TABLE IF NOT EXISTS LogTable (lID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,dID integer,rID integer,TimeValue Long,DateValue Varchar,State integer, UNIQUE(DateValue) ON CONFLICT REPLACE);";
        String Dashboard
                = "CREATE TABLE IF NOT EXISTS DashboardTable (iID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,ItemName Varchar,ItemImage Varchar,ItemPosition integer);";
        String Security
                = "CREATE TABLE IF NOT EXISTS SecurityTable (sID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,SensorName Varchar,SensorImage Varchar,SensorLocation Varchar,SensorLight Varchar,dID integer,ActionName Varchar,ActionValue,RoomCode Varchar,ModuleCode Varchar,SensorNumber Varchar,isEnable integer);";
        String TV
                = "CREATE TABLE IF NOT EXISTS TVTable (tID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,rID integer,TVName Varchar,RoomName Varchar,TVmodel Varchar,TVprotocol Varchar,SETname Varchar,SETProtocol Varchar,TVSTT Varchar,IsSelected integer,SETSTT Varchar,MUTE Varchar,TVUP Varchar,TVDOWN Varchar,TVLEFT Varchar,TVRIGHT Varchar,TVOK Varchar,TVBACK Varchar,TVEXIT Varchar,VOLPLUS Varchar,VOLMINUS Varchar,CHPLUS Varchar,CHMINUS Varchar,TVZERO Varchar,TVONE Varchar,TVTWO Varchar,TVTHREE Varchar,TVFOUR Varchar,TVFIVE Varchar,TVSIX Varchar,TVSEVEN Varchar,TVEIGHT Varchar,TVNINE Varchar,TVSTAR Varchar,TVHAS Varchar);";
        String AC
                = "CREATE TABLE IF NOT EXISTS ACTable (aID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,rID integer,ACName Varchar,RoomName Varchar,ACprotocol Varchar,ACSTT Varchar,ACLEFT Varchar,ACRIGHT Varchar,ACTEMP Varchar,ACPLUS Varchar,ACMINUS Varchar,ACFAN Varchar,ACMODE Varchar,ACSWING Varchar,ACTURBO Varchar,ACSENSOR Varchar,IsSelected integer);";
        String CURTAIN
                = "CREATE TABLE IF NOT EXISTS CurtainTable (cID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,rID integer,CurtainName Varchar,RoomName Varchar,CurtainSTT Varchar,CLEFT Varchar,CRIGHT Varchar,CUP Varchar,CDOWN Varchar,CSTOP Varchar,CurtainImage Varchar,Position Integer);";
        String Channel
                = "CREATE TABLE IF NOT EXISTS ChannelTable (chID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,ChannelName Varchar,ChannelImage Varchar,ChannelNumber integer,ChannelGroup);";
        String WaterUsage
                = "CREATE TABLE IF NOT EXISTS WaterUsageTable (wID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,FlatName Varchar,WaterUsage Varchar,BackupDate Varchar);";
//        String TVModel
//                = "CREATE TABLE IF NOT EXISTS TVModelTable (mID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,TVBrandName Varchar,TVModelName Varchar,PowerData Varchar);";
        String AllChannel
                = "CREATE TABLE IF NOT EXISTS ChannelAllTable (chaID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,Name Varchar,Image Varchar,GTPL Varchar,AIRTEL Varchar,DISHTV Varchar,VIDEOCON Varchar,TATASKY Varchar,RELIANCE Varchar,JIO Varchar);";
        String GroupAppliances
                = "CREATE TABLE IF NOT EXISTS GroupAppliances (gID INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,Name Varchar,Image Varchar,DeviceIds Varchar,Status Integer,IsDelete Integer);";

        db.execSQL(Room);
        db.execSQL(Device);
        db.execSQL(LogTable);
        db.execSQL(Dashboard);
        db.execSQL(Security);
        db.execSQL(TV);
        db.execSQL(AC);
        db.execSQL(CURTAIN);
        db.execSQL(Channel);
        db.execSQL(WaterUsage);
        db.execSQL(AllChannel);
        db.execSQL(GroupAppliances);

        System.out.println("The Database is Created Here :" + db.getPath());


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        String WaterUsage
                = "CREATE TABLE IF NOT EXISTS WaterUsageTable (wID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,FlatName Varchar,WaterUsage Varchar,BackupDate Varchar);";
        String AllChannel
                = "CREATE TABLE IF NOT EXISTS ChannelAllTable (chaID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,Name Varchar,Image Varchar,GTPL Varchar,AIRTEL Varchar,DISHTV Varchar,VIDEOCON Varchar,TATASKY Varchar,RELIANCE Varchar,JIO Varchar);";
        String GroupAppliances
                = "CREATE TABLE IF NOT EXISTS GroupAppliances (gID INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,Name Varchar,Image Varchar,DeviceIds Varchar,Status Integer,IsDelete Integer);";

        sqLiteDatabase.execSQL(WaterUsage);
        sqLiteDatabase.execSQL(AllChannel);
        sqLiteDatabase.execSQL(GroupAppliances);

        System.out.println("UPDATED");

    }
}
