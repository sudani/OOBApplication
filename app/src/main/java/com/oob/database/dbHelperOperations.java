package com.oob.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.oob.entity.entAC;
import com.oob.entity.entChannel;
import com.oob.entity.entChannelModel;
import com.oob.entity.entCurtain;
import com.oob.entity.entDashboard;
import com.oob.entity.entDevice;
import com.oob.entity.entGroupAppliances;
import com.oob.entity.entLogData;
import com.oob.entity.entRooms;
import com.oob.entity.entSecurity;
import com.oob.entity.entTV;
import com.oob.entity.entTvModel;
import com.oob.entity.entWaterUsage;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;

import java.util.ArrayList;
import java.util.List;

//chirag

public class dbHelperOperations {
    private SQLiteDatabase DB;
    private dbHelper DB_HELPER;
    private Context CONTEXT;
    final String ROOM_TABLE = "RoomTable";
    final String CHANNEL_ALL_TABLE = "ChannelAllTable";
    final String DEVICE_TABLE = "DeviceTable";
    final String LOG_TABLE = "LogTable";
    final String DASHBOARD_TABLE = "DashboardTable";
    final String SENSOR_TABLE = "SecurityTable";
    final String TV_TABLE = "TVTable";
    final String AC_TABLE = "ACTable";
    final String CURTAIN_TABLE = "CurtainTable";
    final String CHANNEL_TABLE = "ChannelTable";
    final String TV_MODEL_TABLE = "TVModelTable";
    final String WATER_USAGE_TABLE = "WaterUsageTable";
    final String GROUP_APPLIANCES_TABLE = "GroupAppliances";
    // RoomTable table fields.
    private final String ROOM_ID = "rID";
    private final String ROOM_NAME = "RoomName";
    private final String IS_SELECTED = "IsSelected";
    private final String ROOM_ALL_SIT = "All_STT";
    private final String GRP_SIT = "GRP_STT";
    private final String MODULE_COUNT = "moduleCount";
    private final String ROOM_ACTIVE = "active";
    private final String PROFILE_PHOTO = "profilePhoto";
    private final String GROUP_COLOR = "groupColor";
    private final String GROUP_BUZZ = "groupBuzz";
    // ACTable table fields.
    private final String AC_ID = "aID";
    private final String AC_NAME = "ACName";
    private final String AC_PROTOCOLO = "ACprotocol";
    private final String AC_STT = "ACSTT";
    private final String AC_IS_SELECTED = "IsSelected";
    private final String AC_LEFT = "ACLEFT";
    private final String AC_RIGHT = "ACRIGHT";
    private final String AC_TEMP = "ACTEMP";
    private final String AC_PLUS = "ACPLUS";
    private final String AC_MINUS = "ACMINUS";
    private final String AC_FAN = "ACFAN";
    private final String AC_MODE = "ACMODE";
    private final String AC_SWING = "ACSWING";
    private final String AC_TURBO = "ACTURBO";
    private final String AC_SENSOR = "ACSENSOR";
    // Curtain Table table fields.
    private final String CURTAIN_ID = "cID";
    private final String CURTAIN_NAME = "CurtainName";
    private final String CURTAIN_STT = "CurtainSTT";
    private final String CURTAIN_LEFT = "CLEFT";
    private final String CURTAIN_RIGHT = "CRIGHT";
    private final String CURTAIN_UP = "CUP";
    private final String CURTAIN_DOWN = "CDOWN";
    private final String CURTAIN_STOP = "CSTOP";
    private final String CURTAIN_IMAGE = "CurtainImage";
    private final String CURTAIN_POSITION = "Position";
    // TVTable table fields.
    private final String TV_ID = "tID";
    private final String TV_NAME = "TVName";
    private final String TV_MODEL = "TVmodel";
    private final String TV_PROTOCOLO = "TVprotocol";
    private final String SET_NAME = "SETname";
    private final String SET_PROTOCOL = "SETProtocol";
    private final String TV_STT = "TVSTT";
    private final String TV_IS_SELECTED = "IsSelected";
    private final String TV_SET_STT = "SETSTT";
    private final String TV_MUTE = "MUTE";
    private final String TV_UP = "TVUP";
    private final String TV_DOWN = "TVDOWN";
    private final String TV_LEFT = "TVLEFT";
    private final String TV_RIGHT = "TVRIGHT";
    private final String TV_OK = "TVOK";
    private final String TV_BACK = "TVBACK";
    private final String TV_EXIT = "TVEXIT";
    private final String TV_VOL_PLUS = "VOLPLUS";
    private final String TV_VOL_MINUS = "VOLMINUS";
    private final String TV_CH_PLUS = "CHPLUS";
    private final String TV_CH_MINUS = "CHMINUS";
    private final String TV_ZERO = "TVZERO";
    private final String TV_ONE = "TVONE";
    private final String TV_TWO = "TVTWO";
    private final String TV_THREE = "TVTHREE";
    private final String TV_FOUR = "TVFOUR";
    private final String TV_FIVE = "TVFIVE";
    private final String TV_SIX = "TVSIX";
    private final String TV_SEVEN = "TVSEVEN";
    private final String TV_EIGHT = "TVEIGHT";
    private final String TV_NINE = "TVNINE";
    private final String TV_TEN = "TVTEN";
    private final String TV_STAR = "TVSTAR";
    private final String TV_HAS = "TVHAS";
    // TVModelTable table fields.
    private final String TV_MODEL_ID = "mID";
    private final String TV_BRAND_NAME = "TVBrandName";
    private final String TV_MODEL_NAME = "TVModelName";
    private final String TV_POWER_DATA = "PowerData";
    // Device table fields.
    private final String DEVICE_ID = "dID";
    private final String DEVICE_NAME = "NAME";
    private final String STT = "STT";
    private final String DIM = "DIM";
    private final String TIME_ONE_A = "TIME_ONE_A";
    private final String TIME_ONE_B = "TIME_ONE_B";
    private final String TIME_TWO_A = "TIME_TWO_A";
    private final String TIME_TWO_B = "TIME_TWO_B";
    private final String CL = "CL";
    private final String RGB_STT = "RGBSTT";
    private final String ENGR = "ENGR";
    private final String RED_CLR = "REDCLR";
    private final String GREEN_CLR = "GREENCLR";
    private final String BLUE_CLR = "BLUECLR";
    private final String RGB = "RGB";
    private final String DAYS = "DAYS";
    private final String ALL_STT = "ALLSTT";
    private final String MIC = "MIC";
    private final String GROUP = "GRP";
    private final String DEVICE_COUNT = "deviceCount";
    private final String MODULE = "module";
    private final String GROUP_DIM = "GroupDIM";
    private final String POSITION = "position";
    private final String MODULE_STT = "moduleSTT";
    private final String BACKLIGHT_COLOR = "backLightColor";
    private final String BACKLIGHT_DIM = "backLightDim";
    private final String MODULE_NUMBER = "moduleNumber";
    private final String IS_DELETE = "IsDelete";
    // Log table fields.
    private final String LOG_ID = "lID";
    private final String TIME_VALUE = "TimeValue";
    private final String DATE_VALUE = "DateValue";
    private final String STATE = "State";
    // Dashboard table fields.
    private final String ITME_ID = "iID";
    private final String ITEM_NAME = "ItemName";
    private final String ITEM_IMAGE = "ItemImage";
    private final String ITEM_POSITION = "ItemPosition";
    // Channel table fields.
    private final String CHANNEL_ID = "chID";
    private final String CHANNEL_NAME = "ChannelName";
    private final String CHANNEL_IMAGE = "ChannelImage";
    private final String CHANNEL_NUMBER = "ChannelNumber";
    private final String CHANNEL_GROUP = "ChannelGroup";
    // Security table fields.
    private final String SECURITY_ID = "sID";
    private final String SENSOR_NAME = "SensorName";
    private final String SENSOR_IMAGE = "SensorImage";
    private final String SENSOR_LOCATION = "SensorLocation";
    private final String SENSOR_LIGHT = "SensorLight";
    private final String ACTION_NAME = "ActionName";
    private final String ACTION_VALUE = "ActionValue";
    private final String ROOM_CODE = "RoomCode";
    private final String MODULE_CODE = "ModuleCode";
    private final String SENSOR_NUMBER = "SensorNumber";
    private final String IS_ENABLE = "isEnable";
    // Water Usage table fields.
    private final String WATER_ID = "wID";
    private final String FLAT_NAME = "FlatName";
    private final String WATER_USAGE = "WaterUsage";
    private final String BACKUPDATE = "BackupDate";
    // RoomTable table fields.
    private final String CH_ID = "chaID";
    private final String CH_NAME = "Name";
    private final String CH_IMAGE = "Image";
    private final String GTPL_CODE = "GTPL";
    private final String AITTEL_CODE = "AIRTEL";
    private final String DISHTV_CODE = "DISHTV";
    private final String VIDEOCON_CODE = "VIDEOCON";
    private final String TATASKY_CODE = "TATASKY";
    private final String RELIANCE_CODE = "RELIANCE";
    private final String JIO_CODE = "JIO";
    // GroupAppliances table fields.
    private final String GRP_GROUP_ID = "gID";
    private final String GRP_NAME = "Name";
    private final String GRP_IMAGE = "Image";
    private final String GRP_DEVICE_IDS = "DeviceIds";
    private final String GRP_STATUS = "Status";
    private final String GRP_IS_DELETE = "IsDelete";

    public dbHelperOperations(Context context) {
        DB_HELPER = new dbHelper(context);
    }

    public int insertRooms(String roomName) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB = DB_HELPER.DB.openDatabase(Constants.DB_PTH, null, SQLiteDatabase.OPEN_READONLY);
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(ROOM_NAME, roomName);
            _cv.put(IS_SELECTED, 0);
            _cv.put(ROOM_ALL_SIT, 1);
            _cv.put(GRP_SIT, 1);
            _cv.put(MODULE_COUNT, 0);
            _cv.put(ROOM_ACTIVE, 0);
            _cv.put(PROFILE_PHOTO, "");
            _cv.put(GROUP_COLOR, 0);
            _cv.put(GROUP_BUZZ, 1);

            _id = DB.insert(ROOM_TABLE, null, _cv);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            // DB.endTransaction();
            ex.printStackTrace();

//        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("room id " + _id);
        return (int) _id;
    }

    public int insertDevice(entDevice device) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB = DB_HELPER.DB.openDatabase(Constants.DB_PTH, null, SQLiteDatabase.OPEN_READONLY);
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(ROOM_ID, device.getROOM_Id());
            _cv.put(DEVICE_NAME, device.getDEVICE_NAME());
//            _cv.put(DEVICE_COUNT,getDeviceCount(device.getDEVICE_NAME(), device.getROOM_Id()));
            _cv.put(STT, device.getSTT());
            _cv.put(DIM, device.getDIM());
            _cv.put(TIME_ONE_A, device.getTIME_ONE_A());
            _cv.put(TIME_ONE_B, device.getTIME_ONE_B());
            _cv.put(TIME_TWO_A, device.getTIME_TWO_A());
            _cv.put(TIME_TWO_B, device.getTIME_TWO_B());
            _cv.put(CL, device.getCL());
            _cv.put(RGB_STT, device.getRGBSTT());
            _cv.put(RED_CLR, device.getREDCLR());
            _cv.put(GREEN_CLR, device.getGREENCLR());
            _cv.put(BLUE_CLR, device.getBLUECLR());
            _cv.put(RGB, device.getRGB());
            _cv.put(ENGR, device.getENGRD());
            _cv.put(DAYS, device.getDAYS());
            _cv.put(ALL_STT, device.getALL_STT());
            _cv.put(MIC, device.getMIC());
            _cv.put(GROUP, device.getGROUP());
            _cv.put(MODULE, device.getMODULE());
            _cv.put(GROUP_DIM, device.getGROUP_DIM());
            _cv.put(POSITION, device.getPOSITION());
            _cv.put(MODULE_STT, device.getMODULE_STT());
            _cv.put(BACKLIGHT_COLOR, device.getBACKLIGHT_RGB_STT());
            _cv.put(BACKLIGHT_DIM, device.getBACKLIGHT_DIM_STT());
            _cv.put(MODULE_NUMBER, device.getMODULE_NUMBER());
            _cv.put(IS_DELETE, device.getIS_DELETE());
            String query = "Select dID from DeviceTable Where rId == '" + device.getROOM_Id() + "' And IsDelete == 1";
            Cursor cursor = DB.rawQuery(query, null);
            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
                int dId = cursor.getInt(cursor.getColumnIndex(DEVICE_ID));
                DB.update(DEVICE_TABLE, _cv, DEVICE_ID + " == '" + dId + "' And " + ROOM_ID + " == '" + device.getROOM_Id() + "'", null);
                cursor.close();
            } else {
                _id = DB.insert(DEVICE_TABLE, null, _cv);
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            // DB.endTransaction();
            ex.printStackTrace();

//        } finally {
//            DB.endTransaction();
//            DB.close();
        }
//        System.out.println("device id " + _id);
        return (int) _id;
    }

    public int updateDevice(entDevice device) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB = DB_HELPER.DB.openDatabase(Constants.DB_PTH, null, SQLiteDatabase.OPEN_READONLY);
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(DEVICE_NAME,
                    device.getDEVICE_NAME());
//            _cv.put(DEVICE_COUNT,getDeviceCount(device.getDEVICE_NAME(), device.getROOM_Id()));

            _cv.put(STT, device.getSTT());
            _cv.put(DIM, device.getDIM());
            _cv.put(TIME_ONE_A, device.getTIME_ONE_A());
            _cv.put(TIME_ONE_B, device.getTIME_ONE_B());
            _cv.put(TIME_TWO_A, device.getTIME_TWO_A());
            _cv.put(TIME_TWO_B, device.getTIME_TWO_B());
            _cv.put(CL, device.getCL());
            _cv.put(RGB_STT, device.getRGBSTT());
            _cv.put(RED_CLR, device.getREDCLR());
            _cv.put(GREEN_CLR, device.getGREENCLR());
            _cv.put(BLUE_CLR, device.getBLUECLR());
            _cv.put(RGB, device.getRGB());
            _cv.put(ENGR, device.getENGRD());
            _cv.put(DAYS, device.getDAYS());
            _cv.put(ALL_STT, device.getALL_STT());
            _cv.put(MIC, device.getMIC());
            _cv.put(GROUP, device.getGROUP());
            _cv.put(GROUP_DIM, device.getGROUP_DIM());
            _cv.put(POSITION, device.getPOSITION());
            _cv.put(MODULE_STT, device.getMODULE_STT());
            _cv.put(BACKLIGHT_COLOR, device.getBACKLIGHT_RGB_STT());
            _cv.put(BACKLIGHT_DIM, device.getBACKLIGHT_DIM_STT());
            _cv.put(MODULE_NUMBER, device.getMODULE_NUMBER());
            _cv.put(IS_DELETE, device.getIS_DELETE());

            _id = DB.update(DEVICE_TABLE, _cv, DEVICE_ID + "==" + "'" + device.getDEVICE_Id() + "'",
                    null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

            // DB.endTransaction();
//        } finally {
//            DB.endTransaction();
//            DB.close();
        }
//        System.out.println("device id Updated" + _id);
        return (int) _id;
    }

    public int insertDashboard(String itmeName, String itemImage, int itemPosition) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(ITEM_NAME, itmeName);
            _cv.put(ITEM_IMAGE, itemImage);
            _cv.put(ITEM_POSITION, itemPosition);

            _id = DB.insert(DASHBOARD_TABLE, null, _cv);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
//        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("item id " + _id);

        return (int) _id;
    }

    public int insertSecurity(String Name, String itemImage) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(SENSOR_NAME, Name);
            _cv.put(SENSOR_IMAGE, itemImage);
            _cv.put(SENSOR_LOCATION, "LOCATION");
            _cv.put(SENSOR_LIGHT, "LIGHT 1 \\n [BEDROOM 01]");
            _cv.put(ACTION_NAME, "");
            _cv.put(ACTION_VALUE, "");

            _id = DB.insert(SENSOR_TABLE, null, _cv);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            // DB.endTransaction();
            ex.printStackTrace();

//        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("item id " + _id);

        return (int) _id;
    }

    public List<entDevice> getListOfDevices(int roomId, boolean isSequence) {
        List<entDevice> devices = new ArrayList<entDevice>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            if (roomId == -2) {
                _mCursor = DB.query(false, DEVICE_TABLE,
                        null, IS_DELETE + "==0", null, null,
                        null, null, null);
            } else {
                if (isSequence) {
                    _mCursor = DB.query(false, DEVICE_TABLE,
                            null, ROOM_ID + "==" + roomId + " And " + IS_DELETE + "==0", null, null,
                            null, POSITION + " ASC", null);
                } else {
                    _mCursor = DB.query(false, DEVICE_TABLE,
                            null, ROOM_ID + "==" + roomId + " And " + IS_DELETE + "==0", null, null,
                            null, null, null);
                }
            }
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entDevice device = new entDevice();
                        device.setDEVICE_Id(_mCursor.getInt(0));
                        device.setROOM_Id(_mCursor.getInt(1));
                        device.setDEVICE_NAME(_mCursor.getString(2));
                        device.setSTT(_mCursor.getInt(3));
                        device.setDIM(_mCursor.getInt(4));
                        device.setTIME_ONE_A(_mCursor.getString(5));
                        device.setTIME_ONE_B(_mCursor.getString(6));
                        device.setTIME_TWO_A(_mCursor.getString(7));
                        device.setTIME_TWO_B(_mCursor.getString(8));
                        device.setCL(_mCursor.getInt(9));
                        device.setRGBSTT(_mCursor.getInt(10));
                        device.setENGRD(_mCursor.getInt(11));
                        device.setREDCLR(_mCursor.getInt(12));
                        device.setGREENCLR(_mCursor.getInt(13));
                        device.setBLUECLR(_mCursor.getInt(14));
                        device.setRGB(_mCursor.getInt(15));
                        device.setDAYS(_mCursor.getString(16));
                        device.setALL_STT(_mCursor.getInt(17));
                        device.setMIC(_mCursor.getInt(18));
                        device.setGROUP(_mCursor.getInt(19));
//                        device.setDEVICE_COUNT(_mCursor.getInt(20));
                        device.setMODULE(_mCursor.getString(21));
                        device.setGROUP_DIM(_mCursor.getInt(22));
                        device.setPOSITION(_mCursor.getInt(23));
                        device.setMODULE_STT(_mCursor.getInt(24));
                        device.setBACKLIGHT_RGB_STT(_mCursor.getInt(25));
                        device.setBACKLIGHT_DIM_STT(_mCursor.getInt(26));
                        device.setMODULE_NUMBER(_mCursor.getInt(27));
                        device.setIS_DELETE(_mCursor.getInt(28));
                        device.setIS_EXPAND(false);
                        devices.add(device);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

//        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return devices;
    }

    public List<entDevice> getListOfDevicesForNotification(int roomId) {
        List<entDevice> devices = new ArrayList<entDevice>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();

            _mCursor = DB.query(false, DEVICE_TABLE,
                    null, ROOM_ID + "==" + roomId + " And " + IS_DELETE + "==0", null, null,
                    null, STT + " DESC", null);

            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entDevice device = new entDevice();
                        device.setDEVICE_Id(_mCursor.getInt(0));
                        device.setROOM_Id(_mCursor.getInt(1));
                        device.setDEVICE_NAME(_mCursor.getString(2));
                        device.setSTT(_mCursor.getInt(3));
                        device.setDIM(_mCursor.getInt(4));
                        device.setTIME_ONE_A(_mCursor.getString(5));
                        device.setTIME_ONE_B(_mCursor.getString(6));
                        device.setTIME_TWO_A(_mCursor.getString(7));
                        device.setTIME_TWO_B(_mCursor.getString(8));
                        device.setCL(_mCursor.getInt(9));
                        device.setRGBSTT(_mCursor.getInt(10));
                        device.setENGRD(_mCursor.getInt(11));
                        device.setREDCLR(_mCursor.getInt(12));
                        device.setGREENCLR(_mCursor.getInt(13));
                        device.setBLUECLR(_mCursor.getInt(14));
                        device.setRGB(_mCursor.getInt(15));
                        device.setDAYS(_mCursor.getString(16));
                        device.setALL_STT(_mCursor.getInt(17));
                        device.setMIC(_mCursor.getInt(18));
                        device.setGROUP(_mCursor.getInt(19));
//                        device.setDEVICE_COUNT(_mCursor.getInt(20));
                        device.setMODULE(_mCursor.getString(21));
                        device.setGROUP_DIM(_mCursor.getInt(22));
                        device.setPOSITION(_mCursor.getInt(23));
                        device.setMODULE_STT(_mCursor.getInt(24));
                        device.setBACKLIGHT_RGB_STT(_mCursor.getInt(25));
                        device.setBACKLIGHT_DIM_STT(_mCursor.getInt(26));
                        device.setMODULE_NUMBER(_mCursor.getInt(27));
                        device.setIS_DELETE(_mCursor.getInt(28));
                        device.setIS_EXPAND(false);
                        devices.add(device);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return devices;
    }

    public List<entDashboard> getListOfItems() {
        List<entDashboard> items = new ArrayList<entDashboard>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, DASHBOARD_TABLE,
                    null, null, null, null,
                    null, ITEM_POSITION + " ASC", null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entDashboard item = new entDashboard();
                        item.setITEM_PID(_mCursor.getInt(0));
                        item.setITEM_NAME(_mCursor.getString(1));
                        item.setITEM_IMAGE(_mCursor.getString(2));
                        item.setITEM_POSITION(_mCursor.getInt(3));
                        items.add(item);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return items;
    }

    public List<entRooms> getListOfRooms(boolean isAll) {
        List<entRooms> rooms = new ArrayList<entRooms>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            if (isAll) {
                _mCursor = DB.query(false, ROOM_TABLE,
                        null, null, null, null,
                        null, null, null);
            } else {
                _mCursor = DB.query(false, ROOM_TABLE,
                        null, ROOM_ACTIVE + "==" + "'" + 1 + "'", null, null,
                        null, null, null);
            }
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entRooms room = new entRooms();
                        room.setROOM_Id(_mCursor.getInt(0));
                        room.setROOM_NAME(_mCursor.getString(1));
                        room.setROOM_IS_SELECTED(_mCursor.getInt(2));
                        room.setROOM_ALL_SIT(_mCursor.getInt(3));
                        room.setROOM_GRP_SIT(_mCursor.getInt(4));
                        room.setMODULE_COUNT(_mCursor.getInt(5));
                        room.setACTIVE(_mCursor.getInt(6));
                        room.setPROFILE_DATA(_mCursor.getString(7));
                        room.setGROUP_COLOR(_mCursor.getInt(8));
                        room.setGROUP_BUZZ(_mCursor.getInt(9));
                        rooms.add(room);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return rooms;
    }

    public List<entRooms> getListOfRoomsWhichDeviceAdded() {
        List<entRooms> rooms = new ArrayList<entRooms>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();

            _mCursor = DB.query(false, ROOM_TABLE,
                    null, MODULE_COUNT + "!=" + "'" + 0 + "'", null, null,
                    null, null, null);

            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entRooms room = new entRooms();
                        room.setROOM_Id(_mCursor.getInt(0));
                        room.setROOM_NAME(_mCursor.getString(1));
                        room.setROOM_IS_SELECTED(_mCursor.getInt(2));
                        room.setROOM_ALL_SIT(_mCursor.getInt(3));
                        room.setROOM_GRP_SIT(_mCursor.getInt(4));
                        room.setMODULE_COUNT(_mCursor.getInt(5));
                        room.setACTIVE(_mCursor.getInt(6));
                        room.setPROFILE_DATA(_mCursor.getString(7));
                        room.setGROUP_COLOR(_mCursor.getInt(8));
                        room.setGROUP_BUZZ(_mCursor.getInt(9));
                        rooms.add(room);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return rooms;
    }

    public List<entSecurity> getListSensor() {
        List<entSecurity> sensors = new ArrayList<entSecurity>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, SENSOR_TABLE,
                    null, null, null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entSecurity sensor = new entSecurity();
                        sensor.setSECURITY_ID(_mCursor.getInt(0));
                        sensor.setSENSOR_NAME(_mCursor.getString(1));
                        sensor.setSENSOR_IMAGE(_mCursor.getString(2));
                        sensor.setSENSOR_LOCATION(_mCursor.getString(3));
                        sensor.setSENSOR_LIGHT(_mCursor.getString(4));
                        sensor.setDEVICE_ID(_mCursor.getInt(5));
                        sensor.setACTION_NAME(_mCursor.getString(6));
                        sensor.setACTION_VALUE(_mCursor.getString(7));
                        sensor.setROOM_CODE(_mCursor.getString(8));
                        sensor.setMODULE_CODE(_mCursor.getString(9));
                        sensor.setSENSOR_NUMBER(_mCursor.getString(10));
                        sensor.setIS_ENAMBLE(_mCursor.getInt(11));
                        sensors.add(sensor);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return sensors;
    }

    public List<entTV> getListTV() {
        List<entTV> tvs = new ArrayList<entTV>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, TV_TABLE,
                    null, null, null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entTV tv = new entTV();
                        tv.setTV_Id(_mCursor.getInt(0));
                        tv.setROOM_Id(_mCursor.getInt(1));
                        tv.setTV_NAME(_mCursor.getString(2));
                        tv.setROOM_NAME(_mCursor.getString(3));
                        tv.setTV_MODEL(_mCursor.getString(4));
                        tv.setTV_PROTOCOL(_mCursor.getString(5));
                        tv.setSET_NAME(_mCursor.getString(6));
                        tv.setSET_PROTOCOL(_mCursor.getString(7));
                        tv.setTV_STT(_mCursor.getString(8));
                        tv.setTV_IS_SELECTED(_mCursor.getInt(9));
                        tv.setSET_STT(_mCursor.getString(10));
                        tv.setMUTE(_mCursor.getString(11));
                        tv.setTV_UP(_mCursor.getString(12));
                        tv.setTV_DOWN(_mCursor.getString(13));
                        tv.setTV_LEFT(_mCursor.getString(14));
                        tv.setTV_RIGHT(_mCursor.getString(15));
                        tv.setTV_OK(_mCursor.getString(16));
                        tv.setTV_BACK(_mCursor.getString(17));
                        tv.setTV_EXIT(_mCursor.getString(18));
                        tv.setTV_VOL_PLUS(_mCursor.getString(19));
                        tv.setTV_VOL_MINUS(_mCursor.getString(20));
                        tv.setTV_CH_PLUS(_mCursor.getString(21));
                        tv.setTV_CH_MINUS(_mCursor.getString(22));
                        tv.setTV_ZERO(_mCursor.getString(23));
                        tv.setTV_ONE(_mCursor.getString(24));
                        tv.setTV_TWO(_mCursor.getString(25));
                        tv.setTV_THREE(_mCursor.getString(26));
                        tv.setTV_FOUR(_mCursor.getString(27));
                        tv.setTV_FIVE(_mCursor.getString(28));
                        tv.setTV_SIX(_mCursor.getString(29));
                        tv.setTV_SEVEN(_mCursor.getString(30));
                        tv.setTV_EIGHT(_mCursor.getString(31));
                        tv.setTV_NINE(_mCursor.getString(32));
                        tv.setTV_STAR(_mCursor.getString(33));
                        tv.setTV_HAS(_mCursor.getString(34));

                        tvs.add(tv);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return tvs;
    }

    public void updateItemPosition(int iId, int itemPosition) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(ITEM_POSITION, itemPosition);
            DB.update(DASHBOARD_TABLE, _cv, ITME_ID + "==" + "'" + iId + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void updateSensorLocation(int sId, String location) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(SENSOR_LOCATION, location);
            DB.update(SENSOR_TABLE, _cv, SECURITY_ID + "==" + "'" + sId + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void updateSensorLight(int sId, String light) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(SENSOR_LIGHT, light);
            DB.update(SENSOR_TABLE, _cv, SECURITY_ID + "==" + "'" + sId + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void updateSensor(entSecurity entSecurity) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(SENSOR_NAME, entSecurity.getSENSOR_NAME());
            _cv.put(SENSOR_IMAGE, entSecurity.getSENSOR_IMAGE());
            _cv.put(SENSOR_LOCATION, entSecurity.getSENSOR_LOCATION());
            _cv.put(SENSOR_LIGHT, "LIGHT 1 \\n [BEDROOM 01]");
            _cv.put(ACTION_NAME, entSecurity.getACTION_NAME());
            _cv.put(ACTION_VALUE, entSecurity.getACTION_VALUE());
            _cv.put(DEVICE_ID, entSecurity.getDEVICE_ID());
            _cv.put(ROOM_CODE, entSecurity.getROOM_CODE());
            _cv.put(MODULE_CODE, entSecurity.getMODULE_CODE());
            _cv.put(SENSOR_NUMBER, entSecurity.getSENSOR_NUMBER());
            _cv.put(IS_ENABLE, entSecurity.getIS_ENAMBLE());
            DB.update(SENSOR_TABLE, _cv,
                    SECURITY_ID + "==" + "'" + entSecurity.getSECURITY_ID() + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void updateRoomSelection(int rId, int size) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            int id;
//            System.out.println("ROOM ID " + rId);
            for (int i = 1; i <= size; i++) {
                ContentValues _cv = new ContentValues();
                if (i == rId) {
                    _cv.put(IS_SELECTED, 1);
//                    System.out.println("UPDATE");
                    id = DB.update(ROOM_TABLE, _cv, ROOM_ID + "==" + "'" + i + "'", null);
                } else {
                    _cv.put(IS_SELECTED, 0);
                    id = DB.update(ROOM_TABLE, _cv, ROOM_ID + "==" + "'" + i + "'", null);
                }
//                System.out.println("ID " + id);
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public int getDeviceCount(String name, int roomid) {
        int count = 0;
        try {

//            Cursor cursor = DB.rawQuery(
//                    "SELECT COUNT (*) FROM " + DEVICE_TABLE + " WHERE " + DEVICE_NAME + " LIKE ?" +
//                            "%" + name + "%" + "AND" + ROOM_ID + "==" + "'" + roomid + "'", null);
            Cursor cursor = DB.rawQuery(
                    "SELECT COUNT (*) FROM DeviceTable WHERE NAME LIKE '%" + name + "%' AND rID="
                            + roomid + " And " + IS_DELETE + "==0", null);

            cursor.moveToFirst();
            count = cursor.getInt(0);
            cursor.close();
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        }
        count++;
        return count;
    }

    public int getModuleCount(int roomid) {
        int count = 0;
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, ROOM_TABLE,
                    new String[]{MODULE_COUNT}, ROOM_ID + "==" + "'" + roomid + "'", null, null,
                    null, null, null);
//            System.out.println("count " + _mCursor.getCount());
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
//                do{
                    count = _mCursor.getInt(0);
                    _mCursor.close();
//                    System.out.println("MODULE COUNT >> " + count);
//                }while (_mCursor.moveToNext());

                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
//        System.out.println("MODULE COUNT " + count);
        count++;
        return count;
    }

    public void updateModuleCount(int roomid, int count) {
//        count++;
//        System.out.println("module update tp "+count);
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            int id;
            ContentValues _cv = new ContentValues();
            _cv.put(MODULE_COUNT, count);
//            System.out.println("UPDATE");
            id = DB.update(ROOM_TABLE, _cv, ROOM_ID + "==" + "'" + roomid + "'", null);
//            System.out.println("ID " + id);
//            DB.rawQuery("UPDATE RoomTable SET moduleCount="+count+" WHERE rID="+roomid,null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void deletedevice(int room_id) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            String modul = "";
//            Cursor _mCursor = DB.query(false, DEVICE_TABLE,
//                    new String[]{MODULE}, DEVICE_ID + "==" + "'" + device_id + "'", null, null,
//                    null, null, null);
////            System.out.println("ALL_STT " + _mCursor.getCount());
//            if (_mCursor != null) {
//                if (_mCursor.moveToFirst()) {
//                    modul = _mCursor.getString(0).substring(1,3);
////                    System.out.println("module is "+modul);
//                    _mCursor.close();
//                }
//            }
            ContentValues contentValues = new ContentValues();
            contentValues.put(IS_DELETE, 1);
            DB.update(DEVICE_TABLE, contentValues, ROOM_ID + "==" + "'" + room_id + "'", null);
//            Cursor cursor =DB.rawQuery("SELECT * FROM DeviceTable WHERE module LIKE '%"+modul+"%' AND rID="+room_id,null);
////            System.out.println("CURSOR COUNT "+cursor.getCount());
//            if (cursor.getCount()==0){
//                updateModuleCount(room_id,Integer.parseInt(modul.replace(",",""))-1);
//            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
        }
    }

    public int getAllStt(int room_id) {
        int count = 0;
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, ROOM_TABLE,
                    new String[]{ROOM_ALL_SIT}, ROOM_ID + "==" + "'" + room_id + "'", null, null,
                    null, null, null);
//            System.out.println("ALL_STT " + _mCursor.getCount());
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    count = _mCursor.getInt(0);
                    _mCursor.close();
//                    System.out.println("ALL_STT >> " + count);
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return count;
    }

    public int getAllGrp(int room_id) {
        int count = 0;
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, ROOM_TABLE,
                    new String[]{GRP_SIT}, ROOM_ID + "==" + "'" + room_id + "'", null, null,
                    null, null, null);
//            System.out.println("ALL_STT " + _mCursor.getCount());
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    count = _mCursor.getInt(0);
                    _mCursor.close();
//                    System.out.println("ALL_STT >> " + count);
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return count;
    }

    public void updateAllONOFF(int selectedRoomId, int i) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            int id;
            ContentValues _cv = new ContentValues();
            _cv.put(ROOM_ALL_SIT, i);
//            System.out.println("UPDATE");
            if (selectedRoomId == -1) {
                id = DB.update(ROOM_TABLE, _cv, null, null);
            } else {
                id = DB.update(ROOM_TABLE, _cv, ROOM_ID + "==" + "'" + selectedRoomId + "'", null);
            }
//            System.out.println("ID " + id);
//            DB.rawQuery("UPDATE RoomTable SET ALL_STT="+i+" WHERE rID='"+selectedRoomId+"'",null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void updateDeviceONOFF(int deviceID, int i) {
        try {
            DB = DB_HELPER.getWritableDatabase();
            int id;
            ContentValues _cv = new ContentValues();
            _cv.put(STT, i);
            id = DB.update(DEVICE_TABLE, _cv, DEVICE_ID + "==" + "'" + deviceID + "'", null);
//
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void updateAllGoruping(int selectedRoomId, int i) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            int id;
            ContentValues _cv = new ContentValues();
            _cv.put(GRP_SIT, i);
//            System.out.println("UPDATE");
            id = DB.update(ROOM_TABLE, _cv, ROOM_ID + "==" + "'" + selectedRoomId + "'", null);
//            DB.rawQuery("UPDATE RoomTable SET GRP_STT="+i+" WHERE rID="+selectedRoomId,null);
//            System.out.println("ID " + id);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void updateRoom(entRooms entRooms) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB = DB_HELPER.DB.openDatabase(Constants.DB_PTH, null, SQLiteDatabase.OPEN_READONLY);
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(ROOM_NAME, entRooms.getROOM_NAME());
            _cv.put(IS_SELECTED, entRooms.getROOM_IS_SELECTED());
            _cv.put(ROOM_ALL_SIT, entRooms.getROOM_ALL_SIT());
            _cv.put(GRP_SIT, entRooms.getROOM_GRP_SIT());
            _cv.put(MODULE_COUNT, entRooms.getMODULE_COUNT());
            _cv.put(ROOM_ACTIVE, entRooms.getACTIVE());
//            _cv.put(PROFILE_PHOTO, entRooms.getPROFILE_DATA());
            _cv.put(GROUP_COLOR, entRooms.getGROUP_COLOR());
            _cv.put(GROUP_BUZZ, entRooms.getGROUP_BUZZ());

            DB.update(ROOM_TABLE, _cv, ROOM_ID + "==" + "'" + entRooms.getROOM_Id() + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            // DB.endTransaction();
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void updateRoomPhoto(String data, int id) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB = DB_HELPER.DB.openDatabase(Constants.DB_PTH, null, SQLiteDatabase.OPEN_READONLY);
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(PROFILE_PHOTO, data);

            DB.update(ROOM_TABLE, _cv, ROOM_ID + "==" + "'" + id + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            // DB.endTransaction();
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }

    }

    public long insertTv(entTV entTV) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(TV_NAME, entTV.getTV_NAME());
            _cv.put(ROOM_NAME, entTV.getROOM_NAME());
            _cv.put(SET_NAME, entTV.getSET_NAME());
            _cv.put(TV_IS_SELECTED, 0);

            _id = DB.insert(TV_TABLE, null, _cv);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("tv id " + _id);
        return _id;

    }

    public void updateTv(entTV entTV) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(TV_NAME, entTV.getTV_NAME());
            _cv.put(TV_MODEL, entTV.getTV_MODEL());
            _cv.put(TV_STT, entTV.getTV_STT());
            _cv.put(TV_PROTOCOLO, entTV.getSET_PROTOCOL());
            _cv.put(ROOM_NAME, entTV.getROOM_NAME());
            _cv.put(ROOM_ID, entTV.getROOM_Id());
            _cv.put(SET_NAME, entTV.getSET_NAME());
            _cv.put(SET_PROTOCOL, entTV.getSET_PROTOCOL());
            _cv.put(TV_IS_SELECTED, entTV.getTV_IS_SELECTED());
            _id = DB.update(TV_TABLE, _cv, TV_ID + "==" + "'" + entTV.getTV_Id() + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("tv id " + _id);

    }

    public void insetTvModelDetail(ArrayList<entTvModel> entTvModels) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            for (int i = 0; i < entTvModels.size(); i++) {

                ContentValues _cv = new ContentValues();
                _cv.put(TV_BRAND_NAME, entTvModels.get(i).getBRAND_NAME());
                _cv.put(TV_MODEL_NAME, entTvModels.get(i).getBRAND_MODEL());
                _cv.put(TV_POWER_DATA, entTvModels.get(i).getPOWER_DATA());

                _id = DB.insert(TV_MODEL_TABLE, null, _cv);
                System.out.println("tv id " + _id);
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }

    }

    public void updateRemoteValue(entTV entTV) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(TV_SET_STT, entTV.getSET_STT());
            _cv.put(TV_MUTE, entTV.getMUTE());
            _cv.put(TV_UP, entTV.getTV_UP());
            _cv.put(TV_DOWN, entTV.getTV_DOWN());
            _cv.put(TV_LEFT, entTV.getTV_LEFT());
            _cv.put(TV_RIGHT, entTV.getTV_RIGHT());
            _cv.put(TV_OK, entTV.getTV_OK());
            _cv.put(TV_BACK, entTV.getTV_BACK());
            _cv.put(TV_EXIT, entTV.getTV_EXIT());
            _cv.put(TV_VOL_PLUS, entTV.getTV_VOL_PLUS());
            _cv.put(TV_VOL_MINUS, entTV.getTV_VOL_MINUS());
            _cv.put(TV_CH_PLUS, entTV.getTV_CH_PLUS());
            _cv.put(TV_CH_MINUS, entTV.getTV_CH_MINUS());
            _cv.put(TV_ONE, entTV.getTV_ONE());
            _cv.put(TV_TWO, entTV.getTV_TWO());
            _cv.put(TV_THREE, entTV.getTV_THREE());
            _cv.put(TV_FOUR, entTV.getTV_FOUR());
            _cv.put(TV_FIVE, entTV.getTV_FIVE());
            _cv.put(TV_SIX, entTV.getTV_SIX());
            _cv.put(TV_SEVEN, entTV.getTV_SEVEN());
            _cv.put(TV_EIGHT, entTV.getTV_EIGHT());
            _cv.put(TV_NINE, entTV.getTV_NINE());
            _cv.put(TV_ZERO, entTV.getTV_ZERO());
            _cv.put(TV_STAR, entTV.getTV_STAR());
            _cv.put(TV_HAS, entTV.getTV_HAS());

            _id = DB.update(TV_TABLE, _cv, TV_ID + "==" + "'" + entTV.getTV_Id() + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("tv id " + _id);
    }

    public entTV getTVData(int id) {
        entTV tv = new entTV();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, TV_TABLE,
                    null, TV_ID + "==" + "'" + id + "'", null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        tv.setTV_Id(_mCursor.getInt(0));
                        tv.setROOM_Id(_mCursor.getInt(1));
                        tv.setTV_NAME(_mCursor.getString(2));
                        tv.setROOM_NAME(_mCursor.getString(3));
                        tv.setTV_MODEL(_mCursor.getString(4));
                        tv.setTV_PROTOCOL(_mCursor.getString(5));
                        tv.setSET_NAME(_mCursor.getString(6));
                        tv.setSET_PROTOCOL(_mCursor.getString(7));
                        tv.setTV_STT(_mCursor.getString(8));
                        tv.setTV_IS_SELECTED(_mCursor.getInt(9));
                        tv.setSET_STT(_mCursor.getString(10));
                        tv.setMUTE(_mCursor.getString(11));
                        tv.setTV_UP(_mCursor.getString(12));
                        tv.setTV_DOWN(_mCursor.getString(13));
                        tv.setTV_LEFT(_mCursor.getString(14));
                        tv.setTV_RIGHT(_mCursor.getString(15));
                        tv.setTV_OK(_mCursor.getString(16));
                        tv.setTV_BACK(_mCursor.getString(17));
                        tv.setTV_EXIT(_mCursor.getString(18));
                        tv.setTV_VOL_PLUS(_mCursor.getString(19));
                        tv.setTV_VOL_MINUS(_mCursor.getString(20));
                        tv.setTV_CH_PLUS(_mCursor.getString(21));
                        tv.setTV_CH_MINUS(_mCursor.getString(22));
                        tv.setTV_ZERO(_mCursor.getString(23));
                        tv.setTV_ONE(_mCursor.getString(24));
                        tv.setTV_TWO(_mCursor.getString(25));
                        tv.setTV_THREE(_mCursor.getString(26));
                        tv.setTV_FOUR(_mCursor.getString(27));
                        tv.setTV_FIVE(_mCursor.getString(28));
                        tv.setTV_SIX(_mCursor.getString(29));
                        tv.setTV_SEVEN(_mCursor.getString(30));
                        tv.setTV_EIGHT(_mCursor.getString(31));
                        tv.setTV_NINE(_mCursor.getString(32));
                        tv.setTV_STAR(_mCursor.getString(33));
                        tv.setTV_HAS(_mCursor.getString(34));

                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return tv;
    }

    public long insertAC(entAC entAc) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(AC_NAME, entAc.getAC_NAME());
            _cv.put(ROOM_NAME, entAc.getROOM_NAME());
            _cv.put(TV_IS_SELECTED, 0);

            _id = DB.insert(AC_TABLE, null, _cv);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("tv id " + _id);
        return _id;

    }

    public void updateAc(entAC entAC) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(AC_NAME, entAC.getAC_NAME());
            _cv.put(AC_PROTOCOLO, entAC.getAC_PROTOCOL());
            _cv.put(ROOM_NAME, entAC.getROOM_NAME());
            _cv.put(ROOM_ID, entAC.getROOM_Id());
            _cv.put(TV_IS_SELECTED, entAC.getAC_IS_SELECTED());
            _id = DB.update(AC_TABLE, _cv, AC_ID + "==" + "'" + entAC.getAC_Id() + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("tv id " + _id);

    }

    public List<entAC> getListAC() {
        List<entAC> ACs = new ArrayList<entAC>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, AC_TABLE,
                    null, null, null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entAC ac = new entAC();
                        ac.setAC_Id(_mCursor.getInt(0));
                        ac.setROOM_Id(_mCursor.getInt(1));
                        ac.setAC_NAME(_mCursor.getString(2));
                        ac.setROOM_NAME(_mCursor.getString(3));
                        ac.setAC_PROTOCOL(_mCursor.getString(4));
                        ac.setAC_STT(_mCursor.getString(5));
                        ac.setAC_LEFT(_mCursor.getString(6));
                        ac.setAC_RIGHT(_mCursor.getString(7));
                        ac.setAC_TEMP(_mCursor.getString(8));
                        ac.setAC_PLUS(_mCursor.getString(9));
                        ac.setAC_MINUS(_mCursor.getString(10));
                        ac.setAC_FAN(_mCursor.getString(11));
                        ac.setAC_MODE(_mCursor.getString(12));
                        ac.setAC_SWING(_mCursor.getString(13));
                        ac.setAC_TURBO(_mCursor.getString(14));
                        ac.setAC_SENSOR(_mCursor.getString(15));
                        ac.setAC_IS_SELECTED(_mCursor.getInt(16));

                        ACs.add(ac);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return ACs;

    }

    public entAC getACData(int id) {
        entAC ac = new entAC();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, AC_TABLE,
                    null, AC_ID + "==" + "'" + id + "'", null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        ac.setAC_Id(_mCursor.getInt(0));
                        ac.setROOM_Id(_mCursor.getInt(1));
                        ac.setAC_NAME(_mCursor.getString(2));
                        ac.setROOM_NAME(_mCursor.getString(3));
                        ac.setAC_PROTOCOL(_mCursor.getString(4));
                        ac.setAC_STT(_mCursor.getString(5));
                        ac.setAC_LEFT(_mCursor.getString(6));
                        ac.setAC_RIGHT(_mCursor.getString(7));
                        ac.setAC_TEMP(_mCursor.getString(8));
                        ac.setAC_PLUS(_mCursor.getString(9));
                        ac.setAC_MINUS(_mCursor.getString(10));
                        ac.setAC_FAN(_mCursor.getString(11));
                        ac.setAC_MODE(_mCursor.getString(12));
                        ac.setAC_SWING(_mCursor.getString(13));
                        ac.setAC_TURBO(_mCursor.getString(14));
                        ac.setAC_SENSOR(_mCursor.getString(15));
                        ac.setAC_IS_SELECTED(_mCursor.getInt(16));

                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return ac;
    }

    public void updateACRemoteValue(entAC ac) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(AC_STT, ac.getAC_STT());
            _cv.put(AC_LEFT, ac.getAC_LEFT());
            _cv.put(AC_RIGHT, ac.getAC_RIGHT());
            _cv.put(AC_TEMP, ac.getAC_TEMP());
            _cv.put(AC_PLUS, ac.getAC_PLUS());
            _cv.put(AC_MINUS, ac.getAC_MINUS());
            _cv.put(AC_FAN, ac.getAC_FAN());
            _cv.put(AC_MODE, ac.getAC_MODE());
            _cv.put(AC_SWING, ac.getAC_SWING());
            _cv.put(AC_TURBO, ac.getAC_TURBO());
            _cv.put(AC_SENSOR, ac.getAC_SENSOR());

            _id = DB.update(AC_TABLE, _cv, AC_ID + "==" + "'" + ac.getAC_Id() + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("tv id " + _id);
    }

    public void updateDevicePosition(List<entDevice> itemList) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
            for (int i = 0; i < itemList.size(); i++) {
                ContentValues _cv = new ContentValues();
                _cv.put(POSITION, itemList.get(i).getPOSITION());
                _id = DB.update(DEVICE_TABLE, _cv,
                        DEVICE_ID + "==" + "'" + itemList.get(i).getDEVICE_Id() + "'", null);
                System.out.println("tv id " + _id);

            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public List<entCurtain> getListCurtain() {
        List<entCurtain> entCurtains = new ArrayList<entCurtain>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, CURTAIN_TABLE,
                    null, null, null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entCurtain curtain = new entCurtain();
                        curtain.setCURTAIN_Id(_mCursor.getInt(0));
                        curtain.setROOM_Id(_mCursor.getInt(1));
                        curtain.setCURTAIN_NAME(_mCursor.getString(2));
                        curtain.setROOM_NAME(_mCursor.getString(3));
                        curtain.setCURTAIN_STT(_mCursor.getString(4));
                        curtain.setCURTAIN_LEFT(_mCursor.getString(5));
                        curtain.setCURTAIN_RIGHT(_mCursor.getString(6));
                        curtain.setCURTAIN_UP(_mCursor.getString(7));
                        curtain.setCURTAIN_DOWN(_mCursor.getString(8));
                        curtain.setCURTAIN_STOP(_mCursor.getString(9));
                        curtain.setCURTAIN_IMAGE(_mCursor.getString(10));
                        curtain.setCURTAIN_POSITION(_mCursor.getInt(11));

                        entCurtains.add(curtain);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return entCurtains;
    }

    public long insertCurtain(entCurtain curtain) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(CURTAIN_NAME, curtain.getCURTAIN_NAME());
            _cv.put(ROOM_NAME, curtain.getROOM_NAME());
            _cv.put(CURTAIN_STT, curtain.getCURTAIN_STT());
            _cv.put(CURTAIN_POSITION, 10);

            _id = DB.insert(CURTAIN_TABLE, null, _cv);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("Curtain id " + _id);
        return _id;
    }

    public void updateCurtain(entCurtain entCurtain) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(CURTAIN_NAME, entCurtain.getCURTAIN_NAME());
            _cv.put(ROOM_NAME, entCurtain.getROOM_NAME());
            _cv.put(ROOM_ID, entCurtain.getROOM_Id());
            _cv.put(CURTAIN_LEFT, entCurtain.getCURTAIN_LEFT());
            _cv.put(CURTAIN_RIGHT, entCurtain.getCURTAIN_RIGHT());
            _cv.put(CURTAIN_UP, entCurtain.getCURTAIN_UP());
            _cv.put(CURTAIN_DOWN, entCurtain.getCURTAIN_DOWN());
            _cv.put(CURTAIN_STOP, entCurtain.getCURTAIN_STOP());
            _cv.put(CURTAIN_STT, entCurtain.getCURTAIN_STT());
            _cv.put(CURTAIN_IMAGE, entCurtain.getCURTAIN_IMAGE());
            _cv.put(CURTAIN_POSITION, entCurtain.getCURTAIN_POSITION());

            _id = DB.update(CURTAIN_TABLE, _cv,
                    CURTAIN_ID + "==" + "'" + entCurtain.getCURTAIN_Id() + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("tv id " + _id);

    }

    public String getRoomName(int room_id) {
        Cursor _mCursor = null;
        String roomName = "";
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, ROOM_TABLE,
                    new String[]{ROOM_NAME}, ROOM_ID + "==" + "'" + room_id + "'", null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    roomName = _mCursor.getString(0);

                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return roomName;
    }

    public String getDeviceName(int room_id, int DeviceId) {
        Cursor _mCursor = null;
        String DeviceName = "";
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, DEVICE_TABLE,
                    new String[]{DEVICE_NAME}, ROOM_ID + "==" + "'" + room_id + "' And " + DEVICE_ID + "=='" + DeviceId + "'", null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    DeviceName = _mCursor.getString(0);

                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return DeviceName;
    }

    public int insertSecurity(entSecurity entSecurity) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(SENSOR_NAME, entSecurity.getSENSOR_NAME());
            _cv.put(SENSOR_LOCATION, entSecurity.getSENSOR_LOCATION());
            _cv.put(SENSOR_LIGHT, "");
            _cv.put(ACTION_NAME, "");
            _cv.put(ACTION_VALUE, "");
            _cv.put(ROOM_CODE, "");
            _cv.put(MODULE_CODE, "");
            _cv.put(SENSOR_NUMBER, "");
            _cv.put(IS_ENABLE, 1);

            _id = DB.insert(SENSOR_TABLE, null, _cv);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            // DB.endTransaction();
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("item id " + _id);

        return (int) _id;
    }

    public void updateModuleBackLight(int device_id, int i) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();

            _cv.put(MODULE_STT, i);

            _id = DB.update(DEVICE_TABLE, _cv, DEVICE_ID + "==" + "'" + device_id + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("module id " + _id);
    }

    public void updateModuleBackLightColor(int device_id, int i) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();

            _cv.put(BACKLIGHT_COLOR, i);

            _id = DB.update(DEVICE_TABLE, _cv, DEVICE_ID + "==" + "'" + device_id + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("module id " + _id);
    }

    public void updateModuleBackLightDim(int device_id, int i) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();

            _cv.put(BACKLIGHT_DIM, i);

            _id = DB.update(DEVICE_TABLE, _cv, DEVICE_ID + "==" + "'" + device_id + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("module id " + _id);
    }

    public entDevice getDeviceOfPump() {
        entDevice device = new entDevice();
        try {
            DB = DB_HELPER.getWritableDatabase();
            Cursor _mCursor = DB.rawQuery(
                    "SELECT * FROM DeviceTable WHERE NAME LIKE '%" + "water" + "%' OR NAME LIKE '%"
                            + "pump" + "%' And " + IS_DELETE + " == 0", null);
            if (_mCursor.getCount() > 0) {
                _mCursor.moveToFirst();
                device.setDEVICE_Id(_mCursor.getInt(0));
                device.setROOM_Id(_mCursor.getInt(1));
                device.setDEVICE_NAME(_mCursor.getString(2));
                device.setSTT(_mCursor.getInt(3));
                device.setDIM(_mCursor.getInt(4));
                device.setTIME_ONE_A(_mCursor.getString(5));
                device.setTIME_ONE_B(_mCursor.getString(6));
                device.setTIME_TWO_A(_mCursor.getString(7));
                device.setTIME_TWO_B(_mCursor.getString(8));
                device.setCL(_mCursor.getInt(9));
                device.setRGBSTT(_mCursor.getInt(10));
                device.setENGRD(_mCursor.getInt(11));
                device.setREDCLR(_mCursor.getInt(12));
                device.setGREENCLR(_mCursor.getInt(13));
                device.setBLUECLR(_mCursor.getInt(14));
                device.setRGB(_mCursor.getInt(15));
                device.setDAYS(_mCursor.getString(16));
                device.setALL_STT(_mCursor.getInt(17));
                device.setMIC(_mCursor.getInt(18));
                device.setGROUP(_mCursor.getInt(19));
//                        device.setDEVICE_COUNT(_mCursor.getInt(20));
                device.setMODULE(_mCursor.getString(21));
                device.setGROUP_DIM(_mCursor.getInt(22));
                device.setPOSITION(_mCursor.getInt(23));
                device.setMODULE_STT(_mCursor.getInt(24));
                device.setIS_EXPAND(false);
            }
            _mCursor.close();
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        }

        return device;
    }

    public void insertChannel(entChannel entChannel) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(CHANNEL_NAME, entChannel.getName());
            _cv.put(CHANNEL_IMAGE, entChannel.getImg());
            _cv.put(CHANNEL_GROUP, entChannel.getChannelGroup());
            _cv.put(CHANNEL_NUMBER, entChannel.getNumber());

            _id = DB.insert(CHANNEL_TABLE, null, _cv);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            // DB.endTransaction();
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("item id " + _id);

    }

    public List<entChannel> getListOfChannels(String group) {
        List<entChannel> entChannels = new ArrayList<entChannel>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, CHANNEL_TABLE,
                    null, CHANNEL_GROUP + "==" + "'" + group + "'", null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entChannel channel = new entChannel();
                        channel.setChannelId(_mCursor.getInt(0));
                        channel.setName(_mCursor.getString(1));
                        channel.setImg(_mCursor.getString(2));
                        channel.setNumber(_mCursor.getString(3));
                        channel.setChannelGroup(_mCursor.getString(4));
                        entChannels.add(channel);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return entChannels;
    }

    public void updateChannel(entChannel entChannel) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(CHANNEL_NAME, entChannel.getName());
            _cv.put(CHANNEL_IMAGE, entChannel.getImg());
            _cv.put(CHANNEL_GROUP, entChannel.getChannelGroup());
            _cv.put(CHANNEL_NUMBER, entChannel.getNumber());

            _id = DB.update(CHANNEL_TABLE, _cv,
                    CHANNEL_ID + "==" + "'" + entChannel.getChannelId() + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println(" update channel id " + _id);
    }

    public void deleteChannel(int channelId) {
        try {
            DB = DB_HELPER.getWritableDatabase();
            int id = DB.delete(CHANNEL_TABLE, CHANNEL_ID + "==" + "'" + channelId + "'", null);
            System.out.println("channel delete id " + id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public ArrayList<entSecurity> getSensorAction(String call) {
        Cursor _mCursor = null;
        entSecurity sensor = new entSecurity();
        ArrayList<entSecurity> securities = new ArrayList<>();
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(true, SENSOR_TABLE,
                    null, SENSOR_NAME + " LIKE '%" + call + "%'",
                    null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        sensor = new entSecurity();
                        sensor.setSECURITY_ID(_mCursor.getInt(0));
                        sensor.setSENSOR_NAME(_mCursor.getString(1));
                        sensor.setSENSOR_IMAGE(_mCursor.getString(2));
                        sensor.setSENSOR_LOCATION(_mCursor.getString(3));
                        sensor.setSENSOR_LIGHT(_mCursor.getString(4));
                        sensor.setDEVICE_ID(_mCursor.getInt(5));
                        sensor.setACTION_NAME(_mCursor.getString(6));
                        sensor.setACTION_VALUE(_mCursor.getString(7));
                        sensor.setROOM_CODE(_mCursor.getString(8));
                        sensor.setMODULE_CODE(_mCursor.getString(9));
                        sensor.setSENSOR_NUMBER(_mCursor.getString(10));
                        sensor.setIS_ENAMBLE(_mCursor.getInt(11));
                        securities.add(sensor);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return securities;
    }

    public int getRoomId(String roomName) {
        Cursor _mCursor = null;
        int roomId = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.rawQuery(
                    "SELECT rID FROM RoomTable WHERE RoomName LIKE '%" + roomName + "%'", null);
//            _mCursor = DB.query(false, ROOM_TABLE,
//                    new String[]{ROOM_ID}, ROOM_NAME + "==" + "'" + room_id + "'", null, null,
//                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    roomId = _mCursor.getInt(0);

                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        System.out.println("room id " + roomId);
        return roomId;
    }

    public entDevice getDeviceFromName(String deviceName, int roomId) {
        entDevice device = new entDevice();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//
            _mCursor = DB.query(false, DEVICE_TABLE,
                    null, ROOM_ID + "==" + roomId + " And " + DEVICE_NAME + "==" + "'" + deviceName
                            .toUpperCase() + "' And " + IS_DELETE + "==0", null, null,
                    null, null, null);

//                 _mCursor = DB.rawQuery(
//                        "SELECT * FROM DeviceTable WHERE rID=="+roomId+ " AND NAME=='"
//                                + deviceName.toUpperCase() + "'", null);

            System.out.println("count " + _mCursor.getCount());
            if (_mCursor != null) {
                _mCursor.moveToFirst();

                if (_mCursor.getCount() > 0) {
                    device.setDEVICE_Id(_mCursor.getInt(0));
                }
                device.setROOM_Id(_mCursor.getInt(1));
                device.setDEVICE_NAME(_mCursor.getString(2));
                device.setSTT(_mCursor.getInt(3));
                device.setDIM(_mCursor.getInt(4));
                device.setTIME_ONE_A(_mCursor.getString(5));
                device.setTIME_ONE_B(_mCursor.getString(6));
                device.setTIME_TWO_A(_mCursor.getString(7));
                device.setTIME_TWO_B(_mCursor.getString(8));
                device.setCL(_mCursor.getInt(9));
                device.setRGBSTT(_mCursor.getInt(10));
                device.setENGRD(_mCursor.getInt(11));
                device.setREDCLR(_mCursor.getInt(12));
                device.setGREENCLR(_mCursor.getInt(13));
                device.setBLUECLR(_mCursor.getInt(14));
                device.setRGB(_mCursor.getInt(15));
                device.setDAYS(_mCursor.getString(16));
                device.setALL_STT(_mCursor.getInt(17));
                device.setMIC(_mCursor.getInt(18));
                device.setGROUP(_mCursor.getInt(19));
//                        device.setDEVICE_COUNT(_mCursor.getInt(20));
                device.setMODULE(_mCursor.getString(21));
                device.setGROUP_DIM(_mCursor.getInt(22));
                device.setPOSITION(_mCursor.getInt(23));
                device.setMODULE_STT(_mCursor.getInt(24));
                device.setBACKLIGHT_RGB_STT(_mCursor.getInt(25));
                device.setBACKLIGHT_DIM_STT(_mCursor.getInt(26));
                device.setMODULE_NUMBER(_mCursor.getInt(27));
                device.setIS_EXPAND(false);
                _mCursor.close();
                _mCursor.close();

            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return device;
    }

    public List<entDevice> getListACs(int room_id) {
        List<entDevice> devices = new ArrayList<entDevice>();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            System.out.println(
                    "SELECT * FROM DeviceTable WHERE NAME LIKE '%" + "AC" + "%' AND " + ROOM_ID
                            + "==" + room_id + " And " + IS_DELETE + " ==0");
            _mCursor = DB.rawQuery(
                    "SELECT * FROM DeviceTable WHERE NAME LIKE '%" + "AC" + "%' AND " + ROOM_ID
                            + "==" + room_id + " And " + IS_DELETE + " ==0", null);
            System.out.println("SIZE " + _mCursor.getCount());
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        entDevice device = new entDevice();
                        device.setDEVICE_Id(_mCursor.getInt(0));
                        device.setROOM_Id(_mCursor.getInt(1));
                        device.setDEVICE_NAME(_mCursor.getString(2));
                        device.setSTT(_mCursor.getInt(3));
                        device.setDIM(_mCursor.getInt(4));
                        device.setTIME_ONE_A(_mCursor.getString(5));
                        device.setTIME_ONE_B(_mCursor.getString(6));
                        device.setTIME_TWO_A(_mCursor.getString(7));
                        device.setTIME_TWO_B(_mCursor.getString(8));
                        device.setCL(_mCursor.getInt(9));
                        device.setRGBSTT(_mCursor.getInt(10));
                        device.setENGRD(_mCursor.getInt(11));
                        device.setREDCLR(_mCursor.getInt(12));
                        device.setGREENCLR(_mCursor.getInt(13));
                        device.setBLUECLR(_mCursor.getInt(14));
                        device.setRGB(_mCursor.getInt(15));
                        device.setDAYS(_mCursor.getString(16));
                        device.setALL_STT(_mCursor.getInt(17));
                        device.setMIC(_mCursor.getInt(18));
                        device.setGROUP(_mCursor.getInt(19));
//                        device.setDEVICE_COUNT(_mCursor.getInt(20));
                        device.setMODULE(_mCursor.getString(21));
                        device.setGROUP_DIM(_mCursor.getInt(22));
                        device.setPOSITION(_mCursor.getInt(23));
                        device.setMODULE_STT(_mCursor.getInt(24));
                        device.setIS_EXPAND(false);
                        devices.add(device);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return devices;
    }

    public void deleteSensor(int security_id) {
        try {
            DB = DB_HELPER.getWritableDatabase();
            int id = DB.delete(SENSOR_TABLE, SECURITY_ID + "==" + "'" + security_id + "'", null);
            System.out.println("security_id delete id " + id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public entDevice getDeviceFromModule(int roomId, String module) {

        entDevice device = new entDevice();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//
            _mCursor = DB.query(false, DEVICE_TABLE,
                    null, ROOM_ID + "==" + roomId + " And " + MODULE + "==" + "'" + module + "' And " + IS_DELETE + "==0",
                    null, null,
                    null, null, null);

//                 _mCursor = DB.rawQuery(
//                        "SELECT * FROM DeviceTable WHERE rID=="+roomId+ " AND NAME=='"
//                                + deviceName.toUpperCase() + "'", null);

            System.out.println("count " + _mCursor.getCount());
            if (_mCursor != null) {
                _mCursor.moveToFirst();

                if (_mCursor.getCount() > 0) {
                    device.setDEVICE_Id(_mCursor.getInt(0));

                    device.setROOM_Id(_mCursor.getInt(1));
                    device.setDEVICE_NAME(_mCursor.getString(2));
                    device.setSTT(_mCursor.getInt(3));
                    device.setDIM(_mCursor.getInt(4));
                    device.setTIME_ONE_A(_mCursor.getString(5));
                    device.setTIME_ONE_B(_mCursor.getString(6));
                    device.setTIME_TWO_A(_mCursor.getString(7));
                    device.setTIME_TWO_B(_mCursor.getString(8));
                    device.setCL(_mCursor.getInt(9));
                    device.setRGBSTT(_mCursor.getInt(10));
                    device.setENGRD(_mCursor.getInt(11));
                    device.setREDCLR(_mCursor.getInt(12));
                    device.setGREENCLR(_mCursor.getInt(13));
                    device.setBLUECLR(_mCursor.getInt(14));
                    device.setRGB(_mCursor.getInt(15));
                    device.setDAYS(_mCursor.getString(16));
                    device.setALL_STT(_mCursor.getInt(17));
                    device.setMIC(_mCursor.getInt(18));
                    device.setGROUP(_mCursor.getInt(19));
//                        device.setDEVICE_COUNT(_mCursor.getInt(20));
                    device.setMODULE(_mCursor.getString(21));
                    device.setGROUP_DIM(_mCursor.getInt(22));
                    device.setPOSITION(_mCursor.getInt(23));
                    device.setMODULE_STT(_mCursor.getInt(24));
                    device.setBACKLIGHT_RGB_STT(_mCursor.getInt(25));
                    device.setBACKLIGHT_DIM_STT(_mCursor.getInt(26));
                    device.setMODULE_NUMBER(_mCursor.getInt(27));
                    device.setIS_DELETE(_mCursor.getInt(28));
                    device.setIS_EXPAND(false);
                    _mCursor.close();
                }

            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return device;
    }

    public int getDeviceIdFromModule(int roomId, String module) {
        int deviceId = -1;
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//
            _mCursor = DB.query(false, DEVICE_TABLE,
                    null, ROOM_ID + "==" + roomId + " And " + MODULE + "==" + "'" + module + "' And " + IS_DELETE + "==0",
                    null, null,
                    null, null, null);

//                 _mCursor = DB.rawQuery(
//                        "SELECT * FROM DeviceTable WHERE rID=="+roomId+ " AND NAME=='"
//                                + deviceName.toUpperCase() + "'", null);

            System.out.println("count " + _mCursor.getCount());
            if (_mCursor != null) {
                _mCursor.moveToFirst();

                if (_mCursor.getCount() > 0) {
                    deviceId = _mCursor.getInt(0);
                    _mCursor.close();
                }

            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return deviceId;
    }

    public entRooms getRoomFromId(int selectedRoomId) {
        entRooms room = new entRooms();
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, ROOM_TABLE,
                    null, ROOM_ID + "==" + "'" + selectedRoomId + "'", null, null,
                    null, null, null);

            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    room = new entRooms();
                    room.setROOM_Id(_mCursor.getInt(0));
                    room.setROOM_NAME(_mCursor.getString(1));
                    room.setROOM_IS_SELECTED(_mCursor.getInt(2));
                    room.setROOM_ALL_SIT(_mCursor.getInt(3));
                    room.setROOM_GRP_SIT(_mCursor.getInt(4));
                    room.setMODULE_COUNT(_mCursor.getInt(5));
                    room.setACTIVE(_mCursor.getInt(6));
                    room.setPROFILE_DATA(_mCursor.getString(7));
                    room.setGROUP_COLOR(_mCursor.getInt(8));
                    room.setGROUP_BUZZ(_mCursor.getInt(9));
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return room;
    }

    public void updateRoomName(int room_id, String s) {

        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            ContentValues ac = new ContentValues();
            ac.put(ROOM_NAME, s);

            DB.update(TV_TABLE, ac,
                    ROOM_ID + "==" + "'" + room_id + "'", null);
            DB.update(AC_TABLE, ac,
                    ROOM_ID + "==" + "'" + room_id + "'", null);
            DB.update(CURTAIN_TABLE, ac,
                    ROOM_ID + "==" + "'" + room_id + "'", null);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void deleteCurtain(int curtain_id) {
        try {
            DB = DB_HELPER.getWritableDatabase();
            int id = DB.delete(CURTAIN_TABLE, CURTAIN_ID + "==" + "'" + curtain_id + "'", null);
            System.out.println("curtain delete id " + id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void deleteTv(int tv_id) {
        try {
            DB = DB_HELPER.getWritableDatabase();
            int id = DB.delete(TV_TABLE, TV_ID + "==" + "'" + tv_id + "'", null);
            System.out.println("curtain delete id " + id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public boolean checkBackupForLastMonth(String date) {
        Cursor _mCursor = null;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(false, WATER_USAGE_TABLE,
                    null, BACKUPDATE + "==" + "'" + date + "'", null, null,
                    null, null, null);

            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    return _mCursor.getCount() > 0;
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return false;
    }

    public void backupWaterData(String date) {

        try {
            DB = DB_HELPER.getWritableDatabase();
            String data = Preferences.getAppPrefString(Preferences.KEY_FLAT_WATER);
            if (data != null && !data.equalsIgnoreCase("")) {
                String lable[];
                String value[];
                String main[] = data.split(",");
                lable = new String[main.length];
                value = new String[main.length];
                for (int i = 0; i < main.length; i++) {
                    lable[i] = main[i].substring(0, 4);
                    value[i] = main[i].substring(4, main[i].length());
                    ContentValues _cv = new ContentValues();
                    _cv.put(FLAT_NAME, lable[i]);
                    _cv.put(WATER_USAGE, value[i]);
                    _cv.put(BACKUPDATE, date);
                    DB.insert(WATER_USAGE_TABLE, null, _cv);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
        }
        Preferences.writeSharedPreferences(Preferences.KEY_FLAT_WATER, "");
    }

    public ArrayList<entWaterUsage> getWaterUsageData(String flatName) {
        Cursor _mCursor = null;
        ArrayList<entWaterUsage> waterUsages = new ArrayList<>();
        entWaterUsage usage;
        try {
            DB = DB_HELPER.getWritableDatabase();
            _mCursor = DB.query(false, WATER_USAGE_TABLE,
                    null, FLAT_NAME + "==" + "'" + flatName + "'", null, null,
                    null, null, null);

            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        usage = new entWaterUsage();
                        usage.setWaterId(_mCursor.getInt(0));
                        usage.setFaltName(_mCursor.getString(1));
                        usage.setWaterUsage(_mCursor.getString(2));
                        usage.setBackupDate(_mCursor.getString(3));
                        waterUsages.add(usage);
                    } while (_mCursor.moveToNext());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
        }
        return waterUsages;
    }

    public void insertLogValue(int roomId, int deviceId, String dateTime, int state) {
        Long _id = 0l;
        if (deviceId == -1 || roomId == -1) {
            System.out.println(" WRONG STRING RECEIVE deviceId > " + deviceId + "  roomId>> " + roomId);
            return;
        }
        try {
            DB = DB_HELPER.getWritableDatabase();
            DB.beginTransaction();
            ContentValues contentValues = new ContentValues();
            contentValues.put(ROOM_ID, roomId);
            contentValues.put(DEVICE_ID, deviceId);
            contentValues.put(DATE_VALUE, dateTime);
            contentValues.put(TIME_VALUE, Utils.getDateTimeFromDateString(dateTime));
            contentValues.put(STATE, state);
            System.out.println("VALUE " + contentValues.toString());

            _id = DB.insertWithOnConflict(LOG_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            DB.endTransaction();
        } finally {
            DB.endTransaction();
            DB.close();
        }
        System.out.println("Log record inserted " + _id);


    }

    public ArrayList<entLogData> getLogData(int room_id, String dateFrom, String dateTo) {
//        SELECT * FROM LogTable WHERE DateValue >=('07/04/2017') And  DateValue <('08/04/201700:00:00AM')
        Cursor _mCursor = null;
        System.out.println("dateFrom Daily " + dateFrom);
        System.out.println("dateTo Daily " + dateTo);
        long fromTime = Utils.getLongDate(dateFrom);
        long toTime = Utils.getLongDate(dateTo);
        ArrayList<entLogData> entLogDatas = new ArrayList<>();
        entLogData datas;
        try {
            DB = DB_HELPER.getWritableDatabase();
            DB.beginTransaction();
            Cursor ids = DB.rawQuery("select dId, rId from LogTable where rId=='" + room_id + "' GROUP By dId,rId", null);
            if (ids != null && ids.moveToFirst()) {
                do {
                    int deviceId = ids.getInt(0);
                    int roomId = ids.getInt(1);
                    String query = "select * from LogTable where dID=" + deviceId + " and rID=" + roomId + " and  TimeValue >= '" + fromTime + "' and TimeValue < '" + toTime + "'";
                    System.out.println("query Daily" + query);
                    _mCursor = DB.rawQuery(query, null);
//                    _mCursor = DB.rawQuery("select * from LogTable where dID=" + deviceId + " and rID=" + roomId + " and  DateValue >= ('" + dateFrom + "') and DateValue < ('" + dateTo + "')", null);
//                    _mCursor = DB.query(false, LOG_TABLE,
//                            null, DEVICE_ID + "=='" + deviceId + "' And " + ROOM_ID + "=='" + roomId + "'", null, null,
//                            null, null, null);
//                    _mCursor = DB.query(false, LOG_TABLE,
//                            null, DEVICE_ID + "=='" + deviceId + "' And " + ROOM_ID + "=='" + roomId + "' And "+DATE_VALUE+" >=('"+dateFrom+"') And "+DATE_VALUE+" <('"+dateTo+"')", null, null,
//                            null, null, null);
//                    System.out.println("_mCursor COUNT " + _mCursor.getCount());

                    if (_mCursor != null && _mCursor.moveToFirst()) {
                        long dateTimeTotal = 0l;
                        long dateTimePrevious = 0l;
                        boolean isStatOff = false;
                        int stat;
                        String dateTime;
                        do {
//                            System.out.println("deviceId ID " + deviceId);
                            stat = _mCursor.getInt(5);
                            dateTime = _mCursor.getString(4);
                            if (dateTimePrevious == 0l) {
                                if (stat == 1) {
                                    isStatOff = false;
                                    dateTimePrevious = Utils.getDateTimeFromDateString(dateTime);
                                }
                            } else {
                                if (stat == 0) {
                                    if (!isStatOff) {
                                        isStatOff = true;
                                        long temp = Utils.getDateTimeFromDateString(dateTime);
                                        dateTimeTotal = dateTimeTotal + (temp - dateTimePrevious);
                                        dateTimePrevious = temp;
//                                        System.out.println("TOTAL TIME " + dateTimeTotal);
                                    }
                                } else {
                                    isStatOff = false;
                                    dateTimePrevious = Utils.getDateTimeFromDateString(dateTime);
                                }
                            }

                        } while (_mCursor.moveToNext());
                        if (dateTimeTotal != 0l) {
                            datas = new entLogData();
                            datas.setDeviceId(deviceId);
                            datas.setRoomname(getRoomName(roomId));
                            datas.setDeviceName(getDeviceName(roomId, deviceId));
                            datas.setRoomId(roomId);
                            datas.setDateTime(dateTimeTotal);
                            entLogDatas.add(datas);
                        }
                    }
                } while (ids.moveToNext());
            }
            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            DB.endTransaction();
        } finally {
            DB.endTransaction();
            DB.close();
        }
        return entLogDatas;
    }

    public ArrayList<entLogData> getLogDataMonth(int room_id, String dateFrom, String dateTo) {
//        SELECT * FROM LogTable WHERE DateValue >=('07/04/2017') And  DateValue <('08/04/201700:00:00AM')
        Cursor _mCursor = null;
        System.out.println("dateFrom " + dateFrom);
        System.out.println("dateTo " + dateTo);
        long fromTime = Utils.getLongDate(dateFrom);
        long toTime = Utils.getLongDate(dateTo);
        ArrayList<entLogData> entLogDatas = new ArrayList<>();
        entLogData datas;
        try {
            DB = DB_HELPER.getWritableDatabase();
            DB.beginTransaction();
            Cursor ids = DB.rawQuery("select dId, rId from LogTable where rId=='" + room_id + "' GROUP By dId,rId", null);
            if (ids != null && ids.moveToFirst()) {
                do {
                    int deviceId = ids.getInt(0);
                    int roomId = ids.getInt(1);
                    String query = "select * from LogTable where dID=" + deviceId + " and rID=" + roomId + " and  TimeValue >= '" + fromTime + "' and TimeValue < '" + toTime + "'";
                    System.out.println("query " + query);
                    _mCursor = DB.rawQuery(query, null);
//                    _mCursor = DB.query(false, LOG_TABLE,
//                            null, DEVICE_ID + "=='" + deviceId + "' And " + ROOM_ID + "=='" + roomId + "'", null, null,
//                            null, null, null);
//                    _mCursor = DB.query(false, LOG_TABLE,
//                            null, DEVICE_ID + "=='" + deviceId + "' And " + ROOM_ID + "=='" + roomId + "' And "+DATE_VALUE+" >=('"+dateFrom+"') And "+DATE_VALUE+" <('"+dateTo+"')", null, null,
//                            null, null, null);
//                    System.out.println("_mCursor COUNT " + _mCursor.getCount());

                    if (_mCursor != null && _mCursor.moveToFirst()) {
                        long dateTimeTotal = 0l;
                        long dateTimePrevious = 0l;
                        boolean isStatOff = false;
                        int stat;
                        String dateTime;
                        do {
//                            System.out.println("deviceId ID " + deviceId);
                            stat = _mCursor.getInt(5);
                            dateTime = _mCursor.getString(4);
                            if (dateTimePrevious == 0l) {
                                if (stat == 1) {
                                    isStatOff = false;
                                    dateTimePrevious = Utils.getDateTimeFromDateString(dateTime);
                                }
                            } else {
                                if (stat == 0) {
                                    if (!isStatOff) {
                                        isStatOff = true;
                                        long temp = Utils.getDateTimeFromDateString(dateTime);
                                        dateTimeTotal = dateTimeTotal + (temp - dateTimePrevious);
                                        dateTimePrevious = temp;
//                                        System.out.println("TOTAL TIME " + dateTimeTotal);
                                    }
                                } else {
                                    isStatOff = false;
                                    dateTimePrevious = Utils.getDateTimeFromDateString(dateTime);
                                }
                            }

                        } while (_mCursor.moveToNext());
                        if (dateTimeTotal != 0l) {
                            datas = new entLogData();
                            datas.setDeviceId(deviceId);
                            datas.setRoomname(getRoomName(roomId));
                            datas.setDeviceName(getDeviceName(roomId, deviceId));
                            datas.setRoomId(roomId);
                            datas.setDateTime(dateTimeTotal);
                            entLogDatas.add(datas);
                        }
                    }
                } while (ids.moveToNext());
            }
            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            DB.endTransaction();
        } finally {
            DB.endTransaction();
            DB.close();
        }
        return entLogDatas;
    }

    public void deleteLogData() {
        try {
            DB = DB_HELPER.getWritableDatabase();
            int id = DB.delete(LOG_TABLE, null, null);
            System.out.println("LOG_TABLE delete records " + id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    public void insertChannals(entChannelModel chennel) {
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB = DB_HELPER.DB.openDatabase(Constants.DB_PTH, null, SQLiteDatabase.OPEN_READONLY);
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(CH_NAME, chennel.getCHANNEL_NAME());
            _cv.put(CH_IMAGE, chennel.getCHANNEL_IMAGE());
            _cv.put(GTPL_CODE, chennel.getGTPL_CODE());
            _cv.put(AITTEL_CODE, chennel.getAIRTEL_CODE());
            _cv.put(DISHTV_CODE, chennel.getDISTV_CODE());
            _cv.put(VIDEOCON_CODE, chennel.getVIDEOCON_CODE());
            _cv.put(TATASKY_CODE, chennel.getTATASKY_CODE());
            _cv.put(RELIANCE_CODE, chennel.getRELIANCE_CODE());
            _cv.put(JIO_CODE, chennel.getJIO_CODE());
            DB.insert(CHANNEL_ALL_TABLE, null, _cv);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            // DB.endTransaction();
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public int getChannelNumber(String name, entTV mEntTV) {
        Cursor _mCursor = null;

        try {
            DB = DB_HELPER.getWritableDatabase();
            _mCursor = DB.query(false, CHANNEL_ALL_TABLE,
                    null, CH_NAME + "==" + "'" + name + "'", null, null,
                    null, null, null);

            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    if (mEntTV.getSET_NAME().equalsIgnoreCase("GTPL")) {
                        return _mCursor.getInt(3);
                    } else if (mEntTV.getSET_NAME().equalsIgnoreCase("AIRTEL DTH")) {
                        return _mCursor.getInt(4);
                    } else if (mEntTV.getSET_NAME().equalsIgnoreCase("DISH TV")) {
                        return _mCursor.getInt(5);
                    } else if (mEntTV.getSET_NAME().equalsIgnoreCase("D2H VIDEOCON")) {
                        return _mCursor.getInt(6);
                    } else if (mEntTV.getSET_NAME().equalsIgnoreCase("TATA SKY")) {
                        return _mCursor.getInt(7);
                    } else if (mEntTV.getSET_NAME().equalsIgnoreCase("BIG TV")) {
                        return _mCursor.getInt(8);
                    }

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
        }
        return 0;
    }

    public List<entGroupAppliances> getAllGroups() {
        Cursor _mCursor = null;
        entGroupAppliances groupAppliances;
        ArrayList<entGroupAppliances> appliances = new ArrayList<>();
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB.beginTransaction();
            _mCursor = DB.query(true, GROUP_APPLIANCES_TABLE,
                    null, GRP_IS_DELETE + " = '0'",
                    null, null,
                    null, null, null);
            if (_mCursor != null) {
                if (_mCursor.moveToFirst()) {
                    do {
                        groupAppliances = new entGroupAppliances();
                        groupAppliances.setLgroupId(_mCursor.getInt(_mCursor.getColumnIndex(GRP_GROUP_ID)));
                        groupAppliances.setStatus(_mCursor.getInt(_mCursor.getColumnIndex(GRP_STATUS)));
                        groupAppliances.setIsDelete(_mCursor.getInt(_mCursor.getColumnIndex(GRP_IS_DELETE)));
                        groupAppliances.setName(_mCursor.getString(_mCursor.getColumnIndex(GRP_NAME)));
                        groupAppliances.setDeviceIds(_mCursor.getString(_mCursor.getColumnIndex(GRP_DEVICE_IDS)));
                        groupAppliances.setImage(_mCursor.getString(_mCursor.getColumnIndex(GRP_IMAGE)));
                        appliances.add(groupAppliances);
                    } while (_mCursor.moveToNext());
                    _mCursor.close();
                }
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return appliances;
    }

    public entGroupAppliances insertGroup(entGroupAppliances groupAppliances) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();
//            DB = DB_HELPER.DB.openDatabase(Constants.DB_PTH, null, SQLiteDatabase.OPEN_READONLY);
//            DB.beginTransaction();
            ContentValues _cv = new ContentValues();
            _cv.put(GRP_NAME, groupAppliances.getName());
            _cv.put(GRP_DEVICE_IDS, groupAppliances.getDeviceIds());
            _cv.put(GRP_IS_DELETE, groupAppliances.getIsDelete());
            _cv.put(GRP_STATUS, groupAppliances.getStatus());
            _cv.put(GRP_IMAGE, groupAppliances.getImage());
            String query = "Select gID from GroupAppliances Where IsDelete == 1";
            Cursor cursor = DB.rawQuery(query, null);
            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
                int gId = cursor.getInt(cursor.getColumnIndex(GRP_GROUP_ID));
                DB.update(GROUP_APPLIANCES_TABLE, _cv, GRP_GROUP_ID + " == '" + gId + "'", null);
                groupAppliances.setLgroupId(gId);
            } else {
                _id = DB.insert(GROUP_APPLIANCES_TABLE, null, _cv);
                groupAppliances.setLgroupId((int) _id);
            }
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            // DB.endTransaction();
            ex.printStackTrace();

        } finally {
//            DB.endTransaction();
//            DB.close();
        }
        return groupAppliances;
    }

    public void deleteGroups() {
        try {
            DB = DB_HELPER.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(GRP_IS_DELETE, 1);
            DB.update(GROUP_APPLIANCES_TABLE, contentValues, null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
        }
    }

    public void updateGroup(entGroupAppliances item) {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();

            ContentValues _cv = new ContentValues();
            _cv.put(GRP_NAME, item.getName());
            _cv.put(GRP_STATUS, item.getStatus());
            _cv.put(GRP_DEVICE_IDS, item.getDeviceIds());
            _cv.put(GRP_IS_DELETE, item.getIsDelete());


            _id = DB.update(GROUP_APPLIANCES_TABLE, _cv, GRP_GROUP_ID + "==" + "'" + item.getLgroupId() + "'",
                    null);
            System.out.println("Updare appliances group id " + _id);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public void deleteAllGroup() {
        long _id = 0;
        try {
            DB = DB_HELPER.getWritableDatabase();

            ContentValues _cv = new ContentValues();
            _cv.put(GRP_IS_DELETE, 1);
            _id = DB.update(GROUP_APPLIANCES_TABLE, _cv, null,
                    null);
            System.out.println("Updare appliances group id " + _id);
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();

            // DB.endTransaction();
        } finally {
//            DB.endTransaction();
//            DB.close();
        }
    }

    public entDevice getTvDevice(int room_id) {
        entDevice device = new entDevice();
        try {
            DB = DB_HELPER.getWritableDatabase();
            Cursor _mCursor = DB.rawQuery(
                    "SELECT * FROM DeviceTable WHERE NAME LIKE '%" + "tv" + "%' And " + IS_DELETE + " == 0 And rID == '" + room_id + "'", null);
            if (_mCursor.getCount() > 0) {
                _mCursor.moveToFirst();
                device.setDEVICE_Id(_mCursor.getInt(0));
                device.setROOM_Id(_mCursor.getInt(1));
                device.setDEVICE_NAME(_mCursor.getString(2));
                device.setSTT(_mCursor.getInt(3));
                device.setDIM(_mCursor.getInt(4));
                device.setTIME_ONE_A(_mCursor.getString(5));
                device.setTIME_ONE_B(_mCursor.getString(6));
                device.setTIME_TWO_A(_mCursor.getString(7));
                device.setTIME_TWO_B(_mCursor.getString(8));
                device.setCL(_mCursor.getInt(9));
                device.setRGBSTT(_mCursor.getInt(10));
                device.setENGRD(_mCursor.getInt(11));
                device.setREDCLR(_mCursor.getInt(12));
                device.setGREENCLR(_mCursor.getInt(13));
                device.setBLUECLR(_mCursor.getInt(14));
                device.setRGB(_mCursor.getInt(15));
                device.setDAYS(_mCursor.getString(16));
                device.setALL_STT(_mCursor.getInt(17));
                device.setMIC(_mCursor.getInt(18));
                device.setGROUP(_mCursor.getInt(19));
//                        device.setDEVICE_COUNT(_mCursor.getInt(20));
                device.setMODULE(_mCursor.getString(21));
                device.setGROUP_DIM(_mCursor.getInt(22));
                device.setPOSITION(_mCursor.getInt(23));
                device.setMODULE_STT(_mCursor.getInt(24));
                device.setIS_EXPAND(false);
            }
            _mCursor.close();
//            DB.setTransactionSuccessful();
        } catch (Exception ex) {
            ex.printStackTrace();
            // DB.endTransaction();
        }

        return device;
    }
}
