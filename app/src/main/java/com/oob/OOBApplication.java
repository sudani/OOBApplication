package com.oob;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.oob.activity.LoginActivity;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.tools.WifiUtils;

import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.Arrays;

//import com.activeandroid.ActiveAndroid;


@ReportsCrashes(mailTo = "chirag.sudani@gmail.com",
        customReportContent = {ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT},
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text)

public class OOBApplication extends Application {
    protected static OOBApplication mInstance;

    private static final String TAG = "MQTT";
    public String subscriptionTopic = "OOB_SB";
    public String publishTopic = "OOB_PB";
    public String serverUrl = "m12.cloudmqtt.com";
    public String serverPort = "15699";
    public String clientId = "OOBCLINET";
    public String username = "chlguzxp";
    public String password = "U97aOkUXf-oD";
    public MqttAndroidClient client;
    private boolean isSync = false;
    int count, total = 1;
    Handler mHandler;
    Handler mHandlerSync;

    Runnable mUpdateResults;
    Runnable mUpdateSync;
    String message = "";
    String messageKey = "";

    public OOBApplication() {
        mInstance = this;
    }


    public static OOBApplication getApp() {
        if (mInstance != null && mInstance instanceof OOBApplication) {
            return (OOBApplication) mInstance;
        } else {
            mInstance = new OOBApplication();
            mInstance.onCreate();
            return (OOBApplication) mInstance;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        ActiveAndroid.initialize(this);
        mInstance = this;
        initialiseHandler();
    }

    private void initialiseHandler() {
        mHandler = new Handler();
        mHandlerSync = new Handler();

        mUpdateResults = new Runnable() {
            public void run() {
                isSync = false;
                if (Preferences.getAppPrefBool(Preferences.KEY_DEVELOPER_MODE, false)) {
                    Toast.makeText(getAppContext(), message, Toast.LENGTH_SHORT).show();
                }
                String msg = message
                        .replace(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE), "");
                if (msg.startsWith("WLP")) {
//                    9714090305WLP02000Q
                    messageKey = "water";
                    String progressML = msg.substring(3, msg.length() - 1);
                    String progress = String.valueOf(Long.parseLong(progressML) / 1000);
                    if (!Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS)
                            .equalsIgnoreCase("") && !Preferences
                            .getAppPrefString(Preferences.KEY_HIGH_PROGRESS_ML)
                            .equalsIgnoreCase("")) {
                        int diff = Integer.parseInt(
                                Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS))
                                - Integer.parseInt(progress);
                        long diff_ml = Long.parseLong(
                                Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS_ML))
                                - Long.parseLong(progressML);
                        if (diff > 0) {
                            if (!Preferences.getAppPrefString(Preferences.KEY_TOTAL_USED)
                                    .equalsIgnoreCase("")) {
                                Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED,
                                        (Integer.parseInt(Preferences
                                                .getAppPrefString(Preferences.KEY_TOTAL_USED))
                                                + diff) + "");
                            } else {
                                Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED,
                                        diff + "");
                            }
                        }
                        if (diff_ml > 0) {
                            if (!Preferences.getAppPrefString(Preferences.KEY_TOTAL_USED_ML)
                                    .equalsIgnoreCase("")) {
                                Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED_ML,
                                        (Long.parseLong(Preferences
                                                .getAppPrefString(Preferences.KEY_TOTAL_USED_ML))
                                                + diff_ml) + "");
                            } else {
                                Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED_ML,
                                        diff_ml + "");
                            }
                        }
                    }
                    Preferences.writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS, progress);
                    Preferences
                            .writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS_ML, progressML);

                } else if (msg.startsWith("FLATWLP")) {
//                    FLATWLPA10199999,A10299999,A10399999Q
                    messageKey = "flat_water";
                    String msgFlat = msg.substring(7, msg.length() - 1);
                    System.out.println("msg flat " + msgFlat);
                    Preferences.writeSharedPreferences(Preferences.KEY_FLAT_WATER, msgFlat);

                } else if (msg.contains("ALARM")) {
                    System.out.println("CAll ALARM");
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    Ringtone r = RingtoneManager.getRingtone(getAppContext(), notification);
                    r.play();
                } else if (msg.contains("PHONE")) {
//               SSU010R01M01PHONEQ
                    System.out.println("CAll PHONE");
                    String sensor = msg.substring(2, 3);
                    String sensorNumer = msg.substring(3, 6);
                    generateNotification(Utils.getSensorName(sensor, sensorNumer));
                } else if (msg.startsWith("SYNC")) {
//               17/03/201705:08:00AM-R01M01C1D000RX61Q
                    //9714090305SDCOUNT00020Q
                    isSync = true;
                    msg = msg.replace("SYNC", "");
                    if (msg.contains("SDCOUNT")) {
                        total = 0;
                        count = 0;
                        msg = msg.replace(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "SDCOUNT", "");
                        msg = msg.replace("SDCOUNT", "");
                        total = Integer.parseInt(msg.replace("Q", ""));
                        System.out.println("total > " + total);
                    } else {
                        count++;
                        dbHelperOperations dbHelperOperations = new dbHelperOperations(getAppContext());
                        String msgArray[] = msg.split("-");
                        int roomId = Utils.getRoomId(msgArray[1]);
                        String module = Utils.getModule(msgArray[1]);
                        int deviceId = dbHelperOperations
                                .getDeviceIdFromModule(roomId, module);
                        int state = 0;
                        try {
                            state = Integer.parseInt(msgArray[1].charAt(7) + "");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dbHelperOperations.insertLogValue(roomId, deviceId, msgArray[0], state);
                    }
                    return;
                } else if (msg.startsWith("RTCTIME")) {
                    Toast.makeText(getAppContext(), msg, Toast.LENGTH_SHORT).show();
                } else {
                    dbHelperOperations dbHelperOperations = new dbHelperOperations(getAppContext());
                    if (msg.startsWith("UPDATE")) {
//                    R01M01A0B00C0D0E0F0,
//                    R01M02A0B00C0D0E0F0
//                    R01M03A0B00C0D0E0F0  All module string
//                    R01M01A0000001B0000001C0000001D0000001E0000001F0000001

                        //R01M03A1000001B0000001C0D00001D0000021E1000021F0000021Q
                        //R01M02A0000011B0000011C0000011D0000011E0000011F0000011
                        messageKey = "update";
                        msg = msg.replace("UPDATE", "");
                        String msgArray[] = msg.split(",");
                        for (int i = 0; i < msgArray.length; i++) {
                            String str = msgArray[i];
                            int roomId = Utils.getRoomId(str);
                            if (roomId != -1) {
                                System.out.println("STR>>> " + str);
                                int subModule = getNumberFromSubString(str, 4, 6);
                                // device 1.
                                int m1 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(6) + "");
                                int onOff1 = getNumberFromString(str, 7);
                                int cl = getNumberFromString(str, 8);
                                int dim = getNumberFromSubString(str, 9, 12);
                                int rgb = getNumberFromString(str, 12);
                                int buz = getNumberFromString(str, 13);

                                updateDevice(dbHelperOperations, roomId, m1 + "," + subModule,
                                        onOff1,
                                        cl, dim, rgb, buz);
                                // device 2.
                                int m2 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(14) + "");
                                int onOff2 = getNumberFromString(str, 15);
                                int cl2 = getNumberFromString(str, 16);
                                int dim2 = getNumberFromSubString(str, 17, 20);
                                int rgb2 = getNumberFromString(str, 20);
                                int buz2 = getNumberFromString(str, 21);
                                updateDevice(dbHelperOperations, roomId, m2 + "," + subModule,
                                        onOff2, cl2, dim2, rgb2, buz2
                                );
                                // device 3.
                                int m3 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(22) + "");
                                int onOff3 = getNumberFromString(str, 23);
                                int cl3 = getNumberFromString(str, 24);
                                int dim3 = getNumberFromSubString(str, 25, 28);
                                int rgb3 = getNumberFromString(str, 28);
                                int buz3 = getNumberFromString(str, 29);
                                updateDevice(dbHelperOperations, roomId, m3 + "," + subModule,
                                        onOff3, cl3, dim3, rgb3,
                                        buz3);
                                // device 4.
                                int m4 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(30) + "");
                                int onOff4 = getNumberFromString(str, 31);
                                int cl4 = getNumberFromString(str, 32);
                                int dim4 = getNumberFromSubString(str, 33, 36);
                                int rgb4 = getNumberFromString(str, 36);
                                int buz4 = getNumberFromString(str, 37);
                                updateDevice(dbHelperOperations, roomId, m4 + "," + subModule,
                                        onOff4, cl4, dim4, rgb4,
                                        buz4);
                                // device 5.
                                int m5 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(38) + "");
                                int onOff5 = getNumberFromString(str, 39);
                                int cl5 = getNumberFromString(str, 40);
                                int dim5 = getNumberFromSubString(str, 41, 44);
                                int rgb5 = getNumberFromString(str, 44);
                                int buz5 = getNumberFromString(str, 45);
                                updateDevice(dbHelperOperations, roomId, m5 + "," + subModule,
                                        onOff5, cl5, dim5, rgb5,
                                        buz5);
                                // device 6.
                                int m6 = (Arrays.asList(Constants.DEVICES))
                                        .indexOf(str.charAt(46) + "");
                                int onOff6 = getNumberFromString(str, 47);
                                int cl6 = getNumberFromString(str, 48);
                                int dim6 = getNumberFromSubString(str, 49, 52);
                                int rgb6 = getNumberFromString(str, 52);
                                int buz6 = getNumberFromString(str, 53);
                                updateDevice(dbHelperOperations, roomId, m6 + "," + subModule,
                                        onOff6, cl6, dim6, rgb6,
                                        buz6);
                            }
                        }
                    } else {

                        // for ON/OFF >> R01M01A0D009RX01Q
//                        // for child lock >>> R01M01CCL1Q
                        System.out.println("msg " + msg);
                        messageKey = "device";
                        int roomId = Utils.getRoomId(msg);
                        if (roomId != -1) {
                            String module = Utils.getModule(msg);
                            System.out.println("ROOM ID " + roomId);
                            System.out.println("Module ID " + module);
                            entDevice device = dbHelperOperations
                                    .getDeviceFromModule(roomId, module);
//                        System.out.println("ID "+device.getID());
                            System.out.println("MMMM " + message);
                            if (msg.substring(7, 9).equalsIgnoreCase("CL")) {
                                device.setCL(getNumberFromString(message, 19));
                            } else {
                                device.setSTT(getNumberFromString(message, 17));
                                device.setDIM(getNumberFromSubString(message, 19, 22));
                            }
                            dbHelperOperations.updateDevice(device);
                        }
                    }
                }
                sendBroadCast();
            }

        };
        mUpdateSync = new Runnable() {
            @Override
            public void run() {
                sendBroadCastLogData(total, count);
            }
        };
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        ACRA.init(this);
    }

    public static Context getAppContext() {
        if (mInstance == null) {
            mInstance = new OOBApplication();
        }
        return mInstance;
    }

    public void initMQTT() {
        if (!Preferences.getAppPrefString(Preferences.KEY_SERVER_URL, "").equalsIgnoreCase("")) {
            subscriptionTopic = Preferences.getAppPrefString(Preferences.KEY_SERVER_SUB_APP);
            publishTopic = Preferences.getAppPrefString(Preferences.KEY_SERVER_PUB_APP);
            serverUrl = Preferences.getAppPrefString(Preferences.KEY_SERVER_URL);
            serverPort = Preferences.getAppPrefString(Preferences.KEY_SERVER_PORT);
            username = Preferences.getAppPrefString(Preferences.KEY_SERVER_USER);
            password = Preferences.getAppPrefString(Preferences.KEY_SERVER_PASS);
        }
//        else {
//            return;
//        }
        if (client != null && client.isConnected()) {
            Utils.showToast(getAppContext(), "Connected");
            return;
        }
        client =
                new MqttAndroidClient(this.getApplicationContext(), "tcp://" + serverUrl + ":" + serverPort,
                        clientId + System.currentTimeMillis());
        try {
            MqttConnectOptions options = new MqttConnectOptions();
            options.setAutomaticReconnect(true);
            options.setCleanSession(false);
            options.setUserName(username);
            options.setPassword(password.toCharArray());
            IMqttToken token = client.connect(options);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.e(TAG, "onSuccess");
                    Utils.showToast(getAppContext(), "Connected");
                    subscribeToTopic();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    exception.printStackTrace();
                    Utils.showToast(getAppContext(), "Connection fail");
                    Log.e(TAG, "onFailure");

                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }


    }


    public void subscribeToTopic() {
        try {
            IMqttToken mqttToken = client.subscribe(subscriptionTopic, 1);
            mqttToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.e(TAG, "Subscribed");
                    Utils.showToast(getAppContext(), "Subscribed");

                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    exception.printStackTrace();
                    Log.e(TAG, "Failed to subscribe");
                    Utils.showToast(getAppContext(), "Failed to subscribe");

                }
            });
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {

                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    Log.e(TAG, "Incoming message: " + new String(message.getPayload()));
                    processData(topic, new String(message.getPayload()));

//                    sendBroadCast(topic, new String(message.getPayload()));
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {

                }
            });

            // THIS DOES NOT WORK!
//            mqttAndroidClient.subscribe(subscriptionTopic, 0, new IMqttMessageListener() {
//                @Override
//                public void messageArrived(String topic, MqttMessage message) throws Exception {
//                    // message Arrived!
//                    System.out.println("Message: " + topic + " : " + new String(message.getPayload()));
//                }
//            });

        } catch (MqttException ex) {
            System.err.println("Exception whilst subscribing");
            ex.printStackTrace();
        }
    }

    private void processData(String topic, String msg) {
        Utils.showToast(getAppContext(), "Incoming message: " + msg);

        if (isSync) {
            mHandlerSync.postDelayed(mUpdateSync, 2000);
        }
        message = msg;
        if (message != null && message.length() > 0 && (String
                .valueOf(message.charAt(message.length() - 1)).equalsIgnoreCase("Q"))) {
            System.out.println("INSIDE IF");
            mHandler.post(mUpdateResults);
        }
    }

    private void sendBroadCastLogData(int total, int count) {
        Intent intent = new Intent(BroadcastConstant.BROADCAST_ACTION_LOGDATA);
        intent.putExtra(BroadcastConstant.MESSAGE, total != count);
        intent.putExtra(BroadcastConstant.TOTAL, total);
        intent.putExtra(BroadcastConstant.COUNT, count);
        LocalBroadcastManager.getInstance(getAppContext()).sendBroadcast(intent);

    }

    private void sendBroadCast() {
        if (message != null && message.length() > 0 && (String
                .valueOf(message.charAt(message.length() - 1)).equalsIgnoreCase("Q"))) {
            Intent intent = new Intent(BroadcastConstant.BROADCAST_ACTION);
            System.out.println("KEY " + messageKey);
            intent.putExtra(BroadcastConstant.MESSAGE, messageKey);
//                  intent.putExtra(BroadcastConstant.DATA_CHANGE, bean != null ? bean.getDevices() : null);
            LocalBroadcastManager.getInstance(getAppContext()).sendBroadcast(intent);
        }
//        if (message != null && message.length() > 0 && (String
//                .valueOf(message.charAt(message.length() - 1)).equalsIgnoreCase("Q"))) {
//            String messageKey = "";
//            String msg = message
//                    .replace(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE), "");
//            if (msg.startsWith("WLP")) {
////                    9714090305WLP02000Q
//                messageKey = "water";
//                String progressML = msg.substring(3, msg.length() - 1);
//                String progress = String.valueOf(Long.parseLong(progressML) / 1000);
//                if (!Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS)
//                        .equalsIgnoreCase("") && !Preferences
//                        .getAppPrefString(Preferences.KEY_HIGH_PROGRESS_ML)
//                        .equalsIgnoreCase("")) {
//                    int diff = Integer.parseInt(
//                            Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS))
//                            - Integer.parseInt(progress);
//                    long diff_ml = Long.parseLong(
//                            Preferences.getAppPrefString(Preferences.KEY_HIGH_PROGRESS_ML))
//                            - Long.parseLong(progressML);
//                    if (diff > 0) {
//                        if (!Preferences.getAppPrefString(Preferences.KEY_TOTAL_USED)
//                                .equalsIgnoreCase("")) {
//                            Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED,
//                                    (Integer.parseInt(Preferences
//                                            .getAppPrefString(Preferences.KEY_TOTAL_USED))
//                                            + diff) + "");
//                        } else {
//                            Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED,
//                                    diff + "");
//                        }
//                    }
//                    if (diff_ml > 0) {
//                        if (!Preferences.getAppPrefString(Preferences.KEY_TOTAL_USED_ML)
//                                .equalsIgnoreCase("")) {
//                            Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED_ML,
//                                    (Long.parseLong(Preferences
//                                            .getAppPrefString(Preferences.KEY_TOTAL_USED_ML))
//                                            + diff_ml) + "");
//                        } else {
//                            Preferences.writeSharedPreferences(Preferences.KEY_TOTAL_USED_ML,
//                                    diff_ml + "");
//                        }
//                    }
//                }
//                Preferences.writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS, progress);
//                Preferences
//                        .writeSharedPreferences(Preferences.KEY_HIGH_PROGRESS_ML, progressML);
//
//            } else if (msg.startsWith("FLATWLP")) {
////                    FLATWLPA10199999,A10299999,A10399999Q
//                messageKey = "flat_water";
//                String msgFlat = msg.substring(7, msg.length() - 1);
//                System.out.println("msg flat " + msgFlat);
//                Preferences.writeSharedPreferences(Preferences.KEY_FLAT_WATER, msgFlat);
//
//            } else if (msg.contains("ALARM")) {
//                System.out.println("CAll ALARM");
//                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
//                Ringtone r = RingtoneManager.getRingtone(getAppContext(), notification);
//                r.play();
//            } else if (msg.contains("PHONE")) {
////               SSU010R01M01PHONEQ
//                System.out.println("CAll PHONE");
//                String sensor = msg.substring(2, 3);
//                String sensorNumer = msg.substring(3, 6);
//                generateNotification(Utils.getSensorName(sensor, sensorNumer));
//            } else if (msg.startsWith("SYNC")) {
////               17/03/201705:08:00AM-R01M01C1D000RX61Q
//                //9714090305SDCOUNT00020Q
//                isSync = true;
//                msg = msg.replace("SYNC", "");
//                if (msg.contains("SDCOUNT")) {
//                    total = 0;
//                    count = 0;
//                    msg = msg.replace(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "SDCOUNT", "");
//                    msg = msg.replace("SDCOUNT", "");
//                    total = Integer.parseInt(msg.replace("Q", ""));
//                    System.out.println("total > " + total);
//                } else {
//                    count++;
//                    dbHelperOperations dbHelperOperations = new dbHelperOperations(getAppContext());
//                    String msgArray[] = msg.split("-");
//                    int roomId = Utils.getRoomId(msgArray[1]);
//                    String module = Utils.getModule(msgArray[1]);
//                    int deviceId = dbHelperOperations
//                            .getDeviceIdFromModule(roomId, module);
//                    int state = 0;
//                    try {
//                        state = Integer.parseInt(msgArray[1].charAt(7) + "");
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    dbHelperOperations.insertLogValue(roomId, deviceId, msgArray[0], state);
//                }
//                return;
//            } else if (msg.startsWith("RTCTIME")) {
//                Toast.makeText(getApp(), msg, Toast.LENGTH_SHORT).show();
//            } else {
//                dbHelperOperations dbHelperOperations = new dbHelperOperations(getAppContext());
//                if (msg.startsWith("UPDATE")) {
////                    R01M01A0B00C0D0E0F0,
////                    R01M02A0B00C0D0E0F0
////                    R01M03A0B00C0D0E0F0  All module string
////                    R01M01A0000001B0000001C0000001D0000001E0000001F0000001
//
//                    //R01M03A1000001B0000001C0D00001D0000021E1000021F0000021Q
//                    //R01M02A0000011B0000011C0000011D0000011E0000011F0000011
//                    messageKey = "update";
//                    msg = msg.replace("UPDATE", "");
//                    String msgArray[] = msg.split(",");
//                    for (int i = 0; i < msgArray.length; i++) {
//                        String str = msgArray[i];
//                        int roomId = Utils.getRoomId(str);
//                        if (roomId != -1) {
//                            System.out.println("STR>>> " + str);
//                            int subModule = getNumberFromSubString(str, 4, 6);
//                            // device 1.
//                            int m1 = (Arrays.asList(Constants.DEVICES))
//                                    .indexOf(str.charAt(6) + "");
//                            int onOff1 = getNumberFromString(str, 7);
//                            int cl = getNumberFromString(str, 8);
//                            int dim = getNumberFromSubString(str, 9, 12);
//                            int rgb = getNumberFromString(str, 12);
//                            int buz = getNumberFromString(str, 13);
//
//                            updateDevice(dbHelperOperations, roomId, m1 + "," + subModule,
//                                    onOff1,
//                                    cl, dim, rgb, buz);
//                            // device 2.
//                            int m2 = (Arrays.asList(Constants.DEVICES))
//                                    .indexOf(str.charAt(14) + "");
//                            int onOff2 = getNumberFromString(str, 15);
//                            int cl2 = getNumberFromString(str, 16);
//                            int dim2 = getNumberFromSubString(str, 17, 20);
//                            int rgb2 = getNumberFromString(str, 20);
//                            int buz2 = getNumberFromString(str, 21);
//                            updateDevice(dbHelperOperations, roomId, m2 + "," + subModule,
//                                    onOff2, cl2, dim2, rgb2, buz2
//                            );
//                            // device 3.
//                            int m3 = (Arrays.asList(Constants.DEVICES))
//                                    .indexOf(str.charAt(22) + "");
//                            int onOff3 = getNumberFromString(str, 23);
//                            int cl3 = getNumberFromString(str, 24);
//                            int dim3 = getNumberFromSubString(str, 25, 28);
//                            int rgb3 = getNumberFromString(str, 28);
//                            int buz3 = getNumberFromString(str, 29);
//                            updateDevice(dbHelperOperations, roomId, m3 + "," + subModule,
//                                    onOff3, cl3, dim3, rgb3,
//                                    buz3);
//                            // device 4.
//                            int m4 = (Arrays.asList(Constants.DEVICES))
//                                    .indexOf(str.charAt(30) + "");
//                            int onOff4 = getNumberFromString(str, 31);
//                            int cl4 = getNumberFromString(str, 32);
//                            int dim4 = getNumberFromSubString(str, 33, 36);
//                            int rgb4 = getNumberFromString(str, 36);
//                            int buz4 = getNumberFromString(str, 37);
//                            updateDevice(dbHelperOperations, roomId, m4 + "," + subModule,
//                                    onOff4, cl4, dim4, rgb4,
//                                    buz4);
//                            // device 5.
//                            int m5 = (Arrays.asList(Constants.DEVICES))
//                                    .indexOf(str.charAt(38) + "");
//                            int onOff5 = getNumberFromString(str, 39);
//                            int cl5 = getNumberFromString(str, 40);
//                            int dim5 = getNumberFromSubString(str, 41, 44);
//                            int rgb5 = getNumberFromString(str, 44);
//                            int buz5 = getNumberFromString(str, 45);
//                            updateDevice(dbHelperOperations, roomId, m5 + "," + subModule,
//                                    onOff5, cl5, dim5, rgb5,
//                                    buz5);
//                            // device 6.
//                            int m6 = (Arrays.asList(Constants.DEVICES))
//                                    .indexOf(str.charAt(46) + "");
//                            int onOff6 = getNumberFromString(str, 47);
//                            int cl6 = getNumberFromString(str, 48);
//                            int dim6 = getNumberFromSubString(str, 49, 52);
//                            int rgb6 = getNumberFromString(str, 52);
//                            int buz6 = getNumberFromString(str, 53);
//                            updateDevice(dbHelperOperations, roomId, m6 + "," + subModule,
//                                    onOff6, cl6, dim6, rgb6,
//                                    buz6);
//                        }
//                    }
//                } else {
//
//                    // for ON/OFF >> R01M01A0D009RX01Q
////                        // for child lock >>> R01M01CCL1Q
//                    System.out.println("msg " + msg);
//                    messageKey = "device";
//                    int roomId = Utils.getRoomId(msg);
//                    if (roomId != -1) {
//                        String module = Utils.getModule(msg);
//                        System.out.println("ROOM ID " + roomId);
//                        System.out.println("Module ID " + module);
//                        entDevice device = dbHelperOperations
//                                .getDeviceFromModule(roomId, module);
////                        System.out.println("ID "+device.getID());
//                        System.out.println("MMMM " + message);
//                        if (msg.substring(7, 9).equalsIgnoreCase("CL")) {
//                            device.setCL(getNumberFromString(message, 19));
//                        } else {
//                            device.setSTT(getNumberFromString(message, 17));
//                            device.setDIM(getNumberFromSubString(message, 19, 22));
//                        }
//                        dbHelperOperations.updateDevice(device);
//                    }
//                }
//            }
//            Intent intent = new Intent(BroadcastConstant.BROADCAST_ACTION);
//            System.out.println("KEY " + messageKey);
//            intent.putExtra(BroadcastConstant.MESSAGE, messageKey);
////                  intent.putExtra(BroadcastConstant.DATA_CHANGE, bean != null ? bean.getDevices() : null);
//            LocalBroadcastManager.getInstance(getAppContext()).sendBroadcast(intent);
//        }
//
//
////            Intent intent = new Intent(BroadcastConstant.BROADCAST_ACTION_MASSAGE_ARRAIVED);
////            System.out.println("message " + message);
////            intent.putExtra(BroadcastConstant.MESSAGE, message);
////            intent.putExtra(BroadcastConstant.TOPIC, topic);
////            LocalBroadcastManager.getInstance(getAppContext()).sendBroadcast(intent);

    }

    private int getNumberFromString(String str, int poss) {
        try {
            System.out.println("CHAR " + str.charAt(poss));
            return Integer.parseInt(str.charAt(poss) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int getNumberFromSubString(String str, int poss1, int poss2) {
        try {
            System.out.println("SUBSTRING " + str.substring(poss1, poss2));
            return Integer.parseInt(str.substring(poss1, poss2) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void updateDevice(dbHelperOperations dbHelperOperations, int roomId, String s,
                              int onOff, int cl, int dim, int rgb, int buz) {
        System.out.println("ROOM ID " + roomId);
        System.out.println("Module ID " + s);
        System.out.println("onOff " + onOff);
        System.out.println("cl " + cl);
        System.out.println("dim " + dim);
        System.out.println("rgb " + rgb);
        System.out.println("buz " + buz);
        entDevice device = dbHelperOperations
                .getDeviceFromModule(roomId, s);
//        System.out.println("ID "+device.getID());
//        if (device.getID() != 0) {
        device.setSTT(onOff);
        device.setCL(cl);
        device.setDIM(dim);
        device.setRGBSTT(rgb);
        device.setMIC(buz);
        dbHelperOperations.updateDevice(device);
//
    }

    private void generateNotification(String msg) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getAppContext())
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Oob Security Alert")
                        .setContentText(msg);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(getAppContext(), LoginActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getAppContext());
// Adds the back stack
        stackBuilder.addParentStack(LoginActivity.class);
// Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
// Gets a PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Notification note = mBuilder.build();
        note.defaults |= Notification.DEFAULT_VIBRATE;
        note.defaults |= Notification.DEFAULT_SOUND;
        note.defaults |= Notification.DEFAULT_LIGHTS;
        mNotificationManager.notify(0, note);
    }

    public void publishMessage(String publishMessage) {

        try {
//            publishMessage = "sdsdsadsdkdklasdasdklslsakjasklsldsjkdaksdasdasdsasdsdsddkjdsfjkdfsjdsfjkdfkjfdjkkjdfkjdfkjdfskjsdfkjsdsdsadsdkdklasdasdklslsakjasklsldsjkdaksdasdasdsasdsdsddkjdsfjkdfsjdsfjkdfkjfdjkkjdfkjdfkjdfskjsdfkj100";
            MqttMessage message = new MqttMessage();
            message.setPayload(publishMessage.getBytes());
            if (client != null) {
                client.publish(publishTopic, message);
            }
            Log.e(TAG, "Message Published " + publishMessage);
            Utils.showToast(getAppContext(), "Message Published " + publishMessage);
//            onSuccessPublish(publishMessage);
        } catch (MqttException e) {
            System.err.println("Error Publishing: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void sendData(String json) {
        json = json + "Q";
        json = json.replace(" ", "");
        System.out.println("json" + json);
        int mode;
        if (Utils.isEmptyString(Preferences.getAppPrefString(Preferences.KEY_DATA_TRANSFER_MODE))) {
            mode = 1;
        } else {
            mode = Integer.parseInt(Preferences.getAppPrefString(Preferences.KEY_DATA_TRANSFER_MODE));
        }
        System.out.println("MODE " + mode);
        switch (mode) {
            case 1:
                if (client != null && client.isConnected() && Utils.isNetworkAvailable(this)) {
                    publishMessage(json);
                } else {
                    json = WifiUtils.encodeToServerString("JSON=" + json);
                    Log.d("TEST", json);
                    if (!json.isEmpty()) {
                        new SocketServerTask(this, null)
                                .execute(json, WifiUtils.server_ip, WifiUtils.server_port);

                    }
                }
                break;
            case 2:
                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                if (wifiManager.isWifiEnabled()) {
                    json = WifiUtils.encodeToServerString("JSON=" + json);
                    Log.d("TEST", json);
                    if (!json.isEmpty()) {
                        new SocketServerTask(this, null)
                                .execute(json, WifiUtils.server_ip, WifiUtils.server_port);

                    }
                } else {
                    final String dataJson = json;
                    wifiManager.setWifiEnabled(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final String dataJsonNew = WifiUtils.encodeToServerString("JSON=" + dataJson);
                            Log.d("TEST", dataJsonNew);
                            if (!dataJsonNew.isEmpty()) {
                                new SocketServerTask(getAppContext(), null)
                                        .execute(dataJsonNew, WifiUtils.server_ip, WifiUtils.server_port);

                            }
                        }
                    }, 500);
                }
                break;
            case 3:
                if (netCheckin() || Utils.isNetworkAvailable(this)) {
                    if (client != null && client.isConnected() && Utils.isNetworkAvailable(this)) {
                        publishMessage(json);
                    } else {
                        Utils.showToast(this, "Not connected, Please try again.");
                    }
                } else {
                    try {

                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                        startActivity(intent);
                    }catch (Exception e){
                        e.printStackTrace();
                        Utils.showToast(this, "Please enable internet.");
                    }
                }
                break;

        }

    }

    private boolean netCheckin() {
        boolean isEnabled;
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED) {
            isEnabled = true;
        } else {
            isEnabled = false;
        }
        Log.e("IS ENABLE ", "IS " + isEnabled);
        return isEnabled;
    }

    public void stopMqtt() {
        if (client != null && client.isConnected()) {
            try {
                client.close();
                client.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }
}
