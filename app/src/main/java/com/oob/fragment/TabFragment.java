package com.oob.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.oob.OOBApplication;
import com.oob.activity.TVControlGroupChannelActivity;
import com.oob.adapter.ChannelAdapter;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entChannel;
import com.oob.entity.entTV;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.EditText;

import java.util.ArrayList;
import java.util.List;

import static com.oob.app.R.id.rv_room;

public class TabFragment extends Fragment {

    private RecyclerView rv_security;

    private int pos;

    private String group;

    private List<entChannel> channeList = new ArrayList<>();

    private ChannelAdapter adapter;

    private dbHelperOperations db;

    private entTV mEntTV;

    private String prefix1 = "";

    private String prefixStb = "";

    private String prefixTv = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_channel_group
                , container, false);
        db = new dbHelperOperations(getActivity());
        pos = getArguments().getInt("pos");
        group = getArguments().getString("group");
        channeList = db.getListOfChannels(group);
        System.out.println("size  " + channeList.size());
        initControlls(rootView);
        setTvValue();
        return rootView;
    }

    private void initControlls(View rootView) {
        rv_security = (RecyclerView) rootView.findViewById(rv_room);
        GridLayoutManager lLayout = new GridLayoutManager(getActivity(), 3);
        rv_security.setHasFixedSize(true);
        rv_security.setLayoutManager(lLayout);
        adapter = new ChannelAdapter(getActivity(), R.layout.raw_channel,
                channeList);
        rv_security.setAdapter(adapter);
        rv_security.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int i) {
                channeList = db.getListOfChannels(group);
                sendData(prefix1 + prefixStb + "CH" + Constants.getThreeDigit(channeList.get(i).getNumber()));
            }

            @Override
            public void onItemLongClick(BaseQuickAdapter adapter, View view, int i) {
                System.out.println("NAME " + channeList.get(i).getName());
                channelClickOptions(i);

                super.onItemLongClick(adapter, view, i);
            }
        });
//        adapter.setOnRecyclerViewItemLongClickListener(
//                new BaseQuickAdapter.OnRecyclerViewItemLongClickListener() {
//                    @Override
//                    public boolean onItemLongClick(View view, int i) {
//                        System.out.println("NAME " + channeList.get(i).getName());
//                        channelClickOptions(i);
//                        return true;
//                    }
//                });
//        adapter.setOnRecyclerViewItemClickListener(
//                new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int i) {
//                        channeList = db.getListOfChannels(group);
//                        sendData(prefix1 + prefixStb + "CH" + Constants.getThreeDigit(channeList.get(i).getNumber()));
//                    }
//                });

    }

    private void setTvValue() {

        mEntTV = ((TVControlGroupChannelActivity) getActivity()).getTVdata();
        if (mEntTV != null) {
            prefix1 = Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "IR";
            prefixStb = "STBR" + Constants.getTwoDigit(mEntTV.getROOM_Id());// + mEntTV.getSET_NAME()
//                    + mEntTV
//                    .getSET_PROTOCOL();
            prefixTv = "TVR" + Constants.getTwoDigit(mEntTV.getROOM_Id());// + mEntTV.getTV_NAME()
//                    + mEntTV
//                    .getTV_PROTOCOL();
        }
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        json = json.replace(" ", "");
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(getActivity())) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(getActivity(), null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//            }
//        }
    }

    private void channelClickOptions(final int pos) {
        String[] options = {"Add Channel Number", "Delete"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        openChannelNumberDialog(pos);
                        break;
                    case 1:
                        db.deleteChannel(channeList.get(pos).getChannelId());
                        channeList.remove(pos);
                        adapter.notifyDataSetChanged();
                        break;
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        builder.create();
        builder.show();
    }

    private void openChannelNumberDialog(final int pos) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_channel_number);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);

        if (Utils.isEmptyString(channeList.get(pos).getNumber())) {
            edtName.setText(""+db.getChannelNumber(channeList.get(pos).getName(),mEntTV));
        } else {
            edtName.setText(channeList.get(pos).getNumber());
        }
        edtName.setSelection(edtName.getText().length());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                channeList.get(pos).setNumber(edtName.getText().toString());
                db.updateChannel(channeList.get(pos));
                adapter.notifyDataSetChanged();

            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });


    }
}