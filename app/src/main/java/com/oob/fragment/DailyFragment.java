package com.oob.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.oob.activity.EnergyActivity;
import com.oob.activity.EnergyApplianceMothDetailActivity;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entLogData;
import com.oob.tools.Constants;
import com.oob.tools.MyAxisValueFormatter;
import com.oob.tools.TimeMarkerView;
import com.oob.tools.TimeValueFormatter;
import com.oob.tools.Utils;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import static com.oob.activity.EnergyActivity.BAR_COLOOR;

public class DailyFragment extends Fragment implements OnChartValueSelectedListener, View.OnClickListener {
    protected BarChart mChart;
    TextView tvDate;
    MyAxisValueFormatter xAxisFormatter;
    ArrayList<entLogData> logDatas = new ArrayList<>();
    dbHelperOperations db;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Calendar calendar;

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating 
        //Change R.layout.tab1 in you classes
        View rootView = inflater.inflate(R.layout.daily_fragment, container, false);
        db = new dbHelperOperations(getActivity());
        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        tvDate = (TextView) rootView.findViewById(R.id.tvDate);
        tvDate.setText(mDay + "/" + (mMonth + 1) + "/" + mYear);
        tvDate.setOnClickListener(this);
        initChart(rootView);

        return rootView;
    }

    private void initChart(View rootView) {

        mChart = (BarChart) rootView.findViewById(R.id.chart1);
        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(false);

        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);
        mChart.setScaleMinima(1.5f, 0.5f);

        mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);
        xAxisFormatter = new MyAxisValueFormatter();

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTypeface(mTfRegular);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(10, false);
        xAxis.setTextColor(Color.parseColor("#FFFFFF"));
        xAxis.setValueFormatter(xAxisFormatter);

        TimeValueFormatter custom = new TimeValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
//        leftAxis.setTypeface(mTfRegular);
        leftAxis.setLabelCount(8, false);
        leftAxis.setTextColor(Color.parseColor("#FFFFFF"));
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setEnabled(false);
        rightAxis.setDrawLabels(false);
        mChart.getLegend().setEnabled(false);
        TimeMarkerView mv = new TimeMarkerView(getActivity(), xAxisFormatter);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart
        setDataFromString(((EnergyActivity) getActivity()).roomId, Utils.getCurrentDate(calendar), Utils.getNextDayDate(calendar));
    }

    private void setDataFromString(int room_id, String dateFrom, String dateTo) {
        String lable[];
        String value[];
        logDatas = ((EnergyActivity) getActivity()).getDayData();//generateDummyData();
        setData(logDatas);
        return;
//        logDatas = db.getLogData(room_id, dateFrom, dateTo);
////        waterUsages =
//        if (logDatas.size() > 0) {
//            Collections.sort(logDatas, new Comparator<entLogData>() {
//                @Override
//                public int compare(entLogData o1, entLogData o2) {
//                    return (int) (o2.getDateTime() - o1.getDateTime());
//                }
//            });
//            logDatas = generateDummyData();
//            setData(logDatas);
//            mChart.invalidate();
//        } else {
//            mChart.clear();
//            mChart.invalidate();
//        }

    }

    private ArrayList<entLogData> generateDummyData() {
        ArrayList<entLogData> value = new ArrayList<>();
        entLogData entLogData;
        String devices[] = new String[]{"Plug", "Light", "AC", "TV", "Fan", "Light1", "Fan1", "TV",};
        for (int i = 0; i < 5; i++) {
            long mult = (5000000 + 1);
            long val = (long) (Math.random() * mult);
            entLogData = new entLogData();
            entLogData.setDateTime(val);
            entLogData.setDeviceName(devices[i]);
            value.add(entLogData);
        }
        return value;
    }

    private void setData(ArrayList<entLogData> value) {

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        String lable[] = new String[value.size()];
            String values[] = new String[value.size()];

        int total = 0;
        for (int i = 0; i < value.size(); i++) {
            System.out.println("DEVICE " + value.get(i).getDeviceName() + " TIME " + value.get(i).getDateTime());
            float val = Float.parseFloat("" + value.get(i).getDateTime() / 1000);
            total = total + (int) val;
            yVals1.add(new BarEntry(i, val));
            lable[i] = value.get(i).getDeviceName();
            values[i] = String.valueOf(value.get(i).getDateTime() / 1000);
        }
        xAxisFormatter.setData(lable);
        xAxisFormatter.setDataValue(values);
        System.out.print(total);


        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            xAxisFormatter.setData(lable);
            xAxisFormatter.setDataValue(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "");
            set1.setColors(BAR_COLOOR);
            set1.setValueTextColor(Color.parseColor("#FFFFFF"));
            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTextColor(Color.parseColor("#FFFFFF"));
//            data.setValueTypeface(mTfRegular);
            data.setBarWidth(0.8f);
            data.setDrawValues(false);
            mChart.setData(data);
            mChart.animateY(2000);
//            mChart.zoom(2f,0f,0f,0f);
            mChart.invalidate();
        }
    }
    String name;
    String value;

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        name = xAxisFormatter.getFormattedValue(e.getX(), null);
        System.out.println("Index " + e.getX());
        value = xAxisFormatter.getValueUsage((int) e.getX());
    }

    @Override
    public void onNothingSelected() {
        System.out.println("name " + name);
        System.out.println("value " + value);
//        if (!isDummyData) {
        Intent intent = new Intent(getActivity(), EnergyApplianceMothDetailActivity.class);
        intent.putExtra("name", name);
        intent.putExtra("value", value);
        intent.putExtra("monthName", Constants.getTwoDigit(mMonth+1)+"-"+Constants.getTwoDigit(mYear));
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDate:
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                mYear = year;
                                mMonth = monthOfYear;
                                mDay = dayOfMonth;
                                tvDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                setDataFromString(((EnergyActivity) getActivity()).roomId, Utils.getCurrentDate(calendar), Utils.getNextDayDate(calendar));

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + (1000 * 60 * 60 * 24));
                datePickerDialog.show();
                break;
        }
    }
}