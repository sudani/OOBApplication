package com.oob.fragment;

import com.oob.app.R;
import com.oob.view.TextView;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class IntroFragment extends Fragment {

    TextView tvTitle, tvDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_intro, container, false);
        tvTitle = (TextView) rootView.findViewById(R.id.tvTitle);
        tvDetail = (TextView) rootView.findViewById(R.id.tvDetail);

        tvTitle.setText(getArguments().getString("title"));
        tvDetail.setText(getArguments().getString("detail"));
        return rootView;
    }


}