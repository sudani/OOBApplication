package com.oob.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entAC;
import com.oob.entity.entDevice;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AcContolFragment extends Fragment
        implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    private dbHelperOperations db;
    private TextView tvMainOnOff, tvTemp, tvLeft, tvRight, tvFan,
            tvPlus, tvMinus, tvMode, tvSwing, tvTurbo, tvSensor, tvName;
    private TextView tvFan1, tvFan2, tvFan3, tvFan4;
    private RelativeLayout layOnOff;
    private ImageView ivAuto, ivCool, ivDry, ivFan, ivHot, ivTurbo, ivSwing;
    private boolean isOn = true;
    private boolean swing = false;
    private boolean turbo = false;
    private String unit = "℃";
    private int temp = 24;
    private int fanCount = 0;
    private entAC mEntAC;
    private String prefix1 = "";
    private String prefixAC = "";
    private int modeCount = 0;
    LinearLayout layBottom;
    private TextView tv_fromTimer1, tv_toTimer1, tv_fromTimer2, tv_toTimer2;
    private ImageView ivDevice;
    private LinearLayout ll_days, layTimer1, layTimer2;
    private int position;
    private entDevice device;
    private boolean isTimer1 = true;
    private List<entDevice> lisACs = new ArrayList<>();
    private BroadcastReceiver broadcast;
    private int onOff = 1;
    RotateAnimation rotate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_ac_control
                , container, false);
        db = new dbHelperOperations(getActivity());
        position = getArguments().getInt("pos");
        mEntAC = (entAC) getArguments().getSerializable("entAc");
        initItems(rootView);
        setACfromDeviceList();
        setTvValue();
        allDisable();
        singleDisable(ivTurbo);
        singleDisable(ivSwing);
        if (device != null) {
            broadcastListener();
        }
        rotate = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(500);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setRepeatMode(Animation.RESTART);
        rotate.setInterpolator(new LinearInterpolator());
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
        IntentFilter intentFilter1 = new IntentFilter(BroadcastConstant.BROADCAST_ACTION_MASSAGE_ARRAIVED);

    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcast);
    }

    private void broadcastListener() {
        broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                System.out.println("extras " + intent.getExtras());
                if (intent.hasExtra(BroadcastConstant.MESSAGE)) {
                    if (intent.getExtras().getString(BroadcastConstant.MESSAGE)
                            .equalsIgnoreCase("device")) {
                        setACfromDeviceList();

                    }
//
                }
            }
        };

    }

    private void setACfromDeviceList() {
        lisACs = db.getListACs(mEntAC.getROOM_Id());
        if (lisACs.size() > 0) {
            layBottom.setVisibility(View.VISIBLE);
            if (lisACs.size() > 1) {
                if (position == 0) {
                    device = lisACs.get(0);
                } else {
                    device = lisACs.get(1);
                }
            } else {
                device = lisACs.get(0);
            }
            bottomViewSetup();
        } else {
            layBottom.setVisibility(View.GONE);
        }
    }

    private void allDisable() {
        ivAuto.setColorFilter(Color.parseColor("#000000"),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        ivCool.setColorFilter(Color.parseColor("#000000"),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        ivDry.setColorFilter(Color.parseColor("#000000"),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        ivHot.setColorFilter(Color.parseColor("#000000"),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        ivFan.setColorFilter(Color.parseColor("#000000"),
                android.graphics.PorterDuff.Mode.MULTIPLY);

    }

    private void singleDisable(ImageView imageView) {
        imageView.setColorFilter(Color.parseColor("#000000"),
                android.graphics.PorterDuff.Mode.MULTIPLY);

    }

    private void singleEnable(ImageView imageView) {
        imageView.setColorFilter(Color.parseColor("#00AFEF"),
                android.graphics.PorterDuff.Mode.MULTIPLY);

    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        json = json.replace(" ", "");
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(getActivity())) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(getActivity(), null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//            }
//        }
    }

    private void initItems(View rootView) {
        layBottom = (LinearLayout) rootView.findViewById(R.id.layBottom);
        tvMainOnOff = (TextView) rootView.findViewById(R.id.tvOnOff);
        tvTemp = (TextView) rootView.findViewById(R.id.tvTemp);
        tvLeft = (TextView) rootView.findViewById(R.id.tvLeft);
        tvRight = (TextView) rootView.findViewById(R.id.tvRight);
        tvFan = (TextView) rootView.findViewById(R.id.tvFan);
        tvPlus = (TextView) rootView.findViewById(R.id.tvFanPlus);
        tvMinus = (TextView) rootView.findViewById(R.id.tvFanMinus);
        tvMode = (TextView) rootView.findViewById(R.id.tvMode);
        tvSwing = (TextView) rootView.findViewById(R.id.tvSwing);
        layOnOff = (RelativeLayout) rootView.findViewById(R.id.layOnOff);
        tvTurbo = (TextView) rootView.findViewById(R.id.tvTurbo);
        tvSensor = (TextView) rootView.findViewById(R.id.tvSensor);
        tvFan1 = (TextView) rootView.findViewById(R.id.tvFan1);
        tvFan2 = (TextView) rootView.findViewById(R.id.tvFan2);
        tvFan3 = (TextView) rootView.findViewById(R.id.tvFan3);
        tvFan4 = (TextView) rootView.findViewById(R.id.tvFan4);
        ivAuto = (ImageView) rootView.findViewById(R.id.ivAuto);
        ivCool = (ImageView) rootView.findViewById(R.id.ivCool);
        ivDry = (ImageView) rootView.findViewById(R.id.ivDry);
        ivFan = (ImageView) rootView.findViewById(R.id.ivFan);
        ivHot = (ImageView) rootView.findViewById(R.id.ivHot);
        ivTurbo = (ImageView) rootView.findViewById(R.id.ivTurbo);
        ivSwing = (ImageView) rootView.findViewById(R.id.ivSwing);
        ll_days = (LinearLayout) rootView.findViewById(R.id.ll_days);
        layTimer1 = (LinearLayout) rootView.findViewById(R.id.layTimer1);
        layTimer2 = (LinearLayout) rootView.findViewById(R.id.layTimer2);
        tv_fromTimer1 = (TextView) rootView.findViewById(R.id.tvTimer1From);
        tv_toTimer1 = (TextView) rootView.findViewById(R.id.tvTimer1to);
        tv_fromTimer2 = (TextView) rootView.findViewById(R.id.tvTimer2From);
        tv_toTimer2 = (TextView) rootView.findViewById(R.id.tvTimer2to);
        tvName = (TextView) rootView.findViewById(R.id.tvName);
        ivDevice = (ImageView) rootView.findViewById(R.id.ivDevice);

        tvMainOnOff.setOnClickListener(this);
        layOnOff.setOnClickListener(this);
        tvTemp.setOnClickListener(this);
        tvLeft.setOnClickListener(this);
        tvRight.setOnClickListener(this);
        tvFan.setOnClickListener(this);
        tvPlus.setOnClickListener(this);
        tvMinus.setOnClickListener(this);
        tvMode.setOnClickListener(this);
        tvSwing.setOnClickListener(this);

        tvTurbo.setOnClickListener(this);
        tvSensor.setOnClickListener(this);

    }

    private void bottomViewSetup() {
        tv_fromTimer1.setText(device.getTIME_ONE_A());
        tv_toTimer1.setText(device.getTIME_ONE_B());
        tv_fromTimer2.setText(device.getTIME_TWO_A());
        tv_toTimer2.setText(device.getTIME_TWO_B());
        tvName.setText(device.getDEVICE_NAME());
        temp = device.getDIM() == 0 ? 16 : device.getDIM();
        tvTemp.setText(temp + unit);
        ivDevice.setImageResource(
                device.getSTT() == 1 ? R.drawable.on : R.drawable.off);
        ivDevice.setOnClickListener(this);
        layTimer1.setOnClickListener(this);
        layTimer2.setOnClickListener(this);
        setupDaysClick(ll_days);
        setDaysFromBean(ll_days);
    }

    private void setTvValue() {
        prefix1 = Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "IR";
        prefixAC = "ACR" + Constants.getTwoDigit(mEntAC.getROOM_Id());// + mEntAC.getAC_NAME();
//                + mEntAC
//                .getAC_PROTOCOL();
    }

    @Override
    public void onDestroy() {
        db.updateAc(mEntAC);
        super.onDestroy();
    }

    private void turboMode() {
        if (!turbo) {
            turbo = true;
            singleEnable(ivTurbo);
            sendData(prefix1 + prefixAC + "TURBO1");
        } else {
            turbo = false;
            singleDisable(ivTurbo);
            sendData(prefix1 + prefixAC + "TURBO0");
        }
    }

    private void swingMode() {
        if (!swing) {
            swing = true;
            singleEnable(ivSwing);
            sendData(prefix1 + prefixAC + "SWING1");
        } else {
            swing = false;
            singleDisable(ivSwing);
            sendData(prefix1 + prefixAC + "SWING0");
        }
    }

    @Override
    public void onClick(View v) {
        if (layBottom.getVisibility() == View.GONE) {
            return;
        }
        switch (v.getId()) {
            case R.id.tvOnOff:
            case R.id.layOnOff:
                if (onOff == 1) {
                    onOff = 0;
                } else {
                    onOff = 1;
                }
                layOnOff.setBackgroundResource(
                        onOff == 1 ? R.drawable.circle_shap : R.drawable.circle_shap_red);
                sendData(prefix1 + prefixAC + "ONOFF" + onOff);
                break;
            case R.id.ivDevice:
                device.setSTT(device.getSTT() == 0 ? 1 : 0);
                device.setALL_STT(device.getSTT());
                ivDevice.setImageResource(device.getSTT() == 1 ? R.drawable.on : R.drawable.off);
//                layOnOff.setBackgroundResource(
//                        device.getSTT() == 1 ? R.drawable.circle_shap : R.drawable.circle_shap_red);
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                        + Constants
                        .getRoomcode(device.getROOM_Id())
                        + Constants
                        .getModuleAndDevicecode(device.getMODULE())
                        + device
                        .getSTT()
                        + "D" + Constants
                        .getThreeDigit(device.getDIM()) + "RX"
                        + device.getRGBSTT()
                        + device.getMIC()
                );
//                sendData(prefix1 + prefixAC + (device.getSTT() == 1 ? "OFF" : "ON"));
                db.updateDevice(device);

//                if (isOn) {
//                    layOnOff.setBackgroundResource(R.drawable.circle_shap_red);
//                    isOn = false;
//                    sendData(prefix1 + prefixAC + "OFF"
//                    );
//                } else {
//                    isOn = true;
//                    layOnOff.setBackgroundResource(R.drawable.circle_shap);
//                    sendData(prefix1 + prefixAC + "ON"
//                    );

//                }
                break;
            case R.id.tvLeft:
                if (temp > 16) {
                    temp--;
                    tvTemp.setText(temp + unit);
                    sendData(prefix1 + prefixAC + "TEMP" + temp);
                    device.setDIM(temp);
                    db.updateDevice(device);
                }
                break;
            case R.id.tvRight:
                if (temp < 32) {
                    temp++;
                    tvTemp.setText(temp + unit);
                    device.setDIM(temp);
                    db.updateDevice(device);
                    sendData(prefix1 + prefixAC + "TEMP" + temp);
                }
                break;
            case R.id.tvTemp:
                sendData(prefix1 + prefixAC + "TEMP");
                break;
            case R.id.tvFanPlus:
                if (fanCount <= 4) {

                    sendData(prefix1 + prefixAC + "FANPLUS");
                    if (fanCount == 4) {
                        ivFan.startAnimation(rotate);
                    }
                    fanCount++;
                    fanUnitVisibility();
                }
                break;
            case R.id.tvFanMinus:
                if (fanCount > 0) {
                    fanCount--;
                    if (fanCount == 4) {
                        ivFan.clearAnimation();
                    }
                    sendData(prefix1 + prefixAC + "FANMINUS");
                    fanUnitVisibility();
                }

                break;
            case R.id.tvFan:
                sendData(prefix1 + prefixAC + "FAN");

                break;
            case R.id.tvMode:
                if (modeCount <= 4) {
                    modeCount++;
                } else {
                    modeCount = 1;
                }
                modeDataGenerate();

                break;
            case R.id.tvSwing:
                swingMode();

                break;
            case R.id.tvTurbo:
                turboMode();

                break;
            case R.id.tvSensor:
                sendData(prefix1 + prefixAC + "*");

                break;
            case R.id.layTimer1:
                isTimer1 = true;
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        AcContolFragment.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );

                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
                break;
            case R.id.layTimer2:
                isTimer1 = false;
                Calendar now1 = Calendar.getInstance();
                TimePickerDialog tpd1 = TimePickerDialog.newInstance(
                        AcContolFragment.this,
                        now1.get(Calendar.HOUR_OF_DAY),
                        now1.get(Calendar.MINUTE),
                        false
                );

                tpd1.show(getActivity().getFragmentManager(), "Timepickerdialog");
                break;

        }
    }

    private void modeDataGenerate() {
        switch (modeCount) {
            case 0:
                allDisable();
                sendData(prefix1 + prefixAC + "MODE");
                break;
            case 1:
                allDisable();
                singleEnable(ivAuto);
                sendData(prefix1 + prefixAC + "MODEAUTO");
                break;
            case 2:
                allDisable();
                singleEnable(ivCool);
                sendData(prefix1 + prefixAC + "MODCOOL");
                break;
            case 3:
                allDisable();
                singleEnable(ivDry);
                sendData(prefix1 + prefixAC + "MODEDRY");
                break;
            case 4:
                allDisable();
                singleEnable(ivFan);
                sendData(prefix1 + prefixAC + "MODEFAN");
                break;
            case 5:
                allDisable();
                singleEnable(ivHot);
                sendData(prefix1 + prefixAC + "MODEHEAT");
                break;
        }
    }

    private void fanUnitVisibility() {
        switch (fanCount) {
            case 0:
                tvFan1.setTextColor(getResources().getColor(R.color.Black));
                tvFan2.setTextColor(getResources().getColor(R.color.Black));
                tvFan3.setTextColor(getResources().getColor(R.color.Black));
                tvFan4.setTextColor(getResources().getColor(R.color.Black));

                break;
            case 1:
                tvFan1.setTextColor(getResources().getColor(R.color.blue));
                tvFan2.setTextColor(getResources().getColor(R.color.Black));
                tvFan3.setTextColor(getResources().getColor(R.color.Black));
                tvFan4.setTextColor(getResources().getColor(R.color.Black));
                break;
            case 2:
                tvFan1.setTextColor(getResources().getColor(R.color.blue));
                tvFan2.setTextColor(getResources().getColor(R.color.blue));
                tvFan3.setTextColor(getResources().getColor(R.color.Black));
                tvFan4.setTextColor(getResources().getColor(R.color.Black));
                break;
            case 3:
                tvFan1.setTextColor(getResources().getColor(R.color.blue));
                tvFan2.setTextColor(getResources().getColor(R.color.blue));
                tvFan3.setTextColor(getResources().getColor(R.color.blue));
                tvFan4.setTextColor(getResources().getColor(R.color.Black));
                break;
            case 4:
                tvFan1.setTextColor(getResources().getColor(R.color.blue));
                tvFan2.setTextColor(getResources().getColor(R.color.blue));
                tvFan3.setTextColor(getResources().getColor(R.color.blue));
                tvFan4.setTextColor(getResources().getColor(R.color.blue));
                break;
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd,
            int minuteEnd) {

        int hour, endHour;
        String ampm, endAMPM, FromDate, ToDate, timer;

        System.out.println("hourOfDay " + hourOfDay);
        System.out.println("minute " + minute);
        System.out.println("hourOfDayEnd " + hourOfDayEnd);
        System.out.println("minuteEnd " + minuteEnd);
        if (hourOfDay > 12) {
            hour = hourOfDay - 12;
            ampm = "pm";
        } else {
            hour = hourOfDay;
            ampm = "am";
        }
        if (hourOfDayEnd > 12) {
            endHour = hourOfDayEnd - 12;
            endAMPM = "pm";
        } else {
            endHour = hourOfDayEnd;
            endAMPM = "am";
        }
        FromDate = hour + ":" + minute + " " + ampm;
        ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
        if (isTimer1) {
            timer = "T1";
            device.setTIME_ONE_A(FromDate);
            device.setTIME_ONE_B(ToDate);
            tv_fromTimer1.setText(FromDate);
            tv_toTimer1.setText(ToDate);
        } else {
            timer = "T2";
            device.setTIME_TWO_A(FromDate);
            device.setTIME_TWO_B(ToDate);
            tv_fromTimer2.setText(FromDate);
            tv_toTimer2.setText(ToDate);
        }
        db.updateDevice(device);
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(device.getROOM_Id())
                + Constants.getModuleAndDevicecode(device.getMODULE()) + timer
                + Constants.getTwoDigit(hourOfDay) + Constants.getTwoDigit(minute) + Constants
                .getTwoDigit(hourOfDayEnd) + Constants.getTwoDigit(minuteEnd) + "XXXXREP" + device
                .getDAYS());


    }

    private void setupDaysClick(final LinearLayout ll_days) {
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View view = ll_days.getChildAt(j);
//            if (view instanceof RelativeLayout) {
//                RelativeLayout layout = ((RelativeLayout) view);
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tag = (String) v.getTag();
                        switch (tag) {
                            case "0":
//                                ((TextView) v).setTypeface(null, Typeface.BOLD);
//                                ((TextView) v).setTextColor(Color.CYAN);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_active_bg);
                                v.setTag("1");
                                break;
                            case "1":
//                                ((TextView) v).setTextColor(Color.BLACK);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_bg);
                                v.setTag("0");
                                break;
                        }
                        boolean checked = ((Character) device.getDAYS().charAt(7))
                                .toString()
                                .equals("1");
                        device.setDAYS(getFinalDays(checked, ll_days));
                        db.updateDevice(device);
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                + Constants
                                .getRoomcode(device.getROOM_Id())
                                + Constants.getModuleAndDevicecode(device.getMODULE()) + device
                                .getDAYS());

                    }
                });
            }
//            }
        }
    }

    private void setDaysFromBean(LinearLayout ll_days) {
        String days = device.getDAYS();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            String tag = ((Character) days.charAt(j)).toString();
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setTag(tag);
                switch (tag) {
                    case "0":
                        ((com.oob.view.TextView) v).setBackgroundResource(R.drawable.text_bg);
                        break;
                    case "1":
                        ((com.oob.view.TextView) v)
                                .setBackgroundResource(R.drawable.text_active_bg);
                        break;
                }
            }
        }
    }

    private String getFinalDays(boolean isChecked, LinearLayout ll_days) {
        String daysStatus = getSelectedDays(ll_days);
        if (isChecked) {
            daysStatus = daysStatus + "1";
        } else {
            daysStatus = daysStatus + "0";
        }

        return daysStatus;
    }

    private String getSelectedDays(LinearLayout ll_days) {
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                builder.append((String) v.getTag());
            }

        }
        return builder.toString();
    }

}