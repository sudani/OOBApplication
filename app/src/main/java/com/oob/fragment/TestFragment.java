package com.oob.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oob.app.R;


/**
 * Created by pranav on 4/11/15.
 */
public class TestFragment extends BaseFragment
{

    private TextView text;



    public static TestFragment newInstance() {
        TestFragment fragment = new TestFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View v=inflater.inflate(R.layout.frag_home,container,false);
        bindViewControls(v);

        return v;
    }

    private void bindViewControls(View v) {
        text= (TextView) v.findViewById(R.id.text);
        text.setText("Noise Measure");
    }

}
