package com.oob.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

import com.oob.tools.Constants;

public class PicModeSelectDialogFragment extends DialogFragment {

    private String[] picMode = {Constants.PicModes.CAMERA, Constants.PicModes.GALLERY};

    private IPicModeSelectListener iPicModeSelectListener;
    private int position;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Profile Picture.");
        builder.setItems(picMode, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (iPicModeSelectListener != null) {
                    iPicModeSelectListener.onPicModeSelected(picMode[which],position);
                    Log.e("TAG","NOT NULL");
                }else {
                    Log.e("TAG"," NULL");

                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        return builder.create();
    }

    public void setiPicModeSelectListener(IPicModeSelectListener iPicModeSelectListener, int pos) {
        this.position =pos;
        this.iPicModeSelectListener = iPicModeSelectListener;
    }

    public interface IPicModeSelectListener {
        void onPicModeSelected(String mode,int position);
    }
}
