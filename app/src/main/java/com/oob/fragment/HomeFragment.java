package com.oob.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.oob.activity.TestScreen;
import com.oob.app.R;


/**
 * Created by pranav on 4/11/15.
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener
{
    private View view;
    private Button test,btn_light;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        view =inflater.inflate(R.layout.frag_home,container,false);
        bindViewControls();

        return view;
    }

    private void bindViewControls()
    {
        test= (Button) view.findViewById(R.id.test_server);
        btn_light= (Button) view.findViewById(R.id.btn_light);
        btn_light.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Home");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.test_server:
                startActivity(new Intent(mActivity,TestScreen.class));
                break;
            case R.id.btn_light:
//                openFragment(RoomFragment.newInstance());
                break;
        }
    }
}
