//package com.oob.fragment;
//
//import android.app.ProgressDialog;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.annotation.Nullable;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.oob.adapter.RoomRecyclerAdapter;
//import com.oob.app.R;
//import com.oob.bean.DevicesEntity;
//import com.oob.bean.RoomDevice;
//import com.oob.tools.BroadcastConstant;
//import com.oob.tools.NotifyUtils;
//import com.oob.tools.Preferences;
//import com.oob.tools.WifiUtils;
//
//import java.util.ArrayList;
//import java.util.List;
//
//
///**
// * Created by pranav on 4/11/15.
// */
//public class RoomFragment extends BaseFragment
//{
//    private RecyclerView rv_room;
//    private RoomRecyclerAdapter rv_adapter;
//    private View view;
//    private TextView tv_servermsg;
//    public static final int SERVERPORT = 6000;
//    private BroadcastReceiver broadcast;
//    private ArrayList<DevicesEntity> devicesEntity;
//    private ProgressDialog progressDialog;
//
//
//    public static RoomFragment newInstance() {
//        RoomFragment fragment = new RoomFragment();
//        return fragment;
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
//    {
//        if(view==null)
//        {
//            view =inflater.inflate(R.layout.frag_room, container,false);
//            bindViewControls();
//            initItems();
//            tv_servermsg.setText(String.format("MY IP:%s\n", WifiUtils.getIPAddress(true)));
//
//            broadcastListener();
//            progressDialog=new ProgressDialog(mActivity);
//            progressDialog.setMessage("Waiting for Acknowledgment..");
//
//        }
//
//        return view;
//    }
//    private void bindViewControls() {
//        rv_room= (RecyclerView) view.findViewById(R.id.rv_room);
//        tv_servermsg= (TextView) view.findViewById(R.id.tv_servermsg);
//        //   rangeBar_bottom= (RangeSeekBar<Long>) view.findViewById(R.id.rangeBar_bottom);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mActivity.setTitle("Room");
//        IntentFilter intentFilter=new IntentFilter(BroadcastConstant.BROADCAST_ACTION);
//        LocalBroadcastManager.getInstance(mActivity).registerReceiver(broadcast, intentFilter);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(broadcast);
//
//    }
//
//    private void broadcastListener() {
//        broadcast=new BroadcastReceiver() {
//            @Override
//            public void onReceive(final Context context, Intent intent) {
//
//                if(intent.hasExtra(BroadcastConstant.MESSAGE))
//                {
//                    String msg= intent.getStringExtra(BroadcastConstant.MESSAGE);
//                    if(msg!=null)
//                    {
//                        if(msg.equals(getString(R.string.error_wrong)))
//                            NotifyUtils.toast(mActivity, msg);
//                        else if(msg.equals(getString(R.string.str_ack_msg)))
//                        {
//                            progressDialog.show();
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                    if(progressDialog.isShowing())
//                                    {
//                                        progressDialog.dismiss();
//                                        rv_adapter.updateContentFromDB(selectedRoomId);
//                                        NotifyUtils.toast(mActivity,getString(R.string.str_no_ack));
//                                    }
//
//                                }
//                            }, 5000);
//                        }
//                    }
//                }
//
//                if (intent.hasExtra(BroadcastConstant.DATA_CHANGE)) {
//                    devicesEntity = intent.getParcelableArrayListExtra(BroadcastConstant.DATA_CHANGE);
//
//
//
//                    if (devicesEntity!=null)
//                    {
//                        if(progressDialog.isShowing()) {
//                            progressDialog.dismiss();
//                            NotifyUtils.toast(mActivity, getString(R.string.str_ack_recv));
//                        }
//
//                        if(Preferences.getAppPrefBool(Preferences.KEY_DIALOG_SHOW,true))
//                        {
//                            dataChangeAlert();
//                            Preferences.writeSharedPreferencesBool(Preferences.KEY_DIALOG_SHOW, false);
//                        }
//                        else
//                        {
//                            rv_adapter.updateContentFromDB(selectedRoomId);
//                        }
//                    }
//                    else
//                        NotifyUtils.toast(mActivity, "Not Received successfully");
//                }
//            }
//        };
//    }
//
//    public void dataChangeAlert(){
//        AlertDialog alertDialog = new AlertDialog.Builder(
//                mActivity).create();
//
//        // Setting Dialog Title
//        alertDialog.setTitle(getString(R.string.title_data_change));
//
//        // Setting Dialog Message
//        //alertDialog.setMessage(getString(R.string.msg_data_change));
//
//        // Setting Icon to Dialog
//        alertDialog.setIcon(R.mipmap.ic_launcher);
//
//        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                rv_adapter.updateContentFromDB(selectedRoomId);
//            }
//        });
//
//
//        // Showing Alert Message
//        alertDialog.show();
//    }
//
//
//    private void initItems() {
//        ArrayList<DevicesEntity> al_data=getData();
//        RoomDevice roomDevice=new RoomDevice();
//        roomDevice.setDevices(al_data);
//        rv_adapter=new RoomRecyclerAdapter(mActivity,roomDevice,getActivity());
//        rv_room.setLayoutManager(new LinearLayoutManager(mActivity));
//        rv_room.setAdapter(rv_adapter);
//    }
//
//    private ArrayList<DevicesEntity> getData() {
//        List<DevicesEntity> al_data= DevicesEntity.getAll();
//
//        if(al_data!=null&&!al_data.isEmpty())
//            return (ArrayList<DevicesEntity>) al_data;
//        else
//            al_data=new ArrayList<>();
//
//        for(int i=1;i<=4;i++)
//        {
//            DevicesEntity bean=new DevicesEntity();
//            bean.setID(i);
//            bean.setENRG(0);
//            bean.setGRP(0);
//            bean.setCL(0);
//            bean.setRGBCLR(0);
//            bean.setRGBSTT(0);
//            switch (i)
//            {
//                case 1:
//                    bean.setSTT(0);
//                    bean.setDIM(0);
//                    bean.setTIMA("000000");
//                    bean.setTIMB("000000");
//                    bean.setDays("00000000");
//                    break;
//                case 2:
//                    bean.setSTT(0);
//                    bean.setDIM(0);
//                    bean.setTIMA("000000");
//                    bean.setTIMB("000000");
//                    bean.setDays("00000000");
//                    break;
//                case 3:
//                    bean.setSTT(0);
//                    bean.setDIM(0);
//                    bean.setTIMA("000000");
//                    bean.setTIMB("000000");
//                    bean.setDays("00000000");
//                    break;
//                case 4:
//                    bean.setSTT(0);
//                    bean.setDIM(0);
//                    bean.setTIMA("000000");
//                    bean.setTIMB("000000");
//                    bean.setDays("00000000");
//                    break;
//
//            }
//            bean.save();
//            al_data.add(bean);
//        }
//
//        return (ArrayList<DevicesEntity>) al_data;
//    }
//}
