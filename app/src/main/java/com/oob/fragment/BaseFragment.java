package com.oob.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;

import com.oob.activity.DashBoardActivity;

/**
 * Created by pranav on 4/11/15.
 */
public abstract class BaseFragment extends Fragment
{
    protected DashBoardActivity mActivity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity= (DashBoardActivity) getActivity();
    }
    protected void openFragment(Fragment frag)
    {
//        mActivitytt.getFrag_organizer().openFragment(frag);
    }
    protected void openFragmentWithTransition(Fragment fragOne, Fragment fragTwo, View sharedElement)
    {
//        mActivity.getFrag_organizer().openFragmentWithTransition(fragOne,fragTwo,sharedElement);
    }

    protected void openDialogFragmentWithTransition(Fragment fragOne, AppCompatDialogFragment fragTwo, View sharedElement)
    {
//        mActivity.getFrag_organizer().openDialogFragmentWithTransition(fragOne,fragTwo,sharedElement);
    }
//    protected void popFragmentFromQueue()
//    {
//        mActivity.getFrag_organizer().removeFromBackStack();
//    }
}
