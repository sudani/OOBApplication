package com.oob.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Messenger;
import android.util.Log;

import com.oob.OOBApplication;
import com.oob.SocketServerTask;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.tools.WifiUtils;

public class BroadcastServiceTV extends Service {

    private final static String TAG = "BroadcastServiceAC";

    public static final String COUNTDOWN_BR = "com.oob.countdown_TV";
    Intent bi = new Intent(COUNTDOWN_BR);
    CountDownTimer cdt = null;
    int tvId;
    private entDevice deviceTv;
    private final IBinder mBinder = new MyBinder();
    private Messenger outMessenger;
    private String prefix1;
    private String prefixTv;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "Starting timer...");

        final long diff = Long.parseLong(Preferences.getAppPrefString(Preferences.KEY_TO_TIME)) - Long.parseLong(Preferences.getAppPrefString(Preferences.KEY_FROM_TIME));
        System.out.println("DIFF " + diff);
        cdt = new CountDownTimer(diff * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                bi.putExtra("time", Utils.convertToHourMinutSecond(millisUntilFinished));
                bi.putExtra("tvId", tvId);
                sendBroadcast(bi);
                if (Utils.convertToHourMinutSecond(millisUntilFinished).equalsIgnoreCase("00:00:01")) {
//                    off the tv
                    deviceTv.setSTT(0);
                    deviceTv.setALL_STT(deviceTv.getSTT());
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                            + Constants
                            .getRoomcode(deviceTv.getROOM_Id())
                            + Constants
                            .getModuleAndDevicecode(deviceTv.getMODULE())
                            + deviceTv
                            .getSTT()
                            + "D" + Constants
                            .getThreeDigit(deviceTv.getDIM()) + "RX"
                            + deviceTv.getRGBSTT()
                            + deviceTv.getMIC()
                    );
                    sendData(prefix1 + prefixTv + "TV");
                    dbHelperOperations dbHelperOperations = new dbHelperOperations(BroadcastServiceTV.this);
                    dbHelperOperations.updateDevice(deviceTv);
                    bi.putExtra("time", "00:00:00");
                    bi.putExtra("tvId", tvId);
                    sendBroadcast(bi);
                    stopSelf();
                }
            }

            @Override
            public void onFinish() {
                Log.i(TAG, "Timer finished");
                stopSelf();
            }
        };

        cdt.start();
    }

    @Override
    public void onDestroy() {

        cdt.cancel();
        Log.i(TAG, "Timer cancelled");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public IBinder onBind(Intent arg0) {
        Bundle extras = arg0.getExtras();
        Log.d("service", "onBind");
        // Get messager from the Activity
        tvId = extras.getInt("tvId", 0);
        deviceTv = (entDevice) extras.get("device");
        prefix1 =extras.getString("pre1");
        prefixTv =  extras.getString("preTv");
        System.out.println("tvId " + tvId);
//        if (extras != null) {
//            Log.d("service","onBind with extra");
//            outMessenger = (Messenger) extras.get("MESSENGER");
//        }
        return mBinder;
    }

    public class MyBinder extends Binder {
        BroadcastServiceTV getService() {
            return BroadcastServiceTV.this;
        }
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        json = json.replace(" ", "");
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(this)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(this, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//            }
//        }
    }
}