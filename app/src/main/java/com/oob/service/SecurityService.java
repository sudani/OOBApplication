package com.oob.service;

import com.oob.app.R;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import java.util.Arrays;
import java.util.List;

public class SecurityService extends Service {

    private final static String TAG = "SecurityService";
//List <String> ListSensor = Arrays.asList(this.getResources().getStringArray(R.array.security_sensor));

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate");
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}