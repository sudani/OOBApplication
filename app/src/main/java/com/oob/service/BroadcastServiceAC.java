package com.oob.service;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import com.oob.tools.Preferences;
import com.oob.tools.Utils;

public class BroadcastServiceAC extends Service {

    private final static String TAG = "BroadcastServiceAC";

    public static final String COUNTDOWN_BR = "com.oob.countdown_AC";
    Intent bi = new Intent(COUNTDOWN_BR);
    CountDownTimer cdt = null;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "Starting timer...");
        final long diff = Long.parseLong(Preferences.getAppPrefString(Preferences.KEY_TO_TIME_AC)) - Long.parseLong(Preferences.getAppPrefString(Preferences.KEY_FROM_TIME_AC));
        System.out.println("DIFF " + diff);
        cdt = new CountDownTimer(diff * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                bi.putExtra("time", Utils.convertToHourMinutSecond(millisUntilFinished));
                sendBroadcast(bi);
            }

            @Override
            public void onFinish() {
                Log.i(TAG, "Timer finished");
                stopSelf();
            }
        };

        cdt.start();
    }

    @Override
    public void onDestroy() {

        cdt.cancel();
        Log.i(TAG, "Timer cancelled");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}