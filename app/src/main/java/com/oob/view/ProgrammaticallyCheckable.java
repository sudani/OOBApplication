package com.oob.view;

public interface ProgrammaticallyCheckable {
  void setCheckedProgrammatically(boolean checked);
}