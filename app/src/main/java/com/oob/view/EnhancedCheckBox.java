package com.oob.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;

import java.io.FileNotFoundException;

public class EnhancedCheckBox extends CheckBox implements ProgrammaticallyCheckable{
  private CompoundButton.OnCheckedChangeListener mListener = null;
  public EnhancedCheckBox(Context context) throws FileNotFoundException {
    super(context);
  }

  public EnhancedCheckBox(Context context, AttributeSet attrs, int defStyle)
          throws FileNotFoundException {
    super(context, attrs, defStyle);
  }

  public EnhancedCheckBox(Context context, AttributeSet attrs) throws FileNotFoundException {
    super(context, attrs);
  }

  @Override
  public void setOnCheckedChangeListener(
          CompoundButton.OnCheckedChangeListener listener) {
    if (this.mListener == null) {this.mListener = listener;}
      super.setOnCheckedChangeListener(listener);
  }
  /**
   * Set the checked state of the checkbox programmatically. This is to differentiate it from a user click
   * @param checked Whether to check the checkbox
   */
  @Override
  public void setCheckedProgrammatically(boolean checked) {
    super.setOnCheckedChangeListener(null);
    super.setChecked(checked);
    super.setOnCheckedChangeListener(mListener);
  }
}