package com.oob.entity;


import android.content.Context;


/**
 * Created by Admin on 27-Jun-14.
 */
public class entCurtain {

    private Context CONTEXT;

    private int ROOM_Id;

    private int CURTAIN_Id;

    private String CURTAIN_STT;


    private String ROOM_NAME;

    private String CURTAIN_NAME;

    private String CURTAIN_LEFT;

    private String CURTAIN_RIGHT;

    private String CURTAIN_UP;

    private String CURTAIN_DOWN;

    private String CURTAIN_STOP;

    private String CURTAIN_IMAGE;
    private int  CURTAIN_POSITION;

    public int getCURTAIN_POSITION() {
        return CURTAIN_POSITION;
    }

    public void setCURTAIN_POSITION(int CURTAIN_POSITION) {
        this.CURTAIN_POSITION = CURTAIN_POSITION;
    }

    public String getCURTAIN_IMAGE() {
        return CURTAIN_IMAGE;
    }

    public void setCURTAIN_IMAGE(String CURTAIN_IMAGE) {
        this.CURTAIN_IMAGE = CURTAIN_IMAGE;
    }

    public int getROOM_Id() {
        return ROOM_Id;
    }

    public void setROOM_Id(int ROOM_Id) {
        this.ROOM_Id = ROOM_Id;
    }

    public int getCURTAIN_Id() {
        return CURTAIN_Id;
    }

    public void setCURTAIN_Id(int CURTAIN_Id) {
        this.CURTAIN_Id = CURTAIN_Id;
    }

    public String getCURTAIN_STT() {
        return CURTAIN_STT;
    }

    public void setCURTAIN_STT(String CURTAIN_STT) {
        this.CURTAIN_STT = CURTAIN_STT;
    }

    public String getROOM_NAME() {
        return ROOM_NAME;
    }

    public void setROOM_NAME(String ROOM_NAME) {
        this.ROOM_NAME = ROOM_NAME;
    }

    public String getCURTAIN_NAME() {
        return CURTAIN_NAME;
    }

    public void setCURTAIN_NAME(String CURTAIN_NAME) {
        this.CURTAIN_NAME = CURTAIN_NAME;
    }

    public String getCURTAIN_LEFT() {
        return CURTAIN_LEFT;
    }

    public void setCURTAIN_LEFT(String CURTAIN_LEFT) {
        this.CURTAIN_LEFT = CURTAIN_LEFT;
    }

    public String getCURTAIN_RIGHT() {
        return CURTAIN_RIGHT;
    }

    public void setCURTAIN_RIGHT(String CURTAIN_RIGHT) {
        this.CURTAIN_RIGHT = CURTAIN_RIGHT;
    }

    public String getCURTAIN_UP() {
        return CURTAIN_UP;
    }

    public void setCURTAIN_UP(String CURTAIN_UP) {
        this.CURTAIN_UP = CURTAIN_UP;
    }

    public String getCURTAIN_DOWN() {
        return CURTAIN_DOWN;
    }

    public void setCURTAIN_DOWN(String CURTAIN_DOWN) {
        this.CURTAIN_DOWN = CURTAIN_DOWN;
    }

    public String getCURTAIN_STOP() {
        return CURTAIN_STOP;
    }

    public void setCURTAIN_STOP(String CURTAIN_STOP) {
        this.CURTAIN_STOP = CURTAIN_STOP;
    }
}
