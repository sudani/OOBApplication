package com.oob.entity;


import android.content.Context;

import java.io.Serializable;


/**
 * Created by Admin on 27-Jun-14.
 */
public class entAC implements Serializable{

    private Context CONTEXT;

    private int ROOM_Id;

    private int AC_Id;

    private String AC_STT;

    private int AC_IS_SELECTED;

    private String ROOM_NAME;

    private String AC_NAME;

    private String AC_PROTOCOL;

    private String AC_LEFT;

    private String AC_RIGHT;

    private String AC_TEMP;

    private String AC_PLUS;

    private String AC_MINUS;

    private String AC_FAN;

    private String AC_MODE;

    private String AC_SWING;

    private String AC_TURBO;

    private String AC_SENSOR;

    public int getROOM_Id() {
        return ROOM_Id;
    }

    public void setROOM_Id(int ROOM_Id) {
        this.ROOM_Id = ROOM_Id;
    }

    public int getAC_Id() {
        return AC_Id;
    }

    public void setAC_Id(int AC_Id) {
        this.AC_Id = AC_Id;
    }

    public String getAC_STT() {
        return AC_STT;
    }

    public void setAC_STT(String AC_STT) {
        this.AC_STT = AC_STT;
    }

    public int getAC_IS_SELECTED() {
        return AC_IS_SELECTED;
    }

    public void setAC_IS_SELECTED(int TV_IS_SELECTED) {
        this.AC_IS_SELECTED = TV_IS_SELECTED;
    }

    public String getROOM_NAME() {
        return ROOM_NAME;
    }

    public void setROOM_NAME(String ROOM_NAME) {
        this.ROOM_NAME = ROOM_NAME;
    }

    public String getAC_NAME() {
        return AC_NAME;
    }

    public void setAC_NAME(String AC_NAME) {
        this.AC_NAME = AC_NAME;
    }

    public String getAC_PROTOCOL() {
        return AC_PROTOCOL;
    }

    public void setAC_PROTOCOL(String AC_PROTOCOL) {
        this.AC_PROTOCOL = AC_PROTOCOL;
    }

    public String getAC_LEFT() {
        return AC_LEFT;
    }

    public void setAC_LEFT(String AC_LEFT) {
        this.AC_LEFT = AC_LEFT;
    }

    public String getAC_RIGHT() {
        return AC_RIGHT;
    }

    public void setAC_RIGHT(String AC_RIGHT) {
        this.AC_RIGHT = AC_RIGHT;
    }

    public String getAC_TEMP() {
        return AC_TEMP;
    }

    public void setAC_TEMP(String AC_TEMP) {
        this.AC_TEMP = AC_TEMP;
    }

    public String getAC_PLUS() {
        return AC_PLUS;
    }

    public void setAC_PLUS(String AC_PLUS) {
        this.AC_PLUS = AC_PLUS;
    }

    public String getAC_MINUS() {
        return AC_MINUS;
    }

    public void setAC_MINUS(String AC_MINUS) {
        this.AC_MINUS = AC_MINUS;
    }

    public String getAC_FAN() {
        return AC_FAN;
    }

    public void setAC_FAN(String AC_FAN) {
        this.AC_FAN = AC_FAN;
    }

    public String getAC_MODE() {
        return AC_MODE;
    }

    public void setAC_MODE(String AC_MODE) {
        this.AC_MODE = AC_MODE;
    }

    public String getAC_SWING() {
        return AC_SWING;
    }

    public void setAC_SWING(String AC_SWING) {
        this.AC_SWING = AC_SWING;
    }

    public String getAC_TURBO() {
        return AC_TURBO;
    }

    public void setAC_TURBO(String AC_TURBO) {
        this.AC_TURBO = AC_TURBO;
    }

    public String getAC_SENSOR() {
        return AC_SENSOR;
    }

    public void setAC_SENSOR(String AC_SENSOR) {
        this.AC_SENSOR = AC_SENSOR;
    }
}
