package com.oob.entity;


public class entChannelModel {

    private int CHANNEL_ID;
    private String CHANNEL_NAME;
    private String CHANNEL_IMAGE;
    private int GTPL_CODE;
    private int AIRTEL_CODE;
    private int VIDEOCON_CODE;
    private int TATASKY_CODE;
    private int DISTV_CODE;
    private int RELIANCE_CODE;
    private int JIO_CODE;

    public int getCHANNEL_ID() {
        return CHANNEL_ID;
    }

    public void setCHANNEL_ID(int CHANNEL_ID) {
        this.CHANNEL_ID = CHANNEL_ID;
    }

    public String getCHANNEL_NAME() {
        return CHANNEL_NAME;
    }

    public void setCHANNEL_NAME(String CHANNEL_NAME) {
        this.CHANNEL_NAME = CHANNEL_NAME;
    }

    public String getCHANNEL_IMAGE() {
        return CHANNEL_IMAGE;
    }

    public void setCHANNEL_IMAGE(String CHANNEL_IMAGE) {
        this.CHANNEL_IMAGE = CHANNEL_IMAGE;
    }

    public int getGTPL_CODE() {
        return GTPL_CODE;
    }

    public void setGTPL_CODE(int GTPL_CODE) {
        this.GTPL_CODE = GTPL_CODE;
    }

    public int getAIRTEL_CODE() {
        return AIRTEL_CODE;
    }

    public void setAIRTEL_CODE(int AIRTEL_CODE) {
        this.AIRTEL_CODE = AIRTEL_CODE;
    }

    public int getVIDEOCON_CODE() {
        return VIDEOCON_CODE;
    }

    public void setVIDEOCON_CODE(int VIDEOCON_CODE) {
        this.VIDEOCON_CODE = VIDEOCON_CODE;
    }

    public int getTATASKY_CODE() {
        return TATASKY_CODE;
    }

    public void setTATASKY_CODE(int TATASKY_CODE) {
        this.TATASKY_CODE = TATASKY_CODE;
    }

    public int getDISTV_CODE() {
        return DISTV_CODE;
    }

    public void setDISTV_CODE(int DISTV_CODE) {
        this.DISTV_CODE = DISTV_CODE;
    }

    public int getRELIANCE_CODE() {
        return RELIANCE_CODE;
    }

    public void setRELIANCE_CODE(int RELIANCE_CODE) {
        this.RELIANCE_CODE = RELIANCE_CODE;
    }

    public int getJIO_CODE() {
        return JIO_CODE;
    }

    public void setJIO_CODE(int JIO_CODE) {
        this.JIO_CODE = JIO_CODE;
    }
}
