package com.oob.entity;


import android.database.Cursor;

/**
 * Created by Admin on 27-Jun-14.
 */
public class entGroupAppliances {

    private int lgroupId;
    private int status;
    private int isDelete;
    private String name;
    private String image;
    private String deviceIds;


    public int getLgroupId() {
        return lgroupId;
    }

    public void setLgroupId(int lgroupId) {
        this.lgroupId = lgroupId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDeviceIds() {
        return deviceIds;
    }

    public void setDeviceIds(String deviceIds) {
        this.deviceIds = deviceIds;
    }
}
