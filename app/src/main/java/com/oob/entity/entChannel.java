package com.oob.entity;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class entChannel {
    private  int channelId;
    private String img;
    private String name;
    private String number;
    private String channelGroup;

    public entChannel(String img, String name) {
        this.img = img;
        this.name = name;
    }

    public entChannel() {

    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelGroup() {
        return channelGroup;
    }

    public void setChannelGroup(String channelGroup) {
        this.channelGroup = channelGroup;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
