package com.oob.entity;


import android.content.Context;


/**
 * Created by Admin on 27-Jun-14.
 */
public class entDashboard{

    private Context CONTEXT;
    private int ITEM_PID;
    private String ITEM_NAME;
    private String ITEM_IMAGE;
    private int ITEM_POSITION;
    public int getITEM_POSITION() {
        return ITEM_POSITION;
    }

    public void setITEM_POSITION(int ITEM_POSITION) {
        this.ITEM_POSITION = ITEM_POSITION;
    }

    public int getITEM_PID() {
        return ITEM_PID;
    }

    public void setITEM_PID(int ITEM_PID) {
        this.ITEM_PID = ITEM_PID;
    }

    public String getITEM_NAME() {
        return ITEM_NAME;
    }

    public void setITEM_NAME(String ITEM_NAME) {
        this.ITEM_NAME = ITEM_NAME;
    }

    public String getITEM_IMAGE() {
        return ITEM_IMAGE;
    }

    public void setITEM_IMAGE(String ITEM_IMAGE) {
        this.ITEM_IMAGE = ITEM_IMAGE;
    }



}
