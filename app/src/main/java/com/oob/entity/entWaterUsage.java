package com.oob.entity;


import android.content.Context;


/**
 * Created by Admin on 27-Jun-14.
 */
public class entWaterUsage {

    private int waterId;
    private String faltName;
    private String waterUsage;
    private String backupDate;

    public int getWaterId() {
        return waterId;
    }

    public void setWaterId(int waterId) {
        this.waterId = waterId;
    }

    public String getFaltName() {
        return faltName;
    }

    public void setFaltName(String faltName) {
        this.faltName = faltName;
    }

    public String getWaterUsage() {
        return waterUsage;
    }

    public void setWaterUsage(String waterUsage) {
        this.waterUsage = waterUsage;
    }

    public String getBackupDate() {
        return backupDate;
    }

    public void setBackupDate(String backupDate) {
        this.backupDate = backupDate;
    }
}
