package com.oob.entity;


import android.database.Cursor;

public class entSecurity {

    private int SECURITY_ID;
    private int DEVICE_ID;
    private String SENSOR_NAME;
    private String SENSOR_IMAGE;
    private String SENSOR_LOCATION;
    private String SENSOR_LIGHT;
    private String ACTION_NAME;
    private String ACTION_VALUE;
    private String ROOM_CODE;
    private String MODULE_CODE;
    private String SENSOR_NUMBER;
    private int IS_ENAMBLE;


    public String getSENSOR_NUMBER() {
        return SENSOR_NUMBER;
    }

    public void setSENSOR_NUMBER(String SENSOR_NUMBER) {
        this.SENSOR_NUMBER = SENSOR_NUMBER;
    }

    public String getROOM_CODE() {
        return ROOM_CODE;
    }

    public void setROOM_CODE(String ROOM_CODE) {
        this.ROOM_CODE = ROOM_CODE;
    }

    public String getMODULE_CODE() {
        return MODULE_CODE;
    }

    public void setMODULE_CODE(String MODULE_CODE) {
        this.MODULE_CODE = MODULE_CODE;
    }

    public String getACTION_NAME() {
        return ACTION_NAME;
    }

    public void setACTION_NAME(String ACTION_NAME) {
        this.ACTION_NAME = ACTION_NAME;
    }

    public String getACTION_VALUE() {
        return ACTION_VALUE;
    }

    public void setACTION_VALUE(String ACTION_VALUE) {
        this.ACTION_VALUE = ACTION_VALUE;
    }

    public int getDEVICE_ID() {
        return DEVICE_ID;
    }

    public void setDEVICE_ID(int DEVICE_ID) {
        this.DEVICE_ID = DEVICE_ID;
    }

    public int getSECURITY_ID() {
        return SECURITY_ID;
    }

    public void setSECURITY_ID(int SECURITY_ID) {
        this.SECURITY_ID = SECURITY_ID;
    }

    public String getSENSOR_NAME() {
        return SENSOR_NAME;
    }

    public void setSENSOR_NAME(String SENSOR_NAME) {
        this.SENSOR_NAME = SENSOR_NAME;
    }

    public String getSENSOR_IMAGE() {
        return SENSOR_IMAGE;
    }

    public void setSENSOR_IMAGE(String SENSOR_IMAGE) {
        this.SENSOR_IMAGE = SENSOR_IMAGE;
    }

    public String getSENSOR_LOCATION() {
        return SENSOR_LOCATION;
    }

    public void setSENSOR_LOCATION(String SENSOR_LOCATION) {
        this.SENSOR_LOCATION = SENSOR_LOCATION;
    }

    public String getSENSOR_LIGHT() {
        return SENSOR_LIGHT;
    }

    public void setSENSOR_LIGHT(String SENSOR_LIGHT) {
        this.SENSOR_LIGHT = SENSOR_LIGHT;
    }

    public int getIS_ENAMBLE() {
        return IS_ENAMBLE;
    }

    public void setIS_ENAMBLE(int IS_ENAMBLE) {
        this.IS_ENAMBLE = IS_ENAMBLE;
    }
}
