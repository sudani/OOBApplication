package com.oob.entity;


import android.content.Context;

import org.apache.http.entity.SerializableEntity;

import java.io.Serializable;


public class entDevice implements Serializable{

    private Context CONTEXT;

    private int DEVICE_Id;
    private long ID;

    private int ROOM_Id;

    private String DEVICE_NAME;
    private String ROOM_NAME;

    private int STT;

    private int DIM;

    private String TIME_ONE_A;

    private String TIME_ONE_B;

    private String TIME_TWO_B;

    private String TIME_TWO_A;

    private String DAYS;

    private int CL;

    private int RGBSTT;

    private int ENGRD;

    private int REDCLR;

    private int GREENCLR;

    private int BLUECLR;

    private int RGB;

    private int ALL_STT;

    private int MIC;

    private int GROUP;

    private int DEVICE_COUNT;

    private String MODULE;

    private boolean IS_EXPAND;

    private int POSITION;
    private int MODULE_STT;
    private int BACKLIGHT_RGB_STT;
    private int BACKLIGHT_DIM_STT;
    private int MODULE_NUMBER;
    private int IS_DELETE;
    private int isSelected;

    public int getMODULE_STT() {
        return MODULE_STT;
    }

    public void setMODULE_STT(int MODULE_STT) {
        this.MODULE_STT = MODULE_STT;
    }

    public String getROOM_NAME() {
        return ROOM_NAME;
    }

    public void setROOM_NAME(String ROOM_NAME) {
        this.ROOM_NAME = ROOM_NAME;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public int getPOSITION() {
        return POSITION;
    }

    public void setPOSITION(int POSITION) {
        this.POSITION = POSITION;
    }

    public int getGROUP_DIM() {
        return GROUP_DIM;
    }

    public void setGROUP_DIM(int GROUP_DIM) {
        this.GROUP_DIM = GROUP_DIM;
    }

    private int GROUP_DIM;

    public int getMODULE_NUMBER() {
        return MODULE_NUMBER;
    }

    public void setMODULE_NUMBER(int MODULE_NUMBER) {
        this.MODULE_NUMBER = MODULE_NUMBER;
    }

    public boolean IS_EXPAND() {
        return IS_EXPAND;
    }

    public void setIS_EXPAND(boolean IS_EXPAND) {
        this.IS_EXPAND = IS_EXPAND;
    }

    public String getMODULE() {
        return MODULE;
    }

    public void setMODULE(String MODULE) {
        this.MODULE = MODULE;
    }

    public int getDEVICE_COUNT() {
        return DEVICE_COUNT;
    }

    public void setDEVICE_COUNT(int DEVICE_COUNT) {
        this.DEVICE_COUNT = DEVICE_COUNT;
    }

    public int getDIM() {
        return DIM;
    }

    public void setDIM(int DIM) {
        this.DIM = DIM;
    }

    public String getTIME_TWO_A() {
        return TIME_TWO_A;
    }

    public void setTIME_TWO_A(String TIME_TWO_A) {
        this.TIME_TWO_A = TIME_TWO_A;
    }

    public int getROOM_Id() {
        return ROOM_Id;
    }

    public void setROOM_Id(int ROOM_Id) {
        this.ROOM_Id = ROOM_Id;
    }

    public int getGROUP() {
        return GROUP;
    }

    public void setGROUP(int GROUP) {
        this.GROUP = GROUP;
    }

    public int getDEVICE_Id() {
        return DEVICE_Id;
    }

    public void setDEVICE_Id(int DEVICE_Id) {
        this.DEVICE_Id = DEVICE_Id;
    }

    public String getDEVICE_NAME() {
        return DEVICE_NAME;
    }

    public void setDEVICE_NAME(String DEVICE_NAME) {
        this.DEVICE_NAME = DEVICE_NAME;
    }

    public int getSTT() {
        return STT;
    }

    public void setSTT(int STT) {
        this.STT = STT;
    }

    public String getTIME_ONE_A() {
        return TIME_ONE_A;
    }

    public void setTIME_ONE_A(String TIME_ONE_A) {
        this.TIME_ONE_A = TIME_ONE_A;
    }

    public String getTIME_ONE_B() {
        return TIME_ONE_B;
    }

    public void setTIME_ONE_B(String TIME_ONE_B) {
        this.TIME_ONE_B = TIME_ONE_B;
    }

    public String getTIME_TWO_B() {
        return TIME_TWO_B;
    }

    public void setTIME_TWO_B(String TIME_TWO_B) {
        this.TIME_TWO_B = TIME_TWO_B;
    }

    public String getDAYS() {
        return DAYS;
    }

    public void setDAYS(String DAYS) {
        this.DAYS = DAYS;
    }

    public int getCL() {
        return CL;
    }

    public void setCL(int CL) {
        this.CL = CL;
    }

    public int getRGBSTT() {
        return RGBSTT;
    }

    public void setRGBSTT(int RGBSTT) {
        this.RGBSTT = RGBSTT;
    }

    public int getENGRD() {
        return ENGRD;
    }

    public void setENGRD(int ENGRD) {
        this.ENGRD = ENGRD;
    }


    public int getREDCLR() {
        return REDCLR;
    }

    public void setREDCLR(int REDCLR) {
        this.REDCLR = REDCLR;
    }

    public int getGREENCLR() {
        return GREENCLR;
    }

    public void setGREENCLR(int GREENCLR) {
        this.GREENCLR = GREENCLR;
    }

    public int getBLUECLR() {
        return BLUECLR;
    }

    public void setBLUECLR(int BLUECLR) {
        this.BLUECLR = BLUECLR;
    }


    public int getRGB() {
        return RGB;
    }

    public void setRGB(int RGB) {
        this.RGB = RGB;
    }

    public int getALL_STT() {
        return ALL_STT;
    }

    public void setALL_STT(int ALL_STT) {
        this.ALL_STT = ALL_STT;
    }

    public int getMIC() {
        return MIC;
    }

    public void setMIC(int MIC) {
        this.MIC = MIC;
    }

    public int getBACKLIGHT_RGB_STT() {
        return BACKLIGHT_RGB_STT;
    }

    public void setBACKLIGHT_RGB_STT(int BACKLIGHT_RGB_STT) {
        this.BACKLIGHT_RGB_STT = BACKLIGHT_RGB_STT;
    }

    public int getBACKLIGHT_DIM_STT() {
        return BACKLIGHT_DIM_STT;
    }

    public void setBACKLIGHT_DIM_STT(int BACKLIGHT_DIM_STT) {
        this.BACKLIGHT_DIM_STT = BACKLIGHT_DIM_STT;
    }

    public int getIS_DELETE() {
        return IS_DELETE;
    }

    public void setIS_DELETE(int IS_DELETE) {
        this.IS_DELETE = IS_DELETE;
    }

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }
}
