package com.oob.entity;


import android.content.Context;

import java.io.Serializable;


/**
 * Created by Admin on 27-Jun-14.
 */
public class entTV implements Serializable{

    private Context CONTEXT;
    private int ROOM_Id;
    private int TV_Id;
    private String TV_STT;
    private int TV_IS_SELECTED;
    private String ROOM_NAME;
    private String TV_NAME;
    private String TV_MODEL;
    private String TV_PROTOCOL;
    private String SET_NAME;
    private String SET_PROTOCOL;
    private String SET_STT;
    private String MUTE;
    private String TV_UP;
    private String TV_DOWN;
    private String TV_LEFT;
    private String TV_RIGHT;
    private String TV_OK;
    private String TV_BACK;
    private String TV_EXIT;
    private String TV_VOL_PLUS;
    private String TV_VOL_MINUS;
    private String TV_CH_PLUS;
    private String TV_CH_MINUS;
    private String TV_ONE;
    private String TV_TWO;
    private String TV_THREE;
    private String TV_FOUR;
    private String TV_FIVE;
    private String TV_SIX;
    private String TV_SEVEN;
    private String TV_EIGHT;
    private String TV_NINE;
    private String TV_ZERO;
    private String TV_STAR;
    private String TV_HAS;

    public String getSET_STT() {
        return SET_STT;
    }

    public void setSET_STT(String SET_STT) {
        this.SET_STT = SET_STT;
    }

    public String getMUTE() {
        return MUTE;
    }

    public void setMUTE(String MUTE) {
        this.MUTE = MUTE;
    }

    public String getTV_UP() {
        return TV_UP;
    }

    public void setTV_UP(String TV_UP) {
        this.TV_UP = TV_UP;
    }

    public String getTV_DOWN() {
        return TV_DOWN;
    }

    public void setTV_DOWN(String TV_DOWN) {
        this.TV_DOWN = TV_DOWN;
    }

    public String getTV_LEFT() {
        return TV_LEFT;
    }

    public void setTV_LEFT(String TV_LEFT) {
        this.TV_LEFT = TV_LEFT;
    }

    public String getTV_RIGHT() {
        return TV_RIGHT;
    }

    public void setTV_RIGHT(String TV_RIGHT) {
        this.TV_RIGHT = TV_RIGHT;
    }

    public String getTV_OK() {
        return TV_OK;
    }

    public void setTV_OK(String TV_OK) {
        this.TV_OK = TV_OK;
    }

    public String getTV_BACK() {
        return TV_BACK;
    }

    public void setTV_BACK(String TV_BACK) {
        this.TV_BACK = TV_BACK;
    }

    public String getTV_EXIT() {
        return TV_EXIT;
    }

    public void setTV_EXIT(String TV_EXIT) {
        this.TV_EXIT = TV_EXIT;
    }

    public String getTV_VOL_PLUS() {
        return TV_VOL_PLUS;
    }

    public void setTV_VOL_PLUS(String TV_VOL_PLUS) {
        this.TV_VOL_PLUS = TV_VOL_PLUS;
    }

    public String getTV_VOL_MINUS() {
        return TV_VOL_MINUS;
    }

    public void setTV_VOL_MINUS(String TV_VOL_MINUS) {
        this.TV_VOL_MINUS = TV_VOL_MINUS;
    }

    public String getTV_CH_PLUS() {
        return TV_CH_PLUS;
    }

    public void setTV_CH_PLUS(String TV_CH_PLUS) {
        this.TV_CH_PLUS = TV_CH_PLUS;
    }

    public String getTV_CH_MINUS() {
        return TV_CH_MINUS;
    }

    public void setTV_CH_MINUS(String TV_CH_MINUS) {
        this.TV_CH_MINUS = TV_CH_MINUS;
    }

    public String getTV_ONE() {
        return TV_ONE;
    }

    public void setTV_ONE(String TV_ONE) {
        this.TV_ONE = TV_ONE;
    }

    public String getTV_TWO() {
        return TV_TWO;
    }

    public void setTV_TWO(String TV_TWO) {
        this.TV_TWO = TV_TWO;
    }

    public String getTV_THREE() {
        return TV_THREE;
    }

    public void setTV_THREE(String TV_THREE) {
        this.TV_THREE = TV_THREE;
    }

    public String getTV_FOUR() {
        return TV_FOUR;
    }

    public void setTV_FOUR(String TV_FOUR) {
        this.TV_FOUR = TV_FOUR;
    }

    public String getTV_FIVE() {
        return TV_FIVE;
    }

    public void setTV_FIVE(String TV_FIVE) {
        this.TV_FIVE = TV_FIVE;
    }

    public String getTV_SIX() {
        return TV_SIX;
    }

    public void setTV_SIX(String TV_SIX) {
        this.TV_SIX = TV_SIX;
    }

    public String getTV_SEVEN() {
        return TV_SEVEN;
    }

    public void setTV_SEVEN(String TV_SEVEN) {
        this.TV_SEVEN = TV_SEVEN;
    }

    public String getTV_EIGHT() {
        return TV_EIGHT;
    }

    public void setTV_EIGHT(String TV_EIGHT) {
        this.TV_EIGHT = TV_EIGHT;
    }

    public String getTV_NINE() {
        return TV_NINE;
    }

    public void setTV_NINE(String TV_NINE) {
        this.TV_NINE = TV_NINE;
    }

    public String getTV_ZERO() {
        return TV_ZERO;
    }

    public void setTV_ZERO(String TV_ZERO) {
        this.TV_ZERO = TV_ZERO;
    }

    public String getTV_STAR() {
        return TV_STAR;
    }

    public void setTV_STAR(String TV_STAR) {
        this.TV_STAR = TV_STAR;
    }

    public String getTV_HAS() {
        return TV_HAS;
    }

    public void setTV_HAS(String TV_HAS) {
        this.TV_HAS = TV_HAS;
    }

    public int getTV_IS_SELECTED() {
        return TV_IS_SELECTED;
    }

    public void setTV_IS_SELECTED(int TV_IS_SELECTED) {
        this.TV_IS_SELECTED = TV_IS_SELECTED;
    }

    public int getROOM_Id() {
        return ROOM_Id;
    }

    public void setROOM_Id(int ROOM_Id) {
        this.ROOM_Id = ROOM_Id;
    }

    public int getTV_Id() {
        return TV_Id;
    }

    public void setTV_Id(int TV_Id) {
        this.TV_Id = TV_Id;
    }

    public String getTV_STT() {
        return TV_STT;
    }

    public void setTV_STT(String TV_STT) {
        this.TV_STT = TV_STT;
    }

    public String getROOM_NAME() {
        return ROOM_NAME;
    }

    public void setROOM_NAME(String ROOM_NAME) {
        this.ROOM_NAME = ROOM_NAME;
    }

    public String getTV_NAME() {
        return TV_NAME;
    }

    public void setTV_NAME(String TV_NAME) {
        this.TV_NAME = TV_NAME;
    }

    public String getTV_MODEL() {
        return TV_MODEL;
    }

    public void setTV_MODEL(String TV_MODEL) {
        this.TV_MODEL = TV_MODEL;
    }

    public String getTV_PROTOCOL() {
        return TV_PROTOCOL;
    }

    public void setTV_PROTOCOL(String TV_PROTOCOL) {
        this.TV_PROTOCOL = TV_PROTOCOL;
    }

    public String getSET_NAME() {
        return SET_NAME;
    }

    public void setSET_NAME(String SET_NAME) {
        this.SET_NAME = SET_NAME;
    }

    public String getSET_PROTOCOL() {
        return SET_PROTOCOL;
    }

    public void setSET_PROTOCOL(String SET_PROTOCOL) {
        this.SET_PROTOCOL = SET_PROTOCOL;
    }
}
