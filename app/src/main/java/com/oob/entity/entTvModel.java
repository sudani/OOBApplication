package com.oob.entity;


public class entTvModel {

    private int MODEL_ID;
    private String BRAND_NAME;
    private String BRAND_MODEL;
    private String POWER_DATA;

    public int getMODEL_ID() {
        return MODEL_ID;
    }

    public void setMODEL_ID(int MODEL_ID) {
        this.MODEL_ID = MODEL_ID;
    }

    public String getBRAND_NAME() {
        return BRAND_NAME;
    }

    public void setBRAND_NAME(String BRAND_NAME) {
        this.BRAND_NAME = BRAND_NAME;
    }

    public String getBRAND_MODEL() {
        return BRAND_MODEL;
    }

    public void setBRAND_MODEL(String BRAND_MODEL) {
        this.BRAND_MODEL = BRAND_MODEL;
    }

    public String getPOWER_DATA() {
        return POWER_DATA;
    }

    public void setPOWER_DATA(String POWER_DATA) {
        this.POWER_DATA = POWER_DATA;
    }
}
