package com.oob.entity;


import android.content.Context;


/**
 * Created by Admin on 27-Jun-14.
 */
public class entRooms {

    private Context CONTEXT;
    private int ROOM_Id;
    private String ROOM_NAME;
    private int ROOM_IS_SELECTED;
    private int ROOM_ALL_SIT;
    private int MODULE_COUNT;
    private int ACTIVE;
    private int ROOM_GRP_SIT;
    private int GROUP_COLOR;
    private int GROUP_BUZZ;
    int MODULE;
    private String PROFILE_DATA;
    private boolean isSelected;

    public int getMODULE() {
        return MODULE;
    }

    public void setMODULE(int MODULE) {
        this.MODULE = MODULE;
    }

    public int getACTIVE() {
        return ACTIVE;
    }

    public void setACTIVE(int ACTIVE) {
        this.ACTIVE = ACTIVE;
    }

    public String getPROFILE_DATA() {
        return PROFILE_DATA;
    }

    public void setPROFILE_DATA(String PROFILE_DATA) {
        this.PROFILE_DATA = PROFILE_DATA;
    }

    public int getMODULE_COUNT() {
        return MODULE_COUNT;
    }

    public void setMODULE_COUNT(int MODULE_COUNT) {
        this.MODULE_COUNT = MODULE_COUNT;
    }

    public int getROOM_GRP_SIT() {
        return ROOM_GRP_SIT;
    }

    public void setROOM_GRP_SIT(int ROOM_GRP_SIT) {
        this.ROOM_GRP_SIT = ROOM_GRP_SIT;
    }

    public int getROOM_Id() {
        return ROOM_Id;
    }

    public void setROOM_Id(int ROOM_Id) {
        this.ROOM_Id = ROOM_Id;
    }

    public String getROOM_NAME() {
        return ROOM_NAME;
    }

    public void setROOM_NAME(String ROOM_NAME) {
        this.ROOM_NAME = ROOM_NAME;
    }

    public int getROOM_IS_SELECTED() {
        return ROOM_IS_SELECTED;
    }

    public void setROOM_IS_SELECTED(int ROOM_IS_SELECTED) {
        this.ROOM_IS_SELECTED = ROOM_IS_SELECTED;
    }

    public int getROOM_ALL_SIT() {
        return ROOM_ALL_SIT;
    }

    public void setROOM_ALL_SIT(int ROOM_ALL_SIT) {
        this.ROOM_ALL_SIT = ROOM_ALL_SIT;
    }


    public int getGROUP_BUZZ() {
        return GROUP_BUZZ;
    }

    public void setGROUP_BUZZ(int GROUP_BUZZ) {
        this.GROUP_BUZZ = GROUP_BUZZ;
    }

    public int getGROUP_COLOR() {
        return GROUP_COLOR;
    }

    public void setGROUP_COLOR(int GROUP_COLOR) {
        this.GROUP_COLOR = GROUP_COLOR;
    }

}
