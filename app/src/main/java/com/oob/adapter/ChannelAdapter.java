package com.oob.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.oob.app.R;
import com.oob.entity.entChannel;
import com.oob.tools.Constants;

import java.util.List;

public class ChannelAdapter extends BaseQuickAdapter<entChannel,BaseViewHolder> {
Context mContext;
    public ChannelAdapter(Context context, int layoutResId, List data) {
        super(layoutResId, data);
        mContext=context;
    }

//    @Override
//    protected void startAnim(Animator anim, int index) {
//        super.startAnim(anim, index);
//        if (index < 5) {
//            anim.setStartDelay(index * 150);
//        }
//    }

    @Override
    protected void convert(BaseViewHolder helper, entChannel item) {
        helper.setText(R.id.tvName, item.getName());
        helper.setImageResource(R.id.ivLogo, Constants.getImage(mContext,item.getImg()));

    }
}
