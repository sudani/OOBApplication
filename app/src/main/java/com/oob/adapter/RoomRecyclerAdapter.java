package com.oob.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;
import com.oob.SocketServerTask;
import com.oob.activity.RoomActivity;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.entity.entRooms;
import com.oob.tools.Constants;
import com.oob.tools.HostUtils;
import com.oob.tools.Preferences;
import com.oob.tools.WifiUtils;
import com.oob.view.EditText;
import com.oob.view.EnhancedCheckBox;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class RoomRecyclerAdapter extends RecyclerView.Adapter<RoomRecyclerAdapter.RoomViewHolder>
        implements TimePickerDialog.OnTimeSetListener, ColorPickerDialogListener {

    private List<entDevice> roomItemList = new ArrayList<>();

    private Context mContext;

    private Date minDate, maxDate;

    private DateFormat df;

    //    private RoomDevice roomDevice;
    private Activity activity;

    private int position = -1;

    private int picker = 0;

    private boolean isFromGroup = false, isFromAllOnOff = false;

    dbHelperOperations db;

    //    private int colorCount = 0;
//
//    private ImageView imageView;
    private boolean isMultiSelect = false;

    ArrayList<String> colors = new ArrayList<>();

    SocketServerTask async;

    int fromPosition;

    int toPosition;

    private SocketServerTask task;

    private int selectedRoomId;

    private entRooms entRoom;

    public RoomRecyclerAdapter(Context context, List<entDevice> roomDevice, Activity activity,
                               int selectedRoomId) {
//        this.roomDevice = roomDevice;
        this.roomItemList = roomDevice;
        this.mContext = context;
        this.activity = activity;
        this.selectedRoomId = selectedRoomId;
        db = new dbHelperOperations(mContext);
        entRoom = db.getRoomFromId(selectedRoomId);
        colors = new ArrayList<>(
                Arrays.asList(mContext.getResources().getStringArray(R.array.light_colors)));

        df = new SimpleDateFormat("HH:mm:ss");
        async = new SocketServerTask(mContext, null);
        try {
            minDate = df.parse("00:00:00");
            maxDate = df.parse("23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        task = new SocketServerTask(mContext, null);
    }


    @Override
    public RoomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_frag_room, viewGroup, false);
        return new RoomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RoomViewHolder customViewHolder, final int i) {

//        final entDevice bean = roomItemList.get(i);

//        int screen_height= PixelUtil.getDisplayHeight(mContext)-PixelUtil.getActionbarHeight(mContext)-PixelUtil.getStatusbarHeight(mContext);
//        customViewHolder.ll_main.getLayoutParams().height=screen_height/4;
//        int pos = i + 1;
//        roomItemList.get(i).setPOSITION(pos);
        if (isMultiSelect) {
            customViewHolder.cbSelect.setVisibility(View.VISIBLE);
            customViewHolder.cbSelect
                    .setCheckedProgrammatically(roomItemList.get(i).getGROUP() == 1);

            customViewHolder.cbSelect.setOnCheckedChangeListener(
                    new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            if (roomItemList.get(i).getCL() == 0) {
                                roomItemList.get(i).setGROUP(isChecked ? 1 : 0);
                                roomItemList.get(i).setSTT(isChecked ? 0 : 1);
                                roomItemList.get(i).setALL_STT(roomItemList.get(i).getSTT());
                                customViewHolder.ivDevice.callOnClick();
                            } else {
                                customViewHolder.cbSelect.setCheckedProgrammatically(!isChecked);
                            }
                        }


                    });

            if (roomItemList.get(i).getGROUP() == 1 && roomItemList.get(i).getDEVICE_NAME()
                    .toLowerCase().contains("fan")) {
                customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getGROUP_DIM());
                customViewHolder.tvSeekValue
                        .setText(String.valueOf(roomItemList.get(i).getGROUP_DIM()));

            } else {
                if (roomItemList.get(i).getDEVICE_NAME()
                        .toLowerCase().contains("fan")) {
                    customViewHolder.tvSeekValue
                            .setText(String.valueOf(roomItemList.get(i).getDIM()));
                    customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM());
                } else if (roomItemList.get(i).getDEVICE_NAME()
                        .toLowerCase().contains("ac")) {
                    customViewHolder.tvSeekValue
                            .setText(String.valueOf(roomItemList.get(i).getDIM() + " \u2103"));
                    customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM() - 16);
                } else {
//                    System.out.println("DEVICE " + roomItemList.get(i).getDEVICE_NAME() + " >> A");
                    customViewHolder.tvSeekValue
                            .setText(String.valueOf(roomItemList.get(i).getDIM() + " %"));
                    customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM());
                }
            }

        } else {
            customViewHolder.cbSelect.setVisibility(View.GONE);

            if (roomItemList.get(i).getDEVICE_NAME()
                    .toLowerCase().contains("fan")) {
                customViewHolder.tvSeekValue
                        .setText(String.valueOf(roomItemList.get(i).getDIM()));
                customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM());
            } else if (roomItemList.get(i).getDEVICE_NAME()
                    .toLowerCase().contains("ac")) {
                customViewHolder.tvSeekValue
                        .setText(String.valueOf(roomItemList.get(i).getDIM() + " \u2103"));
                customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM() - 16);
            } else {
//                System.out.println("DEVICE " + roomItemList.get(i).getDEVICE_NAME() + " >> B");
                customViewHolder.tvSeekValue
                        .setText(String.valueOf(roomItemList.get(i).getDIM() + " %"));
                customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM());
            }
//            System.out.println("DEVICE " + roomItemList.get(i).getDEVICE_NAME() + " >> TEXTE >> "
//                    + customViewHolder.tvSeekValue.getText());


        }

        customViewHolder.tvName.setText(
                roomItemList.get(i).getDEVICE_NAME().toUpperCase());
        customViewHolder.ivIcon.setImageResource(
                Constants.getImageForDevice(mContext,
                        roomItemList.get(i).getDEVICE_NAME().toLowerCase()));
        customViewHolder.ivIcon
                .setColorFilter(Color.parseColor(colors.get(roomItemList.get(i).getRGBSTT())),
                        android.graphics.PorterDuff.Mode.MULTIPLY);
        if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
            customViewHolder.seekDimmer.setMax(5);
        } else if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("ac")) {
            customViewHolder.seekDimmer.setMax(16);
        } else {
            customViewHolder.seekDimmer.setMax(100);
        }

        customViewHolder.ivIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customViewHolder.ivIcon.setTag(position);
                if (roomItemList.get(i).getRGBSTT() < 7) {
                    roomItemList.get(i).setRGBSTT(roomItemList.get(i).getRGBSTT() + 1);
                } else {
                    roomItemList.get(i).setRGBSTT(0);

                }
                customViewHolder.ivIcon
                        .setColorFilter(
                                Color.parseColor(colors.get(roomItemList.get(i).getRGBSTT())),
                                android.graphics.PorterDuff.Mode.MULTIPLY);
                // TODO: 4/30/2016  UPDATE DATABSE
                db.updateDevice(roomItemList.get(i));
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(roomItemList.get(i).getROOM_Id())
                        + Constants
                        .getModuleAndDevicecode(roomItemList.get(i).getMODULE()) + "XXXXXRX"
                        //XXXXXRX
                        + roomItemList.get(i).getRGBSTT());

            }
        });
        customViewHolder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNameDialog((com.oob.view.TextView) v, customViewHolder.ivIcon, i);
            }
        });
        customViewHolder.ivColor
                .setColorFilter(Color.rgb(roomItemList.get(i).getREDCLR(),
                        roomItemList.get(i).getGREENCLR(), roomItemList.get(i).getBLUECLR()),
                        android.graphics.PorterDuff.Mode.MULTIPLY);
        customViewHolder.ivColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openColorDialog(i, v);
            }
        });
        if (roomItemList.get(i).getMIC() == 0) {
            customViewHolder.ivBuzer.setImageResource(R.drawable.mute);
        } else {
            customViewHolder.ivBuzer.setImageResource(R.drawable.buzzer);

        }
        customViewHolder.ivBuzer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (roomItemList.get(i).getMIC() == 1) {
                    roomItemList.get(i).setMIC(0);
                    customViewHolder.ivBuzer.setImageResource(R.drawable.mute);
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                            .getRoomcode(roomItemList.get(i).getROOM_Id()) + Constants
                            .getModuleAndDevicecode(roomItemList.get(i).getMODULE()) + "BUZZ"
                            + roomItemList.get(i).getMIC());

                } else {
                    roomItemList.get(i).setMIC(1);
                    customViewHolder.ivBuzer.setImageResource(R.drawable.buzzer);
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                            .getRoomcode(roomItemList.get(i).getROOM_Id()) + Constants
                            .getModuleAndDevicecode(roomItemList.get(i).getMODULE()) + "BUZZ"
                            + roomItemList.get(i).getMIC());

                }
                db.updateDevice(roomItemList.get(i));
                // TODO: 4/30/2016  UDATE DATABSE
            }
        });

        customViewHolder.tv_fromTimer1.setText(roomItemList.get(i).getTIME_ONE_A());
        customViewHolder.tv_toTimer1.setText(roomItemList.get(i).getTIME_ONE_B());
        customViewHolder.tv_fromTimer2.setText(roomItemList.get(i).getTIME_TWO_A());
        customViewHolder.tv_toTimer2.setText(roomItemList.get(i).getTIME_TWO_B());

//        customViewHolder.sw_device.setChecked(bean.getSTT() == 1);
        customViewHolder.ivDevice.setImageResource(
                roomItemList.get(i).getSTT() == 1 ? R.drawable.on : R.drawable.off);

        if (roomItemList.get(i).getCL() == 0) {
//            customViewHolder.sw_device.setEnabled(true);
            customViewHolder.ivDevice.setEnabled(true);
            customViewHolder.ivDevice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    roomItemList.get(i).setSTT(roomItemList.get(i).getSTT() == 0 ? 1 : 0);
                    roomItemList.get(i).setALL_STT(roomItemList.get(i).getSTT());
                    Log.d("TEST", "from check");
                    if (customViewHolder.ivDevice.isShown()) {
                        isFromAllOnOff = false;
                        isFromGroup = false;
                        customViewHolder.ivDevice.setImageResource(
                                roomItemList.get(i).getSTT() == 1 ? R.drawable.on : R.drawable.off);
                        if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
                                .contains("fan")) {
                            customViewHolder.seekDimmer
                                    .setProgress(roomItemList.get(i).getDIM());
                            customViewHolder.tvSeekValue
                                    .setText(String.valueOf(roomItemList.get(i).getDIM()));
                        } else {

                        }
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                + Constants
                                .getRoomcode(roomItemList.get(i).getROOM_Id())
                                + Constants
                                .getModuleAndDevicecode(roomItemList.get(i).getMODULE())
                                + roomItemList.get(i)
                                .getSTT()
                                + "D" + Constants
                                .getThreeDigit(roomItemList.get(i).getDIM()) + "RX"
                                + roomItemList.get(i).getRGBSTT()
                                + roomItemList.get(i).getMIC()
                        );
                        db.updateDevice(roomItemList.get(i));
                    } else {

                    }
                    // TODO: 4/30/2016 UPDATE DATABASE

                }
            });
//
        } else {
//            customViewHolder.sw_device.setEnabled(false);
            customViewHolder.ivDevice.setEnabled(false);
        }

        if (isFromGroup) {
            if (roomItemList.get(i).getGROUP() == 1) {
                if (db.getAllGrp(roomItemList.get(i).getROOM_Id()) == 1) {
//                    customViewHolder.sw_device.setCheckedProgrammatically(true);
                    customViewHolder.ivDevice.setImageResource(R.drawable.on);
                } else {
//                    customViewHolder.sw_device.setCheckedProgrammatically(false);
                    customViewHolder.ivDevice.setImageResource(R.drawable.off);
                }
                if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
                    customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getGROUP_DIM());
//                    customViewHolder.tvSeekValue.setText(String.valueOf(roomItemList.get(i).getGROUP_DIM()));

                } else {
                    customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM());
//                    customViewHolder.tvSeekValue.setText(String.valueOf(roomItemList.get(i).getDIM()));
                }
            } else {

            }
        } else if (isFromAllOnOff) {
            if (roomItemList.get(i).getSTT() == 1 && roomItemList.get(i).getCL() == 0) {
                if (db.getAllStt(roomItemList.get(i).getROOM_Id()) == 1) {
//                    customViewHolder.sw_device.setCheckedProgrammatically(true);
                    customViewHolder.ivDevice.setImageResource(R.drawable.on);

                } else {
//                    customViewHolder.sw_device.setCheckedProgrammatically(false);
                    customViewHolder.ivDevice.setImageResource(R.drawable.off);

                }
                if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
                    customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getGROUP_DIM());
//                    customViewHolder.tvSeekValue.setText(String.valueOf(roomItemList.get(i).getGROUP_DIM()));

                } else {
                    customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM());
//                    customViewHolder.tvSeekValue.setText(String.valueOf(roomItemList.get(i).getDIM()));
                }
            } else {

            }
        } else {
//            if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
//                customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM());
//            }
        }
        customViewHolder.seekDimmer.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        if (fromUser) {
                            if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
                                    .contains("fan")) {
                                if (isMultiSelect && roomItemList.get(i).getGROUP() == 1) {
                                    roomItemList.get(i).setGROUP_DIM(seekBar.getProgress());
                                } else {
                                    roomItemList.get(i).setDIM(seekBar.getProgress());
                                }
                                customViewHolder.tvSeekValue
                                        .setText(String.valueOf(progress));
                            } else if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
                                    .contains("ac")) {
                                roomItemList.get(i).setDIM(progress + 16);
                                customViewHolder.tvSeekValue
                                        .setText(String.valueOf(progress + 16) + " \u2103");
                            } else {
//                                System.out.println(
//                                        "DEVICE " + roomItemList.get(i).getDEVICE_NAME() + " >> C");
                                roomItemList.get(i).setDIM(progress);
                                customViewHolder.tvSeekValue
                                        .setText(String.valueOf(progress) + " %");
                            }
                        } else {
                            // code commented for getting % in fan progress after change from board.
//                            try {
//                                if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
//                                        .contains("fan")) {
//
//                                    customViewHolder.tvSeekValue
//                                            .setText(String.valueOf(progress));
//                                } else if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
//                                        .contains("ac")) {
//
//                                    customViewHolder.tvSeekValue
//                                            .setText(String.valueOf(progress + 16) + " \u2103");
//                                } else {
//                                    System.out.println ("DEVICE "+roomItemList.get(i).getDEVICE_NAME()+" >> D");
//                                    customViewHolder.tvSeekValue
//                                            .setText(String.valueOf(progress) + " %");
//                                }
//                            } catch (Exception e) {
//
//                            }
                        }
//
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
//                        System.out.println("PROGRES " + seekBar.getProgress());
                        if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
                            if (isMultiSelect && roomItemList.get(i).getGROUP() == 1) {
                                roomItemList.get(i).setGROUP_DIM(seekBar.getProgress());
                                db.updateDevice(roomItemList.get(i));
                                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                        + Constants
                                        .getRoomcode(roomItemList.get(i).getROOM_Id())
                                        + Constants
                                        .getModuleAndDevicecode(roomItemList.get(i).getMODULE())
                                        + "XD"
                                        + Constants
                                        .getThreeDigit(roomItemList.get(i).getGROUP_DIM()));
                            } else {
                                roomItemList.get(i).setDIM(seekBar.getProgress());
                                db.updateDevice(roomItemList.get(i));
                                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                        + Constants
                                        .getRoomcode(roomItemList.get(i).getROOM_Id())
                                        + Constants
                                        .getModuleAndDevicecode(roomItemList.get(i).getMODULE())
                                        + "XD"
                                        + Constants
                                        .getThreeDigit(roomItemList.get(i).getDIM())
                                );
                            }
                            customViewHolder.tvSeekValue
                                    .setText(String.valueOf(seekBar.getProgress()));
                        } else if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
                                .contains("ac")) {
                            roomItemList.get(i).setGROUP_DIM(seekBar.getProgress() + 16);
                            db.updateDevice(roomItemList.get(i));
                            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                    + Constants
                                    .getRoomcode(roomItemList.get(i).getROOM_Id())
                                    + Constants
                                    .getModuleAndDevicecode(roomItemList.get(i).getMODULE())
                                    + "XD"
                                    + Constants
                                    .getThreeDigit(roomItemList.get(i).getDIM())
                            );
                        } else {
                            roomItemList.get(i).setGROUP_DIM(seekBar.getProgress());
                            db.updateDevice(roomItemList.get(i));
                            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                    + Constants
                                    .getRoomcode(roomItemList.get(i).getROOM_Id())
                                    + Constants
                                    .getModuleAndDevicecode(roomItemList.get(i).getMODULE())
                                    + "XD"
                                    + Constants
                                    .getThreeDigit(roomItemList.get(i).getDIM())
                            );
                        }

                    }
                });
        customViewHolder.btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TEST", "from save");
//                sendData(roomDevice);
                updateData(roomItemList.get(i));
                if (roomItemList.get(i).getDAYS().equalsIgnoreCase("00000000")) {
                    customViewHolder.btnRepeat.setBackgroundResource(R.drawable.background_white_small_round);
                    customViewHolder.btnRepeat.setTag(0);
                    customViewHolder.btnRepeat.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                }
            }
        });

        customViewHolder.layTimer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = i;
                picker = 1;
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        RoomRecyclerAdapter.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );

                tpd.show(activity.getFragmentManager(), "Timepickerdialog");
            }
        });
        customViewHolder.layTimer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = i;
                picker = 2;
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        RoomRecyclerAdapter.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.show(activity.getFragmentManager(), "Timepickerdialog");
            }
        });
        if (roomItemList.get(i).getCL() == 1) {
            customViewHolder.im_cl.setTag("1");
        } else {
            customViewHolder.im_cl.setTag(("0"));
        }

        if (roomItemList.get(i).IS_EXPAND()) {
            customViewHolder.layDetail.setVisibility(View.VISIBLE);

        } else {
            customViewHolder.layDetail.setVisibility(View.GONE);

        }
        customViewHolder.layHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customViewHolder.layDetail.getVisibility() == View.VISIBLE) {
                    roomItemList.get(i).setIS_EXPAND(false);
                    Constants.collapse(customViewHolder.layDetail);
                } else {
                    Constants.expand(customViewHolder.layDetail);
                    roomItemList.get(i).setIS_EXPAND(true);
                }

            }
        });
//        customViewHolder.layHeader.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                mDragStartListener.onStartDrag(customViewHolder);
//                return true;
//            }
//        });
        refreshChildLock(customViewHolder.im_cl);
        setupDaysClick(customViewHolder.ll_days, customViewHolder, i);
        setupChildLockClick(customViewHolder.im_cl, i);
        setDaysFromBean(customViewHolder.ll_days, customViewHolder, i);

//        System.out.println("POS " + i + " VALUE " + roomItemList.get(i).getSTT());
    }

    private void updateData(entDevice bean) {
        for (entDevice device : roomItemList) {
            db.updateDevice(device);
        }
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(bean.getROOM_Id()) + Constants
                .getModuleAndDevicecode(bean.getMODULE()) + "T1" + Constants
                .getTimeerValue(bean.getTIME_ONE_A()) + Constants
                .getTimeerValue(bean.getTIME_ONE_B()) + "D" + Constants.getThreeDigit(bean.getDIM())
                + "REP" + bean.getDAYS()
        );

    }

    private void openColorDialog(final int position, final View v) {
//        ColorPickerDialog.newBuilder()
//                .setDialogType(ColorPickerDialog.TYPE_CUSTOM)
//                .setAllowPresets(false)
//                .setDialogId(0)
//                .setColor(Color.BLACK)
//                .setShowAlphaSlider(true)
//                .show((Activity) mContext);
        ColorPickerDialogBuilder
                .with(mContext)
                .setTitle("Choose color")
                .initialColor(0xffffffff)
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(12)
                .setPositiveButton("ok", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor,
                                        Integer[] allColors) {
//                        System.out.println("color selected" + selectedColor);
                        roomItemList.get(position).setREDCLR(Color.red(selectedColor));
                        roomItemList.get(position).setGREENCLR(Color.green(selectedColor));
                        roomItemList.get(position).setBLUECLR(Color.blue(selectedColor));
                        ((ImageView) v).setColorFilter(selectedColor,
                                PorterDuff.Mode.MULTIPLY);
                        // TODO: 4/30/2016  UPDATE DATABASE
                        db.updateDevice(roomItemList.get(position));
                        isFromAllOnOff = false;
                        isFromGroup = false;
                        RoomRecyclerAdapter.this.notifyDataSetChanged();
//                        changeBackgroundColor(selectedColor);
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                + Constants
                                .getRoomcode(roomItemList.get(position).getROOM_Id()) + Constants
                                .getModuleAndDevicecode(roomItemList.get(position).getMODULE())
                                + "LED" + Constants
                                .getThreeDigit(roomItemList.get(position).getREDCLR()) + Constants
                                .getThreeDigit(roomItemList.get(position).getGREENCLR()) + Constants
                                .getThreeDigit(roomItemList.get(position).getBLUECLR()));

                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();

    }

    private void openNameDialog(final com.oob.view.TextView tv, final ImageView ivIcon,
                                final int position) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_apliences_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        edtName.setText(tv.getText().toString());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (Constants.isFromOurData(edtName.getText().toString().toLowerCase())) {
//                    System.out.println("IS TRUE");
                    roomItemList.get(position).setDEVICE_NAME(edtName.getText().toString());
                    db.updateDevice(roomItemList.get(position));
                    roomItemList = db
                            .getListOfDevices(roomItemList.get(position).getROOM_Id(), true);
                    notifyDataSetChanged();
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    public void remove(int position) {
        roomItemList.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public void swap(int firstPosition, int secondPosition) {
        Collections.swap(roomItemList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    private void refreshChildLock(View v) {
        String tag = (String) v.getTag();
        switch (tag) {
            case "0":
                ((ImageButton) v).setImageResource(R.drawable.lock);

                break;
            case "1":
                ((ImageButton) v).setImageResource(R.drawable.lock_selected);
                break;
        }
    }

    private void setupChildLockClick(ImageView im_cl, final int i) {
        im_cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = (String) v.getTag();
                switch (tag) {
                    case "0":
                        v.setTag("1");
                        ((ImageButton) v).setImageResource(R.drawable.lock_selected);
                        roomItemList.get(i).setCL(1);
                        break;
                    case "1":
                        v.setTag("0");
                        ((ImageButton) v).setImageResource(R.drawable.lock);
                        roomItemList.get(i).setCL(0);
                        break;
                }

                db.updateDevice(roomItemList.get(i));
                isFromAllOnOff = false;
                isFromGroup = false;
                notifyDataSetChanged();
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(roomItemList.get(i).getROOM_Id())
                        + Constants
                        .getModuleAndDevicecode(roomItemList.get(i).getMODULE())
                        + "CL" + roomItemList.get(i).getCL());
            }
        });
    }

    private void setDaysFromBean(LinearLayout ll_days, final RoomViewHolder viewHolder, int i) {
        final String days = roomItemList.get(i).getDAYS();
        if (days.equalsIgnoreCase("00000000")) {
            viewHolder.btnRepeat.setBackgroundResource(R.drawable.background_white_small_round);
            viewHolder.btnRepeat.setTag(0);
            viewHolder.btnRepeat.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        } else {
            viewHolder.btnRepeat.setBackgroundResource(R.drawable.background_blue_small_round);
            viewHolder.btnRepeat.setTag(1);
            viewHolder.btnRepeat.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        }
        viewHolder.btnRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if ((int)viewHolder.btnRepeat.getTag()==1 && days.equalsIgnoreCase("00000000")){
//                    viewHolder.btnRepeat.setBackgroundResource(R.drawable.background_white_small_round);
//                    viewHolder.btnRepeat.setTag(0);
//                    viewHolder.btnRepeat.setTextColor(ContextCompat.getColor(mContext,R.color.black));

//                }else {
                viewHolder.btnRepeat.setBackgroundResource(R.drawable.background_blue_small_round);
                viewHolder.btnRepeat.setTag(1);
                viewHolder.btnRepeat.setTextColor(ContextCompat.getColor(mContext, R.color.white));

//                }
            }
        });
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            String tag = ((Character) days.charAt(j)).toString();
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setTag(tag);
                switch (tag) {
                    case "0":
                        ((com.oob.view.TextView) v).setBackgroundResource(R.drawable.text_bg);
                        break;
                    case "1":
                        ((com.oob.view.TextView) v)
                                .setBackgroundResource(R.drawable.text_active_bg);
                        break;
                }
            }
        }
    }

    private void sendData(String json) {
        if (mContext instanceof RoomActivity) {
            ((RoomActivity) mContext).sendData(json);
            return;
        }

        json = json + "Q";
        System.out.println("json  " + json);
//        if (Utils.netCheckin(mContext)) {
        HostUtils.sendToHost(mContext, json, "http://gsiot-njf4-3sej.try.yaler.io");
//        } else {
        json = WifiUtils.encodeToServerString("JSON=" + json);
        Log.d("TEST", json);
        if (!json.isEmpty()) {
            if (task == null) {
                task = new SocketServerTask(mContext, null);
            }
//            if (task.getStatus() != AsyncTask.Status.RUNNING) {
            task = new SocketServerTask(mContext, null);
            task.execute(json, WifiUtils.server_ip,
                    WifiUtils.server_port);
        } else {
            System.out.println("RUNNING");
        }

//        }
    }

//    private void saveToDB(RoomDevice roomDevice) {
//
//        for (DevicesEntity bean : roomDevice.getDevices()) {
//            bean.save();
//        }
//
//    }

    private String getFinalDays(boolean isChecked, LinearLayout ll_days) {
        String daysStatus = getSelectedDays(ll_days);
        if (isChecked) {
            daysStatus = daysStatus + "1";
        } else {
            daysStatus = daysStatus + "0";
        }

        return daysStatus;
    }

    private void setupDaysClick(final LinearLayout ll_days, final RoomViewHolder viewHolder, final int i) {
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View view = ll_days.getChildAt(j);
//            if (view instanceof RelativeLayout) {
//                RelativeLayout layout = ((RelativeLayout) view);
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if ((int) viewHolder.btnRepeat.getTag() == 0) {
                            return;
                        }
                        String tag = (String) v.getTag();
                        switch (tag) {
                            case "0":
//                                ((TextView) v).setTypeface(null, Typeface.BOLD);
//                                ((TextView) v).setTextColor(Color.CYAN);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_active_bg);
                                v.setTag("1");
                                break;
                            case "1":
//                                ((TextView) v).setTextColor(Color.BLACK);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_bg);
                                v.setTag("0");
                                break;
                        }
                        boolean checked = ((Character) roomItemList.get(i).getDAYS().charAt(7))
                                .toString()
                                .equals("1");
                        roomItemList.get(i).setDAYS(getFinalDays(checked, ll_days));
//                        db.updateDevice(bean);
                    }
                });
            }
//            }
        }
    }

    private String getSelectedDays(LinearLayout ll_days) {
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                builder.append((String) v.getTag());
            }

        }
        return builder.toString();
    }

    @Override
    public int getItemCount() {
        return roomItemList.size();
        //(null != roomItemList ? roomItemList.size() : 0);
    }

    private String getTimeFromMillis(long millis) {
        // New date object from millis
        Date date = new Date(millis);
        // formattter
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        // Pass date object
        String formatted = df.format(date);
        return formatted;
    }

    public static String hhmmssToStringDate(String hhmmss) {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        try {
            Date d = sdf.parse(hhmmss);
            return new SimpleDateFormat("HH:mm:ss").format(d);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Long hhmmssToDateMillis(String hhmmss) {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        try {
            Date d = sdf.parse(hhmmss);
            return d.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateContentFromDB(int selectedRoomId) {
        this.roomItemList = db.getListOfDevices(selectedRoomId, true);
        isFromAllOnOff = false;
        isFromGroup = false;
        notifyDataSetChanged();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd,
                          int minuteEnd) {
//        System.out.println("TAG " + view.getTag());
//        System.out.println("TAG " + view.getTag());
        int hour, endHour;
        String ampm, endAMPM, FromDate, ToDate, timer;

        if (picker == 1) {
            timer = "T1";
//            System.out.println("hourOfDay " + hourOfDay);
//            System.out.println("minute " + minute);
//            System.out.println("hourOfDayEnd " + hourOfDayEnd);
//            System.out.println("minuteEnd " + minuteEnd);
            if (hourOfDay > 12) {
                hour = hourOfDay - 12;
                ampm = "pm";
            } else if (hourOfDay > 11) {
                hour = hourOfDay;
                ampm = "pm";
            } else {
                hour = hourOfDay;
                ampm = "am";
            }
            if (hourOfDayEnd > 12) {
                endHour = hourOfDayEnd - 12;
                endAMPM = "pm";
            } else if (hourOfDayEnd > 11) {
                endHour = hourOfDayEnd;
                endAMPM = "pm";
            } else {
                endHour = hourOfDayEnd;
                endAMPM = "am";
            }
            FromDate = hour + ":" + minute + " " + ampm;
            ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
            roomItemList.get(position).setTIME_ONE_A(FromDate);
            roomItemList.get(position).setTIME_ONE_B(ToDate);

//            roomItemList.get(position)
        } else {
            timer = "T2";

//            System.out.println("hourOfDay =2   " + hourOfDay);
//            System.out.println("minute =2   " + minute);
//            System.out.println("hourOfDayEnd =2   " + hourOfDayEnd);
//            System.out.println("minuteEnd =2   " + minuteEnd);
            if (hourOfDay > 12) {
                hour = hourOfDay - 12;
                ampm = "pm";
            } else if (hourOfDay > 11) {
                hour = hourOfDay;
                ampm = "pm";
            } else {
                hour = hourOfDay;
                ampm = "am";
            }
            if (hourOfDayEnd > 12) {
                endHour = hourOfDayEnd - 12;
                endAMPM = "pm";
            } else if (hourOfDayEnd > 11) {
                endHour = hourOfDayEnd;
                endAMPM = "pm";
            } else {
                endHour = hourOfDayEnd;
                endAMPM = "am";
            }
            FromDate = hour + ":" + minute + " " + ampm;
            ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
            roomItemList.get(position).setTIME_TWO_A(FromDate);
            roomItemList.get(position).setTIME_TWO_B(ToDate);
        }
        db.updateDevice(roomItemList.get(position));
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(roomItemList.get(position).getROOM_Id())
                + Constants.getModuleAndDevicecode(roomItemList.get(position).getMODULE()) + timer
                + Constants.getTwoDigit(hourOfDay) + Constants.getTwoDigit(minute) + Constants
                .getTwoDigit(hourOfDayEnd) + Constants.getTwoDigit(minuteEnd) + "XXXXREP"
                + roomItemList.get(position).getDAYS());
        isFromAllOnOff = false;
        isFromGroup = false;
        RoomRecyclerAdapter.this.notifyDataSetChanged();
    }

    public void setMultiselectEnable(boolean value) {
        isMultiSelect = value;
        if (!value) {
            for (entDevice device : roomItemList) {
//                System.out.println(" DIM " + device.getDIM());
//                System.out.println("GRP  DIM " + device.getGROUP_DIM());
                db.updateDevice(device);
            }
            openGroupingSettingDialog();
        } else {
            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                    .getRoomcode(selectedRoomId) + "GRP" + entRoom
                    .getGROUP_COLOR() + "B" + entRoom.getGROUP_BUZZ() + "GRLP1Q");
        }
        isFromAllOnOff = false;
        isFromGroup = false;
        notifyDataSetChanged();
    }

    private void openGroupingSettingDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_grouping_setting);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        dialog.show();

        final ImageView ivIcon = (ImageView) dialog.findViewById(R.id.ivIcon);
        final ImageView ivBuzz = (ImageView) dialog.findViewById(R.id.ivBuzzer);
        if (entRoom.getGROUP_BUZZ() == 0) {
            ivBuzz.setImageResource(R.drawable.mute);
        } else {
            ivBuzz.setImageResource(R.drawable.buzzer);

        }
        ivBuzz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (entRoom.getGROUP_BUZZ() == 1) {
                    entRoom.setGROUP_BUZZ(0);
                    ivBuzz.setImageResource(R.drawable.mute);
                } else {
                    entRoom.setGROUP_BUZZ(1);
                    ivBuzz.setImageResource(R.drawable.buzzer);
                }
                db.updateRoom(entRoom);
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(selectedRoomId) + "GRP" + entRoom
                        .getGROUP_COLOR() + "B" + entRoom.getGROUP_BUZZ() + "GRLP0");
            }
        });

        ivIcon.setColorFilter(Color.parseColor(colors.get(entRoom.getGROUP_COLOR())),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        ivIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (entRoom.getGROUP_COLOR() < 7) {
                    entRoom.setGROUP_COLOR(entRoom.getGROUP_COLOR() + 1);
                } else {
                    entRoom.setGROUP_COLOR(0);
                }
                ivIcon.setColorFilter(
                        Color.parseColor(colors.get(entRoom.getGROUP_COLOR())),
                        android.graphics.PorterDuff.Mode.MULTIPLY);
                db.updateRoom(entRoom);
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(selectedRoomId) + "GRP" + entRoom
                        .getGROUP_COLOR() + "B" + entRoom.getGROUP_BUZZ() + "GRLP0");

            }
        });
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                db.updateRoom(entRoom);
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(selectedRoomId) + "GRP" + entRoom
                        .getGROUP_COLOR() + "B" + entRoom.getGROUP_BUZZ() + "GRLP0");
//                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
//                        .getRoomcode(selectedRoomId) + "GRLP0Q");
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
    }

    public void AddData(ArrayList<entDevice> data, int selectedRoomId) {
        roomItemList = new ArrayList<>();
        roomItemList = db.getListOfDevices(selectedRoomId, true);
        isFromAllOnOff = false;
        isFromGroup = false;
        notifyDataSetChanged();

    }

    public void sendGroupingData(boolean isGrouping) {
        String data = "G" + (isGrouping ? "1" : "0");
        String dataOnOff = "";
        isFromGroup = true;
        isFromAllOnOff = false;
        List<entDevice> al_data = db.getListOfDevices(roomItemList.get(0).getROOM_Id(), false);

        for (entDevice device : al_data) {
            if (device.getGROUP() == 1) {
                if (isGrouping) {
                    dataOnOff = "11";
                    device.setSTT(1);
                    db.updateDeviceONOFF(device.getDEVICE_Id(), 1);
                } else {
                    dataOnOff = "10";
                    device.setSTT(0);
                    db.updateDeviceONOFF(device.getDEVICE_Id(), 0);
                }
            } else {
                dataOnOff = "00";
            }
            if (device.getDEVICE_NAME().toLowerCase().contains("fan")) {
                dataOnOff = dataOnOff + "D" + device.getGROUP_DIM();
            }
//            data = data + dataOnOff;
//            System.out.println("DATA " + data);
            if (!data.contains(Constants.getModulecode(device.getMODULE()))) {
                if (data.length() > 4) {
//                    System.out.println("DATA " + data + "  CHAR " + data.charAt(data.length() - 3));
                    if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("D")) {
                        data = data + "E00F00";
                    } else if (String.valueOf(data.charAt(data.length() - 3))
                            .equalsIgnoreCase("C")) {
                        data = data + "D00E00F00";
                    } else if (String.valueOf(data.charAt(data.length() - 5)).equalsIgnoreCase("B")
                            || String.valueOf(data.charAt(data.length() - 3))
                            .equalsIgnoreCase("B")) {
                        data = data + "C00D00E00F00";
                    } else if (String.valueOf(data.charAt(data.length() - 3))
                            .equalsIgnoreCase("A")) {
                        data = data + "B00C00D00E00F00";
                    }
                }
                data = data + Constants.getModuleAndDevicecode(device.getMODULE()) + dataOnOff;
            } else {
                data = data + Constants.getDevicecode(device.getMODULE()) + dataOnOff;
            }

        }
//        System.out.println("END CHAR " + data.charAt(data.length() - 3));
        if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("D")) {
            data = data + "E00F00";
        }
        if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("D")) {
            data = data + "E00F00";
        } else if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("C")) {
            data = data + "D00E00F00";
        } else if (String.valueOf(data.charAt(data.length() - 5)).equalsIgnoreCase("B") || String
                .valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("B")) {
            data = data + "C00D00E00F00";
        } else if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("A")) {
            data = data + "B00C00D00E00F00";
        }
        String str = "G" + (isGrouping ? "1" : "0");
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(al_data.get(0).getROOM_Id()) + str);
        roomItemList = db.getListOfDevices(roomItemList.get(0).getROOM_Id(), true);
        notifyDataSetChanged();
    }

    public void sendAllOnOFFData(boolean isOn) {
        String data = "ALL";
        String dataOnOff = "";
        isFromGroup = false;
        isFromAllOnOff = true;
        List<entDevice> al_data = db.getListOfDevices(roomItemList.get(0).getROOM_Id(), false);
        for (entDevice device : al_data) {
            if (device.getALL_STT() == 1) {
                if (isOn) {
                    dataOnOff = "11";
                } else {
                    dataOnOff = "10";
                }
            } else {
                dataOnOff = "00";
            }
//            data = data + dataOnOff;
            if (!data.contains(Constants.getModulecode(device.getMODULE()))) {
                if (data.length() > 4) {
                    if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("D")) {
                        data = data + "E00F00";
                    } else if (String.valueOf(data.charAt(data.length() - 3))
                            .equalsIgnoreCase("C")) {
                        data = data + "D00E00F00";
                    } else if (String.valueOf(data.charAt(data.length() - 3))
                            .equalsIgnoreCase("B")) {
                        data = data + "C00D00E00F00";
                    } else if (String.valueOf(data.charAt(data.length() - 3))
                            .equalsIgnoreCase("A")) {
                        data = data + "B00C00D00E00F00";
                    }
                }
                data = data + Constants.getModuleAndDevicecode(device.getMODULE()) + dataOnOff;
            } else {
                data = data + Constants.getDevicecode(device.getMODULE()) + dataOnOff;
            }
        }
        if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("D")) {
            data = data + "E00F00";
        } else if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("C")) {
            data = data + "D00E00F00";
        } else if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("B")) {
            data = data + "C00D00E00F00";
        } else if (String.valueOf(data.charAt(data.length() - 3)).equalsIgnoreCase("A")) {
            data = data + "B00C00D00E00F00";
        }
        String str = "ALL" + (isOn ? "1" : "0");
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(al_data.get(0).getROOM_Id()) + str);
        roomItemList = db.getListOfDevices(roomItemList.get(0).getROOM_Id(), true);
        notifyDataSetChanged();
    }

    public void clearAdapter() {
        roomItemList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public List<entDevice> getItemList() {
        return roomItemList;
    }

    public void updateData(List<entDevice> al_data) {
        roomItemList = al_data;
        notifyDataSetChanged();
    }

    @Override
    public void onColorSelected(int dialogId, @ColorInt int color) {

        System.out.println(" color " + color);
    }

    @Override
    public void onDialogDismissed(int dialogId) {

    }

    class RoomViewHolder extends RecyclerView.ViewHolder {

        //        protected RangeSeekBar<Integer> rangeBar_dimmer;
        //        protected RangeSeekBar<Long> rangeBar_timer;
        protected SeekBar seekDimmer;

        protected ImageView ivIcon, im_cl, ivBuzer, ivColor;

        protected TextView tvName, tv_fromTimer1, tv_toTimer1, tv_fromTimer2, tv_toTimer2,
                tv_Timer1, tv_Timer2, tvSeekValue;

        //        protected EnhancedCheckBox sw_device;
        protected ImageView ivDevice;

        protected EnhancedCheckBox cbSelect;

        protected com.oob.view.Button btn_save, btnRepeat;

        //        protected CheckBox chk_repeat;
        protected LinearLayout ll_days;

        //        protected LinearLayout ll_main;
        protected RelativeLayout layHeader;

        protected LinearLayout layDetail, layTimer1, layTimer2;

        public RoomViewHolder(View view) {
            super(view);
//            ll_main = (LinearLayout) view;
//            rangeBar_dimmer = (RangeSeekBar<Integer>) view.findViewById(R.id.rangeBar_dimmer);
            seekDimmer = (SeekBar) view.findViewById(R.id.seekBar);
//            rangeBar_timer = (RangeSeekBar<Long>) view.findViewById(R.id.rangeBar_timer);
            btn_save = (com.oob.view.Button) view.findViewById(R.id.btn_save);
            btnRepeat = (com.oob.view.Button) view.findViewById(R.id.btnRepeat);
            ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
            im_cl = (ImageView) view.findViewById(R.id.im_cl);
            ivBuzer = (ImageView) view.findViewById(R.id.ivBuzzer);
            ivColor = (ImageView) view.findViewById(R.id.ivColor);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tv_fromTimer1 = (TextView) view.findViewById(R.id.tvTimer1From);
            tv_toTimer1 = (TextView) view.findViewById(R.id.tvTimer1to);
            tv_fromTimer2 = (TextView) view.findViewById(R.id.tvTimer2From);
            tv_toTimer2 = (TextView) view.findViewById(R.id.tvTimer2to);
            tv_Timer1 = (TextView) view.findViewById(R.id.tvTimer1);
            tv_Timer2 = (TextView) view.findViewById(R.id.tvTimer2);
//            sw_device = (EnhancedCheckBox) view.findViewById(R.id.sw_device);
            ivDevice = (ImageView) view.findViewById(R.id.ivDevice);
            cbSelect = (EnhancedCheckBox) view.findViewById(R.id.checkbox);
//            chk_repeat = (CheckBox) view.findViewById(R.id.chk_repeat);
            ll_days = (LinearLayout) view.findViewById(R.id.ll_days);
            layHeader = (RelativeLayout) view.findViewById(R.id.layHeader);
            layDetail = (LinearLayout) view.findViewById(R.id.layDetail);
            layTimer1 = (LinearLayout) view.findViewById(R.id.layTimer1);
            layTimer2 = (LinearLayout) view.findViewById(R.id.layTimer2);
            tvSeekValue = (TextView) view.findViewById(R.id.tvSeekValue);

//            rangeBar_timer.setRangeValues(minDate.getTime(), maxDate.getTime());
//            rangeBar_timer.setNotifyWhileDragging(true);

        }

    }
}