package com.oob.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entCurtain;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.view.CircleImageView;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.List;


public class CurtainRecyclerAdapter
        extends RecyclerView.Adapter<CurtainRecyclerAdapter.securityViewHolder> {

    TypedArray imgs;

    List<entCurtain> curtains = new ArrayList<>();

    Context context;

    dbHelperOperations db;

    public CurtainRecyclerAdapter(Context context, List<entCurtain> sensors) {
        this.context = context;
        this.curtains = sensors;
        db = new dbHelperOperations(context);

    }

    @Override
    public securityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.raw_curtain, viewGroup, false);
        return new securityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final securityViewHolder customViewHolder, final int i) {
        customViewHolder.tvCurtainName.setText(curtains.get(i).getCURTAIN_NAME());
        customViewHolder.tvRoomName.setText(curtains.get(i).getROOM_NAME());

        customViewHolder.ivStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("STOP " + curtains.get(i).getCURTAIN_STT());
                if (curtains.get(i).getCURTAIN_STT().equalsIgnoreCase("1")) {
                    curtains.get(i).setCURTAIN_STT("0");
                    customViewHolder.ivStop.setImageResource(R.color.red);
                    db.updateCurtain(curtains.get(i));
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                            .getRoomcode(curtains.get(i).getROOM_Id()) + curtains.get(i)
                            .getCURTAIN_NAME() + "STOP");

                } else {

                }

            }
        });
        customViewHolder.ivUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("UP");
                if (curtains.get(i).getCURTAIN_STT().equalsIgnoreCase("0")) {
                    curtains.get(i).setCURTAIN_STT("1");
                    customViewHolder.ivStop.setImageResource(R.color.blue);
                    db.updateCurtain(curtains.get(i));
                } else {

                }
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(curtains.get(i).getROOM_Id()) + curtains.get(i)
                        .getCURTAIN_NAME() + "UP");

            }
        });
        customViewHolder.ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("DOWN");
                if (curtains.get(i).getCURTAIN_STT().equalsIgnoreCase("0")) {
                    curtains.get(i).setCURTAIN_STT("1");
                    customViewHolder.ivStop.setImageResource(R.color.blue);
                    db.updateCurtain(curtains.get(i));
                } else {

                }
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(curtains.get(i).getROOM_Id()) + curtains.get(i)
                        .getCURTAIN_NAME() + "DOWN");


            }
        });
        customViewHolder.ivLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("LEFT");
                if (curtains.get(i).getCURTAIN_STT().equalsIgnoreCase("0")) {
                    curtains.get(i).setCURTAIN_STT("1");
                    customViewHolder.ivStop.setImageResource(R.color.blue);
                    db.updateCurtain(curtains.get(i));
                } else {

                }
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(curtains.get(i).getROOM_Id()) + curtains.get(i)
                        .getCURTAIN_NAME() + "LEFT");


            }
        });
        customViewHolder.ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("RIGHT");
                if (curtains.get(i).getCURTAIN_STT().equalsIgnoreCase("0")) {
                    curtains.get(i).setCURTAIN_STT("1");
                    customViewHolder.ivStop.setImageResource(R.color.blue);
                    db.updateCurtain(curtains.get(i));
                } else {

                }
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(curtains.get(i).getROOM_Id()) + curtains.get(i)
                        .getCURTAIN_NAME() + "RIGHT");


            }
        });

    }


    @Override
    public int getItemCount() {
        return curtains.size();
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json.replace(" ", "");
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(context)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(context, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

    class securityViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivIcon;

        protected ImageButton ivUp, ivLeft, ivDown, ivRight;

        protected CircleImageView ivStop;

        protected TextView tvCurtainName, tvRoomName, tvUp, tvDown, tvLeft, tvRight, tvStop;


        public securityViewHolder(View view) {
            super(view);

            ivIcon = (ImageView) view.findViewById(R.id.ivLogo);
            tvCurtainName = (TextView) view.findViewById(R.id.tvCurtainName);
            tvRoomName = (TextView) view.findViewById(R.id.tvOpenClose);
            ivUp = (ImageButton) view.findViewById(R.id.tvUp);
            ivDown = (ImageButton) view.findViewById(R.id.tvDown);
            ivLeft = (ImageButton) view.findViewById(R.id.tvLeft);
            ivRight = (ImageButton) view.findViewById(R.id.tvRight);
            ivStop = (CircleImageView) view.findViewById(R.id.tvStop);

        }
    }
}
