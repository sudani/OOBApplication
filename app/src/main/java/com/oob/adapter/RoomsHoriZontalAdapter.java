package com.oob.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.oob.app.R;
import com.oob.entity.entRooms;
import com.oob.view.CircleImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;


public class RoomsHoriZontalAdapter extends BaseQuickAdapter<entRooms,BaseViewHolder> {

    private Context context;
    private List<entRooms> horizontalList;
    public int roomId;

    public RoomsHoriZontalAdapter(List<entRooms> horizontalList, Context context, int roomId) {
        super(R.layout.raw_roome, horizontalList);
        mContext = context;
        this.horizontalList = horizontalList;
        this.context = context;
        this.roomId = roomId;
    }


    @Override
    protected void convert(BaseViewHolder baseViewHolder, entRooms entRooms) {

        baseViewHolder.setText(R.id.tvName, entRooms.getROOM_NAME());

        if (roomId==entRooms.getROOM_Id()){
            baseViewHolder.setBackgroundColor(R.id.tvName, Color.parseColor("#ff00afef"));
        }else {
            baseViewHolder.setBackgroundColor(R.id.tvName, Color.parseColor("#3E3933"));
        }
//        if (entRooms.getPROFILE_DATA().equalsIgnoreCase("")) {
//            Picasso.with(context).load(new File(entRooms.getPROFILE_DATA())).placeholder(R.drawable.app_logo).error(R.drawable.app_logo).into(icon);
//        }
    }


    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}