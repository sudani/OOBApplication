package com.oob.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.oob.fragment.DailyFragment;
import com.oob.fragment.MonthlyFragment;
import com.oob.fragment.WeeklyFragment;
import com.oob.fragment.YearlyFragment;

public class LogDataPager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public LogDataPager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
//        return  new DailyFragment();
        switch (position) {
            case 0:
                return new DailyFragment();
            case 1:
                return new WeeklyFragment();
            case 2:
                return new MonthlyFragment();
            case 3:
                return new YearlyFragment();
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs 
    @Override
    public int getCount() {
        return tabCount;
    }
}