package com.oob.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.oob.app.R;
import com.oob.entity.entChannel;
import com.oob.entity.entChannelModel;
import com.oob.entity.entMySection;
import com.oob.tools.Constants;

import java.util.List;

public class SectionAdapter extends BaseSectionQuickAdapter<entMySection,BaseViewHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param context     The context.
     * @param sectionHeadResId The section head layout id for each item
     * @param layoutResId The layout resource id of each item.
     * @param data        A new list is created out of this one to avoid mutable list
     */
    Context context;
    public SectionAdapter(Context context, int layoutResId, int sectionHeadResId, List data) {
        super(layoutResId, sectionHeadResId, data);
        this.context = context;
    }

    @Override
    protected void convertHead(BaseViewHolder helper, final entMySection item) {
        helper.setText(R.id.tvSection, item.header);
//        helper.setVisible(R.id.more,item.isMroe());
//        helper.setOnClickListener(R.id.more,new OnItemChildClickListener());
    }


    @Override
    protected void convert(BaseViewHolder helper, entMySection item) {
        entChannel channel = (entChannel) item.t;
        //helper.setImageUrl(R.id.iv, video.getImg());
        helper.setText(R.id.tvName, channel.getName());
        helper.setImageResource(R.id.ivLogo, Constants.getImage(context,channel.getImg()));
    }
}