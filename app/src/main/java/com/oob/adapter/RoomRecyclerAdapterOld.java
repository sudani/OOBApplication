//package com.oob.adapter;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.SeekBar;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
//import com.borax12.materialdaterangepicker.time.TimePickerDialog;
//import com.flask.colorpicker.ColorPickerView;
//import com.flask.colorpicker.OnColorSelectedListener;
//import com.flask.colorpicker.builder.ColorPickerClickListener;
//import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
//import com.google.gson.GsonBuilder;
//import com.oob.SocketServerTask;
//import com.oob.app.R;
//import com.oob.tools.Constants;
//import com.oob.tools.WifiUtils;
//import com.oob.view.EditText;
//
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
////import com.oob.bean.DevicesEntity;
////import com.oob.bean.RoomDevice;
//
//
//public class RoomRecyclerAdapterOld extends RecyclerView.Adapter<RoomRecyclerAdapterOld.RoomViewHolder>
//        implements TimePickerDialog.OnTimeSetListener {
//
//    private List<DevicesEntity> roomItemList;
//
//    private Context mContext;
//
//    private Date minDate, maxDate;
//
//    private DateFormat df;
//
//    private RoomDevice roomDevice;
//
//    private Activity activity;
//
//    private int position = -1;
//
//    private int picker = 0;
//
//    private int colorCount = 0;
//
//    private ImageView imageView;
//
//    private boolean isMultiSelect = false;
//
//    public RoomRecyclerAdapterOld(Context context, RoomDevice roomDevice, Activity activity) {
//        this.roomDevice = roomDevice;
//        this.roomItemList = roomDevice.getDevices();
//        this.mContext = context;
//        this.activity = activity;
//        df = new SimpleDateFormat("HH:mm:ss");
//        try {
//            minDate = df.parse("00:00:00");
//            maxDate = df.parse("23:59:59");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public RoomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//        View view = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.row_frag_room, viewGroup, false);
//        return new RoomViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(final RoomViewHolder customViewHolder, final int i) {
//
//        final DevicesEntity bean = roomItemList.get(i);
//
////        int screen_height= PixelUtil.getDisplayHeight(mContext)-PixelUtil.getActionbarHeight(mContext)-PixelUtil.getStatusbarHeight(mContext);
////        customViewHolder.ll_main.getLayoutParams().height=screen_height/4;
//        if (isMultiSelect) {
//            customViewHolder.cbSelect.setVisibility(View.VISIBLE);
//        } else {
//            customViewHolder.cbSelect.setVisibility(View.GONE);
//
//        }
//        switch (bean.getID()) {
//            case 1:
//            case 5:
//                customViewHolder.tvName.setText("LIGHT 1");
//                customViewHolder.ivIcon.setImageResource(R.drawable.light);
//                customViewHolder.seekDimmer.setMax(100);
//
//                break;
//            case 2:
//            case 6:
//                customViewHolder.tvName.setText("LIGHT 2");
//                customViewHolder.ivIcon.setImageResource(R.drawable.light);
//                customViewHolder.seekDimmer.setMax(100);
//
//                break;
//            case 3:
//            case 7:
//                customViewHolder.tvName.setText("FAN");
//                customViewHolder.ivIcon.setImageResource(R.drawable.fan);
//                customViewHolder.seekDimmer.setMax(9);
//                break;
//            case 4:
//            case 8:
//                customViewHolder.tvName.setText("AC");
//                customViewHolder.ivIcon.setImageResource(R.drawable.ac);
//                customViewHolder.seekDimmer.setMax(16);
//                break;
//        }
//        customViewHolder.ivIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                customViewHolder.ivIcon.setTag(position);
//                changeImageColor(customViewHolder.ivIcon, position);
//            }
//        });
//        customViewHolder.tvName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openNameDialog((com.oob.view.TextView) v, position);
//            }
//        });
//        customViewHolder.ivColor.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                openColorDialog(position,v);
//            }
//        });
//        customViewHolder.ivBuzer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                customViewHolder.ivBuzer.setImageResource(R.drawable.mute);
//            }
//        });
//
////        customViewHolder.tv_fromTimer1.setText(hhmmssToStringDate(bean.getTIMA()));
////        customViewHolder.tv_toTimer1.setText(hhmmssToStringDate(bean.getTIMB()));
//        customViewHolder.tv_fromTimer1.setText(bean.getTIMA());
//        customViewHolder.tv_toTimer1.setText(bean.getTIMB());
//
//        customViewHolder.sw_device.setChecked(bean.getSTT() == 1);
////        customViewHolder.rangeBar_dimmer.setSelectedMaxValue(bean.getDIM());
////        customViewHolder.seekDimmer.setMax(bean.getDIM());
//        customViewHolder.tvSeekValue.setText(String.valueOf(bean.getDIM()));
//
////        customViewHolder.rangeBar_timer.setSelectedMinValue(hhmmssToDateMillis(bean.getTIMA()));
////        customViewHolder.rangeBar_timer.setSelectedMaxValue(hhmmssToDateMillis(bean.getTIMB()));
//
//        customViewHolder.sw_device
//                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                        bean.setSTT(isChecked ? 1 : 0);
//                        Log.d("TEST", "from check");
//                        if (customViewHolder.sw_device.isShown()) {
//                            sendData(roomDevice);
//                        }
//
//                    }
//                });
//
////        customViewHolder.rangeBar_timer.setOnRangeSeekBarChangeListener(
////                new RangeSeekBar.OnRangeSeekBarChangeListener<Long>() {
////                    @Override
////                    public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Long minValue,
////                            Long maxValue) {
////                        // handle changed range values
////                        String minval = getTimeFromMillis(minValue);
////                        String maxval = getTimeFromMillis(maxValue);
////                        String minval_num = minval.replace(":", "");
////                        String maxval_num = maxval.replace(":", "");
////                        bean.setTIMA(minval_num);
////                        bean.setTIMB(maxval_num);
////                        customViewHolder.tv_fromTimer1.setText(hhmmssToStringDate(minval_num));
////                        customViewHolder.tv_toTimer1.setText(hhmmssToStringDate(maxval_num));
////                        Log.i("TEST", "User selected new date range: MIN=" + hhmmssToStringDate(
////                                minval_num) + ", MAX=" + hhmmssToStringDate(maxval_num));
////                    }
////                });
////
////        customViewHolder.rangeBar_dimmer.setOnRangeSeekBarChangeListener(
////                new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
////                    @Override
////                    public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue,
////                            Integer maxValue) {
////                        // handle changed range values
////                        Log.i("TEST", "User selected new date range: MIN=" + minValue + ", MAX="
////                                + maxValue);
////                        bean.setDIM(maxValue);
////                    }
////                });
//        customViewHolder.seekDimmer.setOnSeekBarChangeListener(
//                new SeekBar.OnSeekBarChangeListener() {
//                    @Override
//                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//
//                        switch (bean.getID()) {
//                            case 1:
//                            case 2:
//                            case 5:
//                            case 6:
//                                bean.setDIM(progress);
//                                customViewHolder.tvSeekValue
//                                        .setText(String.valueOf(progress) + " %");
//                                break;
//                            case 3:
//                            case 7:
//                                bean.setDIM(progress);
//                                customViewHolder.tvSeekValue
//                                        .setText(String.valueOf(progress));
//                                break;
//                            case 4:
//                            case 8:
//                                    bean.setDIM(progress+16);
//                                    customViewHolder.tvSeekValue
//                                            .setText(String.valueOf(progress+16) + " \u2103");
//
//                                break;
//                        }
//
//                    }
//
//                    @Override
//                    public void onStartTrackingTouch(SeekBar seekBar) {
//
//                    }
//
//                    @Override
//                    public void onStopTrackingTouch(SeekBar seekBar) {
//
//                    }
//                });
//        customViewHolder.btn_save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("TEST", "from save");
//                sendData(roomDevice);
//            }
//        });
////        customViewHolder.chk_repeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
////            @Override
////            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
////
////                bean.setDays(getFinalDays(isChecked, customViewHolder.ll_days));
////
////                //NotifyUtils.toast(mContext,daysStatus);
////            }
////        });
//        customViewHolder.layTimer1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                position = i;
//                picker = 1;
//                Calendar now = Calendar.getInstance();
//                TimePickerDialog tpd = TimePickerDialog.newInstance(
//                        RoomRecyclerAdapterOld.this,
//                        now.get(Calendar.HOUR_OF_DAY),
//                        now.get(Calendar.MINUTE),
//                        false
//                );
//
//                tpd.show(activity.getFragmentManager(), "Timepickerdialog");
//            }
//        });
//        customViewHolder.layTimer2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                position = i;
//                picker = 2;
//                Calendar now = Calendar.getInstance();
//                TimePickerDialog tpd = TimePickerDialog.newInstance(
//                        RoomRecyclerAdapterOld.this,
//                        now.get(Calendar.HOUR_OF_DAY),
//                        now.get(Calendar.MINUTE),
//                        false
//                );
//                tpd.show(activity.getFragmentManager(), "Timepickerdialog");
//            }
//        });
//        if (bean.getCL() == 1) {
//            customViewHolder.im_cl.setTag("1");
//        } else {
//            customViewHolder.im_cl.setTag(("0"));
//        }
//
//        customViewHolder.layHeader.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (customViewHolder.layDetail.getVisibility() == View.VISIBLE) {
//                    Constants.collapse(customViewHolder.layDetail);
//                } else {
//                    Constants.expand(customViewHolder.layDetail);
//                }
//
//            }
//        });
//        refreshChildLock(customViewHolder.im_cl);
//        setupDaysClick(bean, customViewHolder.ll_days);
//        setupChildLockClick(bean, customViewHolder.im_cl);
//        setDaysFromBean(bean, customViewHolder.ll_days);
//    }
//
//    private void openColorDialog(int position, final View v) {
//
//        ColorPickerDialogBuilder
//                .with(mContext)
//                .setTitle("Choose color")
//                .initialColor(0xffffffff)
//                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
//                .density(12)
//                .setOnColorSelectedListener(new OnColorSelectedListener() {
//                    @Override
//                    public void onColorSelected(int selectedColor) {
//                        System.out.println("color "+selectedColor);
////                        toast("onColorSelected: 0x" + Integer.toHexString(selectedColor));
//                        ((ImageView)v).setColorFilter(selectedColor,android.graphics.PorterDuff.Mode.MULTIPLY);
//                    }
//                })
//                .setPositiveButton("ok", new ColorPickerClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int selectedColor,
//                            Integer[] allColors) {
//                        System.out.println("color selected"+selectedColor);
//                        ((ImageView)v).setColorFilter(selectedColor,android.graphics.PorterDuff.Mode.MULTIPLY);
////                        changeBackgroundColor(selectedColor);
//                        if (allColors != null) {
//                            StringBuilder sb = null;
//
//                            for (Integer color : allColors) {
//                                if (color == null) {
//                                    continue;
//                                }
//                                if (sb == null) {
//                                    sb = new StringBuilder("Color List:");
//                                }
//                                sb.append("\r\n#" + Integer.toHexString(color).toUpperCase());
//                            }
//
//                            if (sb != null) {
//                                Toast.makeText(mContext, sb.toString(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//                })
//                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                    }
//                })
////                .showColorEdit(true)
////                .setColorEditTextColor(
////                        mContext.getResources().getColor(android.R.color.holo_blue_bright))
//                .build()
//                .show();
//
//    }
//
//    private void openNameDialog(final com.oob.view.TextView tv, final int position) {
//        final Dialog dialog = new Dialog(mContext);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        // Include .xml file
//        dialog.setContentView(R.layout.dialog_apliences_name);
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().setBackgroundDrawable(
//                new ColorDrawable(Color.TRANSPARENT));
//
//        // set values for custom dialog components - text, image and button
//        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
//
//        dialog.show();
//
//        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
//        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
//        // if decline button is clicked, close the custom dialog
//
//        btn_ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
////               roomItemList.get(position).ge
//                tv.setText(edtName.getText().toString().toUpperCase());
//
//            }
//        });
//        btn_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//
//            }
//        });
//
//
//    }
//
//    private void changeImageColor(ImageView ivIcon, int position) {
////        int[] rainbow = mContext.getResources().getIntArray(R.array.light_colors);
//        ArrayList<String> colors = new ArrayList<>(
//                Arrays.asList(mContext.getResources().getStringArray(R.array.light_colors)));
//        if (imageView != null) {
//            if (imageView.getTag() == ivIcon.getTag()) {
//                if (colorCount < 7) {
//                    ivIcon.setColorFilter(Color.parseColor(colors.get(colorCount)),
//                            android.graphics.PorterDuff.Mode.MULTIPLY);
//                    colorCount++;
//                } else {
//                    imageView = ivIcon;
//                    colorCount = 0;
//                    ivIcon.setColorFilter(Color.parseColor(colors.get(colorCount)),
//                            android.graphics.PorterDuff.Mode.MULTIPLY);
//                    colorCount++;
//                }
//            } else {
//                imageView = ivIcon;
//                colorCount = 0;
//                ivIcon.setColorFilter(Color.parseColor(colors.get(colorCount)),
//                        android.graphics.PorterDuff.Mode.MULTIPLY);
//                colorCount++;
//            }
//        } else {
//            imageView = ivIcon;
//            colorCount = 0;
//            ivIcon.setColorFilter(Color.parseColor(colors.get(colorCount)),
//                    android.graphics.PorterDuff.Mode.MULTIPLY);
//            colorCount++;
//        }
////        ivIcon.setColorFilter(Color.parseColor("#00FF00"), android.graphics.PorterDuff.Mode.MULTIPLY);
//
//        System.out.println("COUNT " + colorCount);
////this.notifyDataSetChanged();
//    }
//
//    private void refreshChildLock(View v) {
//        String tag = (String) v.getTag();
//        switch (tag) {
//            case "0":
//                ((ImageButton) v).setImageResource(R.drawable.lock);
//
//                break;
//            case "1":
//                ((ImageButton) v).setImageResource(R.drawable.lock_selected);
//                break;
//        }
//    }
//
//
//    private void setupChildLockClick(final DevicesEntity bean, ImageView im_cl) {
//        im_cl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String tag = (String) v.getTag();
//                switch (tag) {
//                    case "0":
//                        v.setTag("1");
//                        ((ImageButton) v).setImageResource(R.drawable.lock_selected);
//                        bean.setCL(1);
//                        break;
//                    case "1":
//                        v.setTag("0");
//                        ((ImageButton) v).setImageResource(R.drawable.lock);
//                        bean.setCL(0);
//                        break;
//                }
//            }
//        });
//    }
//
//    private void setDaysFromBean(DevicesEntity bean, LinearLayout ll_days) {
//        String days = bean.getDays();
//        for (int j = 0; j < ll_days.getChildCount(); j++) {
//            String tag = ((Character) days.charAt(j)).toString();
////            View view = ll_days.getChildAt(j);
////            if (view instanceof RelativeLayout) {
////                RelativeLayout layout = ((RelativeLayout) view);
//            View v = ll_days.getChildAt(j);
//            if (v.getTag() != null) {
//                v.setTag(tag);
//                switch (tag) {
//                    case "0":
//                        System.out.println("00");
////                        ((TextView) v).setTextColor(Color.BLACK);
//                        ((com.oob.view.TextView) v).setBackgroundResource(R.drawable.text_bg);
//                        break;
//                    case "1":
//                        System.out.println("11");
//
////                        ((TextView) v).setTypeface(null, Typeface.BOLD);
////                        ((TextView) v).setTextColor(Color.CYAN);
//                        ((com.oob.view.TextView) v).setBackgroundResource(R.drawable.text_active_bg);
//                        break;
//                }
//            }
////            else
////                chk_repeat.setChecked(tag.equals("1"));
//        }
////        }
//    }
//
//    private void sendData(RoomDevice roomDevice) {
//        saveToDB(roomDevice);
//        String json = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
//                .toJson(roomDevice);
//        System.out.println("json" + json);
//        //json="GET /?JSON="+json+" HTTP/1.1";
//        json = WifiUtils.encodeToServerString("JSON=" + json);
//        Log.d("TEST", json);
//        System.out.println("json >" + json);
//        if (!json.isEmpty()) {
//            new SocketServerTask(mContext, null)
//                    .execute(json, WifiUtils.server_ip, WifiUtils.server_port);
//        }
//    }
//
//    private void saveToDB(RoomDevice roomDevice) {
//
//        for (DevicesEntity bean : roomDevice.getDevices()) {
//            bean.save();
//        }
//
//    }
//
//
//    private String getFinalDays(boolean isChecked, LinearLayout ll_days) {
//        String daysStatus = getSelectedDays(ll_days);
//        if (isChecked) {
//            daysStatus = daysStatus + "1";
//        } else {
//            daysStatus = daysStatus + "0";
//        }
//
//        return daysStatus;
//    }
//
//    private void setupDaysClick(final DevicesEntity bean, final LinearLayout ll_days) {
//        for (int j = 0; j < ll_days.getChildCount(); j++) {
//            View view = ll_days.getChildAt(j);
////            if (view instanceof RelativeLayout) {
////                RelativeLayout layout = ((RelativeLayout) view);
//            View v = ll_days.getChildAt(j);
//            if (v.getTag() != null) {
//                v.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        String tag = (String) v.getTag();
//                        switch (tag) {
//                            case "0":
////                                ((TextView) v).setTypeface(null, Typeface.BOLD);
////                                ((TextView) v).setTextColor(Color.CYAN);
//                                ((com.oob.view.TextView) v).setBackgroundResource(R.drawable.text_active_bg);
//                                v.setTag("1");
//                                break;
//                            case "1":
////                                ((TextView) v).setTextColor(Color.BLACK);
//                                ((com.oob.view.TextView) v).setBackgroundResource(R.drawable.text_bg);
//                                v.setTag("0");
//                                break;
//                        }
//                        boolean checked = ((Character) bean.getDays().charAt(7)).toString()
//                                .equals("1");
//                        bean.setDays(getFinalDays(checked, ll_days));
//                    }
//                });
//            }
////            }
//        }
//    }
//
//    private String getSelectedDays(LinearLayout ll_days) {
//        StringBuilder builder = new StringBuilder();
//        for (int j = 0; j < ll_days.getChildCount(); j++) {
//            View v = ll_days.getChildAt(j);
//            if (v.getTag() != null) {
//                builder.append((String) v.getTag());
//            }
//
//        }
//        return builder.toString();
//    }
//
//    @Override
//    public int getItemCount() {
//        return roomItemList.size();
//        //(null != roomItemList ? roomItemList.size() : 0);
//    }
//
//    private String getTimeFromMillis(long millis) {
//        // New date object from millis
//        Date date = new Date(millis);
//        // formattter
//        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
//        // Pass date object
//        String formatted = df.format(date);
//        return formatted;
//    }
//
//
//    public static String hhmmssToStringDate(String hhmmss) {
//        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
//        try {
//            Date d = sdf.parse(hhmmss);
//            return new SimpleDateFormat("HH:mm:ss").format(d);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public static Long hhmmssToDateMillis(String hhmmss) {
//        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
//        try {
//            Date d = sdf.parse(hhmmss);
//            return d.getTime();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public void updateContentFromDB() {
//        this.roomItemList = DevicesEntity.getAll();
//        this.roomDevice.setDevices(roomItemList);
//        notifyDataSetChanged();
//    }
//
//    @Override
//    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd,
//            int minuteEnd) {
//        System.out.println("TAG " + view.getTag());
//        System.out.println("TAG " + view.getTag());
//        int hour, endHour;
//        String ampm, endAMPM, FromDate, ToDate;
//
//        if (picker == 1) {
//            System.out.println("hourOfDay " + hourOfDay);
//            System.out.println("minute " + minute);
//            System.out.println("hourOfDayEnd " + hourOfDayEnd);
//            System.out.println("minuteEnd " + minuteEnd);
//            if (hourOfDay > 12) {
//                hour = hourOfDay - 12;
//                ampm = "pm";
//            } else {
//                hour = hourOfDay;
//                ampm = "am";
//            }
//            if (hourOfDayEnd > 12) {
//                endHour = hourOfDayEnd - 12;
//                endAMPM = "pm";
//            } else {
//                endHour = hourOfDayEnd;
//                endAMPM = "am";
//            }
//            FromDate = hour + ":" + minute + " " + ampm;
//            ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
//            roomItemList.get(position).setTIMA(FromDate);
//            roomItemList.get(position).setTIMB(ToDate);
//            RoomRecyclerAdapterOld.this.notifyDataSetChanged();
////            roomItemList.get(position)
//        } else {
//            System.out.println("hourOfDay =2   " + hourOfDay);
//            System.out.println("minute =2   " + minute);
//            System.out.println("hourOfDayEnd =2   " + hourOfDayEnd);
//            System.out.println("minuteEnd =2   " + minuteEnd);
//        }
//
//    }
//
//    public void setMultiselectEnable(boolean value) {
//        isMultiSelect = value;
//        notifyDataSetChanged();
//    }
//
//    public void AddData(ArrayList<DevicesEntity> data) {
//        roomItemList.addAll(data);
//        notifyDataSetChanged();
//
//    }
//
//
//    class RoomViewHolder extends RecyclerView.ViewHolder {
//
////        protected RangeSeekBar<Integer> rangeBar_dimmer;
//
//        //        protected RangeSeekBar<Long> rangeBar_timer;
//        protected SeekBar seekDimmer;
//
//        protected ImageView ivIcon, im_cl, ivBuzer, ivColor;
//
//        protected TextView tvName, tv_fromTimer1, tv_toTimer1, tv_fromTimer2, tv_toTimer2,
//                tv_Timer1, tv_Timer2, tvSeekValue;
//
//        protected CheckBox sw_device;
//
//        protected com.oob.view.CheckBox cbSelect;
//
//        protected com.oob.view.Button btn_save;
//
//        //        protected CheckBox chk_repeat;
//        protected LinearLayout ll_days;
//
//        protected LinearLayout ll_main;
//
//        protected RelativeLayout layHeader;
//
//        protected LinearLayout layDetail, layTimer1, layTimer2;
//
//        public RoomViewHolder(View view) {
//            super(view);
//            ll_main = (LinearLayout) view;
////            rangeBar_dimmer = (RangeSeekBar<Integer>) view.findViewById(R.id.rangeBar_dimmer);
//            seekDimmer = (SeekBar) view.findViewById(R.id.seekBar);
////            rangeBar_timer = (RangeSeekBar<Long>) view.findViewById(R.id.rangeBar_timer);
//            btn_save = (com.oob.view.Button) view.findViewById(R.id.btn_save);
//            ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
//            im_cl = (ImageView) view.findViewById(R.id.im_cl);
//            ivBuzer = (ImageView) view.findViewById(R.id.ivBuzzer);
//            ivColor = (ImageView) view.findViewById(R.id.ivColor);
//            tvName = (TextView) view.findViewById(R.id.tvName);
//            tv_fromTimer1 = (TextView) view.findViewById(R.id.tvTimer1From);
//            tv_toTimer1 = (TextView) view.findViewById(R.id.tvTimer1to);
//            tv_fromTimer2 = (TextView) view.findViewById(R.id.tvTimer2From);
//            tv_toTimer2 = (TextView) view.findViewById(R.id.tvTimer2to);
//            tv_Timer1 = (TextView) view.findViewById(R.id.tvTimer1);
//            tv_Timer2 = (TextView) view.findViewById(R.id.tvTimer2);
//            sw_device = (CheckBox) view.findViewById(R.id.sw_device);
//            cbSelect = (com.oob.view.CheckBox) view.findViewById(R.id.checkbox);
////            chk_repeat = (CheckBox) view.findViewById(R.id.chk_repeat);
//            ll_days = (LinearLayout) view.findViewById(R.id.ll_days);
//            layHeader = (RelativeLayout) view.findViewById(R.id.layHeader);
//            layDetail = (LinearLayout) view.findViewById(R.id.layDetail);
//            layTimer1 = (LinearLayout) view.findViewById(R.id.layTimer1);
//            layTimer2 = (LinearLayout) view.findViewById(R.id.layTimer2);
//            tvSeekValue = (TextView) view.findViewById(R.id.tvSeekValue);
//
////            rangeBar_timer.setRangeValues(minDate.getTime(), maxDate.getTime());
////            rangeBar_timer.setNotifyWhileDragging(true);
//
//        }
//    }
//}