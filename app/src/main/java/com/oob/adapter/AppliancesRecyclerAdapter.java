package com.oob.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.oob.OOBApplication;
import com.oob.SocketServerTask;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.tools.WifiUtils;
import com.oob.view.EnhancedCheckBox;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.List;


public class AppliancesRecyclerAdapter
        extends RecyclerView.Adapter<AppliancesRecyclerAdapter.securityViewHolder> {

    TypedArray imgs;

    List<entDevice> appliances = new ArrayList<>();

    Context context;

    boolean allOnOff, isOn;

    dbHelperOperations db;

    public AppliancesRecyclerAdapter(Context context, List<entDevice> sensors) {
        this.context = context;
        this.appliances = sensors;
        db = new dbHelperOperations(context);
        for (entDevice device : appliances) {
            device.setROOM_NAME(db.getRoomName(device.getROOM_Id()));
        }

    }

    @Override
    public securityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.raw_appliances, viewGroup, false);
        return new securityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final securityViewHolder customViewHolder, final int i) {

        customViewHolder.tvName.setText(appliances.get(i).getDEVICE_NAME());
        customViewHolder.tvRoomName.setText(appliances.get(i).getROOM_NAME());

        customViewHolder.ivIcon.setImageResource(
                Constants.getImageForDevice(context,
                        appliances.get(i).getDEVICE_NAME().toLowerCase()));
        customViewHolder.cbOnOff.setChecked(appliances.get(i).getSTT() == 1);
        if (allOnOff&&appliances.get(i).getSTT() == 1&&appliances.get(i).getCL() == 0) {
            customViewHolder.cbOnOff.setCheckedProgrammatically(isOn);
        } else {

        }
        customViewHolder.cbOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appliances.get(i).setSTT(appliances.get(i).getSTT() == 0 ? 1 : 0);
                appliances.get(i).setALL_STT(appliances.get(i).getSTT());
                Log.d("TEST", "from check");
                if (customViewHolder.cbOnOff.isShown()) {
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                            + Constants
                            .getRoomcode(appliances.get(i).getROOM_Id())
                            + Constants
                            .getModuleAndDevicecode(appliances.get(i).getMODULE())
                            + appliances.get(i)
                            .getSTT() + "D" + Constants
                            .getThreeDigit(appliances.get(i).getDIM()) + "RX"
                            + appliances.get(i).getRGBSTT());
                    db.updateDevice(appliances.get(i));
                } else {

                }


            }
        });

    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(context)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(context, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }


    @Override
    public int getItemCount() {
        return appliances.size();
    }

    public void updateData(List<entDevice> al_data) {
        appliances = al_data;
        notifyDataSetChanged();
    }

    public void sendAllOnOFFData(boolean isOn) {
        this.isOn = isOn;
        allOnOff = true;
        appliances = db.getListOfDevices(-2, true);
        for (entDevice device : appliances) {
            device.setROOM_NAME(db.getRoomName(device.getROOM_Id()));
        }
        notifyDataSetChanged();
    }


    class securityViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivIcon;

        protected TextView tvName, tvRoomName;

        protected EnhancedCheckBox cbOnOff;


        public securityViewHolder(View view) {
            super(view);

            ivIcon = (ImageView) view.findViewById(R.id.ivLogo);
            tvName = (TextView) view.findViewById(R.id.tvAppliancesName);
            tvRoomName = (TextView) view.findViewById(R.id.tvRoomName);
            cbOnOff = (EnhancedCheckBox) view.findViewById(R.id.cbOnOff);

        }
    }
}
