package com.oob.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;


import com.oob.activity.RoomSelectionActivity;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entRooms;
import com.oob.fragment.PicModeSelectDialogFragment;
import com.oob.tools.Constants;
import com.oob.view.CircleImageView;
import com.oob.view.EditText;
import com.oob.view.EnhancedCheckBox;
import com.oob.view.TextView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class RoomSelectionRecyclerAdapter
        extends RecyclerView.Adapter<RoomSelectionRecyclerAdapter.securityViewHolder>
        implements PicModeSelectDialogFragment.IPicModeSelectListener {


    List<entRooms> rooms = new ArrayList<>();

    Context context;

    dbHelperOperations db;

    public RoomSelectionRecyclerAdapter(Context context, List<entRooms> sensors) {
        this.context = context;
        this.rooms = sensors;
        db = new dbHelperOperations(context);

    }

    @Override
    public securityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_room_selection, viewGroup, false);
        return new securityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final securityViewHolder customViewHolder, final int i) {

        customViewHolder.tvName.setText(rooms.get(i).getROOM_NAME());

//        if (!rooms.get(i).getPROFILE_DATA().equalsIgnoreCase("")) {
        Picasso.with(context).load(new File(rooms.get(i).getPROFILE_DATA())).placeholder(R.drawable.app_logo).error(R.drawable.app_logo).into(customViewHolder.ivProfile);
//        Glide.with(context)
//                .load(new File(rooms.get(i).getPROFILE_DATA()))
//                .placeholder(R.drawable.app_logo).error(R.drawable.app_logo)
//                .skipMemoryCache(false)
//                .into(customViewHolder.ivProfile);
//            customViewHolder.ivProfile
//                    .setImageBitmap(Constants.ByteToBitmap(rooms.get(i).getPROFILE_DATA()));
//        } else {
//            customViewHolder.ivProfile.setImageResource(R.drawable.app_logo);
//        }
        customViewHolder.cbSelect.setCheckedProgrammatically(rooms.get(i).getACTIVE() == 1);
//        customViewHolder.cbSelect.setText(curtains.get(i).getSENSOR_LOCATION());
//        customViewHolder.tvAction.setText(curtains.get(i).getSENSOR_LIGHT());
//        customViewHolder.ivIcon
//                .setImageResource(Constants.getImage(context, curtains.get(i).getSENSOR_IMAGE()));
//
        customViewHolder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNameDialog(customViewHolder.tvName, i);
            }
        });
        customViewHolder.ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddProfilePicDialog(i);
            }
        });
        customViewHolder.cbSelect
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        rooms.get(i).setACTIVE(isChecked ? 1 : 0);
                        db.updateRoom(rooms.get(i));

                    }
                });
//        customViewHolder.tvAction.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

    }


    @Override
    public int getItemCount() {
        return rooms.size();
    }

    private void showAddProfilePicDialog(int pos) {
        PicModeSelectDialogFragment dialogFragment = new PicModeSelectDialogFragment();
        dialogFragment.setiPicModeSelectListener(this, pos);
        dialogFragment.show(((RoomSelectionActivity) context).getSupportFragmentManager(),
                "picModeSelector");
    }

    private void openNameDialog(final com.oob.view.TextView tv,
                                final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_apliences_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        edtName.setText(tv.getText().toString());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                System.out.println("IS TRUE");
                rooms.get(position).setROOM_NAME(edtName.getText().toString());
                db.updateRoom(rooms.get(position));
                db.updateRoomName(rooms.get(position).getROOM_Id(), edtName.getText().toString());
//                    roomItemList = db.getListOfDevices(roomItemList.get(position).getROOM_Id());
                notifyDataSetChanged();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });


    }

    @Override
    public void onPicModeSelected(String mode, int position) {
        String action = mode.equalsIgnoreCase(Constants.PicModes.CAMERA)
                ? Constants.IntentExtras.ACTION_CAMERA : Constants.IntentExtras.ACTION_GALLERY;
        ((RoomSelectionActivity) context).actionProfilePic(action, position);
    }

    public void updateData(String byteData, int position) {
        rooms.get(position).setPROFILE_DATA(byteData);
        this.notifyDataSetChanged();
    }

    class securityViewHolder extends RecyclerView.ViewHolder {

        protected CircleImageView ivProfile;

        protected EnhancedCheckBox cbSelect;

        protected TextView tvName;


        public securityViewHolder(View view) {
            super(view);

            tvName = (TextView) view.findViewById(R.id.tvName);
            ivProfile = (CircleImageView) view.findViewById(R.id.ivProfile);
            cbSelect = (EnhancedCheckBox) view.findViewById(R.id.cbSelect);

        }
    }

}
