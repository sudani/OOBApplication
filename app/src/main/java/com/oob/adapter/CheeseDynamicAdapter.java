package com.oob.adapter;

/**
 * Author: alex askerov
 * Date: 9/9/13
 * Time: 10:52 PM
 */

import com.oob.app.R;
import com.oob.entity.entDashboard;
import com.oob.tools.Constants;

import org.askerov.dynamicgrid.BaseDynamicGridAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Author: alex askerov
 * Date: 9/7/13
 * Time: 10:56 PM
 */

public class CheeseDynamicAdapter extends BaseDynamicGridAdapter {


    public CheeseDynamicAdapter(Context context, List<?> items, int columnCount) {
        super(context, items, columnCount);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CheeseViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dashboard_row, null);
            holder = new CheeseViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (CheeseViewHolder) convertView.getTag();
        }
        holder.build((entDashboard) getItem(position));
        return convertView;
    }

    private class CheeseViewHolder {

        private TextView titleText;

        private ImageView image;

        private CheeseViewHolder(View view) {
            titleText = (TextView) view.findViewById(R.id.tvName);
            image = (ImageView) view.findViewById(R.id.ivLogo);
        }

        void build(entDashboard title) {
            titleText.setText(title.getITEM_NAME());
            image.setImageResource(Constants.getImage(getContext(),title.getITEM_IMAGE()));

        }

    }


}