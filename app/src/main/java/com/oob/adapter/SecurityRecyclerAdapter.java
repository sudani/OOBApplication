package com.oob.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.entity.entRooms;
import com.oob.entity.entSecurity;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.tools.Utils;
import com.oob.view.EditText;
import com.oob.view.TextView;
import com.oob.wheel.ArrayWheelAdapter;
import com.oob.wheel.WheelView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SecurityRecyclerAdapter
        extends RecyclerView.Adapter<SecurityRecyclerAdapter.securityViewHolder> {

    //    TypedArray imgs;
    String wheelSenseors[] = new String[]{};

    String wheelActions[];

    List<String> wheelActionsList = new ArrayList<>();

    List<entSecurity> sensors = new ArrayList<>();

    List<entDevice> devices = new ArrayList<>();

    Context context;

    dbHelperOperations db;

    public SecurityRecyclerAdapter(Context context, List<entSecurity> sensors) {
        this.context = context;
        db = new dbHelperOperations(context);
//        imgs = context.getResources().obtainTypedArray(R.array.security_image);
        this.sensors = sensors;
        wheelSenseors = context.getResources().getStringArray(R.array.security_sensor);

//        for (int i = 0; i < sensors.size(); i++) {
//            wheelSenseors[i] = sensors.get(i).getSENSOR_NAME();
//        }
        devices = db.getListOfDevices(-2, true);
        for (int i = 0; i < devices.size(); i++) {
            String data = devices.get(i).getDEVICE_NAME().toLowerCase() + ",(" + db
                    .getRoomName(devices.get(i).getROOM_Id()).toLowerCase() + ")";
//            Log.e("data ", data);
            wheelActionsList.add(data);
        }
        wheelActionsList.addAll(Arrays
                .asList(context.getResources().getStringArray(R.array.security_sensor_action)));

        wheelActions = new String[wheelActionsList.size()];
        for (int i = 0; i < wheelActionsList.size(); i++) {
            wheelActions[i] = wheelActionsList.get(i);
        }
    }

    @Override
    public securityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_sensors, viewGroup, false);
        return new securityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final securityViewHolder customViewHolder, final int i) {

        customViewHolder.tvSensor.setText(sensors.get(i).getSENSOR_NAME());
        customViewHolder.tvLocation.setText(sensors.get(i).getSENSOR_LOCATION());
        customViewHolder.tvAction.setText(sensors.get(i).getACTION_NAME());
        customViewHolder.ivIcon.setImageResource(
                Constants.getImageForSensor(context,
                        sensors.get(i).getSENSOR_NAME().toLowerCase()));
//        customViewHolder.ivIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                System.out.println(
//                        "room " + sensors.get(i).getROOM_CODE() + " module " + sensors.get(i)
//                                .getMODULE_CODE());
//            }
//        });
        customViewHolder.ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSensorDialog(i);
            }
        });
//
        customViewHolder.tvSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSensorNameDialog(i);
            }
        });
        customViewHolder.tvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLocationDialog(i);
            }
        });
        customViewHolder.tvAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActionValueDialog(i);
            }
        });
        customViewHolder.layMain.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                deleteDialog(i);
                return true;
            }
        });
        customViewHolder.layMain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (sensors.get(i).getACTION_NAME()!=null){
                enableDialog(i);}
            }
        });
        System.out.println("ENABLE " + sensors.get(i).getIS_ENAMBLE());

        if (sensors.get(i).getIS_ENAMBLE() == 0) {
            customViewHolder.tvAction.setTextColor(Color.BLACK);
            customViewHolder.tvLocation.setTextColor(Color.BLACK);
            customViewHolder.tvSensor.setTextColor(Color.BLACK);
            customViewHolder.ivIcon.setColorFilter(
                    Color.parseColor("#000000"),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
            customViewHolder.ivMenu.setColorFilter(
                    Color.parseColor("#000000"),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
            customViewHolder.tvAction.setTextColor(context.getResources().getColor(R.color.blue));
            customViewHolder.tvLocation.setTextColor(context.getResources().getColor(R.color.blue));
            customViewHolder.tvSensor.setTextColor(context.getResources().getColor(R.color.blue));
            customViewHolder.ivIcon.clearColorFilter();
            customViewHolder.ivMenu.clearColorFilter();
        }
    }

    private void deleteDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete Sensor!");
        builder.setMessage("Do you want to delete this sensor?");
        builder.setIcon(R.drawable.app_logo);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.deleteSensor(sensors.get(position).getSECURITY_ID());
                sensors.remove(position);
                notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setCancelable(true);
        builder.show();
    }

    private void enableDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (sensors.get(position).getIS_ENAMBLE() == 1) {
            builder.setTitle("Disable Sensor!");
            builder.setMessage("Do you want to disable this sensor?");
        } else {
            builder.setTitle("Enable Sensor!");
            builder.setMessage("Do you want to enable this sensor?");
        }
        builder.setIcon(R.drawable.app_logo);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (sensors.get(position).getIS_ENAMBLE() == 1) {
                    sensors.get(position).setIS_ENAMBLE(0);
                } else {
                    sensors.get(position).setIS_ENAMBLE(1);
                }
                db.updateSensor(sensors.get(position));
                notifyDataSetChanged();
                sendStringAfterEnableState(position);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setCancelable(true);
        builder.show();
    }

    private void sendStringAfterEnableState(int posi) {
        String deviceCode = "";
        for (int i = 0; i < devices.size(); i++) {
            String data = devices.get(i).getDEVICE_NAME().toLowerCase() + ",(" + db
                    .getRoomName(devices.get(i).getROOM_Id()).toLowerCase() + ")";
            if (data.equalsIgnoreCase(sensors.get(posi).getACTION_NAME())) {
                deviceCode = Constants.getDevicecode(devices.get(i).getMODULE());
                break;
            }
        }
            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "SS" + Utils
                    .getSensorCode(sensors.get(posi).getSENSOR_NAME(),
                            sensors.get(posi).getSENSOR_NUMBER()) + sensors.get(posi)
                    .getROOM_CODE() + sensors.get(posi).getMODULE_CODE() + Utils
                    .getDevideCode(deviceCode, sensors.get(posi).getACTION_NAME()) + "EN" + sensors.get(posi).getIS_ENAMBLE());
        }

    @Override
    public int getItemCount() {
        return sensors.size();
    }

    public void addSensor(entSecurity entSecurity) {
        sensors.add(entSecurity);
        System.out.println("ENABLE >> " + entSecurity.getIS_ENAMBLE());
        notifyDataSetChanged();
    }

    class securityViewHolder extends RecyclerView.ViewHolder {

        protected ImageView ivIcon;

        protected RelativeLayout layMain;

        protected ImageView ivMenu;

        protected TextView tvSensor, tvLocation, tvAction;

        public securityViewHolder(View view) {
            super(view);

            layMain = (RelativeLayout) view.findViewById(R.id.card);
            ivIcon = (ImageView) view.findViewById(R.id.ivLogo);
            ivMenu = (ImageView) view.findViewById(R.id.ivMenu);
            tvSensor = (TextView) view.findViewById(R.id.tvName);
            tvLocation = (TextView) view.findViewById(R.id.tvLocation);
            tvAction = (TextView) view.findViewById(R.id.tvAction);

        }
    }

    private void openSensorDialog(
            final int position) {
        final String w1Value, w2Value;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_sensor_picker);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final WheelView w1 = (WheelView) dialog.findViewById(R.id.p1);
        final WheelView w2 = (WheelView) dialog.findViewById(R.id.p2);
        final EditText edtNumber = (EditText) dialog.findViewById(R.id.edtNumber);
        edtNumber.setText(sensors.get(position).getSENSOR_NUMBER());
        w1.setViewAdapter(
                new ArrayWheelAdapter(context, wheelSenseors, R.layout.simple_spiner_item_white));
        w1.setVisibleItems(2);
        w1.setCurrentItem(
                Arrays.asList(wheelSenseors).indexOf(sensors.get(position).getSENSOR_NAME()));
        w2.setViewAdapter(
                new ArrayWheelAdapter(context, wheelActions, R.layout.simple_spiner_item_white));
        w2.setVisibleItems(2);
        w2.setCurrentItem(
                Arrays.asList(wheelActions).indexOf(sensors.get(position).getACTION_NAME()));
        dialog.show();
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                int currentPosW1 = w1.getCurrentItem();
                int currentPosW2 = w2.getCurrentItem();
                System.out.println("W1 " + wheelSenseors[currentPosW1]);
                System.out.println("W2 " + wheelActions[currentPosW2]);
                sensors.get(position).setSENSOR_NAME(wheelSenseors[currentPosW1]);
                sensors.get(position).setACTION_NAME(wheelActions[currentPosW2]);
                sensors.get(position).setSENSOR_NUMBER(edtNumber.getText().toString());
//                sensors.get(position).setDEVICE_ID(devices.get(currentPosW2).getDEVICE_Id());
                db.updateSensor(sensors.get(position));
                SecurityRecyclerAdapter.this.notifyDataSetChanged();
//                if (isFromLighing(wheelActions[currentPosW2])) {
                selectRoomModuleDialog(position,currentPosW2);
//                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    private boolean isFromLighing(String wheelAction) {
//        String action[] = context.getResources().getStringArray(R.array.security_sensor_action_lighting);
//        for (int i = 0; i < action.length; i++) {
//            if (action[i].equalsIgnoreCase(wheelAction)) {
//                return true;
//            }
//        }
        if (wheelAction.equalsIgnoreCase("SMS")) {
            return false;
        } else if (wheelAction.equalsIgnoreCase("PHONE ALERT")) {
            return false;
        }
        return true;
    }

    private void openLocationDialog(final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);

        edtName.setText(sensors.get(position).getSENSOR_LOCATION());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sensors.get(position).setSENSOR_LOCATION(edtName.getText().toString());
                db.updateSensor(sensors.get(position));
                SecurityRecyclerAdapter.this.notifyDataSetChanged();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    private void openActionValueDialog(final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        final TextInputLayout inputLayout = (TextInputLayout) dialog
                .findViewById(R.id.ilayUsername);
        inputLayout.setHint("Action Value");
        edtName.setInputType(InputType.TYPE_CLASS_PHONE);
        edtName.setText(sensors.get(position).getACTION_VALUE());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sensors.get(position).setACTION_VALUE(edtName.getText().toString());
                db.updateSensor(sensors.get(position));
                SecurityRecyclerAdapter.this.notifyDataSetChanged();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    private void openSensorNameDialog(final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        edtName.setText(sensors.get(position).getSENSOR_NAME());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sensors.get(position).setSENSOR_NAME(edtName.getText().toString());
                db.updateSensor(sensors.get(position));
                SecurityRecyclerAdapter.this.notifyDataSetChanged();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    private void selectRoomModuleDialog(
            final int posi, final int currentPosW2) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_tv_picker);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtModel);
        edtName.setVisibility(View.GONE);
        final AppCompatSpinner spinRooms = (AppCompatSpinner) dialog.findViewById(R.id.spinTvName);
        final AppCompatSpinner spinModules = (AppCompatSpinner) dialog
                .findViewById(R.id.spintvProtocol);
 final AppCompatSpinner spinOnOff = (AppCompatSpinner) dialog
                .findViewById(R.id.spinOnOff);
        spinOnOff.setVisibility(View.VISIBLE);

        final List<entRooms> list = db.getListOfRooms(true);
        final String[] arrayRooms = new String[(list.size())];
        for (int i = 0; i < list.size(); i++) {
            arrayRooms[i] = list.get(i).getROOM_NAME();
        }
        final ArrayAdapter adapterRooms = new ArrayAdapter<String>(context,
                R.layout.simple_spiner_item_white, arrayRooms);
        adapterRooms.setDropDownViewResource(R.layout.simple_spiner_item_white);
 final ArrayAdapter adapterOnOff = new ArrayAdapter<String>(context,
                R.layout.simple_spiner_item_white, new String[]{"OFF","ON"});
        adapterOnOff.setDropDownViewResource(R.layout.simple_spiner_item_white);

        spinRooms.setAdapter(adapterRooms);
        spinOnOff.setAdapter(adapterOnOff);
        if (currentPosW2<devices.size()){

            String room =db.getRoomName(devices.get(currentPosW2).getROOM_Id());
            int pos =(adapterRooms.getPosition(room));
            System.out.println("room "+room+"  pos "+pos);
            spinRooms.setSelection(pos);
        }
        spinRooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] arrayModules = new String[list.get(position).getMODULE_COUNT()];
                for (int i = 0; i < arrayModules.length; i++) {
                    arrayModules[i] = "MODULE " + (i + 1);
                }
                ArrayAdapter adapterProtocol = new ArrayAdapter<String>(context,
                        R.layout.simple_spiner_item_white, arrayModules);
                adapterProtocol.setDropDownViewResource(R.layout.simple_spiner_item_white);

                spinModules.setAdapter(adapterProtocol);
                sensors.get(posi)
                        .setROOM_CODE("R" + Constants.getTwoDigit(list.get(position).getROOM_Id()));
                if (currentPosW2<devices.size()){
                    String module = "MODULE "+devices.get(currentPosW2).getMODULE().split(",")[1];
                    int po =adapterProtocol.getPosition(module);
                    System.out.println("module "+module);
                    spinModules.setSelection(po);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinModules.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sensors.get(posi).setMODULE_CODE("M" + Constants.getTwoDigit((position + 1)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                System.out.println("IS TRUE");
                String deviceCode = "";
                db.updateSensor(sensors.get(posi));
                for (int i = 0; i < devices.size(); i++) {
                    String data = devices.get(i).getDEVICE_NAME().toLowerCase() + ",(" + db
                            .getRoomName(devices.get(i).getROOM_Id()).toLowerCase() + ")";
                    if (data.equalsIgnoreCase(sensors.get(posi).getACTION_NAME())) {
                        deviceCode = Constants.getDevicecode(devices.get(i).getMODULE());
                        System.out.println("deviceCode " + deviceCode);
                        break;
                    }
                }
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + "SS" + Utils
                        .getSensorCode(sensors.get(posi).getSENSOR_NAME(),
                                sensors.get(posi).getSENSOR_NUMBER()) + sensors.get(posi)
                        .getROOM_CODE() + sensors.get(posi).getMODULE_CODE() + Utils
                        .getDevideCode(deviceCode, sensors.get(posi).getACTION_NAME())+"EN"+sensors.get(posi).getIS_ENAMBLE()+"AC"+spinOnOff.getSelectedItemPosition());
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json  " + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(context)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(context, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

}
