package com.oob.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entGroupAppliances;
import com.oob.view.EditText;

import java.util.List;

import static com.oob.app.R.id.textView;

public class GroupAppliancesRecyclerAdapter
        extends BaseQuickAdapter<entGroupAppliances, BaseViewHolder> {

    Context mContext;
    dbHelperOperations db;

    public GroupAppliancesRecyclerAdapter(Context context, int layoutResId, List data) {
        super(layoutResId, data);
        mContext = context;
        db = new dbHelperOperations(context);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final entGroupAppliances item) {
        helper.setText(R.id.tvGroupName, item.getName());
//        TextView textView = helper.getView(R.id.tvGroupName);
        helper.addOnClickListener(R.id.tvGroupName);
        helper.addOnClickListener(R.id.ivOnOff);
        helper.setImageResource(R.id.ivOnOff, item.getStatus() == 1 ? R.drawable.on : R.drawable.off);
//        textView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                openNameDialog(item, helper.getLayoutPosition());
//            }
//        });

    }


}
