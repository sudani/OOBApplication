package com.oob.adapter;

import com.oob.activity.ACRemoteSetupActivity;
import com.oob.activity.TVRemoteSetupActivity;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entAC;
import com.oob.entity.entRooms;
import com.oob.entity.entTV;
import com.oob.view.EditText;
import com.oob.view.TextView;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;


public class ACSetupRecyclerAdapter
        extends RecyclerView.Adapter<ACSetupRecyclerAdapter.securityViewHolder> {


    List<entAC> acList = new ArrayList<>();

    Context context;

    dbHelperOperations db;

    public ACSetupRecyclerAdapter(Context context, List<entAC> sensors) {
        this.context = context;
        this.acList = sensors;
        db = new dbHelperOperations(context);

    }

    @Override
    public securityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_ac_setup, viewGroup, false);
        return new securityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final securityViewHolder customViewHolder, final int i) {

        customViewHolder.tvRoomName.setText(acList.get(i).getROOM_NAME());
        customViewHolder.tvTVName.setText(acList.get(i).getAC_NAME());
        customViewHolder.tvRoomName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRoomDialog(i);
            }
        });
        customViewHolder.tvTVName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTVDialog(i);

            }
        });

    }

    private void showRoomDialog(final int pos) {
        final List<entRooms> list = db.getListOfRooms(true);
        final String[] array = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i).getROOM_NAME();
        }

        new AlertDialog.Builder(context)
                .setSingleChoiceItems(array, -1, null)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog) dialog).getListView()
                                .getCheckedItemPosition();
                        if (selectedPosition != -1) {
                            acList.get(pos).setROOM_NAME(array[selectedPosition]);
                            acList.get(pos).setROOM_Id(list.get(selectedPosition).getROOM_Id());
                            ACSetupRecyclerAdapter.this.notifyDataSetChanged();
                            db.updateAc(acList.get(pos));
                        }                        // Do something useful withe the position of the selected radio button
                    }
                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })
                .show();
    }

    @Override
    public int getItemCount() {
        return acList.size();
    }


    private void openTVDialog(
            final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_tv_picker);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final TextView tvTitle= (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setText("Select AC");
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtModel);
        edtName.setVisibility(View.GONE);
        final AppCompatSpinner spinBrand = (AppCompatSpinner) dialog.findViewById(R.id.spinTvName);
        final AppCompatSpinner spinProtocol = (AppCompatSpinner) dialog
                .findViewById(R.id.spintvProtocol);
        spinProtocol.setVisibility(View.GONE);
        final String[] brand = context.getResources().getStringArray(R.array.ac_brands);
        final String[] protocol = context.getResources().getStringArray(R.array.protocol);
        ArrayAdapter adapter = new ArrayAdapter<String>(context,
                R.layout.simple_spiner_item_white, brand);
        adapter.setDropDownViewResource(R.layout.simple_spiner_item_white);
//        ArrayAdapter adapterProtocol = new ArrayAdapter<String>(context,
//                R.layout.simple_spiner_item_white, protocol);
//        adapterProtocol.setDropDownViewResource(R.layout.simple_spiner_item_white);
        spinBrand.setAdapter(adapter);
//        spinProtocol.setAdapter(adapterProtocol);


        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
//        Button btn_remote_setup = (Button) dialog.findViewById(R.id.btnSetupRemot);
        // if decline button is clicked, close the custom dialog
//        btn_remote_setup.setVisibility(View.VISIBLE);
//        btn_remote_setup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                System.out.println("IS TRUE");
//                acList.get(position).setAC_NAME(brand[spinBrand.getSelectedItemPosition()]);
//                acList.get(position)
//                        .setAC_PROTOCOL(protocol[spinProtocol.getSelectedItemPosition()]);
//                ACSetupRecyclerAdapter.this.notifyDataSetChanged();
//                db.updateAc(acList.get(position));
//                Intent intent = new Intent(context, ACRemoteSetupActivity.class);
//                intent.putExtra("id", acList.get(position).getAC_Id());
//                context.startActivity(intent);
//            }
//        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                System.out.println("IS TRUE");
                acList.get(position).setAC_NAME(brand[spinBrand.getSelectedItemPosition()]);
//                acList.get(position)
//                        .setAC_PROTOCOL(protocol[spinProtocol.getSelectedItemPosition()]);
                ACSetupRecyclerAdapter.this.notifyDataSetChanged();
                db.updateAc(acList.get(position));
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });


    }



    public void addAC(entAC entac) {
        acList.add(entac);
        notifyDataSetChanged();
    }


    class securityViewHolder extends RecyclerView.ViewHolder {


        protected TextView tvRoomName;

        protected TextView tvTVName;

        protected TextView tvSetName;


        public securityViewHolder(View view) {
            super(view);

            tvRoomName = (TextView) view.findViewById(R.id.tvRoomName);
            tvTVName = (TextView) view.findViewById(R.id.tvTvName);
            tvSetName = (TextView) view.findViewById(R.id.tvSetName);


        }
    }

}
