package com.oob.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.view.EnhancedCheckBox;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BackLightRecyclerAdapter
        extends RecyclerView.Adapter<BackLightRecyclerAdapter.securityViewHolder> {

    List<entDevice> modules = new ArrayList<>();

    Context context;

    dbHelperOperations db;

    ArrayList<String> colors = new ArrayList<>();

    public BackLightRecyclerAdapter(Context context, List<entDevice> sensors) {
        this.context = context;
        this.modules = sensors;
        colors = new ArrayList<>(
                Arrays.asList(context.getResources().getStringArray(R.array.light_colors)));
        db = new dbHelperOperations(context);
        for (entDevice device : modules) {
            device.setROOM_NAME(db.getRoomName(device.getROOM_Id()));
        }

    }

    @Override
    public securityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.raw_back_light, viewGroup, false);
        return new securityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final securityViewHolder customViewHolder, final int i) {

        customViewHolder.tvName.setText(modules.get(i).getROOM_NAME());
        customViewHolder.tvModule.setText("M" + modules.get(i).getMODULE().split(",")[1]);

        customViewHolder.cbOnOff
                .setCheckedProgrammatically(modules.get(i).getMODULE_STT() == 1 ? true : false);
        customViewHolder.ivIcon
                .setColorFilter(
                        Color.parseColor(colors.get(modules.get(i).getBACKLIGHT_RGB_STT())),
                        android.graphics.PorterDuff.Mode.MULTIPLY);
        customViewHolder.seekBar.setProgress(modules.get(i).getBACKLIGHT_DIM_STT());
        customViewHolder.tvSeekValue.setText(Constants.getTwoDigit(modules.get(i).getBACKLIGHT_DIM_STT()) + " %");
        customViewHolder.cbOnOff
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        modules.get(i).setMODULE_STT(isChecked ? 1 : 0);
                        db.updateModuleBackLight(modules.get(i).getDEVICE_Id(), isChecked ? 1 : 0);
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                + Constants
                                .getRoomcode(modules.get(i).getROOM_Id())
                                + customViewHolder.tvModule.getText().toString()+"BKL" +  modules.get(i).getMODULE_STT() +modules.get(i).getBACKLIGHT_RGB_STT()+Constants.getTwoDigit(modules.get(i).getBACKLIGHT_DIM_STT())+"%");
                    }
                });
        customViewHolder.ivIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customViewHolder.ivIcon.setTag(i);
                if (modules.get(i).getBACKLIGHT_RGB_STT() < 7) {
                    modules.get(i).setBACKLIGHT_RGB_STT(modules.get(i).getBACKLIGHT_RGB_STT() + 1);
                } else {
                    modules.get(i).setBACKLIGHT_RGB_STT(0);

                }
                customViewHolder.ivIcon
                        .setColorFilter(
                                Color.parseColor(colors.get(modules.get(i).getBACKLIGHT_RGB_STT())),
                                android.graphics.PorterDuff.Mode.MULTIPLY);
                db.updateModuleBackLightColor(modules.get(i).getDEVICE_Id(),
                        modules.get(i).getBACKLIGHT_RGB_STT());
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                        + Constants
                        .getRoomcode(modules.get(i).getROOM_Id())
                        + customViewHolder.tvModule.getText().toString()+"BKL" +  modules.get(i).getMODULE_STT() +modules.get(i).getBACKLIGHT_RGB_STT()+Constants.getTwoDigit(modules.get(i).getBACKLIGHT_DIM_STT())+"%");
            }
        });
        customViewHolder.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                customViewHolder.tvSeekValue.setText(progress + " %");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                modules.get(i).setBACKLIGHT_DIM_STT(seekBar.getProgress());
                db.updateModuleBackLightDim(modules.get(i).getDEVICE_Id(),
                        modules.get(i).getBACKLIGHT_DIM_STT());
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                        + Constants
                        .getRoomcode(modules.get(i).getROOM_Id())
                        + customViewHolder.tvModule.getText().toString()+"BKL" +  modules.get(i).getMODULE_STT() +modules.get(i).getBACKLIGHT_RGB_STT()+ Constants.getTwoDigit(modules.get(i).getBACKLIGHT_DIM_STT())+"%");
            }
        });

    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(context)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(context, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

    @Override
    public int getItemCount() {
        return modules.size();
    }

    class securityViewHolder extends RecyclerView.ViewHolder {

        protected TextView tvName, tvModule;

        protected EnhancedCheckBox cbOnOff;

        protected LinearLayout laySeek;

        protected TextView tvSeekValue;

        protected SeekBar seekBar;

        protected ImageView ivIcon;

        public securityViewHolder(View view) {
            super(view);

            tvName = (TextView) view.findViewById(R.id.tvRoomName);
            tvModule = (TextView) view.findViewById(R.id.tvModuleName);
            cbOnOff = (EnhancedCheckBox) view.findViewById(R.id.cbSelect);
            laySeek = (LinearLayout) view.findViewById(R.id.ll_seek);
            tvSeekValue = (TextView) view.findViewById(R.id.tvSeekValue);
            seekBar = (SeekBar) view.findViewById(R.id.seekBar);
            ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (laySeek.getVisibility() == View.VISIBLE) {
                        Constants.collapse(laySeek);
                    } else {
                        Constants.expand(laySeek);
                    }
                }
            });
        }
    }
}
