package com.oob.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entRooms;
import com.oob.entity.entTV;
import com.oob.view.EditText;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.List;


public class TVSetupRecyclerAdapter
        extends RecyclerView.Adapter<TVSetupRecyclerAdapter.securityViewHolder> {


    List<entTV> tvList = new ArrayList<>();

    Context context;

    dbHelperOperations db;

    public TVSetupRecyclerAdapter(Context context, List<entTV> sensors) {
        this.context = context;
        this.tvList = sensors;
        db = new dbHelperOperations(context);

    }

    @Override
    public securityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_tv_setup, viewGroup, false);
        return new securityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final securityViewHolder customViewHolder, final int i) {

        customViewHolder.tvRoomName.setText(tvList.get(i).getROOM_NAME());
        customViewHolder.tvTVName.setText(tvList.get(i).getTV_NAME());
        customViewHolder.tvSetName.setText(tvList.get(i).getSET_NAME());
        customViewHolder.tvRoomName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRoomDialog(i);
            }
        });
        customViewHolder.tvTVName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTVDialog(i);

            }
        });
        customViewHolder.tvSetName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSETDialog(i);
            }
        });
        customViewHolder.tvTVName.setTag(i);
        customViewHolder.tvRoomName.setTag(i);
        customViewHolder.tvSetName.setTag(i);
        customViewHolder.tvTVName.setOnLongClickListener(onLongClickListener);
        customViewHolder.tvRoomName.setOnLongClickListener(onLongClickListener);
        customViewHolder.tvSetName.setOnLongClickListener(onLongClickListener);

    }

    View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            System.out.println("Long clicked  " + v.getTag());
            deleteCurtainDialog((int) v.getTag());
            return true;
        }
    };

    private void deleteCurtainDialog(final int tag) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setIcon(R.drawable.app_logo);
        alertDialogBuilder.setTitle(tvList.get(tag).getTV_NAME());
        alertDialogBuilder.setMessage("Are you sure, You wanted to delete this TV?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        db.deleteTv(tvList.get(tag).getTV_Id());
                        tvList = db.getListTV();
                        notifyDataSetChanged();
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private void showRoomDialog(final int pos) {
        final List<entRooms> list = db.getListOfRooms(true);
        final String[] array = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i).getROOM_NAME();
        }

        new AlertDialog.Builder(context)
                .setSingleChoiceItems(array, -1, null)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog) dialog).getListView()
                                .getCheckedItemPosition();
                        if (selectedPosition != -1) {
                            tvList.get(pos).setROOM_NAME(array[selectedPosition]);
                            tvList.get(pos).setROOM_Id(list.get(selectedPosition).getROOM_Id());
                            TVSetupRecyclerAdapter.this.notifyDataSetChanged();
                        }                        // Do something useful withe the position of the selected radio button
                    }
                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })
                .show();
    }

    @Override
    public int getItemCount() {
        return tvList.size();
    }


    private void openTVDialog(
            final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_tv_picker);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final TextView tvTitle= (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setText("Select TV");
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtModel);
        final AppCompatSpinner spinBrand = (AppCompatSpinner) dialog.findViewById(R.id.spinTvName);
        final AppCompatSpinner spinProtocol = (AppCompatSpinner) dialog
                .findViewById(R.id.spintvProtocol);
        spinProtocol.setVisibility(View.GONE);
        final String[] brand = context.getResources().getStringArray(R.array.tv_brands);
//        final String[] protocol = context.getResources().getStringArray(R.array.protocol);
        ArrayAdapter adapter = new ArrayAdapter<String>(context,
                R.layout.simple_spiner_item_white, brand);
        adapter.setDropDownViewResource(R.layout.simple_spiner_item_white);
//        ArrayAdapter adapterProtocol = new ArrayAdapter<String>(context,
//                R.layout.simple_spiner_item_white, protocol);
//        adapterProtocol.setDropDownViewResource(R.layout.simple_spiner_item_white);
        spinBrand.setAdapter(adapter);
//        spinProtocol.setAdapter(adapterProtocol);

        edtName.setText(tvList.get(position).getTV_STT());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                System.out.println("IS TRUE");
                tvList.get(position).setTV_NAME(brand[spinBrand.getSelectedItemPosition()]);
//                tvList.get(position)
//                        .setTV_PROTOCOL(protocol[spinProtocol.getSelectedItemPosition()]);
                tvList.get(position).setTV_STT(edtName.getText().toString());
                TVSetupRecyclerAdapter.this.notifyDataSetChanged();
                db.updateTv(tvList.get(position));
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });


    }

    private void openSETDialog(
            final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_tv_picker);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtModel);
        final TextView tvTitle= (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setText("Select Setup Box");
        edtName.setVisibility(View.GONE);
        final AppCompatSpinner spinBrand = (AppCompatSpinner) dialog.findViewById(R.id.spinTvName);
        final AppCompatSpinner spinProtocol = (AppCompatSpinner) dialog
                .findViewById(R.id.spintvProtocol);
        spinProtocol.setVisibility(View.GONE);
        final String[] brand = context.getResources().getStringArray(R.array.set_up_box_brands);
        final String[] protocol = context.getResources().getStringArray(R.array.protocol);
        ArrayAdapter adapter = new ArrayAdapter<String>(context,
                R.layout.simple_spiner_item_white, brand);
        adapter.setDropDownViewResource(R.layout.simple_spiner_item_white);
//        ArrayAdapter adapterProtocol = new ArrayAdapter<String>(context,
//                R.layout.simple_spiner_item_white, protocol);
//        adapterProtocol.setDropDownViewResource(R.layout.simple_spiner_item_white);

        spinBrand.setAdapter(adapter);
//        spinProtocol.setAdapter(adapterProtocol);

        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
//        Button btn_remote_setup = (Button) dialog.findViewById(R.id.btnSetupRemot);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog
//        btn_remote_setup.setVisibility(View.VISIBLE);
//        btn_remote_setup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                System.out.println("IS TRUE");
//                tvList.get(position).setSET_NAME(brand[spinBrand.getSelectedItemPosition()]);
//                tvList.get(position)
//                        .setSET_PROTOCOL(protocol[spinProtocol.getSelectedItemPosition()]);
//                TVSetupRecyclerAdapter.this.notifyDataSetChanged();
//                db.updateTv(tvList.get(position));
//                Intent intent = new Intent(context, TVRemoteSetupActivity.class);
//                intent.putExtra("id", tvList.get(position).getTV_Id());
//                context.startActivity(intent);
//            }
//        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                System.out.println("IS TRUE");
                tvList.get(position).setSET_NAME(brand[spinBrand.getSelectedItemPosition()]);
//                tvList.get(position)
//                        .setSET_PROTOCOL(protocol[spinProtocol.getSelectedItemPosition()]);
                TVSetupRecyclerAdapter.this.notifyDataSetChanged();
                db.updateTv(tvList.get(position));
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });


    }


    public void addTV(entTV entTV) {
        tvList.add(entTV);
        notifyDataSetChanged();
    }


    class securityViewHolder extends RecyclerView.ViewHolder {


        protected TextView tvRoomName;

        protected TextView tvTVName;

        protected TextView tvSetName;


        public securityViewHolder(View view) {
            super(view);

            tvRoomName = (TextView) view.findViewById(R.id.tvRoomName);
            tvTVName = (TextView) view.findViewById(R.id.tvTvName);
            tvSetName = (TextView) view.findViewById(R.id.tvSetName);


        }
    }

}
