package com.oob.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.oob.SocketServerTask;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.interfaces.OnCustomerListChangedListener;
import com.oob.interfaces.OnStartDragListener;
import com.oob.tools.Constants;
import com.oob.tools.ItemTouchHelperAdapter;
import com.oob.tools.ItemTouchHelperViewHolder;
import com.oob.tools.Preferences;
import com.oob.tools.WifiUtils;
import com.oob.view.EnhancedCheckBox;
import com.oob.view.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PrioritySetupRecyclerAdapter
        extends RecyclerView.Adapter<PrioritySetupRecyclerAdapter.securityViewHolder>
        implements ItemTouchHelperAdapter {

    TypedArray imgs;

    List<entDevice> appliances = new ArrayList<>();

    Context context;

    dbHelperOperations db;

    private OnStartDragListener mDragStartListener;

    private OnCustomerListChangedListener mListChangedListener;

    public PrioritySetupRecyclerAdapter(Context context, List<entDevice> sensors,
            OnStartDragListener dragLlistener,
            OnCustomerListChangedListener listChangedListener) {
        this.context = context;
        this.appliances = sensors;
        mDragStartListener = dragLlistener;
        mListChangedListener = listChangedListener;
        db = new dbHelperOperations(context);

    }

    @Override
    public securityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.raw_priority_setup, viewGroup, false);
        return new securityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final securityViewHolder customViewHolder, final int i) {

        customViewHolder.tvName.setText(appliances.get(i).getDEVICE_NAME());
        customViewHolder.ivIcon.setImageResource(
                Constants.getImageForDevice(context,
                        appliances.get(i).getDEVICE_NAME().toLowerCase()));
        customViewHolder.cbOnOff
                .setColorFilter(Color.parseColor("#FFFFFF"),
                        android.graphics.PorterDuff.Mode.MULTIPLY);
        customViewHolder.cbOnOff.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                System.out.println("ACTION > " + MotionEventCompat.getActionMasked(event));
                System.out.println("ACTION >> " + event.getAction());
                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(customViewHolder);
                } else {
                    System.out.println("ACTION " + MotionEventCompat.getActionMasked(event));
                    mListChangedListener.onNoteListChanged(appliances);
                    mDragStartListener.onStopDrag(customViewHolder);

                }
                return false;
            }
        });
    }


    @Override
    public int getItemCount() {
        return appliances.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(appliances, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
//        mListChangedListener.onNoteListChanged(modules);
    }

    @Override
    public void onItemDismiss(int position) {

    }

    public List<entDevice> getDataList() {
        return appliances;
    }


    class securityViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {


        protected ImageView ivIcon;

        protected TextView tvName;

        protected ImageView cbOnOff;


        public securityViewHolder(View view) {
            super(view);
            ivIcon = (ImageView) view.findViewById(R.id.ivLogo);
            tvName = (TextView) view.findViewById(R.id.tvAppliancesName);
            cbOnOff = (ImageView) view.findViewById(R.id.cbOnOff);

        }

        @Override
        public void onItemSelected() {

        }

        @Override
        public void onItemClear() {

        }
    }
}
