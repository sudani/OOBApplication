package com.oob.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.Constants;

import java.util.List;

public class AppliancesSelectionAdapter
        extends BaseQuickAdapter<entDevice, BaseViewHolder> {

    Context mContext;
    dbHelperOperations db;

    public AppliancesSelectionAdapter(Context context, int layoutResId, List data) {
        super(layoutResId, data);
        mContext = context;
        db = new dbHelperOperations(context);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final entDevice item) {
        CardView cardView = helper.getView(R.id.card);
        helper.setText(R.id.tvAppliancesName, item.getDEVICE_NAME());
        helper.setText(R.id.tvRoomName, item.getROOM_NAME());
        helper.setImageResource(R.id.ivLogo, Constants.getImageForDevice(mContext,
                item.getDEVICE_NAME().toLowerCase()));
        helper.setVisible(R.id.cbOnOff, false);
        cardView.setCardBackgroundColor(item.getIsSelected() == 1 ? Color.parseColor("#00afef") : Color.parseColor("#3E3933"));
    }

}
