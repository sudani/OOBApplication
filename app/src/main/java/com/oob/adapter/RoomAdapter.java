package com.oob.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.oob.OOBApplication;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entDevice;
import com.oob.tools.Constants;
import com.oob.tools.Preferences;
import com.oob.view.EditText;

import org.askerov.dynamicgrid.BaseDynamicGridAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RoomAdapter extends BaseDynamicGridAdapter implements TimePickerDialog.OnTimeSetListener {

    private LayoutInflater INFLATER;

    public List<entDevice> roomItemList = new ArrayList<>();

    private Context mContext;


    private Activity activity;

    private int position = -1;

    private int picker = 0;

    dbHelperOperations db;

    private boolean isFromGroup = false, isFromAllOnOff = false;

    private boolean isMultiSelect = false;

    ArrayList<String> colors = new ArrayList<>();

    public RoomAdapter(Context context, List<entDevice> items, int columnCount) {
        super(context, items, columnCount);
        INFLATER = (LayoutInflater) context.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.roomItemList = items;
        this.mContext = context;
        this.activity = activity;
        colors = new ArrayList<>(
                Arrays.asList(mContext.getResources().getStringArray(R.array.light_colors)));
        db = new dbHelperOperations(mContext);
    }

//    public RoomAdapter(Context context, List<entDevice> roomDevice, Activity activity) {
//        INFLATER = (LayoutInflater) context.getApplicationContext()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.roomItemList = roomDevice;
//        this.mContext = context;
//        this.activity = activity;
//        colors = new ArrayList<>(
//                Arrays.asList(mContext.getResources().getStringArray(R.array.light_colors)));
//        db = new dbHelperOperations(mContext);
////        async = new SocketServerTask(mContext, null);
//
//    }

//    @Override
//    public int getCount() {
//        return roomItemList.size();
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return i;
//    }


    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {


        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_frag_room, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.buildd(i);
//        final ViewHolder customViewHolder;
//        if (convertView == null) {
//            customViewHolder = new ViewHolder();
//            convertView = INFLATER.inflate(R.layout.row_frag_room, viewGroup, false);
//            customViewHolder.seekDimmer = (SeekBar) convertView.findViewById(R.id.seekBar);
//            customViewHolder.btn_save = (com.oob.view.Button) convertView
//                    .findViewById(R.id.btn_save);
//            customViewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
//            customViewHolder.im_cl = (ImageView) convertView.findViewById(R.id.im_cl);
//            customViewHolder.ivBuzer = (ImageView) convertView.findViewById(R.id.ivBuzzer);
//            customViewHolder.ivColor = (ImageView) convertView.findViewById(R.id.ivColor);
//            customViewHolder.tvName = (TextView) convertView.findViewById(R.id.tvName);
//            customViewHolder.tv_fromTimer1 = (TextView) convertView.findViewById(R.id.tvTimer1From);
//            customViewHolder.tv_toTimer1 = (TextView) convertView.findViewById(R.id.tvTimer1to);
//            customViewHolder.tv_fromTimer2 = (TextView) convertView.findViewById(R.id.tvTimer2From);
//            customViewHolder.tv_toTimer2 = (TextView) convertView.findViewById(R.id.tvTimer2to);
//            customViewHolder.tv_Timer1 = (TextView) convertView.findViewById(R.id.tvTimer1);
//            customViewHolder.tv_Timer2 = (TextView) convertView.findViewById(R.id.tvTimer2);
////            customViewHolder.sw_device = (EnhancedCheckBox) convertView
////                    .findViewById(R.id.sw_device);
//            customViewHolder.cbSelect = (com.oob.view.CheckBox) convertView
//                    .findViewById(R.id.checkbox);
//            customViewHolder.ll_days = (LinearLayout) convertView.findViewById(R.id.ll_days);
//            customViewHolder.layHeader = (RelativeLayout) convertView.findViewById(R.id.layHeader);
//            customViewHolder.layDetail = (LinearLayout) convertView.findViewById(R.id.layDetail);
//            customViewHolder.layTimer1 = (LinearLayout) convertView.findViewById(R.id.layTimer1);
//            customViewHolder.layTimer2 = (LinearLayout) convertView.findViewById(R.id.layTimer2);
//            customViewHolder.tvSeekValue = (TextView) convertView.findViewById(R.id.tvSeekValue);
//            customViewHolder.ivDevice = (ImageView) convertView.findViewById(R.id.ivDevice);
//
//            convertView.setTag(customViewHolder);
//        } else {
//            customViewHolder = (ViewHolder) convertView.getTag();
//        }


        return convertView;
    }


    private void updateData(entDevice bean) {
        for (entDevice device : roomItemList) {
            db.updateDevice(device);
        }
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(bean.getROOM_Id()) + Constants
                .getModuleAndDevicecode(bean.getMODULE()) + "T1" + Constants
                .getTimeerValue(bean.getTIME_ONE_A()) + Constants
                .getTimeerValue(bean.getTIME_ONE_B()) + "D" + Constants.getThreeDigit(bean.getDIM())
                + "REP" + bean.getDAYS()
        );

    }

    private void openColorDialog(final int position, final View v) {

        ColorPickerDialogBuilder
                .with(mContext)
                .setTitle("Choose color")
                .initialColor(0xffffffff)
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(12)
                .setPositiveButton("ok", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor,
                                        Integer[] allColors) {
                        System.out.println("color selected" + selectedColor);
                        roomItemList.get(position).setREDCLR(Color.red(selectedColor));
                        roomItemList.get(position).setGREENCLR(Color.green(selectedColor));
                        roomItemList.get(position).setBLUECLR(Color.blue(selectedColor));
                        ((ImageView) v).setColorFilter(selectedColor,
                                PorterDuff.Mode.MULTIPLY);
                        // TODO: 4/30/2016  UPDATE DATABASE
                        db.updateDevice(roomItemList.get(position));
                        isFromAllOnOff = false;
                        isFromGroup = false;
                        RoomAdapter.this.notifyDataSetChanged();
//                        changeBackgroundColor(selectedColor);
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                + Constants
                                .getRoomcode(roomItemList.get(position).getROOM_Id()) + Constants
                                .getModuleAndDevicecode(roomItemList.get(position).getMODULE())
                                + "LED" + Constants
                                .getThreeDigit(roomItemList.get(position).getREDCLR()) + Constants
                                .getThreeDigit(roomItemList.get(position).getGREENCLR()) + Constants
                                .getThreeDigit(roomItemList.get(position).getBLUECLR()));


                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();

    }

    private void openNameDialog(final com.oob.view.TextView tv, final ImageView ivIcon,
                                final int position) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_apliences_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        edtName.setText(tv.getText().toString());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (Constants.isFromOurData(edtName.getText().toString().toLowerCase())) {
                    System.out.println("IS TRUE");
                    roomItemList.get(position).setDEVICE_NAME(edtName.getText().toString());
                    db.updateDevice(roomItemList.get(position));
                    roomItemList = db.getListOfDevices(roomItemList.get(position).getROOM_Id(),true);
                    notifyDataSetChanged();
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });


    }


    private void refreshChildLock(View v) {
        String tag = (String) v.getTag();
        switch (tag) {
            case "0":
                ((ImageButton) v).setImageResource(R.drawable.lock);

                break;
            case "1":
                ((ImageButton) v).setImageResource(R.drawable.lock_selected);
                break;
        }
    }


    private void setupChildLockClick(ImageView im_cl, final int i) {
        im_cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = (String) v.getTag();
                switch (tag) {
                    case "0":
                        v.setTag("1");
                        ((ImageButton) v).setImageResource(R.drawable.lock_selected);
                        roomItemList.get(i).setCL(1);
                        break;
                    case "1":
                        v.setTag("0");
                        ((ImageButton) v).setImageResource(R.drawable.lock);
                        roomItemList.get(i).setCL(0);
                        break;
                }

                db.updateDevice(roomItemList.get(i));
                isFromAllOnOff = false;
                isFromGroup = false;
                notifyDataSetChanged();
                sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                        .getRoomcode(roomItemList.get(i).getROOM_Id())
                        + Constants
                        .getModuleAndDevicecode(roomItemList.get(i).getMODULE())
                        + "CL" + roomItemList.get(i).getCL());
            }
        });
    }

    private void setDaysFromBean(LinearLayout ll_days, int i) {
        String days = roomItemList.get(i).getDAYS();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            String tag = ((Character) days.charAt(j)).toString();
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setTag(tag);
                switch (tag) {
                    case "0":
                        ((com.oob.view.TextView) v).setBackgroundResource(R.drawable.text_bg);
                        break;
                    case "1":
                        ((com.oob.view.TextView) v)
                                .setBackgroundResource(R.drawable.text_active_bg);
                        break;
                }
            }
        }
    }

    private void sendData(String json) {
        OOBApplication.getApp().sendData(json);
        return;
//        json = json + "Q";
//        System.out.println("json" + json);
//        if (OOBApplication.getApp().client != null && OOBApplication.getApp().client.isConnected() && Utils.isNetworkAvailable(mContext)) {
//            OOBApplication.getApp().publishMessage(json);
//        } else {
//            json = WifiUtils.encodeToServerString("JSON=" + json);
//            Log.d("TEST", json);
//            if (!json.isEmpty()) {
//                new SocketServerTask(mContext, null)
//                        .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, json, WifiUtils.server_ip,
//                                WifiUtils.server_port);
//
//            }
//        }
    }

//    private void saveToDB(RoomDevice roomDevice) {
//
//        for (DevicesEntity bean : roomDevice.getDevices()) {
//            bean.save();
//        }
//
//    }


    private String getFinalDays(boolean isChecked, LinearLayout ll_days) {
        String daysStatus = getSelectedDays(ll_days);
        if (isChecked) {
            daysStatus = daysStatus + "1";
        } else {
            daysStatus = daysStatus + "0";
        }

        return daysStatus;
    }

    private void setupDaysClick(final LinearLayout ll_days, final int i) {
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View view = ll_days.getChildAt(j);
//            if (view instanceof RelativeLayout) {
//                RelativeLayout layout = ((RelativeLayout) view);
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tag = (String) v.getTag();
                        switch (tag) {
                            case "0":
//                                ((TextView) v).setTypeface(null, Typeface.BOLD);
//                                ((TextView) v).setTextColor(Color.CYAN);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_active_bg);
                                v.setTag("1");
                                break;
                            case "1":
//                                ((TextView) v).setTextColor(Color.BLACK);
                                ((com.oob.view.TextView) v)
                                        .setBackgroundResource(R.drawable.text_bg);
                                v.setTag("0");
                                break;
                        }
                        boolean checked = ((Character) roomItemList.get(i).getDAYS().charAt(7))
                                .toString()
                                .equals("1");
                        roomItemList.get(i).setDAYS(getFinalDays(checked, ll_days));
//                        db.updateDevice(bean);
                    }
                });
            }
//            }
        }
    }

    private String getSelectedDays(LinearLayout ll_days) {
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < ll_days.getChildCount(); j++) {
            View v = ll_days.getChildAt(j);
            if (v.getTag() != null) {
                builder.append((String) v.getTag());
            }

        }
        return builder.toString();
    }


    private String getTimeFromMillis(long millis) {
        // New date object from millis
        Date date = new Date(millis);
        // formattter
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        // Pass date object
        String formatted = df.format(date);
        return formatted;
    }


    public static String hhmmssToStringDate(String hhmmss) {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        try {
            Date d = sdf.parse(hhmmss);
            return new SimpleDateFormat("HH:mm:ss").format(d);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Long hhmmssToDateMillis(String hhmmss) {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        try {
            Date d = sdf.parse(hhmmss);
            return d.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateContentFromDB(int selectedRoomId) {
        this.roomItemList = db.getListOfDevices(selectedRoomId,true);
        isFromAllOnOff = false;
        isFromGroup = false;
        notifyDataSetChanged();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd,
                          int minuteEnd) {
        System.out.println("TAG " + view.getTag());
        System.out.println("TAG " + view.getTag());
        int hour, endHour;
        String ampm, endAMPM, FromDate, ToDate, timer;

        if (picker == 1) {
            timer = "T1";
            System.out.println("hourOfDay " + hourOfDay);
            System.out.println("minute " + minute);
            System.out.println("hourOfDayEnd " + hourOfDayEnd);
            System.out.println("minuteEnd " + minuteEnd);
            if (hourOfDay > 12) {
                hour = hourOfDay - 12;
                ampm = "pm";
            } else {
                hour = hourOfDay;
                ampm = "am";
            }
            if (hourOfDayEnd > 12) {
                endHour = hourOfDayEnd - 12;
                endAMPM = "pm";
            } else {
                endHour = hourOfDayEnd;
                endAMPM = "am";
            }
            FromDate = hour + ":" + minute + " " + ampm;
            ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
            roomItemList.get(position).setTIME_ONE_A(FromDate);
            roomItemList.get(position).setTIME_ONE_B(ToDate);

//            roomItemList.get(position)
        } else {
            timer = "T2";

            System.out.println("hourOfDay =2   " + hourOfDay);
            System.out.println("minute =2   " + minute);
            System.out.println("hourOfDayEnd =2   " + hourOfDayEnd);
            System.out.println("minuteEnd =2   " + minuteEnd);
            if (hourOfDay > 12) {
                hour = hourOfDay - 12;
                ampm = "pm";
            } else {
                hour = hourOfDay;
                ampm = "am";
            }
            if (hourOfDayEnd > 12) {
                endHour = hourOfDayEnd - 12;
                endAMPM = "pm";
            } else {
                endHour = hourOfDayEnd;
                endAMPM = "am";
            }
            FromDate = hour + ":" + minute + " " + ampm;
            ToDate = endHour + ":" + minuteEnd + " " + endAMPM;
            roomItemList.get(position).setTIME_TWO_A(FromDate);
            roomItemList.get(position).setTIME_TWO_B(ToDate);
        }
        db.updateDevice(roomItemList.get(position));
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(roomItemList.get(position).getROOM_Id())
                + Constants.getModuleAndDevicecode(roomItemList.get(position).getMODULE()) + timer
                + Constants.getTwoDigit(hourOfDay) + Constants.getTwoDigit(minute) + Constants
                .getTwoDigit(hourOfDayEnd) + Constants.getTwoDigit(minuteEnd));
        isFromAllOnOff = false;
        isFromGroup = false;
        RoomAdapter.this.notifyDataSetChanged();
    }

    public void setMultiselectEnable(boolean value) {
        isMultiSelect = value;
        if (!value) {
            for (entDevice device : roomItemList) {
                System.out.println(" DIM " + device.getDIM());
                System.out.println("GRP  DIM " + device.getGROUP_DIM());
                db.updateDevice(device);
            }
        }
        isFromAllOnOff = false;
        isFromGroup = false;
        notifyDataSetChanged();
    }

    public void AddData(ArrayList<entDevice> data, int selectedRoomId) {
        roomItemList = new ArrayList<>();
        roomItemList = db.getListOfDevices(selectedRoomId,true);
        isFromAllOnOff = false;
        isFromGroup = false;
        notifyDataSetChanged();

    }

    public void sendGroupingData(boolean isGrouping) {
        String data = "G";
        String dataOnOff = "";
        isFromGroup = true;
        isFromAllOnOff = false;

        for (entDevice device : roomItemList) {

            if (device.getGROUP() == 1) {
                if (isGrouping) {
                    dataOnOff = "11";
                    device.setSTT(1);
                    db.updateDeviceONOFF(device.getDEVICE_Id(), 1);

                } else {
                    dataOnOff = "10";
                    device.setSTT(0);
                    db.updateDeviceONOFF(device.getDEVICE_Id(), 0);
                }
            } else {
                dataOnOff = "00";
            }
            if (device.getDEVICE_NAME().toLowerCase().contains("fan")) {
                dataOnOff = dataOnOff + "D" + device.getGROUP_DIM();
            }
            data = data + Constants.getModuleAndDevicecode(device.getMODULE()) + dataOnOff;
        }
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(roomItemList.get(0).getROOM_Id()) + data);
        notifyDataSetChanged();
    }

    public void sendAllOnOFFData(boolean isOn) {
        String data = "ALL";
        String dataOnOff = "";
        isFromGroup = false;
        isFromAllOnOff = true;
        for (entDevice device : roomItemList) {

            if (device.getALL_STT() == 1) {
                if (isOn) {
                    dataOnOff = "11";
                } else {
                    dataOnOff = "10";
                }
            } else {
                dataOnOff = "00";
            }
            data = data + Constants.getModuleAndDevicecode(device.getMODULE()) + dataOnOff;
        }
        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                .getRoomcode(roomItemList.get(0).getROOM_Id()) + data);
        notifyDataSetChanged();
    }

    public void clearAdapter() {
        roomItemList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public class ViewHolder {
        public SeekBar seekDimmer;

        public ImageView ivIcon, im_cl, ivBuzer, ivColor;

        public TextView tvName, tv_fromTimer1, tv_toTimer1, tv_fromTimer2, tv_toTimer2,
                tv_Timer1, tv_Timer2, tvSeekValue;

        //        public EnhancedCheckBox sw_device;
        public ImageView ivDevice;
        public com.oob.view.CheckBox cbSelect;

        public com.oob.view.Button btn_save;

        public LinearLayout ll_days;

        public LinearLayout ll_main;

        public RelativeLayout layHeader;

        public LinearLayout layDetail, layTimer1, layTimer2;

        public ViewHolder(View convertView) {
            seekDimmer = (SeekBar) convertView.findViewById(R.id.seekBar);
            btn_save = (com.oob.view.Button) convertView
                    .findViewById(R.id.btn_save);
            ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
            im_cl = (ImageView) convertView.findViewById(R.id.im_cl);
            ivBuzer = (ImageView) convertView.findViewById(R.id.ivBuzzer);
            ivColor = (ImageView) convertView.findViewById(R.id.ivColor);
            tvName = (TextView) convertView.findViewById(R.id.tvName);
            tv_fromTimer1 = (TextView) convertView.findViewById(R.id.tvTimer1From);
            tv_toTimer1 = (TextView) convertView.findViewById(R.id.tvTimer1to);
            tv_fromTimer2 = (TextView) convertView.findViewById(R.id.tvTimer2From);
            tv_toTimer2 = (TextView) convertView.findViewById(R.id.tvTimer2to);
            tv_Timer1 = (TextView) convertView.findViewById(R.id.tvTimer1);
            tv_Timer2 = (TextView) convertView.findViewById(R.id.tvTimer2);
//            customViewHolder.sw_device = (EnhancedCheckBox) convertView
//                    .findViewById(R.id.sw_device);
            cbSelect = (com.oob.view.CheckBox) convertView
                    .findViewById(R.id.checkbox);
            ll_days = (LinearLayout) convertView.findViewById(R.id.ll_days);
            layHeader = (RelativeLayout) convertView.findViewById(R.id.layHeader);
            layDetail = (LinearLayout) convertView.findViewById(R.id.layDetail);
            layTimer1 = (LinearLayout) convertView.findViewById(R.id.layTimer1);
            layTimer2 = (LinearLayout) convertView.findViewById(R.id.layTimer2);
            tvSeekValue = (TextView) convertView.findViewById(R.id.tvSeekValue);
            ivDevice = (ImageView) convertView.findViewById(R.id.ivDevice);
        }

        public void buildd(final int i) {
            if (isMultiSelect) {
                cbSelect.setVisibility(View.VISIBLE);
                cbSelect.setChecked(roomItemList.get(i).getGROUP() == 1);
                cbSelect.setOnCheckedChangeListener(
                        new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                roomItemList.get(i).setGROUP(isChecked ? 1 : 0);
                                roomItemList.get(i).setSTT(isChecked ? 0 : 1);
                                roomItemList.get(i).setALL_STT(roomItemList.get(i).getSTT());
                                ivDevice.callOnClick();

                            }
                        });
                if (roomItemList.get(i).getGROUP() == 1 && roomItemList.get(i).getDEVICE_NAME()
                        .toLowerCase().contains("fan")) {
                    seekDimmer.setProgress(roomItemList.get(i).getGROUP_DIM());
                    tvSeekValue
                            .setText(String.valueOf(roomItemList.get(i).getGROUP_DIM()));

                } else {
                    if (roomItemList.get(i).getDEVICE_NAME()
                            .toLowerCase().contains("fan")) {
                        tvSeekValue
                                .setText(String.valueOf(roomItemList.get(i).getDIM()));
                        seekDimmer.setProgress(roomItemList.get(i).getDIM());
                    } else if (roomItemList.get(i).getDEVICE_NAME()
                            .toLowerCase().contains("ac")) {
                        tvSeekValue
                                .setText(String.valueOf(roomItemList.get(i).getDIM() + " \u2103"));
                        seekDimmer.setProgress(roomItemList.get(i).getDIM() - 16);
                    } else {
                        tvSeekValue
                                .setText(String.valueOf(roomItemList.get(i).getDIM() + " %"));
                        seekDimmer.setProgress(roomItemList.get(i).getDIM());
                    }
                }

            } else {
                cbSelect.setVisibility(View.GONE);

                if (roomItemList.get(i).getDEVICE_NAME()
                        .toLowerCase().contains("fan")) {
                    tvSeekValue
                            .setText(String.valueOf(roomItemList.get(i).getDIM()));
                    seekDimmer.setProgress(roomItemList.get(i).getDIM());
                } else if (roomItemList.get(i).getDEVICE_NAME()
                        .toLowerCase().contains("ac")) {
                    tvSeekValue
                            .setText(String.valueOf(roomItemList.get(i).getDIM() + " \u2103"));
                    seekDimmer.setProgress(roomItemList.get(i).getDIM() - 16);
                } else {
                    tvSeekValue
                            .setText(String.valueOf(roomItemList.get(i).getDIM() + " %"));
                    seekDimmer.setProgress(roomItemList.get(i).getDIM());
                }

            }

            tvName.setText(
                    roomItemList.get(i).getDEVICE_NAME().toUpperCase());
            ivIcon.setImageResource(
                    Constants.getImageForDevice(mContext,
                            roomItemList.get(i).getDEVICE_NAME().toLowerCase()));
            ivIcon
                    .setColorFilter(Color.parseColor(colors.get(roomItemList.get(i).getRGBSTT())),
                            android.graphics.PorterDuff.Mode.MULTIPLY);
            if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
                seekDimmer.setMax(9);

            } else if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("ac")) {
                seekDimmer.setMax(16);
            } else {
                seekDimmer.setMax(100);
            }

            ivIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ivIcon.setTag(position);
                    if (roomItemList.get(i).getRGBSTT() < 6) {
                        roomItemList.get(i).setRGBSTT(roomItemList.get(i).getRGBSTT() + 1);
                    } else {
                        roomItemList.get(i).setRGBSTT(0);

                    }
                    ivIcon
                            .setColorFilter(
                                    Color.parseColor(colors.get(roomItemList.get(i).getRGBSTT())),
                                    android.graphics.PorterDuff.Mode.MULTIPLY);
                    // TODO: 4/30/2016  UPDATE DATABSE
                    db.updateDevice(roomItemList.get(i));
                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                            .getRoomcode(roomItemList.get(i).getROOM_Id())
                            + Constants
                            .getModuleAndDevicecode(roomItemList.get(i).getMODULE()) + "XXXXXRX"
                            + roomItemList.get(i).getRGBSTT());

                }
            });
            tvName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openNameDialog((com.oob.view.TextView) v, ivIcon, i);
                }
            });
            ivColor
                    .setColorFilter(Color.rgb(roomItemList.get(i).getREDCLR(),
                            roomItemList.get(i).getGREENCLR(), roomItemList.get(i).getBLUECLR()),
                            android.graphics.PorterDuff.Mode.MULTIPLY);
            ivColor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    openColorDialog(i, v);
                }
            });
            if (roomItemList.get(i).getMIC() == 0) {
                ivBuzer.setImageResource(R.drawable.mute);
            } else {
                ivBuzer.setImageResource(R.drawable.buzzer);

            }
            ivBuzer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (roomItemList.get(i).getMIC() == 1) {
                        roomItemList.get(i).setMIC(0);
                        ivBuzer.setImageResource(R.drawable.mute);
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                                .getRoomcode(roomItemList.get(i).getROOM_Id()) + Constants
                                .getModuleAndDevicecode(roomItemList.get(i).getMODULE()) + "BUZZ"
                                + roomItemList.get(i).getMIC());

                    } else {
                        roomItemList.get(i).setMIC(1);
                        ivBuzer.setImageResource(R.drawable.buzzer);
                        sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE) + Constants
                                .getRoomcode(roomItemList.get(i).getROOM_Id()) + Constants
                                .getModuleAndDevicecode(roomItemList.get(i).getMODULE()) + "BUZZ"
                                + roomItemList.get(i).getMIC());

                    }
                    db.updateDevice(roomItemList.get(i));
                    // TODO: 4/30/2016  UDATE DATABSE
                }
            });

            tv_fromTimer1.setText(roomItemList.get(i).getTIME_ONE_A());
            tv_toTimer1.setText(roomItemList.get(i).getTIME_ONE_B());
            tv_fromTimer2.setText(roomItemList.get(i).getTIME_TWO_A());
            tv_toTimer2.setText(roomItemList.get(i).getTIME_TWO_B());

//        customViewHolder.sw_device.setChecked(bean.getSTT() == 1);
            ivDevice.setImageResource(
                    roomItemList.get(i).getSTT() == 1 ? R.drawable.on : R.drawable.off);

            if (roomItemList.get(i).getCL() == 0) {
//            customViewHolder.sw_device.setEnabled(true);
                ivDevice.setEnabled(true);
                ivDevice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        roomItemList.get(i).setSTT(roomItemList.get(i).getSTT() == 0 ? 1 : 0);
                        roomItemList.get(i).setALL_STT(roomItemList.get(i).getSTT());
                        Log.d("TEST", "from check");
                        if (ivDevice.isShown()) {
                            isFromAllOnOff = false;
                            isFromGroup = false;
                            ivDevice.setImageResource(
                                    roomItemList.get(i).getSTT() == 1 ? R.drawable.on : R.drawable.off);
                            if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
                                    .contains("fan")) {
                                seekDimmer
                                        .setProgress(roomItemList.get(i).getDIM());
                                tvSeekValue
                                        .setText(String.valueOf(roomItemList.get(i).getDIM()));
                            } else {

                            }
                            sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                    + Constants
                                    .getRoomcode(roomItemList.get(i).getROOM_Id())
                                    + Constants
                                    .getModuleAndDevicecode(roomItemList.get(i).getMODULE())
                                    + roomItemList.get(i)
                                    .getSTT() + "D" + Constants
                                    .getThreeDigit(roomItemList.get(i).getDIM()) + "RX"
                                    + roomItemList.get(i).getRGBSTT());
                            db.updateDevice(roomItemList.get(i));
                        } else {

                        }
                        // TODO: 4/30/2016 UPDATE DATABASE

                    }
                });
//
            } else {
//            customViewHolder.sw_device.setEnabled(false);
                ivDevice.setEnabled(false);
            }

            if (isFromGroup) {
                if (roomItemList.get(i).getGROUP() == 1) {
                    if (db.getAllGrp(roomItemList.get(i).getROOM_Id()) == 1) {
//                    customViewHolder.sw_device.setCheckedProgrammatically(true);
                        ivDevice.setImageResource(R.drawable.on);
                    } else {
//                    customViewHolder.sw_device.setCheckedProgrammatically(false);
                        ivDevice.setImageResource(R.drawable.off);
                    }
                    if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
                        seekDimmer.setProgress(roomItemList.get(i).getGROUP_DIM());
//                    customViewHolder.tvSeekValue.setText(String.valueOf(roomItemList.get(i).getGROUP_DIM()));

                    } else {
                        seekDimmer.setProgress(roomItemList.get(i).getDIM());
//                    customViewHolder.tvSeekValue.setText(String.valueOf(roomItemList.get(i).getDIM()));
                    }
                } else {

                }
            } else if (isFromAllOnOff) {
                if (roomItemList.get(i).getSTT() == 1) {
                    if (db.getAllStt(roomItemList.get(i).getROOM_Id()) == 1) {
//                    customViewHolder.sw_device.setCheckedProgrammatically(true);
                        ivDevice.setImageResource(R.drawable.on);

                    } else {
//                    customViewHolder.sw_device.setCheckedProgrammatically(false);
                        ivDevice.setImageResource(R.drawable.off);

                    }
                    if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
                        seekDimmer.setProgress(roomItemList.get(i).getGROUP_DIM());
//                    customViewHolder.tvSeekValue.setText(String.valueOf(roomItemList.get(i).getGROUP_DIM()));

                    } else {
                        seekDimmer.setProgress(roomItemList.get(i).getDIM());
//                    customViewHolder.tvSeekValue.setText(String.valueOf(roomItemList.get(i).getDIM()));
                    }
                } else {

                }
            } else {
//            if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
//                customViewHolder.seekDimmer.setProgress(roomItemList.get(i).getDIM());
//            }
            }
            seekDimmer.setOnSeekBarChangeListener(
                    new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            if (fromUser) {
                                if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
                                        .contains("fan")) {
                                    if (isMultiSelect && roomItemList.get(i).getGROUP() == 1) {
                                        roomItemList.get(i).setGROUP_DIM(seekBar.getProgress());
                                    } else {
                                        roomItemList.get(i).setDIM(seekBar.getProgress());
                                    }
                                    tvSeekValue
                                            .setText(String.valueOf(progress));
                                } else if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
                                        .contains("ac")) {
                                    roomItemList.get(i).setDIM(progress + 16);
                                    tvSeekValue
                                            .setText(String.valueOf(progress + 16) + " \u2103");
                                } else {
                                    roomItemList.get(i).setDIM(progress);
                                    tvSeekValue
                                            .setText(String.valueOf(progress) + " %");
                                }
                            } else {
                                try {
                                    if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
                                            .contains("fan")) {

                                        tvSeekValue
                                                .setText(String.valueOf(progress));
                                    } else if (roomItemList.get(i).getDEVICE_NAME().toLowerCase()
                                            .contains("ac")) {

                                        tvSeekValue
                                                .setText(String.valueOf(progress + 16) + " \u2103");
                                    } else {

                                        tvSeekValue
                                                .setText(String.valueOf(progress) + " %");
                                    }
                                } catch (Exception e) {

                                }
                            }
//
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
//                        System.out.println("PROGRES " + seekBar.getProgress());
                            if (roomItemList.get(i).getDEVICE_NAME().toLowerCase().contains("fan")) {
                                if (isMultiSelect && roomItemList.get(i).getGROUP() == 1) {
                                    roomItemList.get(i).setGROUP_DIM(seekBar.getProgress());
                                    db.updateDevice(roomItemList.get(i));
                                } else {
                                    roomItemList.get(i).setDIM(seekBar.getProgress());
                                    db.updateDevice(roomItemList.get(i));
                                    sendData(Preferences.getAppPrefString(Preferences.KEY_SECURITY_CODE)
                                            + Constants
                                            .getRoomcode(roomItemList.get(i).getROOM_Id())
                                            + Constants
                                            .getModuleAndDevicecode(roomItemList.get(i).getMODULE())
                                            + "XD"
                                            + Constants
                                            .getThreeDigit(roomItemList.get(i).getDIM())
                                    );
                                }
                                tvSeekValue
                                        .setText(String.valueOf(seekBar.getProgress()));
                            } else {

                            }

                        }
                    });
            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TEST", "from save");
//                sendData(roomDevice);
                    updateData(roomItemList.get(i));
                    // TODO: 4/30/2016  UPDATE DATABASE
                }
            });

            layTimer1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    position = i;
                    picker = 1;
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog tpd = TimePickerDialog.newInstance(
                            RoomAdapter.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            false
                    );

                    tpd.show(activity.getFragmentManager(), "Timepickerdialog");
                }
            });
            layTimer2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    position = i;
                    picker = 2;
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog tpd = TimePickerDialog.newInstance(
                            RoomAdapter.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            false
                    );
                    tpd.show(activity.getFragmentManager(), "Timepickerdialog");
                }
            });
            if (roomItemList.get(i).getCL() == 1) {
                im_cl.setTag("1");
            } else {
                im_cl.setTag(("0"));
            }

            if (roomItemList.get(i).IS_EXPAND()) {
                layDetail.setVisibility(View.VISIBLE);

            } else {
                layDetail.setVisibility(View.GONE);

            }
            layHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (layDetail.getVisibility() == View.VISIBLE) {
                        roomItemList.get(i).setIS_EXPAND(false);
                        Constants.collapse(layDetail);
                    } else {
                        Constants.expand(layDetail);
                        roomItemList.get(i).setIS_EXPAND(true);
                    }

                }
            });
//        customViewHolder.layHeader.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                mDragStartListener.onStartDrag(customViewHolder);
//                return true;
//            }
//        });
            refreshChildLock(im_cl);
            setupDaysClick(ll_days, i);
            setupChildLockClick(im_cl, i);
            setDaysFromBean(ll_days, i);


        }


    }


}


