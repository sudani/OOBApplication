package com.oob.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.oob.activity.CurtainSetupActivity;
import com.oob.app.R;
import com.oob.database.dbHelperOperations;
import com.oob.entity.entCurtain;
import com.oob.entity.entRooms;
import com.oob.fragment.PicModeSelectDialogFragment;
import com.oob.tools.Constants;
import com.oob.view.EditText;
import com.oob.view.TextView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CurtainSetupRecyclerAdapter
        extends RecyclerView.Adapter<CurtainSetupRecyclerAdapter.securityViewHolder>
        implements PicModeSelectDialogFragment.IPicModeSelectListener {
    List<entCurtain> curtainList = new ArrayList<>();
    Context context;
    dbHelperOperations db;

    public CurtainSetupRecyclerAdapter(Context context, List<entCurtain> sensors) {
        this.context = context;
        this.curtainList = sensors;
        db = new dbHelperOperations(context);

    }

    @Override
    public securityViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_ac_setup, viewGroup, false);
        return new securityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final securityViewHolder customViewHolder, final int i) {

        customViewHolder.tvRoomName.setText(curtainList.get(i).getROOM_NAME());
        customViewHolder.tvTVName.setText(curtainList.get(i).getCURTAIN_NAME());
        customViewHolder.tvRoomName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRoomDialog(i);
            }
        });
        customViewHolder.tvTVName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNameDialog(i);

            }
        });
        customViewHolder.ivCurtain.setVisibility(View.VISIBLE);
        customViewHolder.ivCurtain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddProfilePicDialog(i);
            }
        });
        if (curtainList.get(i).getCURTAIN_IMAGE() != null) {
            Picasso.with(context)
                    .load(new File(curtainList.get(i).getCURTAIN_IMAGE()))
                    .placeholder(R.drawable.curtain).error(R.drawable.curtain)
//                    .skipMemoryCache(false)
                    .into(customViewHolder.ivCurtain);
        }
        customViewHolder.tvTVName.setTag(i);
        customViewHolder.tvRoomName.setTag(i);
        customViewHolder.tvTVName.setOnLongClickListener(onLongClickListener);
        customViewHolder.tvRoomName.setOnLongClickListener(onLongClickListener);

    }

    View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            System.out.println("Long clicked  " + v.getTag());
            deleteCurtainDialog((int) v.getTag());
            return true;
        }
    };

    private void deleteCurtainDialog(final int tag) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setIcon(R.drawable.app_logo);
        alertDialogBuilder.setTitle(curtainList.get(tag).getCURTAIN_NAME());
        alertDialogBuilder.setMessage("Are you sure, You wanted to delete this curtain?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        db.deleteCurtain(curtainList.get(tag).getCURTAIN_Id());
                        curtainList = db.getListCurtain();
                        notifyDataSetChanged();
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showAddProfilePicDialog(int pos) {
        PicModeSelectDialogFragment dialogFragment = new PicModeSelectDialogFragment();
        dialogFragment.setiPicModeSelectListener(this, pos);
        dialogFragment.show(((CurtainSetupActivity) context).getSupportFragmentManager(),
                "picModeSelector");
    }

    private void openNameDialog(
            final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include .xml file
        dialog.setContentView(R.layout.dialog_apliences_name);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // set values for custom dialog components - text, image and button
        final EditText edtName = (EditText) dialog.findViewById(R.id.edtName);
        edtName.setText(curtainList.get(position).getCURTAIN_NAME());
        dialog.show();

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        // if decline button is clicked, close the custom dialog

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                System.out.println("IS TRUE");
                curtainList.get(position).setCURTAIN_NAME(edtName.getText().toString());
                db.updateCurtain(curtainList.get(position));
                CurtainSetupRecyclerAdapter.this.notifyDataSetChanged();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    private void showRoomDialog(final int pos) {
        final List<entRooms> list = db.getListOfRooms(true);
        final String[] array = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i).getROOM_NAME();
        }

        new AlertDialog.Builder(context)
                .setSingleChoiceItems(array, -1, null)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog) dialog).getListView()
                                .getCheckedItemPosition();
                        if (selectedPosition != -1) {
                            curtainList.get(pos).setROOM_NAME(array[selectedPosition]);
                            curtainList.get(pos)
                                    .setROOM_Id(list.get(selectedPosition).getROOM_Id());
                            db.updateCurtain(curtainList.get(pos));
                            CurtainSetupRecyclerAdapter.this.notifyDataSetChanged();
                        }                        // Do something useful withe the position of the selected radio button
                    }
                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })
                .show();
    }

    @Override
    public int getItemCount() {
        return curtainList.size();
    }

    public void addCurtain(entCurtain entac) {
        curtainList.add(entac);
        notifyDataSetChanged();
    }

    @Override
    public void onPicModeSelected(String mode, int position) {
        String action = mode.equalsIgnoreCase(Constants.PicModes.CAMERA)
                ? Constants.IntentExtras.ACTION_CAMERA : Constants.IntentExtras.ACTION_GALLERY;
        ((CurtainSetupActivity) context).actionProfilePic(action, position);
    }

    public void updateData(String byteData, int position) {
        curtainList.get(position).setCURTAIN_IMAGE(byteData);
        db.updateCurtain(curtainList.get(position));
        this.notifyDataSetChanged();
    }

    class securityViewHolder extends RecyclerView.ViewHolder {
        protected TextView tvRoomName;
        protected TextView tvTVName;
        protected ImageView ivCurtain;
        protected TextView tvSetName;
        protected CardView cardView;

        public securityViewHolder(View view) {
            super(view);

            tvRoomName = (TextView) view.findViewById(R.id.tvRoomName);
            ivCurtain = (ImageView) view.findViewById(R.id.ivCurtain);
            tvTVName = (TextView) view.findViewById(R.id.tvTvName);
            tvSetName = (TextView) view.findViewById(R.id.tvSetName);
            cardView = (CardView) view.findViewById(R.id.card);

        }
    }

}
