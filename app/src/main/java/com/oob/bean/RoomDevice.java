//package com.oob.bean;
//
//import android.os.Parcel;
//import android.os.Parcelable;
//
//import com.activeandroid.Model;
//import com.activeandroid.annotation.Column;
//import com.activeandroid.annotation.Table;
//import com.activeandroid.query.Select;
//import com.google.gson.annotations.Expose;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by ViRus on 1/3/2016.
// */
//public class RoomDevice
//{
//
//    /**
//     * ID : 1
//     * STT : 2
//     * DIM : 50
//     * TIMA : 010203
//     * TIMB : 040506
//     * CL : 1
//     * RGBSTT : 1
//     * ENRG : 0
//     * RGBCLR : 1
//     * GRP : 1
//     */
//    @Expose
//    private List<DevicesEntity> devicesdata;
//
//    public void setDevices(List<DevicesEntity> devicesdata) {
//        this.devicesdata = devicesdata;
//    }
//
//    public ArrayList<DevicesEntity> getDevices() {
//        return (ArrayList<DevicesEntity>) devicesdata;
//    }
//
//
//}
