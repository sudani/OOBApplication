//package com.oob.bean;
//
//import android.os.Parcel;
//import android.os.Parcelable;
//
//import com.activeandroid.Model;
//import com.activeandroid.annotation.Column;
//import com.activeandroid.annotation.Table;
//import com.activeandroid.query.Select;
//import com.google.gson.annotations.Expose;
//
//import java.util.List;
//
//@Table(name = "RoomDevice")
//public class DevicesEntity extends Model implements Parcelable {
//        @Expose
//        @Column(name = "_ID", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
//        public int ID;
//        @Expose
//        @Column(name = "STT")
//        public int STT;
//        @Expose
//        @Column(name = "DIM")
//        public int DIM;
//        @Expose
//        @Column(name = "TIMA")
//        public String TIMA;
//        @Expose
//        @Column(name = "TIMB")
//        public String TIMB;
//        @Expose
//        @Column(name = "CL")
//        public int CL;
//        @Expose
//        @Column(name = "RGBSTT")
//        public int RGBSTT;
//        @Expose
//        @Column(name = "ENRG")
//        public int ENRG;
//        @Expose
//        @Column(name = "RGBCLR")
//        public int RGBCLR;
//        @Expose
//        @Column(name = "GRP")
//        public int GRP;
//        @Expose
//        @Column(name = "Days")
//        public String days="00000000";
//
//        public DevicesEntity() {
//            super();
//        }
//
//
//        public static List<DevicesEntity> getAll()
//        {
//           return new Select().from(DevicesEntity.class).orderBy("_ID").execute();
//        }
//
//
//        public String getDays() {
//            return days;
//        }
//
//        public void setDays(String days) {
//            this.days = days;
//        }
//
//        public void setID(int ID) {
//            this.ID = ID;
//        }
//
//        public void setSTT(int STT) {
//            this.STT = STT;
//        }
//
//        public void setDIM(int DIM) {
//            this.DIM = DIM;
//        }
//
//        public void setTIMA(String TIMA) {
//            this.TIMA = TIMA;
//        }
//
//        public void setTIMB(String TIMB) {
//            this.TIMB = TIMB;
//        }
//
//        public void setCL(int CL) {
//            this.CL = CL;
//        }
//
//        public void setRGBSTT(int RGBSTT) {
//            this.RGBSTT = RGBSTT;
//        }
//
//        public void setENRG(int ENRG) {
//            this.ENRG = ENRG;
//        }
//
//        public void setRGBCLR(int RGBCLR) {
//            this.RGBCLR = RGBCLR;
//        }
//
//        public void setGRP(int GRP) {
//            this.GRP = GRP;
//        }
//
//        public int getID() {
//            return ID;
//        }
//
//        public int getSTT() {
//            return STT;
//        }
//
//        public int getDIM() {
//            return DIM;
//        }
//
//        public String getTIMA() {
//            return TIMA;
//        }
//
//        public String getTIMB() {
//            return TIMB;
//        }
//
//        public int getCL() {
//            return CL;
//        }
//
//        public int getRGBSTT() {
//            return RGBSTT;
//        }
//
//        public int getENRG() {
//            return ENRG;
//        }
//
//        public int getRGBCLR() {
//            return RGBCLR;
//        }
//
//        public int getGRP() {
//            return GRP;
//        }
//
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel dest, int flags) {
//            dest.writeInt(this.ID);
//            dest.writeInt(this.STT);
//            dest.writeInt(this.DIM);
//            dest.writeString(this.TIMA);
//            dest.writeString(this.TIMB);
//            dest.writeInt(this.CL);
//            dest.writeInt(this.RGBSTT);
//            dest.writeInt(this.ENRG);
//            dest.writeInt(this.RGBCLR);
//            dest.writeInt(this.GRP);
//            dest.writeString(this.days);
//        }
//
//        protected DevicesEntity(Parcel in) {
//            this.ID = in.readInt();
//            this.STT = in.readInt();
//            this.DIM = in.readInt();
//            this.TIMA = in.readString();
//            this.TIMB = in.readString();
//            this.CL = in.readInt();
//            this.RGBSTT = in.readInt();
//            this.ENRG = in.readInt();
//            this.RGBCLR = in.readInt();
//            this.GRP = in.readInt();
//            this.days = in.readString();
//        }
//
//        public static final Creator<DevicesEntity> CREATOR = new Creator<DevicesEntity>() {
//            public DevicesEntity createFromParcel(Parcel source) {
//                return new DevicesEntity(source);
//            }
//
//            public DevicesEntity[] newArray(int size) {
//                return new DevicesEntity[size];
//            }
//        };
//    }