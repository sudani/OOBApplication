package com.oob;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.oob.interfaces.TaskListener;
import com.oob.tools.BroadcastConstant;
import com.oob.tools.HostUtils;
import com.oob.tools.NotifyUtils;
import com.oob.tools.Preferences;

public class SocketServerTask extends AsyncTask<String, String, Void> {
    private final TaskListener<String> taskListener;
    private String data;
    private boolean success;
    //    private ProgressDialog pd;
    private Context mContext;
    private String ip;
    private String port;
    private String TAG = "TEST";
    private String msg_failed = "Failed to connect to host..";
    private boolean isData;
    private boolean isIP;
    public String message = "Sending data....";

    public SocketServerTask(Context mContext, TaskListener<String> taskListener) {
        this.mContext = mContext;
        this.taskListener = taskListener;
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();
        WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled()) {
//            pd = new ProgressDialog(mContext);
//            pd.setMessage(message);
//            pd.setCancelable(false);
//            pd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                    SocketServerTask.this.cancel(true);
//                }
//            });
//            pd.show();
//            System.out.println("START "+System.currentTimeMillis());
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (getStatus() == Status.RUNNING && !pd.isShowing())
//
//                }
//            }, 500);
        } else {
            this.cancel(true);
        }
    }

    @Override
    protected Void doInBackground(String... params) {

        data = params[0];
        ip = Preferences.getAppPrefString(Preferences.KEY_IP_ADDRESS);//params[1];
        port = params[2];
        System.out.println("IP " + ip);
        isData = data.contains("devicesdata");
        isIP = data.contains("CLIENTIP");
//        System.out.println("SEND "+System.currentTimeMillis());
        success = HostUtils.sendToHost(data, ip, port);
//        success = HostUtils.sendToHost(data, ip, port);

        return null;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        NotifyUtils.toast(mContext, "Canceled by user ..");
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        NotifyUtils.toast(mContext, values[0]);
    }

    @Override
    protected void onPostExecute(Void result) {
//        pd.dismiss();
//        System.out.println("FINISH "+System.currentTimeMillis());
        if (success) {
//            if (isData)
            NotifyUtils.toast(mContext, "Successfully sent..");
            Intent intent = new Intent(BroadcastConstant.BROADCAST_ACTION_LOGDATA);
            intent.putExtra(BroadcastConstant.MESSAGE, true);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

        } else {
            NotifyUtils.toast(mContext, msg_failed);
        }
    }

}